# Project Structure

Here are some project structure explanations.

## Table of Contents

1. [General Structure](#general-structure)
2. [Code Base Structure](#code-base-structure)
3. [Components](#components)

## General Structure

The general structure of this project is as follow.

```
├─── .gitlab/    # gitlab related template
├─── docs/       # some documentation files
├─── public/     # static assets
├─── src/        # code base of this project
```

## Code Base Structure

Most of the time, we will do the coding below the `/src` folder. The structure of this folder is as follow.

```
src/
├─── api/              # API interfaces and request manager
├─── assets/           # files of animations, images, and styles (SCSS)
├─── components/       # all components here
├─── consts/           # constants used in whole application
├─── helpers/          # common helper functions used globally
├─── hooks/            # custom react hooks function
├─── layouts/          # application layouts
├─── pages/            # feature related
├─── services/         # state managements

```

### Components

This folder used for all components, including:

##### Pages

For page components that loaded on `src/pages` folder. For naming, the folder name should be the same with routing name (ex: `account-settings` to `AccountSettings` using pascal-case). And for file naming, the file should use suffix `Page`. For example: `LoginPage.js`. It should be in English too. You can also create some folders here to groups the pages.

You can be a little bit _"free"_ in here. It means that you can add additional folders that can help you do the clean code.

###### Components

This folder consists all components that ony used in this scene. So there is no cross import (from other scene).

###### Validations

We encourage you to seperate the validation related to another file (and another folder). So all rule definitions of a form is in here. Just use the Kebab Case to name the file (e.g. `new-user.js`)

###### Utils

Some helper functions that are used in this scene only.


##### Common

This folder consist all global components and can be used for any pages. The components created must use Styled Components. Components name should be in English and in Pascal Case (e.g: `AlertError.js`). You can also create some folders here to groups the components.


