# Libraries

We are using [NextJS](https://nextjs.org/) and React as the JavaScript library. And of course we use other libraries. Here are some of them.

> Note
> We just mention the libraries here and give a little explanation about it. For more detailed information please get it in each library documentation page.

## Table of Contents

1. [UI Related](#ui-related)
2. [Routing Related](#routing-related)
3. [State Management](#state-management)
4. [API Fetching](#api-fetching)
5. [Internationalization (i18n)](#internationalization-i18n)
6. [Form Validation](#form-validation)
7. [Error Monitoring](#error-monitoring)
8. [Customers Messaging](#customer-messaging)
9. [Other Supporting Libraries](#other-supporting-libraries)

## UI Related

##### Styled Components

It is used to create a component. Usually we crate file with suffix `.styled.js` to distinguish it from the functional component file (e.g. `Button.styled.js`). More info in [Styled Components documentation](https://styled-components.com/).

##### Tailwindcss & Bootstrap

Yes, we still use Bootstrap. For Bootstrap, we still use it for resetting CSS and grid system. For React-Bootstrap, we still use some components from it, such as `Container`, `Row`, and `Col`. Why just those components? Other components used in Big Flip are in house components. More info in [Bootstrap docs](https://getbootstrap.com/) and [React-Bootstrap docs](https://react-bootstrap.github.io/).

##### Fontawesome

It consists tons of icons, so just use it. Currently we still use the free version of fontawesome, that's still enough. Fontawesome for React is explained more in [Fontawesome for React docs](https://fontawesome.com/how-to-use/on-the-web/using-with/react). List of icons is available in https://fontawesome.com/icons.

## Routing Related

##### NextJS Built In Routing

We use built in routing feature in NextJS. More info in [NextJS Router documentation](https://nextjs.org/docs/routing/introduction)

## State Management

##### Redux

For state management we use Redux. More info in [Redux docs](https://redux.js.org/).

##### Supporting Libraries

For enhancing our Redux, we use some supporting libraries, such as:

- [unstated-next](https://github.com/jamiebuilds/unstated-next) -> for component scooped state management
- [redux-logger](https://www.npmjs.com/package/redux-logger) -> for logging redux action
- [redux-promise-middleware](https://www.npmjs.com/package/redux-promise-middleware) -> for enabling async action creators in redux
- [redux-thunk](https://www.npmjs.com/package/redux-thunk) -> for more flexibility in managing action creators

## API Fetching

##### Axios

We use Axios for fetching data through API. Of course, there are some configurations and interceptors created for Big Flip. More info about Axios in [Axios docs](https://github.com/axios/axios)

##### SWR

We also use SWR (stale-while-revalidate) for caching and revalidating data on clientside [SWR docs](https://swr.vercel.app/)

## Internationalization (i18n)

##### i18next and React i18next

Currently Big Flip supports two languages, Bahasa and English. To implement that capability, we use `react-i18next`, which is an extended version of `i18next` for React. It uses JSON files which consist strings of each language (pair key-value of a text), then we can use those string keys in out view. More info in [React i18next docs](https://react.i18next.com/) and [i18next docs](https://www.i18next.com/)

## Form Validation

##### Formik

To ease field validation, we use Formik to help us validating and submitting form. It handles the process from field value changes, prevent submit, generating errors, and submit if valid. More info in [Formik docs](https://jaredpalmer.com/formik/docs/overview)

##### Yup

Yup is an object schema validation. It is used as a [complimentary package for Formik](https://jaredpalmer.com/formik/docs/overview#complementary-packages). Yup consits tons of validation rules ready to use. So we use it to get the validation rules for each type of field. More info in [Yup docs](https://github.com/jquense/yup).


## Other Supporting Libraries

##### date-fns

We use `date-fns` as helper for date functions. Check it in [its docs](https://date-fns.org/)
