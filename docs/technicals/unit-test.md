# Unit Tests

## Summary

We use Jest + Enzym to build our unit tests. The unit tests cover every parts in the code base. Why do we need unit test? To ease us when there are changes needed in the future (such as refactoring, etc). It will become the boundaries when we are refactoring our codes. Hence, it is necessary to make sure that our unit tests are covering all (or most of) aspects of the codes, i.e the coverage resulted is high (close to 100% or is 100%).

## What need to be tested?

- Components
- Components V2
- Helpers
- Hooks
- Layouts (pages and components)
- Scenes (pages and components)

## Getting started

1. Setup File

   - `src/setupTests.js`
   - to put all testing setups configurations, such as adapters and general mocks.

2. **Jest Config**

   - `jest.config.js`
   - to put all configuration for compiling and running the tests.

3. **Run the tests**
   - **without** coverage report:
     - for all tests: `yarn test`
     - for specific test: `yarn test src/path/to/test/file`
   - **with** coverage report:
     - for all tests: `yarn test:coverage`
     - for specific test: `yarn test:coverage src/path/to/test/file`

## Conventions

1. **Folder and Name**

   - Put the test file in **the same** folder as the tested file.
   - Name the test file the same as the tested file, then add `.test` as suffix (before the extension).
   - Example 1:
     - tested file: `src/helpers/number/index.js`
     - unit test file: `src/helpers/number/index.test.js`
   - Example 2:
     - tested file: `src/scenes/Guest/pages/LoginPage.js`
     - unit test file: `src/scenes/Guest/pages/LoginPage.test.js`

2. **Mock**

   - Create the mocks needed in the same file as your test file. E.g the mocks declaration is written under the dependencies import statements.
   - If you find that the mock function is too long, you may create a `__mocks__` folder in mocked function directory. E.g `__mocks__` folder in `api/interface` folder.
   - You may install mocking library as a dev dependency. E.g `axios-mock`, etc.

3. **Others (just follow the existing tests to create a new one)**

## Others

- **Script for test commands**
  - general: `scripts/test.js`
  - with coverage (modified from general): `scripts/test-cov.js`
