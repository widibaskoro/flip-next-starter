# Locales (Internationalization or i18n)

> For every single wording, you must put it in locale file.

## Summary

We use `react-i18next` to support multi language in our applications. Currently we support 2 languages, Bahasa and English.

Here are some notes:

- Make sure all wordings are in locale file.
- For each locale files, give a descriptive name, such as the scene name (e.g: `deposit.json` is for Deposit scene).
- For each keys in locale file, it should represent the wording contained (e.g: `"email-required": "Email harus diisi.`).

## How to add a new wording

1. If the locale file is already exist, then just put the wording in there.
   ```js
   /* in en/home.json */
   {
     "a-wording-key": "This is a wording!"
   }
   ```
2. Make sure you make a short and descriptive key for each wordings.
3. You also can create a nesteed object if you feel that will improve readability.
4. Also make sure you add the same key for each languages, in `en` and `id` folders.
5. In your page/compoents, use the key as follows.

   ```jsx
   import { useTranslation } from "react-i18next";
   ...
   function Home(){
     ...
     const { t } = useTranslation("home");
     ...

     ...
     <p>{t("a-wording-key")}</p>
     ...
   }
   ```

   The `"home"` in `useTranslation("home")` is the locale file name.

## How to add a new locale file

1. If you create new scene, then most likely you need you need to make a new locale file.
2. Create a new json file in `en` and `id` folders, the locale file in each folders must have a same name (e.g: `transaction.json`).
3. Register your locale file in `src/consts/locales.js`. There are two types of locale:
   - `GUEST_LOCALES`
   - `APPLICATION_LOCALES`
     Put your locale file in one of them depends on the layout used in your page/scene. If your page uses Application layout, then put it in `APPLICATION_LOCALES`, if your page uses Guest layout, then put it in `GUEST_LOCALES`.
4. Add your wordings as stated in [add wording section](#how-to-add-a-new-wording).

## Special locale files

There are some locale files that used in many places. Those are:

- errors.json -> consists list of errors from API
- transalation.json -> consists some wordings used globally
- validation.json -> consists wordings about validation

## Further readings

- `src/i18n.js`
  This file consists the initiation of i18n.

- External links
  - [i18next documentation](https://www.i18next.com/)
  - [React i18next documentation](https://react.i18next.com/)
