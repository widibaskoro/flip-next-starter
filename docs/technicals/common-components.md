# Common Components

## Component documentation

You can see the documentation for these components in: https://bigflip.id/style-guides-flip-v2 or http://localhost:3000/style-guides.


## How to add a new component

1. Create a new branch from master and a new Merge Request for the component.
2. Create a new folder in `src/components/common/[component-name]` with the component's name.
3. Write your component.
   - Create one `index.js` file.
   - It must use Styled Components. Add a `[component-name].styled.js` file to put the styled-components components.
   - You can import colors, typography, and other settings from style settings file. E.g:
     ```js
     import { colors } from "src/assets/styles/settings";
     ```
   - You can manage your component's files in the component's folder, such as add other files to make it more modular, etc.
4. Register your component in `src/components/common/index.js`. E.g:
   ```js
   export { default as Button } from "./Button";
   ```
5. Make a documentation for your component in `src/components/pages/StyleGuides/components`.
6. Get your codes reviewed.
7. Merge and deploy your component.
