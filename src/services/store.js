import {createStore} from 'redux';
import rootReducer from 'src/services/reducers';

const store = createStore(rootReducer);

export default store;