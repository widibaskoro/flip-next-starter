const scrollbarMixin = `
  ::-webkit-scrollbar-track {
    border-radius: 12px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
  }

  ::-webkit-scrollbar {
    width: 6px;
    background-color: #F5F5F5;
  }

  ::-webkit-scrollbar-thumb {
    border-radius: 12px;
    background-color:#AAA;
  }

  * {
    scrollbar-color: #AAA #F5F5F5;
    scrollbar-width: thin;
  }
`;

export default scrollbarMixin;
