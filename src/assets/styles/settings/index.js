export { default as colors } from "./colors";
export { default as paths } from "./paths";
export { default as typography } from "./typography";
export { default as zIndex } from "./z-index";
