const colors = {
  // Shade and Tints: Flip Orange
  cinnamon: "#BC4B31",
  brick: "#DF593A",
  flip_orange: "#FD6542",
  peach: "#FD8468",
  anemia: "#FEC1B3",

  // Shade and Tints: Black Bekko
  black_bekko: "#333333",
  dark_smoke: "#545454",
  dark_grey: "#A9A9A9",
  light_grey: "#DEDEDE",
  light_smoke: "#F9F9F9",

  // Shade and Tints: Ketchup Tomato
  red_wine: "#990F28",
  clown_red: "#B61F3C",
  ketchup_tomato: "#D42E52",
  blush: "#E58297",
  sakura: "#FBEBEE",

  // Shade and Tints: Mango Yellow
  coin: "#64500C",
  goldenrod: "#DBA314",
  mango_yellow: "#FFC122",
  blonde: "#FFDA7A",
  banana: "#FFF8E5",

  // Shade and Tints: Intel Blue
  space_blue: "#034C87",
  pepsi: "#0464B1",
  intel_blue: "#067AD8",
  tie_dye: "#6AAFE8",
  bubble: "#DCEFFF",

  // Shade and Tints: Jade
  spinach: "#06604C",
  pine: "#078167",
  jade: "#00BC93",
  slight_jade: "#91DEC6",
  light_jade: "#E9FFF8",

  // Punchy Colors
  warning: "#D41A1A",
  sunrise: "#FFDB1E",
  sky_blue: "#2FC0F0",
  greeneon: "#22D48C",
  catskill_white: "#F3F7F9",

  // Miscellaneous Colors
  white: "#FFFFFF",
  table_stripe: "#FCFDFC",
  main_background: "#F3F5F6",
  disabled: "#F2F2F2",
  light_anemia: "#FFF5F3",

  // Gradient
  flip_gradient: "linear-gradient(141.01deg, #FC6542 -0.73%, #D42E52 123.86%)",
  green_lantern: "linear-gradient(180deg, #00C1A5 0%, #22D48C 100%)",
  loading: "linear-gradient(270deg, #DADADA 0%, #EBEBEB 50.52%, #DADADA 100%)"
};

export default colors
