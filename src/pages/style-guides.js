import StyleGuides from "src/components/pages/StyleGuides"

export default function StyleGuidesPage(){
  return (
    <>
      <StyleGuides />
    </>
  )
}