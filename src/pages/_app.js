import store from 'src/services/store'
import { Provider } from 'react-redux';
import 'src/assets/styles/global.scss'

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  )
}

export default MyApp
