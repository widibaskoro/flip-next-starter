import {
  getRandomInt,
  toPositiveInt,
  delimiter,
  money,
  simpleMoney,
  bytesToSize,
  preventNotNumber,
  formatDelimiter,
  toStringNumber,
  numberOnly
} from "./index";

describe("Helper: Number", () => {
  describe("Method: getRandomInt", () => {
    it("Should return number", () => {
      const tryCount = 5;
      for (let i = 0; i < tryCount; i += 1) {
        const random = getRandomInt(1, 5);
        expect(typeof random).toBe("number");
      }
    });

    it("Should return number in between min & max", () => {
      const range = [
        [0, 5],
        [1, 10],
        [5, 5],
        [0, 1000]
      ];

      range.forEach((r) => {
        const random = getRandomInt(r[0], r[1]);
        expect(random).toBeGreaterThanOrEqual(r[0]);
        expect(random).toBeLessThanOrEqual(r[1]);
      });
    });

    it("Should throw error if max is less than min", () => {
      expect(() => getRandomInt(5, 1)).toThrow();
    });
  });

  describe("Method: toPositiveInt", () => {
    it("Should return positive integer", () => {
      expect(toPositiveInt(3)).toEqual(3);
      expect(toPositiveInt(-5)).toEqual(5);
      expect(toPositiveInt(0)).toEqual(0);
    });

    it("Should throw error if input is not number", () => {
      const errorValues = ["foo", null, [1, 2], { key: "val" }];

      // eslint-disable-next-line array-callback-return
      errorValues.map((val) => {
        expect(() => toPositiveInt(val)).toThrow();
      });
    });
  });

  describe("Method: delimiter", () => {
    it("Should delimit correctly", () => {
      expect(delimiter(-1000)).toEqual("-1.000");
      expect(delimiter(1000)).toEqual("1.000");
      expect(delimiter(1000000)).toEqual("1.000.000");
    });
  });

  describe("Method: money", () => {
    it("Should convert number to money correctly", () => {
      expect(money()).toEqual("Rp0");
      expect(money(-1000)).toEqual("-Rp1.000");
      expect(money(1000)).toEqual("Rp1.000");
      expect(money(1000.05)).toEqual("Rp1.000,05");
      expect(money(1000000)).toEqual("Rp1.000.000");
    });
  });

  describe("Method: simpeMoney", () => {
    it("Should transform number to simple currency format correctly", () => {
      expect(simpleMoney(0)).toEqual("Rp0");
      expect(simpleMoney(50000)).toEqual("Rp50rb");
      expect(simpleMoney(123099999)).toEqual("Rp123jt");
      expect(simpleMoney(123321000)).toEqual("Rp123,3jt");
      expect(simpleMoney(123123123123)).toEqual("Rp123,1M");
      expect(simpleMoney(-500000)).toEqual("-Rp500rb");
    });
  });

  describe("Method: bytesToSize", () => {
    it("Should transform number to byte format correctly", () => {
      expect(bytesToSize(0)).toEqual("0 bytes");
      expect(bytesToSize(1023)).toEqual("1023 bytes");
      expect(bytesToSize(1024)).toEqual("1.0 KB");
      expect(bytesToSize(1048575)).toEqual("1024.0 KB");
      expect(bytesToSize(1048576)).toEqual("1.0 MB");
      expect(bytesToSize(2097152)).toEqual("2.0 MB");
      expect(bytesToSize(5242880)).toEqual("5.0 MB");
    });
  });

  describe("Method: preventNotNumber", () => {
    it("Should return '0' if no string passed", () => {
      expect(preventNotNumber("")).toEqual("0");
    });

    it("Should return only number from a string", () => {
      expect(preventNotNumber("abc")).toEqual("0");
      expect(preventNotNumber("1abc")).toEqual("1");
      expect(preventNotNumber("1a2b3c4")).toEqual("1234");
      expect(preventNotNumber("1a2b3c4 -?jfw56+78--q9ZZ0")).toEqual("1234567890");
    });
  });

  describe("Method: formatDelimiter", () => {
    it("Should transform number to delimeter format correctly", () => {
      expect(formatDelimiter("01")).toEqual("1");
      expect(formatDelimiter(1)).toEqual("1");
      expect(formatDelimiter(100)).toEqual("100");
      expect(formatDelimiter(1000)).toEqual("1.000");
      expect(formatDelimiter(10000)).toEqual("10.000");
      expect(formatDelimiter(1000000)).toEqual("1.000.000");
      expect(formatDelimiter(1000000000)).toEqual("1.000.000.000");
    });
  });

  describe("Method: toStringNumber", () => {
    it("Should transform string (money formatted with ID locale) to JS number format correctly", () => {
      expect(toStringNumber("1000")).toEqual("1000");
      expect(toStringNumber("1.000")).toEqual("1000");
      expect(toStringNumber("1.000,99")).toEqual("1000.99");
    });
  });

  describe("Method: numberOnly", () => {
    it("Should return empty string if no string passed", () => {
      expect(numberOnly("")).toEqual("");
    });

    it("Should return empty string if string consist no number", () => {
      expect(numberOnly("abc")).toEqual("");
    });

    it("Should return only number from a string", () => {
      expect(numberOnly("1abc")).toEqual("1");
      expect(numberOnly("1a2b3c4")).toEqual("1234");
      expect(numberOnly("1a2b3c4 -?jfw56+78--q9ZZ0")).toEqual("1234567890");
    });
  });
});
