/**
 * @module helpers/helper
 */

/**
 * Return random integer in between min and max
 *
 * @example
 * getRandomInt(0, 5)
 * // => 0, 1, 2, 3, 4 or 5
 *
 * @param  {Number} min minimum possible value
 * @param  {Number} max maximum possible value
 *
 * @return {Number}
 */
export function getRandomInt(min, max) {
  if (max < min) {
    throw new Error("Max must be greater or equal than min");
  }

  // eslint-disable-next-line no-mixed-operators
  return Math.floor(Math.random() * (max - min + 1) + min);
}

/**
 * Return the positive of a number
 *
 * @example
 * toPositiveInt(-4)
 * // => 4
 *
 * @param  {Number} val
 * @return {Number}
 */
export function toPositiveInt(val) {
  if (typeof val !== "number") {
    throw new Error("toPositiveInt must receive number as input");
  }
  return Math.abs(val);
}

/**
 * Delimit number
 *
 * @example
 * delimiter(1000000)
 * // => '1.000.000'
 *
 * @param  {Number|String} val Number to delimit
 * @return {String} Delimited number
 */
export function delimiter(val) {
  const str = `${val}`; // convert to string
  const delimited = str.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  return `${val < 0 ? "-" : ""}${delimited}`;
}

/**
 * Convert number to money
 *
 * @example
 * money(1000000)
 * // => 'Rp1.000.000'
 *
 * @param  {Number|String} val Number to convert
 * @return {String} Converted number in money format
 */
export function money(val) {
  const int = delimiter(parseInt(val, 10) || 0);
  const fraction = Math.abs(val % Math.floor(Math.abs(val))) || 0;
  const fractionStr =
    fraction > 0
      ? fraction
          .toFixed(2) // Delimit only 2 digits based on PUEBI
          .slice(1)
          .replace(".", ",")
      : "";
  if (int.charAt(0) === "-") {
    return int.replace(/^-/g, "-Rp") + fractionStr;
  }
  return `Rp${int}${fractionStr}`;
}

/**
 * Transform number to simple format
 * can't satisfy these cases below
 * 15.750 ---> 15,7rb
 * 13.456.000 ---> 13,4jt
 *
 * @example
 * simpleNumber(500000)
 * // => '500rb'
 * simpleNumber(500000, ['', 'k', 'M', 'B', 'T'])
 * // => '500k'
 *
 * @param {Number} amount
 * @param {Array} units
 * @return {String} Tranformed number
 */
export function simpleNumber(amount, units = ["", "rb", "jt", "M", "T"]) {
  let parsedNumber = Math.abs(amount) || 0;
  let unitIndex = 0;
  const unitsComplete = [...units, ...["", "rb", "jt", "M", "T"].slice(units.length)];

  while (parsedNumber >= 1000) {
    parsedNumber /= 1000;
    unitIndex += 1;
  }

  // Get the final number with a max of one decimal place only
  parsedNumber = parseFloat(Math.floor(parsedNumber * 10) / 10);

  const simpleFormat = `${parsedNumber.toString().replace(".", ",")}${unitsComplete[unitIndex]}`;
  return `${amount < 0 ? "-" : ""}${simpleFormat}`;
}

/**
 * Transform number to simple currency format
 * can't satisfy these cases below
 * 15.750 ---> 15,7rb
 * 13.456.000 ---> 13,4jt
 *
 * @example
 * simpleMoney(500000)
 * // => 'Rp500rb'
 *
 * @param  {Number} Amount required e.g. 10000
 * @param  {String} Symbol optional e.g. 'Rp'
 * @return {String} Transformed number
 */
export function simpleMoney(amount, symbol = "Rp") {
  const simpleFormat = `${symbol}${simpleNumber(amount).replace(/^-/, "")}`;
  return `${amount < 0 ? "-" : ""}${simpleFormat}`;
}

/**
 * Convert number to bytes size format
 *
 * @example
 * bytesToSize(1024)
 * // => '1KB'
 *
 * @param  {Number} val Number to convert
 * @return {String} bytes size format
 */
export const bytesToSize = (number) => {
  if (number < 1024) {
    return number + " bytes";
  } else if (number >= 1024 && number < 1048576) {
    return (number / 1024).toFixed(1) + " KB";
  } else {
    return (number / 1048576).toFixed(1) + " MB";
  }
};

/**
 * Return numeric only from a string
 * If string is empty, return 0
 *
 * @example
 * preventNotNumber("1a2b3c")
 * // => '123'
 *
 * preventNotNumber("")
 * // => '0'
 *
 * @param  {String} str String to convert
 * @return {String} string of numerics only
 */
export function preventNotNumber(str) {
  if (str === "") return "0";

  let number = str.replace(/[^0-9]/g, "");
  if (number === "") return "0";

  return number;
}

/**
 * Transform number to money with delimeter format
 *
 * @example
 * formatDelimiter(1000000)
 * // => '1.000.000'
 *
 * @param  {Number|String} val Number to convert
 * @return {String} Converted number in money format
 */
export function formatDelimiter(number) {
  let str = `${number}`;

  if (str.length > 1 && str.charAt(0) === "0" && str.charAt(1) !== "," && str.charAt(1) !== ".") {
    str = str.slice(1);
  }
  return str.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

/**
 * Transform string (money formatted with ID locale) to JS number format
 *
 * @example
 * toStringNumber("1.000")
 * // => '1000'
 *
 * toStringNumber("1.000,99")
 * // => '1000.99'
 *
 * @param  {Number|String} val Number to convert
 * @return {String} Converted number
 */
export function toStringNumber(stringNumber) {
  return stringNumber.toString().replace(/\./g, "").replace(/,/g, ".");
}

/**
 * Return numeric only from a string
 * If string is empty, return emtpy string ("")
 *
 * @example
 * numberOnly("1a2b3c")
 * // => '123'
 *
 * numberOnly("")
 * // => ''
 *
 * @param  {String} str String to convert
 * @return {String} string of numerics only
 */
export function numberOnly(str) {
  let number = str.replace(/[^0-9]/g, "");
  return number;
}

export default {
  getRandomInt,
  toPositiveInt,
  delimiter,
  money,
  simpleMoney,
  simpleNumber,
  preventNotNumber,
  formatDelimiter,
  toStringNumber,
  numberOnly
};
