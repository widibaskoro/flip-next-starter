import { copyText } from "./index";

// Add "execCommand" mock function for document object
Object.defineProperty(document, "execCommand", {
  writable: true,
  value: jest.fn().mockImplementation((key) => key)
});

describe("Helper: String", () => {
  describe("Method: copyText", () => {
    it("Should create a new element, call copy method, and remove element", () => {
      const textToCopy = "some text here!";
      const createElement = jest.spyOn(document, "createElement");
      const execCommand = jest.spyOn(document, "execCommand");
      const bodyAppendChild = jest.spyOn(document.body, "appendChild");
      const bodyRemoveChild = jest.spyOn(document.body, "removeChild");

      const el = document.createElement("textarea");
      el.value = textToCopy;

      copyText(textToCopy);
      expect(createElement).toHaveBeenCalledWith("textarea");
      expect(bodyAppendChild).toHaveBeenCalledWith(el);
      expect(execCommand).toHaveBeenCalledWith("copy");
      expect(bodyRemoveChild).toHaveBeenCalledWith(el);
    });
  });
});
