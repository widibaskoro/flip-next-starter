export function copyText(textCopy) {
  const el = document.createElement("textarea");
  el.value = textCopy;
  document.body.appendChild(el);
  el.select();
  document.execCommand("copy");
  document.body.removeChild(el);
}
