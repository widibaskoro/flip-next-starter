export const idMonths = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "November",
  "Desember"
];

export const enMonths = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

export function toSecondUnix(unix) {
  return unix > 9999999999 ? parseInt(unix / 1000, 10) : unix;
}

export function toMilisecondUnix(unix) {
  return unix > 9999999999 ? unix : parseInt(unix * 1000, 10);
}

export function getLocalizeMonth(locale) {
  return locale === "en" ? enMonths : idMonths;
}

export function formatSecondToMinuteSecond(seconds) {
  const minutes = Math.floor(seconds / 60);
  const remainingSeconds = seconds - minutes * 60;
  return `0${minutes}`.slice(-2) + ":" + `0${remainingSeconds}`.slice(-2);
}
