import { toSecondUnix, toMilisecondUnix, getLocalizeMonth, formatSecondToMinuteSecond } from "./index";

describe("Helper: Date", () => {
  describe("Method: toSecondUnix", () => {
    it("Should return unix in milisecond to unix in second", () => {
      expect(toSecondUnix(1598237188071)).toEqual(1598237188);
    });

    it("Should return and not change the given unix in second", () => {
      expect(toSecondUnix(1598237188)).toEqual(1598237188);
    });
  });

  describe("Method: toMilisecondUnix", () => {
    it("Should return unix in second to unix in milisecond", () => {
      expect(toMilisecondUnix(1598237188)).toEqual(1598237188000);
    });

    it("Should return and not change the given unix in milisecond", () => {
      expect(toMilisecondUnix(1598237188071)).toEqual(1598237188071);
    });
  });

  describe("Method: getLocalizeMonth", () => {
    it("Should return months in Bahasa (given locale: none/undefined)", () => {
      const months = getLocalizeMonth("id");
      expect(months[0]).toEqual("Januari");
      expect(months[5]).toEqual("Juni");
      expect(months[11]).toEqual("Desember");
    });

    it("Should return months in Bahasa (given locale: id)", () => {
      const months = getLocalizeMonth("id");
      expect(months[0]).toEqual("Januari");
      expect(months[5]).toEqual("Juni");
      expect(months[11]).toEqual("Desember");
    });

    it("Should return months in English (given locale: en)", () => {
      const months = getLocalizeMonth("en");
      expect(months[0]).toEqual("January");
      expect(months[5]).toEqual("June");
      expect(months[11]).toEqual("December");
    });
  });

  describe("Method: formatSecondToMinuteSecond", () => {
    it("Should return correct format from second to minute", () => {
      expect(formatSecondToMinuteSecond(1)).toEqual("00:01");
      expect(formatSecondToMinuteSecond(12)).toEqual("00:12");
      expect(formatSecondToMinuteSecond(59)).toEqual("00:59");
      expect(formatSecondToMinuteSecond(60)).toEqual("01:00");
      expect(formatSecondToMinuteSecond(65)).toEqual("01:05");
      expect(formatSecondToMinuteSecond(120)).toEqual("02:00");
    });
  });
});
