import { getCurrentLocale, getTranslationManually } from "./index";

import Cookies from "js-cookie";

describe("Helper: Locale", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe("Method: getCurrentLocale", () => {
    it("Should return 'id' locale if no cookie stored", () => {
      expect(getCurrentLocale()).toEqual("id");
    });

    it("Should return 'en' locale", () => {
      Cookies.get = jest.fn().mockImplementation(() => "en");
      expect(getCurrentLocale()).toEqual("en");
    });

    it("Should return 'id' locale", () => {
      Cookies.get = jest.fn().mockImplementation(() => "id");
      expect(getCurrentLocale()).toEqual("id");
    });
  });

  describe("Method: getTranslationManually", () => {
    it("Should return text in English", () => {
      Cookies.get = jest.fn().mockImplementation(() => "en");
      expect(getTranslationManually("bahasa", "english")).toEqual("english");
    });

    it("Should return text in Bahasa", () => {
      Cookies.get = jest.fn().mockImplementation(() => "id");
      expect(getTranslationManually("bahasa", "english")).toEqual("bahasa");
    });
  });
});
