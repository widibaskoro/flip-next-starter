import Cookies from "js-cookie";

export function getCurrentLocale() {
  return Cookies.get("locale") ? Cookies.get("locale") : "id";
}

export function getTranslationManually(idText, enText) {
  return Cookies.get("locale") === "en" ? enText : idText;
}
