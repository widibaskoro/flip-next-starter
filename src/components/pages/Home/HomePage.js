import { Button, Alert } from '@flip-ui'
export default function HomePage(){
  return (
    <div>
      <h1>Homepage</h1>
      <p className='u-text-lg'>Homepage test</p>
      <Button>Test</Button>
      <Alert color='red'>Test</Alert>
    </div>
  )
}