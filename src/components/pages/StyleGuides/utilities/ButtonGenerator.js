import React from "react";

import { Button, Col, Row } from "@flip-ui";
import { Box } from "./Boxes";
import CodeBlock from "./CodeBlock";

import { faEdit, faRedo, faTrash, faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { startCase } from "lodash";

const Separator = () => <div className="u-h-2" />;

function ButtonGenerator(props) {
  const { variant, noOutline, noIconOnly, noBlock, ...restProps } = props;

  const title = startCase(variant);
  const text = title + " Button";

  return (
    <div {...restProps}>
      {/**********************************************************/}
      {/******************** DEFAULT SECTION *********************/}
      {/**********************************************************/}
      <Box>
        <h2 className="u-font-bold">{title}: Default</h2>
        <Row>
          <Col sm={3}>
            <h4 className="u-font-bold">Default</h4>
            <Button variant={variant}>{text}</Button>
            <Separator />
            <Button variant={variant} color="green">
              {text}
            </Button>
            <Separator />
            <Button variant={variant} color="red">
              {text}
            </Button>
            <Separator />
            <Button variant={variant} color="grey">
              {text}
            </Button>
          </Col>

          <Col sm={3}>
            <h4 className="u-font-bold">With Icon</h4>
            <Button variant={variant} icon={faEdit}>
              {text}
            </Button>
            <Separator />
            <Button variant={variant} color="green" icon={faRedo}>
              {text}
            </Button>
            <Separator />
            <Button variant={variant} color="red" icon={faTrash}>
              {text}
            </Button>
            <Separator />
            <Button variant={variant} color="grey" icon={faInfoCircle}>
              {text}
            </Button>
          </Col>

          <Col sm={3}>
            <h4 className="u-font-bold">Loading</h4>
            <Button variant={variant} activated>
              {text}
            </Button>
            <Separator />
            <Button variant={variant} color="green" activated>
              {text}
            </Button>
            <Separator />
            <Button variant={variant} color="red" activated>
              {text}
            </Button>
            <Separator />
            <Button variant={variant} color="grey" activated>
              {text}
            </Button>
          </Col>

          <Col sm={3}>
            <h4 className="u-font-bold">Disabled</h4>
            <Button variant={variant} disabled>
              {text}
            </Button>

            <h4 className="u-font-bold u-mt-10">Disabled + Loading</h4>
            <Button variant={variant} activated disabled>
              {text}
            </Button>
          </Col>
        </Row>

        <CodeBlock className="u-mt-5">
          <div>{"<Button"}</div>
          <div>&nbsp;&nbsp;{`variant="${variant}"`}</div>
          <div>&nbsp;&nbsp;{'color="..."'}</div>
          <div>&nbsp;&nbsp;{"..."}</div>
          <div>{">"}</div>
          <div>&nbsp;&nbsp;{"Text"}</div>
          <div>{"</Button>"}</div>
        </CodeBlock>
      </Box>

      {/**********************************************************/}
      {/******************** OUTLINE SECTION *********************/}
      {/**********************************************************/}
      {!noOutline && (
        <Box className="u-mt-10">
          <h2 className="u-font-bold">{title}: Outline</h2>
          <Row>
            <Col sm={3}>
              <h4 className="u-font-bold">Default</h4>
              <Button variant={variant} outline>
                {text}
              </Button>
              <Separator />
              <Button variant={variant} color="green" outline>
                {text}
              </Button>
              <Separator />
              <Button variant={variant} color="red" outline>
                {text}
              </Button>
              <Separator />
              <Button variant={variant} olor="grey" outline>
                {text}
              </Button>
            </Col>

            <Col sm={3}>
              <h4 className="u-font-bold">With Icon</h4>
              <Button variant={variant} icon={faEdit} outline>
                {text}
              </Button>
              <Separator />
              <Button variant={variant} color="green" icon={faRedo} outline>
                {text}
              </Button>
              <Separator />
              <Button variant={variant} color="red" icon={faTrash} outline>
                {text}
              </Button>
              <Separator />
              <Button variant={variant} color="grey" icon={faInfoCircle} outline>
                {text}
              </Button>
            </Col>

            <Col sm={3}>
              <h4 className="u-font-bold">Loading</h4>
              <Button variant={variant} activated outline>
                {text}
              </Button>
              <Separator />
              <Button variant={variant} color="green" activated outline>
                {text}
              </Button>
              <Separator />
              <Button variant={variant} color="red" activated outline>
                {text}
              </Button>
              <Separator />
              <Button variant={variant} color="grey" activated outline>
                {text}
              </Button>
            </Col>

            <Col sm={3}>
              <h4 className="u-font-bold">Disabled</h4>
              <Button variant={variant} disabled outline>
                {text}
              </Button>

              <h4 className="u-font-bold u-mt-10">Disabled + Loading</h4>
              <Button variant={variant} activated disabled outline>
                {text}
              </Button>
            </Col>
          </Row>

          <CodeBlock className="u-mt-5">
            <div>{"<Button"}</div>
            <div>&nbsp;&nbsp;{`variant="${variant}"`}</div>
            <div>&nbsp;&nbsp;{'color="..."'}</div>
            <div>&nbsp;&nbsp;{"outline"}</div>
            <div>&nbsp;&nbsp;{"..."}</div>
            <div>{">"}</div>
            <div>&nbsp;&nbsp;{"Text"}</div>
            <div>{"</Button>"}</div>
          </CodeBlock>
        </Box>
      )}

      {/**********************************************************/}
      {/******************* ICON ONLY SECTION ********************/}
      {/**********************************************************/}
      {!noIconOnly && (
        <Box className="u-mt-10">
          <h2 className="u-font-bold">{title}: Icon Only</h2>
          <Row>
            <Col sm={3}>
              <h4 className="u-font-bold">Default</h4>
              <Button variant={variant} icon={faEdit} />
              <Separator />
              <Button variant={variant} color="green" icon={faRedo} />
              <Separator />
              <Button variant={variant} color="red" icon={faTrash} />
              <Separator />
              <Button variant={variant} color="grey" icon={faInfoCircle} />
            </Col>

            <Col sm={3}>
              <h4 className="u-font-bold">Outline</h4>
              <Button variant={variant} icon={faEdit} outline />
              <Separator />
              <Button variant={variant} color="green" icon={faRedo} outline />
              <Separator />
              <Button variant={variant} color="red" icon={faTrash} outline />
              <Separator />
              <Button variant={variant} color="grey" icon={faInfoCircle} outline />
            </Col>

            <Col sm={3}>
              <h4 className="u-font-bold">Disabled</h4>
              <Button variant={variant} icon={faTrash} disabled />
            </Col>
          </Row>

          <CodeBlock className="u-mt-5">
            <div>{"<Button"}</div>
            <div>&nbsp;&nbsp;{`variant="${variant}"`}</div>
            <div>&nbsp;&nbsp;{'color="..."'}</div>
            <div>&nbsp;&nbsp;{"icon={...}"}</div>
            <div>&nbsp;&nbsp;{"..."}</div>
            <div>{"/>"}</div>
          </CodeBlock>
        </Box>
      )}

      {/**********************************************************/}
      {/********************** BLOCK SECTION *********************/}
      {/**********************************************************/}
      {!noBlock && (
        <Box className="u-mt-10">
          <h2 className="u-font-bold">{title}: Block (Full Width)</h2>
          <Row>
            <Col sm={12}>
              <Button variant={variant} block>
                {text}
              </Button>
              <Separator />
              <Button variant={variant} color="green" block>
                {text}
              </Button>
              <Separator />
              <Button variant={variant} color="red" block>
                {text}
              </Button>
              <Separator />
              <Button variant={variant} color="grey" block>
                {text}
              </Button>
            </Col>
          </Row>

          <CodeBlock className="u-mt-5">
            <div>{"<Button"}</div>
            <div>&nbsp;&nbsp;{`variant="${variant}"`}</div>
            <div>&nbsp;&nbsp;{'color="..."'}</div>
            <div>&nbsp;&nbsp;{"block"}</div>
            <div>&nbsp;&nbsp;{"..."}</div>
            <div>{">"}</div>
            <div>&nbsp;&nbsp;{"Text"}</div>
            <div>{"</Button>"}</div>
          </CodeBlock>
        </Box>
      )}
    </div>
  );
}

export default ButtonGenerator;
