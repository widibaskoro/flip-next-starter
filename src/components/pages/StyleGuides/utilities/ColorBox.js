import React from "react";

import styled from "styled-components";
import { startCase } from "lodash";

const Wrapper = styled.div`
  display: inline-block;
  margin: 0 12px;
  width: 170px;
`;

const ColorContainer = styled.div`
  background: ${({ background }) => background};
  border-radius: 4px;
  margin-bottom: 4px;
  height: 60px;
`;

const Code = styled.code`
  position: relative;
  top: -4px;
`;

function ColorBox(props) {
  const { title, hex, displayHex } = props;

  return (
    <Wrapper>
      <ColorContainer background={hex} className={title === "white" ? "u-border" : ""} />
      <div className="u-flex u-justify-between">
        <span className="u-text-dark-smoke u-text-base u-font-bold">{startCase(title)}</span>
        <span className="u-text-dark-grey u-text-base u-font-bold u-uppercase u-text-right">{displayHex || hex}</span>
      </div>
      <Code>{`colors.${title}`}</Code>
    </Wrapper>
  );
}

export default ColorBox;
