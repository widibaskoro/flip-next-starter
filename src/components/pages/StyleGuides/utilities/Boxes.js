import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

export const BodyContainer = styled.div`
  background: #e9e9e9;
  padding: 20px 10px 0 10px;
`;

export const Container = styled.div`
  background: white;
  width: 100%;
  padding: 20px;
  border-radius: 8px;
  margin-bottom: 25px;
`;

export const Box = styled.div`
  margin-bottom: 12px;
`;

export const ColorBox = styled.div`
  display: inline-block;
  margin: 12px;
  text-align: center;
  width: 168px;

  h5,
  p,
  code {
    margin: 0;
  }
`;

export const MenuWrapper = styled.div`
  background: white;
  position: fixed;
  height: 100vh;
  width: 16.67%;
  padding: 34px 10px 24px 24px;
  margin: -20px -10px 0 -25px;
  overflow: auto;
`;

export const Menus = styled.div`
  margin: 0;
  padding: 0;
  width: 100%;

  a {
    border-radius: 8px;
    color: ${colors.black_bekko};
    cursor: pointer;
    display: block;
    font-style: normal;
    font-weight: ${typography.font_weight.bold};
    font-size: ${typography.font_size.base};
    line-height: ${typography.line_height};
    height: 40px;
    padding: 8px 12px;
    text-decoration: none;

    &:hover {
      background-color: ${colors.light_anemia};
      color: ${colors.flip_orange};
      text-decoration: none;
    }
  }
`;

export const Title = styled.h1`
  font-size: 3.75rem;
  font-weight: ${typography.font_weight.bold};
`;

export const Separator = styled.hr`
  border: 0;
  border-top: 2px solid white;
  margin: 32px auto;
  width: 65%;
`;

export const OneThirdBox = styled.div`
  display: inline-block;
  margin: 0;
  padding: 16px;
  width: 33%;
  vertical-align: top;
`;
