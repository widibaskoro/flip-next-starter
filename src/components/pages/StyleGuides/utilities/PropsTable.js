import React from "react";

import styled from "styled-components";
import { colors, typography } from "src/assets/styles/settings";

const Wrapper = styled.div`
  display: block;
  padding: 8px 0 20px;
`;

const Table = styled.table`
  border-collapse: collapse;
  width: 100%;

  thead > tr > th {
    border-bottom: 1px solid ${colors.light_grey};
    color: ${colors.dark_grey};
    font-size: ${typography.font_size.small};
    font-weight: ${typography.font_weight.bold};
    line-height: ${typography.line_height};
    padding: 4px 12px;
    text-align: left;
    text-transform: uppercase;
    vertical-align: bottom;
  }

  th,
  td {
    border: 1px solid ${colors.light_grey};
  }

  td {
    padding: 8px 12px;
  }
`;

const Prop = styled.div`
  padding: 0;
  margin: 0;
`;

function PropsTable(props) {
  const { children, title, withOtherInfo } = props;

  function renderChildren() {
    const content = Array.isArray(children) ? children : [children];

    return content.map((child, i) => {
      const { name, type, desc, def, required } = child.props;

      return (
        <tr key={i}>
          <td>
            <code>{name}</code>
          </td>
          <td>
            <code>{type}</code>
          </td>
          <td>{desc}</td>
          <td>
            <code>{def}</code>
          </td>
          <td>{required ? "Yes" : ""}</td>
        </tr>
      );
    });
  }

  return (
    <div>
      <Wrapper>
        <h4>{title || "Props:"}</h4>

        {withOtherInfo && (
          <div className="u-mb-2">
            <em>Other props (that are not written here) are the same as its HTML tag attributes/props.</em>
          </div>
        )}

        <Table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Type</th>
              <th>Description</th>
              <th>Default value</th>
              <th>Required</th>
            </tr>
          </thead>
          <tbody>{renderChildren()}</tbody>
        </Table>
      </Wrapper>
    </div>
  );
}

PropsTable.Prop = Prop;

export default PropsTable;
