import React from "react";

import styled from "styled-components";
import { colors } from "src/assets/styles/settings";

const Wrapper = styled.div`
  background: ${colors.black_bekko};
  border-radius: 6px;
  padding: 12px;

  code {
    color: ${colors.catskill_white};
    font-size: 13px;
  }
`;

function CodeBlock(props) {
  const { children, ...restProps } = props;
  return (
    <Wrapper {...restProps}>
      <code>{children}</code>
    </Wrapper>
  );
}

export default CodeBlock;
