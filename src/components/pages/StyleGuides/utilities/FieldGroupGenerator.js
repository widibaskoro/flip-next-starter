import React, { useState } from "react";

import { Alert, Button, Col, Dropdown, Field, FieldGroup, Icon, Row } from "@flip-ui";

import { faSearch, faHome } from "@fortawesome/free-solid-svg-icons";
import CodeBlock from "../utilities/CodeBlock";

const addressOptions = [
  {
    label: "Depok",
    value: "1"
  },
  {
    label: "Jakarta",
    value: "2"
  },
  {
    label: "Medan",
    value: "3"
  },
  {
    label: "Tanah Abang",
    value: "4"
  }
];

const dataOptions = [
  {
    label: "ID Transaksi",
    value: "1"
  },
  {
    label: "Nama",
    value: "2"
  },
  {
    label: "Bank",
    value: "3"
  }
];

function FieldGroupGenerator(props) {
  const { type } = props;

  const [selectedSingle, setSelectedSingle] = useState(dataOptions[0]);
  const [selectedSingle2, setSelectedSingle2] = useState({});

  const Component = type === "Prepend" ? FieldGroup.Prepend : FieldGroup.Append;

  return (
    <div>
      <h2 className="u-font-bold u-mt-10 u-mb-0">{type}</h2>

      {/***********************************************/}
      {/******************** TEXT *********************/}
      {/***********************************************/}
      <h4 className="u-font-bold u-mt-3">Text</h4>
      <Row>
        <Col sm={4}>
          <FieldGroup>
            <Component>Rp</Component>
            <Field placeholder="A placeholder" />
          </FieldGroup>

          <FieldGroup className="u-mt-4">
            <Component>Rp</Component>
            <Field placeholder="A placeholder" disabled />
          </FieldGroup>

          <FieldGroup className="u-mt-4">
            <Component>Rp</Component>
            <Field placeholder="A placeholder" isInvalid />
            <Field.Feedback>Invalid message</Field.Feedback>
          </FieldGroup>
        </Col>
        <Col sm={8}>
          <CodeBlock>
            <div>{'import { FieldGroup, Field } from "@flip-ui";'}</div>
            <br />
            <div>{"<FieldGroup>"}</div>
            <div>&nbsp;&nbsp;{`<FieldGroup.${type}>Text</FieldGroup.${type}>`}</div>
            <div>&nbsp;&nbsp;{"<Field />"}</div>
            <div>{"</FieldGroup>"}</div>
          </CodeBlock>
        </Col>
      </Row>

      {/***********************************************/}
      {/******************** ICON *********************/}
      {/***********************************************/}
      <h4 className="u-font-bold u-mt-5">Icon</h4>
      <Row>
        <Col sm={4}>
          <FieldGroup>
            <Component>
              <Icon icon={faSearch} />
            </Component>
            <Field placeholder="A placeholder" />
          </FieldGroup>

          <FieldGroup className="u-mt-4">
            <Component>
              <Icon icon={faSearch} />
            </Component>
            <Field placeholder="A placeholder" disabled />
          </FieldGroup>

          <FieldGroup className="u-mt-4">
            <Component>
              <Icon icon={faSearch} />
            </Component>
            <Field placeholder="A placeholder" isInvalid />
            <Field.Feedback>Invalid message</Field.Feedback>
          </FieldGroup>
        </Col>
        <Col sm={8}>
          <CodeBlock>
            <div>{'import { FieldGroup, Field, Icon } from "@flip-ui";'}</div>
            <br />
            <div>{"<FieldGroup>"}</div>
            <div>&nbsp;&nbsp;{`<FieldGroup.${type}>`}</div>
            <div>&nbsp;&nbsp;&nbsp;&nbsp;{"<Icon icon={...} />"}</div>
            <div>&nbsp;&nbsp;{`</FieldGroup.${type}>`}</div>
            <div>&nbsp;&nbsp;{"<Field />"}</div>
            <div>{"</FieldGroup>"}</div>
          </CodeBlock>
        </Col>
      </Row>

      {/***************************************************/}
      {/******************** DROPDOWN *********************/}
      {/***************************************************/}
      <h4 className="u-font-bold u-mt-5">Dropdown</h4>
      <Row>
        <Col sm={4}>
          <FieldGroup>
            <Component
              as={Dropdown}
              options={dataOptions}
              selected={selectedSingle}
              placeholder="A placeholder"
              onChange={(selected) => setSelectedSingle(selected)}
            />
            <Field placeholder="A placeholder" />
          </FieldGroup>

          <FieldGroup className="u-mt-4">
            <Component
              as={Dropdown}
              options={dataOptions}
              selected={selectedSingle}
              placeholder="A placeholder"
              onChange={(selected) => setSelectedSingle(selected)}
              disabled
            />
            <Field placeholder="A placeholder" disabled />
          </FieldGroup>

          <FieldGroup className="u-mt-4">
            <Component
              as={Dropdown}
              options={dataOptions}
              selected={selectedSingle}
              placeholder="A placeholder"
              onChange={(selected) => setSelectedSingle(selected)}
            />
            <Field placeholder="A placeholder" isInvalid />
            <Field.Feedback>Invalid message</Field.Feedback>
          </FieldGroup>
        </Col>
        <Col sm={8}>
          <Alert color="blue">
            Use <code>{"as={Dropdown}"}</code> to make the {type} element become a Dropdown. The rest props needed are
            as the same as Dropdown component.
          </Alert>

          <CodeBlock className="u-mt-4">
            <div>{'import { Dropdown, FieldGroup, Field } from "@flip-ui";'}</div>
            <br />
            <div>{"<FieldGroup>"}</div>
            <div>&nbsp;&nbsp;{`<FieldGroup.${type}`}</div>
            <div>&nbsp;&nbsp;&nbsp;&nbsp;{"as={Dropdown}"}</div>
            <div>&nbsp;&nbsp;{"/>"}</div>
            <div>&nbsp;&nbsp;{"<Field />"}</div>
            <div>{"</FieldGroup>"}</div>
          </CodeBlock>
        </Col>
      </Row>

      {/*************************************************/}
      {/******************** BUTTON *********************/}
      {/*************************************************/}
      <h4 className="u-font-bold u-mt-5">Button</h4>
      <Row>
        <Col sm={4}>
          <FieldGroup>
            <Component as={Button} variant="secondary">
              Cari
            </Component>
            <Field placeholder="A placeholder" />
          </FieldGroup>

          <FieldGroup className="u-mt-4">
            <Component as={Button} variant="secondary" disabled>
              Cari
            </Component>
            <Field placeholder="A placeholder" disabled />
          </FieldGroup>

          <FieldGroup className="u-mt-4">
            <Component as={Button} variant="secondary">
              Cari
            </Component>
            <Field placeholder="A placeholder" isInvalid />
            <Field.Feedback>Invalid message</Field.Feedback>
          </FieldGroup>
        </Col>
        <Col sm={8}>
          <Alert color="blue">
            Use <code>{'as={Button} variant="secondary"'}</code> to make the {type} element become a Button. The rest
            props needed are as the same as Button component. Notice that you need to pass the button text as children,
            just like the Button component do.
          </Alert>

          <CodeBlock className="u-mt-4">
            <div>{'import { Button, FieldGroup, Field } from "@flip-ui";'}</div>
            <br />
            <div>{"<FieldGroup>"}</div>
            <div>&nbsp;&nbsp;{`<FieldGroup.${type} as={Button} variant="secondary">`}</div>
            <div>&nbsp;&nbsp;&nbsp;&nbsp;{"Text"}</div>
            <div>&nbsp;&nbsp;{`</FieldGroup.${type}>`}</div>
            <div>&nbsp;&nbsp;{"<Field />"}</div>
            <div>{"</FieldGroup>"}</div>
          </CodeBlock>
        </Col>
      </Row>

      {/***************************************************************/}
      {/******************** TEXT/ICON + DROPDOWN *********************/}
      {/***************************************************************/}
      <h4 className="u-font-bold u-mt-5">Text/Icon + Dropdown for field</h4>
      <Row>
        <Col sm={4}>
          <FieldGroup>
            <FieldGroup>
              <Component>
                <Icon icon={faHome} />
              </Component>
              <Dropdown
                options={addressOptions}
                selected={selectedSingle2}
                placeholder="A placeholder"
                onChange={(selected) => setSelectedSingle2(selected)}
                fullWidth
              />
            </FieldGroup>
          </FieldGroup>

          <FieldGroup className="u-mt-4">
            <FieldGroup>
              <Component>
                <Icon icon={faHome} />
              </Component>
              <Dropdown
                options={addressOptions}
                selected={selectedSingle2}
                placeholder="A placeholder"
                onChange={(selected) => setSelectedSingle2(selected)}
                fullWidth
                disabled
              />
            </FieldGroup>
          </FieldGroup>

          <FieldGroup className="u-mt-4">
            <FieldGroup>
              <Component>
                <Icon icon={faHome} />
              </Component>
              <Dropdown
                options={addressOptions}
                selected={selectedSingle2}
                placeholder="A placeholder"
                onChange={(selected) => setSelectedSingle2(selected)}
                fullWidth
                isInvalid
              />
              <Field.Feedback>Invalid message</Field.Feedback>
            </FieldGroup>
          </FieldGroup>
        </Col>
        <Col sm={8}>
          <CodeBlock>
            <div>{'import { Dropdown, FieldGroup } from "@flip-ui";'}</div>
            <br />
            <div>{"<FieldGroup>"}</div>
            <div>&nbsp;&nbsp;{`<FieldGroup.${type}>Text</FieldGroup.${type}>`}</div>
            <div>&nbsp;&nbsp;{"<Dropdown ... />"}</div>
            <div>{"</FieldGroup>"}</div>
          </CodeBlock>
        </Col>
      </Row>
    </div>
  );
}

export default FieldGroupGenerator;
