import React from "react";

import { Steps } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

const steps = [
  {
    label: "Metode Input",
    status: "done"
  },
  {
    label: "Buat Transaksi",
    status: "done"
  },
  {
    label: "Metode Pembayaran",
    status: "done"
  },
  {
    label: "Kirim",
    status: "active"
  },
  {
    label: "Silakan Pulang",
    status: ""
  }
];

function StepsComp() {
  return (
    <Container>
      <h1 className="u-font-bold">Steps</h1>

      <Box>
        <Steps steps={steps} />
      </Box>

      <CodeBlock className="u-my-5">
        <div>{'import { Steps } from "@flip-ui";'}</div>
        <br />
        <div>{'<Steps steps={[{ label: "...", status: "..." }]} />'}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Steps">
        <PropsTable.Prop
          name="steps"
          type="array"
          desc={
            <>
              Array of objects of steps. What need to be included in object of steps?
              <ul>
                <li>label [string] [Required!]: step label</li>
                <li>status [string] [Required!]: step status ('active', 'done', 'none')</li>
              </ul>
              Example:
              <br />
              <code>
                {"["}
                <br />
                &nbsp;&nbsp;{"{"}
                <br />
                &nbsp;&nbsp;&nbsp; label: "First Step",
                <br />
                &nbsp;&nbsp;&nbsp; status: "done"
                <br />
                &nbsp;&nbsp;{"},"}
                <br />
                &nbsp;&nbsp;{"{"}
                <br />
                &nbsp;&nbsp;&nbsp; label: "Second Step",
                <br />
                &nbsp;&nbsp;&nbsp; status: "active"
                <br />
                &nbsp;&nbsp;{"}"}
                <br />
                {"]"}
              </code>
            </>
          }
          required
        />
      </PropsTable>
    </Container>
  );
}

export default StepsComp;
