import React from "react";

import { Dot } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function DotComp() {
  return (
    <Container>
      <h1 className="u-font-bold">Dot</h1>

      <Box>
        <Dot />
        <span className="u-ml-3">Orange</span>
      </Box>
      <Box>
        <Dot color="blue" />
        <span className="u-ml-3">Blue</span>
      </Box>
      <Box>
        <Dot color="green" />
        <span className="u-ml-3">Green</span>
      </Box>
      <Box>
        <Dot color="grey" />
        <span className="u-ml-3">Grey</span>
      </Box>
      <Box>
        <Dot color="red" />
        <span className="u-ml-3">Red</span>
      </Box>
      <Box>
        <Dot color="yellow" />
        <span className="u-ml-3">Yellow</span>
      </Box>
      <Box>
        <Dot color="light-grey" />
        <span className="u-ml-3">Light Grey</span>
      </Box>

      <CodeBlock className="u-mt-10">
        <div>{'import { Dot } from "@flip-ui";'}</div>
        <br />
        <div>{'<Dot color="..." />'}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Dot">
        <PropsTable.Prop
          name="color"
          type="string"
          desc="Set dot color ['orange', 'blue', 'green', 'grey', 'red', 'yellow', 'light-grey']"
          def="'orange'"
        />
      </PropsTable>
    </Container>
  );
}

export default DotComp;
