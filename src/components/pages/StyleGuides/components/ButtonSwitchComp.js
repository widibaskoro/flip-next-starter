import React, { useState } from "react";

import { ButtonSwitch } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

const ButtonGroupComp = () => {
  const [active, setActive] = useState("all");
  const [active2, setActive2] = useState("all");

  return (
    <Container>
      <h1 className="u-font-bold">ButtonSwitch</h1>

      <Box>
        <div className="u-w-6/12">
          <ButtonSwitch active={active}>
            <ButtonSwitch.Button key="all" onClick={() => setActive("all")}>
              Semua
            </ButtonSwitch.Button>
            <ButtonSwitch.Button key="credit" onClick={() => setActive("credit")}>
              Kredit
            </ButtonSwitch.Button>
            <ButtonSwitch.Button key="debit" onClick={() => setActive("debit")}>
              Debit
            </ButtonSwitch.Button>
          </ButtonSwitch>

          <p>Active key: "{active}"</p>
        </div>

        <div className="u-w-6/12">
          <ButtonSwitch active={active2}>
            <ButtonSwitch.Button key="all" onClick={() => setActive2("all")}>
              Semua
            </ButtonSwitch.Button>
            <ButtonSwitch.Button key="credit" onClick={() => setActive2("credit")}>
              Kredit
            </ButtonSwitch.Button>
            <ButtonSwitch.Button key="debit" onClick={() => setActive2("debit")} disabled>
              Debit
            </ButtonSwitch.Button>
          </ButtonSwitch>
        </div>
      </Box>

      <CodeBlock className="u-mt-10">
        <div>{'import { ButtonSwitch } from "@flip-ui";'}</div>
        <br />
        <div>{"<ButtonSwitch active={...}>"}</div>
        <div>&nbsp;&nbsp;{'<ButtonSwitch.Button key="..." onClick={...}>Semua</ButtonSwitch.Button>'}</div>
        <div>&nbsp;&nbsp;{"..."}</div>
        <div>{"</ButtonSwitch>"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="ButtonSwitch">
        <PropsTable.Prop name="active" type="string" desc="Current active key" def="false" required />
      </PropsTable>

      <PropsTable title="ButtonSwitch.Button">
        <PropsTable.Prop name="key" type="string" desc="To identify which button is active" def="false" required />
      </PropsTable>
    </Container>
  );
};

export default ButtonGroupComp;
