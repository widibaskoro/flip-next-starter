import React, { useState } from "react";

import { Button, Tabs } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import CodeBlock from "../utilities/CodeBlock";
import PropsTable from "../utilities/PropsTable";

function TabsComp() {
  const [key, setTabKey] = useState("tab1");

  return (
    <Container>
      <h1 className="u-font-bold">Tabs</h1>

      <Box>
        <h4 className="u-font-bold">Default</h4>
        <div className="u-p-10 u-mb-3 u-bg-main-background u-rounded">
          <Tabs activeTabKey="tab1">
            <Tabs.Tab label="1st Tab" tabKey="tab1">
              Content of 1st tab
            </Tabs.Tab>
            <Tabs.Tab label="2nd Tab" tabKey="tab2">
              Content of 2nd tab
            </Tabs.Tab>
            <Tabs.Tab label="3rd Tab" tabKey="tab3">
              Content of 3rd tab
            </Tabs.Tab>
          </Tabs>
        </div>
        <CodeBlock>
          <div>{'import { Tabs } from "@flip-ui";'}</div>
          <br />
          <div>{'<Tabs activeTabKey="...">'}</div>
          <div>&nbsp;&nbsp;{'<Tabs.Tab label="..." tabKey="...">'}</div>
          <div>&nbsp;&nbsp;&nbsp;&nbsp;Content</div>
          <div>&nbsp;&nbsp;{"</Tabs.Tab>"}</div>
          <div>&nbsp;&nbsp;{"..."}</div>
          <div>{"</Tabs>"}</div>
        </CodeBlock>

        <h4 className="u-font-bold u-mt-6">Justify</h4>
        <div className="u-p-10 u-mb-3 u-bg-main-background u-rounded">
          <Tabs activeTabKey="tab1" justify>
            <Tabs.Tab label="1st Tab" tabKey="tab1">
              Content of 1st tab
            </Tabs.Tab>
            <Tabs.Tab label="2nd Tab" tabKey="tab2">
              Content of 2nd tab
            </Tabs.Tab>
            <Tabs.Tab label="3rd Tab" tabKey="tab3">
              Content of 3rd tab
            </Tabs.Tab>
          </Tabs>
        </div>
        <CodeBlock>
          <div>{'import { Tabs } from "@flip-ui";'}</div>
          <br />
          <div>{'<Tabs activeTabKey="..." justify>'}</div>
          <div>&nbsp;&nbsp;{'<Tabs.Tab label="..." tabKey="...">'}</div>
          <div>&nbsp;&nbsp;&nbsp;&nbsp;Content</div>
          <div>&nbsp;&nbsp;{"</Tabs.Tab>"}</div>
          <div>&nbsp;&nbsp;{"..."}</div>
          <div>{"</Tabs>"}</div>
        </CodeBlock>

        <h4 className="u-font-bold  u-mt-6">Controlled from Outside Nav</h4>
        <Button className="u-mb-3" variant="mini" onClick={() => setTabKey("tab3")}>
          Go to 3rd tab
        </Button>
        <div className="u-p-10 u-mb-3 u-bg-main-background u-rounded">
          <Tabs activeTabKey={key} onChange={setTabKey}>
            <Tabs.Tab label="1st Tab" tabKey="tab1">
              Content of 1st tab
            </Tabs.Tab>
            <Tabs.Tab label="2nd Tab" tabKey="tab2">
              Content of 2nd tab
            </Tabs.Tab>
            <Tabs.Tab label="3rd Tab" tabKey="tab3">
              Content of 3rd tab
            </Tabs.Tab>
          </Tabs>
        </div>
        <CodeBlock>
          <div>{'import { Tabs } from "@flip-ui";'}</div>
          <br />
          <div>{"<Tabs activeTabKey={...} onChange={setTabKey}>"}</div>
          <div>&nbsp;&nbsp;{'<Tabs.Tab label="..." tabKey="...">'}</div>
          <div>&nbsp;&nbsp;&nbsp;&nbsp;Content</div>
          <div>&nbsp;&nbsp;{"</Tabs.Tab>"}</div>
          <div>&nbsp;&nbsp;{"..."}</div>
          <div>{"</Tabs>"}</div>
          <br />
          <div>{'// somewhere in a function: setTabKey("tabkey")'}</div>
        </CodeBlock>
      </Box>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Tabs">
        <PropsTable.Prop name="activeTabKey" type="string" desc="Set which tab is active with key" required />
        <PropsTable.Prop name="justify" type="boolean" desc="Set tab nav being equal with and full" def="false" />
        <PropsTable.Prop
          name="fullHeight"
          type="boolean"
          desc="Set tab to be full height (to the bottom of page)"
          def="false"
        />
        <PropsTable.Prop
          name="onChange"
          type="function"
          desc="Callback function when tab nav is clicked"
          def="() => {}"
        />
      </PropsTable>

      <PropsTable title="Tabs.Tab">
        <PropsTable.Prop name="tabKey" type="string" desc="Tab nav key" required />
        <PropsTable.Prop name="label" type="string" desc="Tab nav label" def="''" />
      </PropsTable>
    </Container>
  );
}

export default TabsComp;
