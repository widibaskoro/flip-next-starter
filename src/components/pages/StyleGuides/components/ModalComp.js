import React, { useState } from "react";

import { Button, Modal } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import CodeBlock from "../utilities/CodeBlock";
import PropsTable from "../utilities/PropsTable";

const ModalComp = () => {
  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);
  const [show3, setShow3] = useState(false);
  const [show4, setShow4] = useState(false);
  const [show5, setShow5] = useState(false);
  const [show6, setShow6] = useState(false);
  const [show7, setShow7] = useState(false);

  return (
    <Container>
      <h1 className="u-font-bold">Modal</h1>

      <Box>
        <h4 className="u-font-bold">Default</h4>
        <Button variant="secondary" onClick={() => setShow(true)}>
          Show Modal
        </Button>

        <Modal title="Default Modal" show={show} onClose={() => setShow(false)}>
          <span>Default Modal</span>
          <div className="u-text-right u-mt-2">
            <Button variant="secondary" onClick={() => setShow(false)}>
              OK
            </Button>
          </div>
        </Modal>
      </Box>

      <CodeBlock>
        <div>{'import { Modal } from "@flip-ui";'}</div>
        <br />
        <div>{'<Modal title="Default Modal" size="large" show={...} onClose={...}>'}</div>
        <div>&nbsp;&nbsp;Modal content</div>
        <div>{"</Modal>"}</div>
      </CodeBlock>

      <Box>
        <h4 className="u-font-bold u-mt-5">Close on Overlay</h4>
        <Button variant="secondary" onClick={() => setShow2(true)}>
          Show Modal
        </Button>

        <Modal title="Modal Closed When Overlay Clicked" show={show2} onClose={() => setShow2(false)} closeOnOverlay>
          <span>This modal will be closed if the overlay is clicked.</span>
          <div className="u-text-right u-mt-2">
            <Button variant="secondary" onClick={() => setShow2(false)}>
              OK
            </Button>
          </div>
        </Modal>
      </Box>

      <CodeBlock>
        <div>{'import { Modal } from "@flip-ui";'}</div>
        <br />
        <div>
          {'<Modal title="Modal Closed When Overlay Clicked" closeOnOverlay size="large" show={...} onClose={...}>'}
        </div>
        <div>&nbsp;&nbsp;Modal content</div>
        <div>{"</Modal>"}</div>
      </CodeBlock>

      <Box>
        <h4 className="u-font-bold u-mt-5">Sizes</h4>

        <div className="u-flex u-space-x-5 u-w-full">
          <div className="u-w-4/12">
            <Button variant="secondary" onClick={() => setShow3(true)} className="u-mb-3">
              Show Modal Small
            </Button>
            <CodeBlock>
              <div>{'import { Modal } from "@flip-ui";'}</div>
              <br />
              <div>{'<Modal size="small" ... >'}</div>
              <div>&nbsp;&nbsp;Modal content</div>
              <div>{"</Modal>"}</div>
            </CodeBlock>
          </div>

          <div className="u-w-4/12">
            <Button variant="secondary" onClick={() => setShow4(true)} className="u-mb-3">
              Show Modal Large
            </Button>
            <CodeBlock>
              <div>{'import { Modal } from "@flip-ui";'}</div>
              <br />
              <div>{'<Modal size="large" ... >'}</div>
              <div>&nbsp;&nbsp;Modal content</div>
              <div>{"</Modal>"}</div>
            </CodeBlock>
          </div>

          <div className="u-w-4/12">
            <Button variant="secondary" onClick={() => setShow7(true)} className="u-mb-3">
              Show Modal Huge
            </Button>
            <CodeBlock>
              <div>{'import { Modal } from "@flip-ui";'}</div>
              <br />
              <div>{'<Modal size="huge" ... >'}</div>
              <div>&nbsp;&nbsp;Modal content</div>
              <div>{"</Modal>"}</div>
            </CodeBlock>
          </div>
        </div>

        <Modal title="Modal Small" show={show3} size="small" onClose={() => setShow3(false)}>
          <span>Modal small: 380px</span>
          <div className="u-text-right u-mt-2">
            <Button variant="secondary" onClick={() => setShow3(false)}>
              OK
            </Button>
          </div>
        </Modal>

        <Modal title="Modal Large" show={show4} size="large" onClose={() => setShow4(false)}>
          <span>Modal large: 696px</span>
          <div className="u-text-right u-mt-2">
            <Button variant="secondary" onClick={() => setShow4(false)}>
              OK
            </Button>
          </div>
        </Modal>

        <Modal title="Modal Huge" show={show7} size="huge" onClose={() => setShow7(false)}>
          <span>Modal huge: 900px</span>
          <div className="u-text-right u-mt-2">
            <Button variant="secondary" onClick={() => setShow7(false)}>
              OK
            </Button>
          </div>
        </Modal>
      </Box>

      <Box>
        <h4 className="u-font-bold u-mt-5">Without Header</h4>
        <Button variant="secondary" onClick={() => setShow5(true)}>
          Show Modal
        </Button>

        <Modal show={show5} onClose={() => setShow5(false)} withoutHeader>
          <div className="u-text-center">
            <h5 className="u-font-bold">Modal without header</h5>
            <p>This modal has no header</p>
            <Button variant="secondary" onClick={() => setShow5(false)} block>
              OK
            </Button>
          </div>
        </Modal>
      </Box>

      <CodeBlock>
        <div>{'import { Modal } from "@flip-ui";'}</div>
        <br />
        <div>{"<Modal withoutHeader show={...} onClose={...}>"}</div>
        <div>&nbsp;&nbsp;Modal content</div>
        <div>{"</Modal>"}</div>
      </CodeBlock>

      <Box>
        <h4 className="u-font-bold u-mt-5">Without Close Button</h4>
        <Button variant="secondary" onClick={() => setShow6(true)}>
          Show Modal
        </Button>

        <Modal title="Without Close Button" show={show6} withoutCloseButton>
          <span>Modal that has no close button</span>
          <div className="u-text-right u-mt-2">
            <Button variant="secondary" onClick={() => setShow6(false)}>
              OK
            </Button>
          </div>
        </Modal>
      </Box>

      <CodeBlock>
        <div>{'import { Modal } from "@flip-ui";'}</div>
        <br />
        <div>{'<Modal title="Without Close Button" withoutCloseButton show={...} onClose={...}>'}</div>
        <div>&nbsp;&nbsp;Modal content</div>
        <div>{"</Modal>"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Modal">
        <PropsTable.Prop name="title" type="string" desc="Set modal title" def="''" />
        <PropsTable.Prop name="show" type="boolean" desc="Set modal showed or hidden" def="false" />
        <PropsTable.Prop
          name="size"
          type="string"
          desc={
            <>
              Set modal size ['default', 'small', 'large', 'huge']
              <ul>
                <li>small: 380px</li>
                <li>default: 540px</li>
                <li>large: 696px</li>
                <li>huge: 900px</li>
              </ul>
            </>
          }
          def="'default'"
        />
        <PropsTable.Prop name="onClose" type="function" desc="Function for hiding modal" def="() => {}" />
        <PropsTable.Prop
          name="closeOnOverlay"
          type="boolean"
          desc="Set modal to be hidden when overlay clicked"
          def="false"
        />
        <PropsTable.Prop name="withoutHeader" type="boolean" desc="Set modal has no header" def="false" />
        <PropsTable.Prop name="withoutCloseButton" type="boolean" desc="Set modal has no close button" def="false" />
      </PropsTable>
    </Container>
  );
};

export default ModalComp;
