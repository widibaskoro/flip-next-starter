import React, { useState } from "react";

import { Col, FileInput, Row } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function FileInputComp() {
  const [uploadedFile, setUploadedFile] = useState(null);
  const [isUploading, setIsUploading] = useState(false);
  const [isUploading2, setIsUploading2] = useState(false);

  function handleOnChange(file) {
    setUploadedFile(file);
    setIsUploading(true);
    setTimeout(() => setIsUploading(false), 5000);
  }

  function handleOnChange2(file) {
    setUploadedFile(file);
    setIsUploading2(true);
    setTimeout(() => setIsUploading2(false), 5000);
  }

  return (
    <Container>
      <h1 className="u-font-bold">FileInput</h1>

      <Box>
        <Row>
          <Col sm={7}>
            <h4 className="u-font-bold">Default</h4>
            <p>
              Uploaded file name: <b>{uploadedFile ? uploadedFile.name : ""}</b>
            </p>
            <FileInput
              placeholder="Taruh file CSV di sini"
              onChange={handleOnChange}
              isUploading={isUploading}
              fileTypes=".csv, .xls, .xlsx, .numbers"
              maxFileSize={2097152}
            />
          </Col>
        </Row>
      </Box>

      <Box className="u-mt-5">
        <Row>
          <Col sm={7}>
            <h4 className="u-font-bold">Disabled</h4>
            <FileInput
              placeholder="Taruh file CSV di sini"
              onChange={handleOnChange2}
              isUploading={isUploading2}
              fileTypes=".csv, .xls, .xlsx, .numbers"
              maxFileSize={2097152}
              disabled
            />
          </Col>
        </Row>
      </Box>

      <CodeBlock className="u-mt-7">
        <div>{'import { FileInput } from "@flip-ui";'}</div>
        <br />
        <div>{"<FileInput"}</div>
        <div>&nbsp;&nbsp;{'placeholder="..."'}</div>
        <div>&nbsp;&nbsp;{"fileTypes={...}"}</div>
        <div>&nbsp;&nbsp;{"maxFileSize={...}"}</div>
        <div>&nbsp;&nbsp;{"onChange={...}"}</div>
        <div>&nbsp;&nbsp;{"isUploading={...}"}</div>
        <div>&nbsp;&nbsp;{"..."}</div>
        <div>{"/>"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="FileInput">
        <PropsTable.Prop name="placeholder" type="string" desc="Set field placeholder" def="'Taruh di sini'" />
        <PropsTable.Prop name="maxFileSize" type="number" desc="Set max file size (in bytes)" def="2097152 (2 MB)" />
        <PropsTable.Prop name="isUploading" type="boolean" desc="Set field to be uploading state" def="false" />
        <PropsTable.Prop name="disabled" type="boolean" desc="Set field to be disabled" def="false" />
        <PropsTable.Prop name="onChange" type="function" desc="Callback after file accepted" def="() => {}" />
        <PropsTable.Prop
          name="fileTypes"
          type="string"
          desc={
            <>
              Set accepted file types (file extension separated by coma)
              <br />
              Example:
              <code>".png, .jpg"</code>
              <br />
              <span>see these links for more info:</span>
              <ul>
                <li>
                  <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types">
                    https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types
                  </a>
                </li>
                <li>
                  <a href="http://www.iana.org/assignments/media-types/media-types.xhtml">
                    http://www.iana.org/assignments/media-types/media-types.xhtml
                  </a>
                </li>
              </ul>
            </>
          }
          def="''"
        />
      </PropsTable>
    </Container>
  );
}

export default FileInputComp;
