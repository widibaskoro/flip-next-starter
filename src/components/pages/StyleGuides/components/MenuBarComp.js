import React from "react";

import { MenuBar } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

const allMenus = {
  home: {
    label: ["Beranda", "Home"],
    as: "a",
    to: "#aa",
    icon: "home",
    active_url_prefixes: ["home"],
    show: true
  },
  send_money: {
    label: ["Kirim Uang", "Send Money"],
    as: "a",
    to: "#bb",
    icon: "send",
    active_url_prefixes: ["transaction"],
    show: true
  },
  transaction_history: {
    label: ["Riwayat Transaksi", "Transaction History"],
    as: "a",
    to: "#cc",
    icon: "note",
    active_url_prefixes: ["history"],
    show: true,
    childs: [
      { label: ["Overview", "Overview"], path: ["#a"] },
      { label: ["Laporan Transaksi", "Transaction Report"], path: ["#b"] }
    ]
  },
  approval: {
    label: ["Persetujuan", "Approval"],
    as: "a",
    to: "#dd",
    icon: "check",
    active_url_prefixes: ["approval"],
    show: true
  },
  deposit: {
    label: ["Deposit Saya", "My Deposit"],
    as: "a",
    to: "#ee",
    icon: "wallet",
    active_url_prefixes: ["deposit"],
    show: true,
    childs: [
      { label: ["Overview", "Overview"], path: ["#c"] },
      {
        label: ["Top-up Deposit", "Top-up Deposit"],
        path: ["#k"]
      },
      { label: ["Riwayat Top-up", "Top-up History"], path: ["#d"] },
      { label: ["Laporan Deposit", "Deposit Statement"], path: ["#e"] }
    ]
  },
  bank_accounts: {
    label: ["Daftar Rekening", "Bank Accounts"],
    as: "a",
    to: "#ff",
    icon: "book",
    active_url_prefixes: ["bank-accounts"],
    show: true
  },
  refund: {
    label: ["Refund", "Refund"],
    as: "a",
    to: "#gg",
    icon: "redo",
    active_url_prefixes: ["refund"],
    show: true,
    childs: [
      { label: ["Overview", "Overview"], path: ["#f"] },
      { label: ["Buat Refund", "Create Refund"], path: ["#g"] }
    ]
  },
  accounts: {
    label: ["Kelola Multi Akun", "Manage Multi Account"],
    as: "a",
    to: "#hh",
    icon: "user",
    active_url_prefixes: ["accounts"],
    show: true,
    break: true
  },
  settings: {
    label: ["Pengaturan", "Settings"],
    as: "a",
    to: "#ii",
    icon: "gear",
    active_url_prefixes: ["settings"],
    show: true,
    childs: [
      { label: ["Profil", "Profile"], path: ["#h"] },
      { label: ["Notifikasi", "Notification"], path: ["#i"] },
      { label: ["Kelola API", "Manage API"], path: ["#j"] }
    ]
  }
};

const MenuBarComp = () => {
  const menus = Object.values(allMenus);

  return (
    <Container>
      <h1 className="u-font-bold">MenuBar</h1>

      <div className="u-grid u-grid-cols-2 u-gap-5">
        <Box>
          <h4 className="u-font-bold">Default</h4>
          <div style={{ width: 275 }} className="u-flex u-justify-center u-bg-white u-py-4 u-border u-rounded-md">
            <div style={{ width: 245 }}>
              <MenuBar menus={menus} currentPath="/transaction" />
            </div>
          </div>
        </Box>

        <Box>
          <h4 className="u-font-bold">Collapsed</h4>
          <div style={{ width: 71 }} className="u-flex u-justify-center u-bg-white u-py-4 u-border u-rounded-md">
            <div style={{ width: 41 }}>
              <MenuBar menus={menus} currentPath="/settings" collapsed />
            </div>
          </div>
        </Box>
      </div>

      <div className="u-grid u-grid-cols-2 u-gap-5 u-mt-5">
        <CodeBlock>
          <div>{'import { MenuBar } from "@flip-ui";'}</div>
          <br />
          <div>{'<MenuBar menus={...} currentPath="..." />'}</div>
        </CodeBlock>
        <CodeBlock>
          <div>{'import { MenuBar } from "@flip-ui";'}</div>
          <br />
          <div>{'<MenuBar menus={...} currentPath="..." collapsed />'}</div>
        </CodeBlock>
      </div>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="MenuBar">
        <PropsTable.Prop name="menus" type="array" desc="Set menu bar menus" required />
        <PropsTable.Prop name="collapsed" type="boolean" desc="Set menu bar to be collapsed mode" def="false" />
        <PropsTable.Prop
          name="currentPath"
          type="string"
          desc="Current location path to determine which menu is active"
          def="''"
        />
      </PropsTable>
    </Container>
  );
};

export default MenuBarComp;
