import React from "react";

import { Alert, Col, Field, FieldGroup, FieldLabel, Form, Icon, Row } from "@flip-ui";

import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function FormComp() {
  return (
    <Container>
      <h1 className="u-font-bold">Form</h1>

      <Box>
        <h4 className="u-font-bold">Form Group</h4>
        <Form>
          <Row>
            <Col sm={4}>
              <Form.Group>
                <FieldLabel>First name</FieldLabel>
                <Field placeholder="Your first name" />
              </Form.Group>

              <Form.Group>
                <FieldLabel>Search transaction</FieldLabel>
                <FieldGroup>
                  <FieldGroup.Prepend>
                    <Icon icon={faSearch} />
                  </FieldGroup.Prepend>
                  <Field placeholder="Transaction ID, name, date" />
                </FieldGroup>
              </Form.Group>
            </Col>
          </Row>
        </Form>
      </Box>

      <Alert color="grey" className="u-mt-4 u-w-6/12">
        <div>
          A <code>Form</code> and <code>Form.Group</code> are just a wrapper for the <code>FieldLabel</code>,{" "}
          <code>FieldGroup</code>, <code>Field</code>, and/or <code>Dropdown</code> components.
        </div>
      </Alert>

      <CodeBlock className="u-my-5">
        <div>{'import { Form } from "@flip-ui";'}</div>
        <br />
        <div>{"<Form>"}</div>
        <div>&nbsp;&nbsp;{"<Form.Group>"}</div>
        <div>&nbsp;&nbsp;&nbsp;&nbsp;{"<FieldLabel>Search transaction</FieldLabel>"}</div>
        <div>&nbsp;&nbsp;&nbsp;&nbsp;{"<FieldGroup>"}</div>
        <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{"<FieldGroup.Prepend>...</FieldGroup.Prepend>"}</div>
        <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{"<Field ... />"}</div>
        <div>&nbsp;&nbsp;&nbsp;&nbsp;{"</FieldGroup>"}</div>
        <div>&nbsp;&nbsp;{"</Form.Group>"}</div>
        <div>{"</Form>"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Form">
        <PropsTable.Prop name="children" type="any" desc="Set form content" />
      </PropsTable>
      <PropsTable title="Form.Group">
        <PropsTable.Prop name="children" type="any" desc="Set form group content" />
      </PropsTable>
    </Container>
  );
}

export default FormComp;
