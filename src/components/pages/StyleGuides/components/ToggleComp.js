import React, { useState } from "react";

import { Toggle } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function ToggleComp() {
  const [checked, setChecked] = useState(true);
  const [checked2, setChecked2] = useState(false);
  const [checked3, setChecked3] = useState(false);

  return (
    <Container>
      <h1 className="u-font-bold">Toggle</h1>

      <Box>
        <h3 className="u-font-bold">Big</h3>
        <div className="u-flex u-w-8/12 u-justify-between">
          <div>
            <h5 className="u-font-bold">On</h5>
            <Toggle checked={checked} onChange={(event) => setChecked(event.target.checked)} />
          </div>
          <div>
            <h5 className="u-font-bold">Off</h5>
            <Toggle checked={checked2} onChange={(event) => setChecked2(event.target.checked)} />
          </div>
          <div>
            <h5 className="u-font-bold">On Disabled</h5>
            <Toggle checked disabled />
          </div>
          <div>
            <h5 className="u-font-bold">Off Disabled</h5>
            <Toggle disabled />
          </div>
        </div>
        <CodeBlock className="u-mt-3">
          <div>{'import { Toggle } from "@flip-ui";'}</div>
          <br />
          <div>{'<Toggle size="big" checked={...} onChange={...} />'}</div>
        </CodeBlock>

        <h3 className="u-font-bold u-mt-8">Small</h3>
        <div className="u-flex u-w-8/12 u-justify-between">
          <div>
            <h5 className="u-font-bold">On</h5>
            <Toggle size="small" checked={checked} onChange={(event) => setChecked(event.target.checked)} />
          </div>
          <div>
            <h5 className="u-font-bold">Off</h5>
            <Toggle size="small" checked={checked2} onChange={(event) => setChecked2(event.target.checked)} />
          </div>
          <div>
            <h5 className="u-font-bold">On Disabled</h5>
            <Toggle size="small" checked disabled />
          </div>
          <div>
            <h5 className="u-font-bold">Off Disabled</h5>
            <Toggle size="small" disabled />
          </div>
        </div>
        <CodeBlock className="u-mt-3">
          <div>{'import { Toggle } from "@flip-ui";'}</div>
          <br />
          <div>{'<Toggle size="small" checked={...} onChange={...} />'}</div>
        </CodeBlock>

        <h3 className="u-font-bold u-mt-8">Labels</h3>
        <div className="u-flex u-w-10/12 u-justify-between">
          <div>
            <h5 className="u-font-bold">Big</h5>
            <Toggle
              size="big"
              checked={checked3}
              labels={["Off", "On"]}
              onChange={(event) => setChecked3(event.target.checked)}
            />
          </div>
          <div>
            <h5 className="u-font-bold">Small</h5>
            <Toggle
              size="small"
              checked={checked3}
              labels={["Off", "On"]}
              onChange={(event) => setChecked3(event.target.checked)}
            />
          </div>
          <div>
            <h5 className="u-font-bold">Big Disabled</h5>
            <Toggle size="big" labels={["Off", "On"]} disabled />
          </div>
          <div>
            <h5 className="u-font-bold">Small Disabled</h5>
            <Toggle size="small" labels={["Off", "On"]} disabled />
          </div>
        </div>
        <CodeBlock className="u-mt-3">
          <div>{'import { Toggle } from "@flip-ui";'}</div>
          <br />
          <div>{'<Toggle size="..." labels={["...", "..."]} checked={...} onChange={...} />'}</div>
        </CodeBlock>
      </Box>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Toogle" withOtherInfo>
        <PropsTable.Prop name="checked" type="boolean" desc="Set toggle to be checked" def="false" />
        <PropsTable.Prop name="disabled" type="boolean" desc="Set toggle to be disabled" def="false" />
        <PropsTable.Prop name="labels" type="array" desc="Set toggle labels (array of strings of labels)" def="[]" />
        <PropsTable.Prop name="size" type="string" desc="Set toggle size" def="big" />
      </PropsTable>
    </Container>
  );
}

export default ToggleComp;
