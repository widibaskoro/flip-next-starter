import React from "react";

import { Copy } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function CopyComp() {
  return (
    <Container>
      <h1 className="u-font-bold">Copy</h1>

      <Box>
        <div className="u-flex u-w-6/12 u-justify-between">
          <div>
            <h4 className="u-font-bold">Default (outline)</h4>
            <Copy textCopy="ABCDE" />
          </div>
          <div>
            <h4 className="u-font-bold">Default (fill)</h4>
            <Copy textCopy="ABCDE" iconType="fill" />
          </div>
        </div>
      </Box>

      <Box>
        <div className="u-flex u-w-6/12 u-justify-between">
          <div>
            <h4 className="u-font-bold">Icon Only (outline)</h4>
            <Copy textCopy="ABCDE" iconOnly />
          </div>
          <div>
            <h4 className="u-font-bold">Icon Only (fill)</h4>
            <Copy textCopy="ABCDE" iconType="fill" iconOnly />
          </div>
        </div>
      </Box>

      <CodeBlock className="u-mt-7">
        <div>{'import { Copy } from "@flip-ui";'}</div>
        <br />
        <div>{'<Copy textCopy="..." iconOnly={...} iconType="..." />'}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Copy">
        <PropsTable.Prop name="textCopy" type="string" desc="Set text to be copied" def="'' (empty string)" />
        <PropsTable.Prop name="iconOnly" type="boolean" desc="Set button to show only the icon" def="false" />
        <PropsTable.Prop
          name="iconType"
          type="string"
          desc="Override default icon type ['outline', 'fill']"
          def="'outline'"
        />
        <PropsTable.Prop name="tooltipPosition" type="string" desc="Override default tooltip position" def="'top'" />
      </PropsTable>
    </Container>
  );
}

export default CopyComp;
