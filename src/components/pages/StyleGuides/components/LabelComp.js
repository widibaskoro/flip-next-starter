import React from "react";

import { Alert, Label } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function LabelComp() {
  return (
    <Container>
      <h1 className="u-font-bold">Label</h1>

      <Box>
        <table className="u-w-5/12">
          <tbody>
            <tr>
              <td className="u-w-6/12">
                <h4 className="u-font-bold">Default</h4>
              </td>
              <td className="u-w-6/12">
                <h4 className="u-font-bold">Large</h4>
              </td>
            </tr>

            <tr>
              <td className="u-p-3">
                <Label>ORANGE</Label>
              </td>
              <td className="u-p-3">
                <Label size="large">Orange</Label>
              </td>
            </tr>

            <tr>
              <td className="u-p-3">
                <Label color="blue">BLUE</Label>
              </td>
              <td className="u-p-3">
                <Label color="blue" size="large">
                  Blue
                </Label>
              </td>
            </tr>

            <tr>
              <td className="u-p-3">
                <Label color="green">GREEN</Label>
              </td>
              <td className="u-p-3">
                <Label color="green" size="large">
                  Green
                </Label>
              </td>
            </tr>

            <tr>
              <td className="u-p-3">
                <Label color="grey">GREY</Label>
              </td>
              <td className="u-p-3">
                <Label color="grey" size="large">
                  Grey
                </Label>
              </td>
            </tr>

            <tr>
              <td className="u-p-3">
                <Label color="red">RED</Label>
              </td>
              <td className="u-p-3">
                <Label color="red" size="large">
                  Red
                </Label>
              </td>
            </tr>

            <tr>
              <td className="u-p-3">
                <Label color="yellow">YELLOW</Label>
              </td>
              <td className="u-p-3">
                <Label color="yellow" size="large">
                  Yellow
                </Label>
              </td>
            </tr>

            <tr>
              <td className="u-p-3">
                <Label color="dark-yellow">DARK YELLOW</Label>
              </td>
              <td className="u-p-3">
                <Label color="dark-yellow" size="large">
                  Dark Yellow
                </Label>
              </td>
            </tr>
          </tbody>
        </table>
      </Box>

      <Alert color="blue" className="u-mt-7 u-w-7/12">
        Q: What if I want to alter the style? E.g font weight normal, display block, etc.
        <br />
        A: Please use style utilities instead.
      </Alert>

      <CodeBlock className="u-mt-5">
        <div>{'import { Label } from "@flip-ui";'}</div>
        <br />
        <div>{'<Label color="..." size="...">'}</div>
        <div>&nbsp;&nbsp;Label Text</div>
        <div>{"</Label>"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Label">
        <PropsTable.Prop
          name="color"
          type="string"
          desc="Set alert color ['orange', 'blue', 'green', 'grey', 'red', 'yellow', 'dark-yellow']"
          def="'orange'"
        />
        <PropsTable.Prop name="size" type="string" desc="Set alert size ['default', 'large']" def="'default'" />
      </PropsTable>
    </Container>
  );
}

export default LabelComp;
