import React from "react";

import { Container } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import ButtonGenerator from "../utilities/ButtonGenerator";

function ButtonComp() {
  return (
    <Container>
      <h1 className="u-font-bold">Buttons</h1>

      <ButtonGenerator className="u-mb-20" variant="primary" />
      <hr />
      <ButtonGenerator className="u-mb-20" variant="secondary" />
      <hr />
      <ButtonGenerator className="u-mb-20" variant="tertiary" noOutline noIconOnly />
      <hr />
      <ButtonGenerator variant="mini" noOutline noBlock />

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Button" withOtherInfo>
        <PropsTable.Prop
          name="activated"
          type="boolean"
          desc="Set button spinner to be loading state with spinner"
          def="false"
        />
        <PropsTable.Prop name="block" type="boolean" desc="Set button full width and block" def="false" />
        <PropsTable.Prop
          name="color"
          type="string"
          desc="Set button color ['orange', 'green', 'red', 'grey']"
          def="'orange'"
        />
        <PropsTable.Prop name="icon" type="object" desc="Icon object from Font Awesome" def="null" />
        <PropsTable.Prop name="outline" type="boolean" desc="Set if button is outline mode" def="false" />
        <PropsTable.Prop name="type" type="string" desc="Set button type" def="'button'" />
        <PropsTable.Prop
          name="variant"
          type="string"
          desc="Set button vaariant ['primary', 'secondary', 'tertiary', 'mini]"
          def="'primary'"
        />
      </PropsTable>
    </Container>
  );
}

export default ButtonComp;
