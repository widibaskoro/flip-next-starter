import React, { useState } from "react";

import { Alert, Col, DatePicker, FieldLabel, Form, Row } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function DatePickerComp() {
  const [dateValue, setDateValue] = useState("");
  const [dateValue2, setDateValue2] = useState("");

  return (
    <Container>
      <h1 className="u-font-bold">DatePicker</h1>

      <Box>
        <Row>
          <Col sm={4}>
            <h4 className="u-font-bold">Default</h4>
            <Form.Group>
              <FieldLabel>Normal datepicker</FieldLabel>
              <DatePicker selectedDate={dateValue} onChangeDate={setDateValue} />
            </Form.Group>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Invalid</h4>
            <Form.Group>
              <FieldLabel>Invalid datepicker</FieldLabel>
              <DatePicker selectedDate={dateValue2} onChangeDate={setDateValue2} isInvalid />
              <DatePicker.Feedback>Invalid message</DatePicker.Feedback>
            </Form.Group>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Disabled</h4>
            <Form.Group>
              <FieldLabel>Disabled datepicker</FieldLabel>
              <DatePicker selectedDate={dateValue} onChangeDate={setDateValue} disabled />
            </Form.Group>
          </Col>
        </Row>
      </Box>

      <Alert color="blue" className="u-mt-2 u-w-6/12">
        Looking for Date Picker with ranged value? Go to <a href="#range-datepicker">RangeDatePicker</a>.
      </Alert>

      <CodeBlock className="u-mt-5">
        <div>{'import { DatePicker } from "@flip-ui";'}</div>
        <br />
        <div>{"<DatePicker"}</div>
        <div>&nbsp;&nbsp;{"selectedDate={...}"}</div>
        <div>&nbsp;&nbsp;{"onChangeDate={...}"}</div>
        <div>&nbsp;&nbsp;{"..."}</div>
        <div>{"/>"}</div>
        <div>{"<DatePicker.Feedback>Text</DatePicker.Feedback>"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <Alert color="grey" className="u-mt-1 u-w-9/12">
        A full documentation about what props can be used, please see{" "}
        <a href="https://reactdatepicker.com/" target="_blank" rel="noopener noreferrer">
          React Datepicker docs page
        </a>
        .
      </Alert>

      <PropsTable title="DatePicker">
        <PropsTable.Prop name="disabled" type="boolean" desc="Set datepicker disabled" def="false" />
        <PropsTable.Prop name="inputPlaceholder" type="string" desc="Set datepicker input placeholder" def="''" />
        <PropsTable.Prop name="isInvalid" type="boolean" desc="Set datepicker is invalid" def="false" />
        <PropsTable.Prop name="selectedDate" type="date" desc="Set date picker value" def="new Date()" />
        <PropsTable.Prop
          name="onChangeDate"
          type="function"
          desc="Callback after date has been selected (returning selected date)"
          def="() => {}"
        />
        <PropsTable.Prop
          name="position"
          type="string"
          desc="Set datepicker position ['left', 'right', 'bottom']"
          def="'left'"
        />
      </PropsTable>

      <PropsTable title="DatePicker.Feedback">
        <PropsTable.Prop name="children" type="any" desc="Set feedback text" def="''" />
      </PropsTable>
    </Container>
  );
}

export default DatePickerComp;
