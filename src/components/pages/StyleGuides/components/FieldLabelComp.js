import React from "react";

import { Col, FieldLabel, Row } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function FieldLabelComp() {
  return (
    <Container>
      <h1 className="u-font-bold">FieldLabel</h1>

      <Box>
        <Row>
          <Col sm={4}>
            <h4 className="u-font-bold">Normal</h4>
            <FieldLabel>Normal field label</FieldLabel>

            <CodeBlock className="u-mt-5">
              <div>{'import { FieldLabel } from "@flip-ui";'}</div>
              <br />
              <div>{"<FieldLabel>"}</div>
              <div>&nbsp;&nbsp;Normal Label</div>
              <div>{"</FieldLabel>"}</div>
            </CodeBlock>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Required</h4>
            <FieldLabel required>Required field label</FieldLabel>

            <CodeBlock className="u-mt-5">
              <div>{'import { FieldLabel } from "@flip-ui";'}</div>
              <br />
              <div>{"<FieldLabel required>"}</div>
              <div>&nbsp;&nbsp;Required Label</div>
              <div>{"</FieldLabel>"}</div>
            </CodeBlock>
          </Col>
          <Col sm={4}>
            <h4 className="u-font-bold">Optional</h4>
            <FieldLabel optional>Optional field label</FieldLabel>

            <CodeBlock className="u-mt-5">
              <div>{'import { FieldLabel } from "@flip-ui";'}</div>
              <br />
              <div>{"<FieldLabel optional>"}</div>
              <div>&nbsp;&nbsp;Optional Label</div>
              <div>{"</FieldLabel>"}</div>
            </CodeBlock>
          </Col>
        </Row>
      </Box>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="FieldLabel">
        <PropsTable.Prop name="required" type="boolean" desc="Set label with required mark" def="false" />
        <PropsTable.Prop name="optional" type="boolean" desc="Set label with optional mark" def="false" />
      </PropsTable>
    </Container>
  );
}

export default FieldLabelComp;
