import React, { useState } from "react";

import { Accordion, Alert, Button, Col, FileInput, Row } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

import iconIdCard from "src/assets/images/verification/icon-id-card-photo.png";

function AccordionComp() {
  const [, setUploadedFile] = useState(null);
  const [isUploading, setIsUploading] = useState(false);

  function handleOnChange(file) {
    setUploadedFile(file);
    setIsUploading(true);
    setTimeout(() => setIsUploading(false), 5000);
  }

  return (
    <Container>
      <h1 className="u-font-bold">Accordion</h1>

      <Box>
        <Row>
          <Col sm={6}>
            <h4 className="u-font-bold">Default</h4>
            <Accordion title="Ingin uji coba sistem H2H menggunakan API?">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
              ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
              fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
              mollit anim id est laborum.
            </Accordion>

            <CodeBlock className="u-mt-5">
              <div>{'import { Accordion } from "@flip-ui";'}</div>
              <br />
              <div>{'<Accordion title="...">Content</Accordion>'}</div>
            </CodeBlock>
          </Col>

          <Col sm={6}>
            <h4 className="u-font-bold">With Helper Image & Helper Text</h4>
            <Accordion
              title="Unggah Foto Wajah"
              helperText="Foto wajah Anda harus jelas, lengkap/tidak terpotong, dan horizontal."
              helperImageSrc={iconIdCard}
            >
              <FileInput
                placeholder="Taruh file PDF, JPEG, atau PNG di sini"
                onChange={handleOnChange}
                isUploading={isUploading}
                fileTypes=".pdf, .png, .jpg, .jpeg"
                maxFileSize={2097152}
              />
              <Alert color="yellow" emoticon="📌" className="u-mt-6">
                Foto kartu identitas <b>harus jelas, lengkap/tidak terpotong, dan horizontal.</b> <br />
                Apabila KTP belum selesai dibuat di Kecamatan, silakan foto sambil memegang surat keterangan yang masih
                berlaku
              </Alert>
              <div className="u-mt-6 u-flex u-justify-between u-items-center">
                <Button variant="tertiary" color="grey">
                  Lihat Contoh
                </Button>
                <Button>Submit Foto</Button>
              </div>
            </Accordion>

            <CodeBlock className="u-mt-5">
              <div>{'import { Accordion } from "@flip-ui";'}</div>
              <br />
              <div>{'<Accordion title="..." helperText="..." helperImageSrc={...}>Content</Accordion>'}</div>
            </CodeBlock>
          </Col>
        </Row>
      </Box>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Accordion">
        <PropsTable.Prop name="title" type="string" desc="Set accordion title" def="''" />
        <PropsTable.Prop name="helperText" type="string" desc="Set helper text bellow the title" def="''" />
        <PropsTable.Prop name="helperImageSrc" type="string" desc="Set helper image on the left of title" def="''" />
      </PropsTable>
    </Container>
  );
}

export default AccordionComp;
