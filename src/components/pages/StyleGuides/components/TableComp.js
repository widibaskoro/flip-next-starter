import React from "react";

import { Table } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

const heads = ["#", "Id Transaksi", "Nama Penerima", "Bank Penerima", "Nomor Rekening", "Waktu Dibuat"];
const contents = [
  ["1", "12755093", "Bambang Kurniawan", "BNI/BNI Syariah", "127383928372321", "08 Jul 2020 17:01:11"],
  ["2", "12755061", "Kunto Coroko", "Bank Syariah Mandiri", "31329892444", "08 Jul 2020 17:01:11"],
  ["3", "12755738", "Asmiranda Budiman", "BCA", "888372823", "08 Jul 2020 17:01:11"]
];

function TableComp() {
  return (
    <Container>
      <h1 className="u-font-bold">Table</h1>

      <Box>
        <h4 className="u-font-bold">Default</h4>
        <Table>
          <thead>
            <tr>
              {heads.map((item, i) => (
                <th key={i}>{item}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {contents.map((item, i) => (
              <tr key={i}>
                {item.map((col, j) => (
                  <td key={j}>{col}</td>
                ))}
              </tr>
            ))}
          </tbody>
        </Table>
        <CodeBlock className="u-mt-4">
          <div>{'import { Table } from "@flip-ui";'}</div>
          <br />
          <div>{"<Table>"}</div>
          <div>&nbsp;&nbsp;{"<thead>"}</div>
          <div>&nbsp;&nbsp;&nbsp;&nbsp;{"<tr>...</tr>"}</div>
          <div>&nbsp;&nbsp;{"</thead>"}</div>
          <div>&nbsp;&nbsp;{"<tbody>"}</div>
          <div>&nbsp;&nbsp;&nbsp;&nbsp;{"<tr>...</tr>"}</div>
          <div>&nbsp;&nbsp;{"</tbody>"}</div>
          <div>{"</Table>"}</div>
        </CodeBlock>

        <h4 className="u-font-bold u-mt-6">Zebra</h4>
        <Table zebra>
          <thead>
            <tr>
              {heads.map((item, i) => (
                <th key={i}>{item}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {contents.map((item, i) => (
              <tr key={i}>
                {item.map((col, j) => (
                  <td key={j}>{col}</td>
                ))}
              </tr>
            ))}
          </tbody>
        </Table>
        <CodeBlock className="u-mt-4">
          <div>{'import { Table } from "@flip-ui";'}</div>
          <br />
          <div>{"<Table zebra>"}</div>
          <div>&nbsp;&nbsp;{"<thead>"}</div>
          <div>&nbsp;&nbsp;&nbsp;&nbsp;{"<tr>...</tr>"}</div>
          <div>&nbsp;&nbsp;{"</thead>"}</div>
          <div>&nbsp;&nbsp;{"<tbody>"}</div>
          <div>&nbsp;&nbsp;&nbsp;&nbsp;{"<tr>...</tr>"}</div>
          <div>&nbsp;&nbsp;{"</tbody>"}</div>
          <div>{"</Table>"}</div>
        </CodeBlock>
      </Box>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Table" withOtherInfo>
        <PropsTable.Prop name="zebra" type="boolean" desc="Set rows to be having striped style" def="false" />
      </PropsTable>
    </Container>
  );
}

export default TableComp;
