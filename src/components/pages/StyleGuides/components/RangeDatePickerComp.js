import React, { useState } from "react";

import { Alert, Col, RangeDatePicker, FieldLabel, Form, Row } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

import { subDays } from "date-fns";

const RangeDatePickerComp = () => {
  const today = new Date();
  const prevWeek = subDays(today, 7);

  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [startDate2, setStartDate2] = useState(prevWeek);
  const [endDate2, setEndDate2] = useState(today);

  const handleChangeDate = (startDateVal, endDateVal, actionType) => {
    setStartDate(startDateVal);
    setEndDate(endDateVal);
    console.log(actionType);
  };

  const handleChangeDate2 = (startDateVal, endDateVal, actionType) => {
    setStartDate2(startDateVal);
    setEndDate2(endDateVal);
    console.log(actionType);
  };

  return (
    <Container>
      <h1 className="u-font-bold">RangeDatePicker</h1>

      <Box>
        <Row>
          <Col sm={4}>
            <h4 className="u-font-bold">Default</h4>
            <Form.Group>
              <FieldLabel>Normal datepicker</FieldLabel>
              <RangeDatePicker startDateValue={startDate} endDateValue={endDate} onChangeDate={handleChangeDate} />
            </Form.Group>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Invalid</h4>
            <Form.Group>
              <FieldLabel>Invalid datepicker</FieldLabel>
              <RangeDatePicker
                startDateValue={startDate2}
                endDateValue={endDate2}
                startDateReset={prevWeek}
                endDateReset={today}
                onChangeDate={handleChangeDate2}
                isInvalid
              />
              <RangeDatePicker.Feedback>Invalid message</RangeDatePicker.Feedback>
            </Form.Group>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Disabled</h4>
            <Form.Group>
              <FieldLabel>Disabled datepicker</FieldLabel>
              <RangeDatePicker
                startDateValue={startDate}
                endDateValue={endDate}
                startDateReset={prevWeek}
                endDateReset={today}
                onChangeDate={handleChangeDate}
                disabled
              />
            </Form.Group>
          </Col>
        </Row>
      </Box>

      <CodeBlock className="u-mt-5">
        <div>{'import { RangeDatePicker } from "@flip-ui";'}</div>
        <br />
        <div>{"<RangeDatePicker"}</div>
        <div>&nbsp;&nbsp;{"startDateValue={...}"}</div>
        <div>&nbsp;&nbsp;{"endDateValue={...}"}</div>
        <div>&nbsp;&nbsp;{"startDateReset={...}"}</div>
        <div>&nbsp;&nbsp;{"endDateReset={...}"}</div>
        <div>&nbsp;&nbsp;{"onChangeDate={...}"}</div>
        <div>&nbsp;&nbsp;{"..."}</div>
        <div>{"/>"}</div>
        <div>{"<RangeDatePicker.Feedback>Text</RangeDatePicker.Feedback>"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <Alert color="grey" className="u-mt-1 u-w-9/12">
        <span>Notes:</span>
        <br />
        <ul>
          <li>
            Please make sure that <code>startDateValue</code> and <code>startDateReset</code> are not the same variable
            (or state)
          </li>
          <li>
            Please make sure that <code>endDateValue</code> and <code>endDateReset</code> are not the same variable (or
            state)
          </li>
          <li>
            APPLY button: apply selected range and trigger <code>onChangeDate</code> with actionType = "apply"
          </li>
          <li>
            RESET button: reset selected range to (by default): today - 7 days until today, and trigger{" "}
            <code>onChangeDate</code> with actionType = "reset"
          </li>
        </ul>
      </Alert>

      <PropsTable title="RangeDatePicker">
        <PropsTable.Prop name="startDateValue" type="date" desc="Set date picker start date value" required />
        <PropsTable.Prop name="endDateValue" type="date" desc="Set date picker end date value" required />
        <PropsTable.Prop
          name="startDateReset"
          type="date"
          desc="Set date picker to specific start date when reset"
          required
        />
        <PropsTable.Prop
          name="endDateReset"
          type="date"
          desc="Set date picker to specific end date when reset"
          required
        />
        <PropsTable.Prop
          name="onChangeDate"
          type="function"
          desc={
            <>
              Handler when date picker has been applied or reset
              <br />
              <span>this method returns three value</span>
              <br />
              <ol>
                <li>start date value (Date type)</li>
                <li>end date value (Date type)</li>
                <li>action type ("apply" or "reset")</li>
              </ol>
            </>
          }
          required
        />
        <PropsTable.Prop
          name="inputPlaceholder"
          type="string"
          desc="Set datepicker input placeholder"
          def="undefined"
        />
        <PropsTable.Prop name="disabled" type="boolean" desc="Set datepicker id disabled" def="false" />
        <PropsTable.Prop name="isInvalid" type="boolean" desc="Set datepicker is invalid" def="false" />
        <PropsTable.Prop
          name="datePickerPosition"
          type="string"
          desc="Set Datepicker to be aligned to the left or right of the input"
          def="'left'"
        />
      </PropsTable>

      <PropsTable title="RangeDatePicker.Feedback">
        <PropsTable.Prop name="children" type="any" desc="Set feedback text" def="''" />
      </PropsTable>
    </Container>
  );
};

export default RangeDatePickerComp;
