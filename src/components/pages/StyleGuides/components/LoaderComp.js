import React from "react";

import { Loader } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

const LoaderComp = () => {
  return (
    <Container>
      <h1 className="u-font-bold">Loader</h1>

      <Box>
        <table className="u-w-5/12 u-text-center">
          <tbody>
            <tr>
              <td className="u-w-6/12">
                <h4 className="u-font-bold">Default</h4>
              </td>
              <td className="u-w-6/12">
                <h4 className="u-font-bold">Small</h4>
              </td>
            </tr>

            <tr>
              <td>
                <Loader />
              </td>
              <td>
                <Loader size="small" />
              </td>
            </tr>
            <tr>
              <td>
                <Loader color="blue" />
              </td>
              <td>
                <Loader color="blue" size="small" />
              </td>
            </tr>
            <tr>
              <td>
                <Loader color="green" />
              </td>
              <td>
                <Loader color="green" size="small" />
              </td>
            </tr>
            <tr>
              <td>
                <Loader color="grey" />
              </td>
              <td>
                <Loader color="grey" size="small" />
              </td>
            </tr>
            <tr>
              <td>
                <Loader color="red" />
              </td>
              <td>
                <Loader color="red" size="small" />
              </td>
            </tr>
            <tr>
              <td>
                <Loader color="yellow" />
              </td>
              <td>
                <Loader color="yellow" size="small" />
              </td>
            </tr>
            <tr>
              <td>
                <Loader color="black" />
              </td>
              <td>
                <Loader color="black" size="small" />
              </td>
            </tr>
            <tr>
              <td className=" u-bg-black-bekko u-rounded-lg">
                <Loader color="white" />
              </td>
              <td className=" u-bg-black-bekko u-rounded-lg">
                <Loader color="white" size="small" />
              </td>
            </tr>
          </tbody>
        </table>
      </Box>

      <CodeBlock className="u-mt-5">
        <div>{'import { Loader } from "@flip-ui";'}</div>
        <br />
        <div>{'<Loader color="..." size="..." />'}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Loader">
        <PropsTable.Prop
          name="color"
          type="string"
          desc="Set Loader color ['orange', 'blue', 'green', 'grey', 'red', 'yellow', 'white', 'black']"
          def="'orange'"
        />
        <PropsTable.Prop name="size" type="string" desc="Set Loader size ['default', 'small']" def="'default'" />
      </PropsTable>
    </Container>
  );
};

export default LoaderComp;
