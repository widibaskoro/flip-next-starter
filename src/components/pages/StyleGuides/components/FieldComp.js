import React, { useState } from "react";

import { Field, Col, Row } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function FieldComp() {
  const [text, setText] = useState("A value");
  const [text2, setText2] = useState("A value");

  return (
    <Container>
      <h1 className="u-font-bold">Field</h1>

      <Box>
        <Row>
          <Col sm={4}>
            <h4 className="u-font-bold">Default</h4>
            <Field placeholder="A placeholder" />
            <br />
            <Field placeholder="A placeholder" value={text} onChange={(e) => setText(e.target.value)} />
          </Col>
          <Col sm={4}>
            <h4 className="u-font-bold">Disabled</h4>
            <Field placeholder="A placeholder" disabled />
            <br />
            <Field placeholder="A placeholder" value="A value, but disabled" disabled />
          </Col>
          <Col sm={4}>
            <h4 className="u-font-bold">Invalid (Error)</h4>
            <Field placeholder="A placeholder" isInvalid />
            <Field.Feedback>A feedback</Field.Feedback>
            <br />
            <Field placeholder="A placeholder" value={text2} onChange={(e) => setText2(e.target.value)} isInvalid />
            <Field.Feedback>A feedback</Field.Feedback>
          </Col>
        </Row>

        <Row>
          <Col sm={4}>
            <h4 className="u-font-bold">Textarea</h4>
            <p className="u-mb-1">
              with props <code>as="textarea"</code>
            </p>
            <Field as="textarea" rows="3" placeholder="A textarea" />
          </Col>
        </Row>
      </Box>

      <CodeBlock className="u-mt-10">
        <div>{'import { Field } from "@flip-ui";'}</div>
        <br />
        <div>{"<Field value={...} onChange={...} isInvalid={...} />"}</div>
        <div>{"<Field.Feedback>Text</Field.Feedback>"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Field" withOtherInfo>
        <PropsTable.Prop name="isInvalid" type="boolean" desc="Set input field is invalid" def="false" />
      </PropsTable>
    </Container>
  );
}

export default FieldComp;
