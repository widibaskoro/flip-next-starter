import React, { useState } from "react";

import { RadioButton } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

const RadioButtonComp = () => {
  const [radio, setRadio] = useState("val1");
  const [radio2, setRadio2] = useState("val1");
  const [radio3, setRadio3] = useState("val1");
  const [radio4, setRadio4] = useState("val1");

  return (
    <Container>
      <h1 className="u-font-bold">RadioButton</h1>

      <Box>
        <h3 className="u-font-bold">Primary</h3>
        <div className="u-flex u-w-10/12 u-justify-between">
          <div>
            <h5 className="u-font-bold">Checked</h5>
            <RadioButton
              name="radio"
              value="val1"
              checked={radio === "val1"}
              onChange={(event) => setRadio(event.target.value)}
            >
              Checked
            </RadioButton>
          </div>
          <div>
            <h5 className="u-font-bold">Unchecked</h5>
            <RadioButton
              name="radio"
              value="val2"
              checked={radio === "val2"}
              onChange={(event) => setRadio(event.target.value)}
            >
              Unchecked
            </RadioButton>
          </div>
          <div>
            <h5 className="u-font-bold">Disabled</h5>
            <RadioButton
              name="radio"
              value="val3"
              checked={radio === "val3"}
              onChange={(event) => setRadio(event.target.value)}
              disabled
            >
              Disabled
            </RadioButton>
          </div>
          <div>
            <h5 className="u-font-bold">Icon Only</h5>
            <RadioButton
              name="radio"
              value="val4"
              checked={radio === "val4"}
              onChange={(event) => setRadio(event.target.value)}
            />
          </div>
        </div>
        <CodeBlock className="u-mt-3">
          <div>{'import { RadioButton } from "@flip-ui";'}</div>
          <br />
          <div>{'<RadioButton variant="primary" name="..." checked={...} onChange={...}>text</RadioButton>'}</div>
        </CodeBlock>

        <h3 className="u-font-bold u-mt-8">Secondary</h3>
        <div className="u-flex u-w-10/12 u-justify-between">
          <div>
            <h5 className="u-font-bold">Checked</h5>
            <RadioButton
              name="radio2"
              value="val1"
              variant="secondary"
              checked={radio2 === "val1"}
              onChange={(event) => setRadio2(event.target.value)}
            >
              Checked
            </RadioButton>
          </div>
          <div>
            <h5 className="u-font-bold">Unchecked</h5>
            <RadioButton
              name="radio2"
              value="val2"
              variant="secondary"
              checked={radio2 === "val2"}
              onChange={(event) => setRadio2(event.target.value)}
            >
              Unchecked
            </RadioButton>
          </div>
          <div>
            <h5 className="u-font-bold">Disabled</h5>
            <RadioButton
              name="radio2"
              value="val3"
              variant="secondary"
              checked={radio2 === "val3"}
              onChange={(event) => setRadio2(event.target.value)}
              disabled
            >
              Disabled
            </RadioButton>
          </div>
          <div>
            <h5 className="u-font-bold">Icon Only</h5>
            <RadioButton
              name="radio2"
              value="val4"
              variant="secondary"
              checked={radio2 === "val4"}
              onChange={(event) => setRadio2(event.target.value)}
            />
          </div>
        </div>
        <CodeBlock className="u-mt-3">
          <div>{'import { RadioButton } from "@flip-ui";'}</div>
          <br />
          <div>{'<RadioButton variant="secondary" name="..." checked={...} onChange={...}>text</RadioButton>'}</div>
        </CodeBlock>

        <h3 className="u-font-bold u-mt-8">Tertiary</h3>
        <div className="u-flex u-w-7/12 u-justify-between">
          <div>
            <h5 className="u-font-bold">Checked</h5>
            <RadioButton
              name="radio3"
              value="val1"
              variant="tertiary"
              checked={radio3 === "val1"}
              onChange={(event) => setRadio3(event.target.value)}
            >
              Checked
            </RadioButton>
          </div>
          <div>
            <h5 className="u-font-bold">Unchecked</h5>
            <RadioButton
              name="radio3"
              value="val2"
              variant="tertiary"
              checked={radio3 === "val2"}
              onChange={(event) => setRadio3(event.target.value)}
            >
              Unchecked
            </RadioButton>
          </div>
          <div>
            <h5 className="u-font-bold">Disabled</h5>
            <RadioButton
              name="radio3"
              value="val3"
              variant="tertiary"
              checked={radio3 === "val3"}
              onChange={(event) => setRadio3(event.target.value)}
              disabled
            >
              Disabled
            </RadioButton>
          </div>
        </div>
        <div className="u-flex u-w-7/12 u-justify-between u-mt-4">
          <div>
            <h5 className="u-font-bold">Checked</h5>
            <RadioButton
              name="radio4"
              value="val1"
              variant="tertiary"
              checked={radio4 === "val1"}
              onChange={(event) => setRadio4(event.target.value)}
            >
              <span className="c-bank-logo c-bank-logo--bca c-bank-logo--small" />
            </RadioButton>
          </div>
          <div>
            <h5 className="u-font-bold">Unchecked</h5>
            <RadioButton
              name="radio4"
              value="val2"
              variant="tertiary"
              checked={radio4 === "val2"}
              onChange={(event) => setRadio4(event.target.value)}
            >
              <span className="c-bank-logo c-bank-logo--bca c-bank-logo--small" />
            </RadioButton>
          </div>
          <div>
            <h5 className="u-font-bold">Disabled</h5>
            <RadioButton
              name="radio4"
              value="val3"
              variant="tertiary"
              checked={radio4 === "val3"}
              onChange={(event) => setRadio4(event.target.value)}
              disabled
            >
              <span className="c-bank-logo c-bank-logo--bca c-bank-logo--small" />
            </RadioButton>
          </div>
        </div>
        <CodeBlock className="u-mt-3">
          <div>{'import { RadioButton } from "@flip-ui";'}</div>
          <br />
          <div>{'<RadioButton variant="tertiary" name="..." checked={...} onChange={...}>text</RadioButton>'}</div>
        </CodeBlock>
      </Box>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="RadioButton" withOtherInfo>
        <PropsTable.Prop name="checked" type="boolean" desc="Set radio button to be checked" def="false" />
        <PropsTable.Prop name="disabled" type="boolean" desc="Set radio button to be disabled" def="false" />
        <PropsTable.Prop
          name="variant"
          type="string"
          desc="Set radio button variant ['primary', 'secondary', 'tertiary']"
          def="'primary'"
        />
      </PropsTable>
    </Container>
  );
};

export default RadioButtonComp;
