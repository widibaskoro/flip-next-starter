import React from "react";

import { Icon } from "@flip-ui";

import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function IconComp() {
  return (
    <Container>
      <h1 className="u-font-bold">Icon</h1>

      <Box>
        <div className="u-flex u-justify-between u-w-6/12">
          <div>
            <h4 className="u-font-bold">Default</h4>
            <Icon icon={faCheckCircle} />
          </div>
          <div>
            <h4 className="u-font-bold">Color</h4>
            <p>To change icon color just use color style utils</p>
            <Icon icon={faCheckCircle} className="u-text-jade" />
          </div>
        </div>
      </Box>

      <CodeBlock className="u-mt-3">
        <div>{'import { Icon } from "@flip-ui";'}</div>
        <br />
        <div>{"<Icon icon={...} />"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Icon">
        <PropsTable.Prop
          name="icon"
          type="object"
          desc={
            <>
              Set the icon
              <br />
              See this link for icon usage:
              <br />
              <a href="https://github.com/FortAwesome/react-fontawesome">
                https://github.com/FortAwesome/react-fontawesome
              </a>
            </>
          }
          required
        />
      </PropsTable>
    </Container>
  );
}

export default IconComp;
