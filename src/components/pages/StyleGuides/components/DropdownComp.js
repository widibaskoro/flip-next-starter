import React, { useState } from "react";
import styled from "styled-components";

import { Alert, Col, Dropdown, Row } from "@flip-ui";

import ReactCountryFlag from "react-country-flag";
import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

import { SUPPORTED_BANKS } from "src/consts/banks";

const BankLogo = styled.span`
  margin-right: 8px;
`;

const LanguageItem = styled.div`
  display: inline-flex;
  align-items: center;

  .c-language__flag {
    margin-top: 1px;
    margin-right: 8px;
  }
`;

const statusOptions = [
  {
    label: "Semua",
    value: "0"
  },
  {
    label: "Berhasil",
    value: "1"
  },
  {
    label: "Sedang diproses",
    value: "2"
  },
  {
    label: "Menunggu diproses",
    value: "3"
  },
  {
    label: "Butuh Konfirmasi",
    value: "4"
  }
];

const banksOptions = Object.values(SUPPORTED_BANKS).map((bank) => {
  return {
    label: bank.name,
    bankLabel: (
      <div className="u-inline-flex">
        <div>
          <BankLogo className={`c-bank-logo c-bank-logo--tiny c-bank-logo--${bank.key}`} />
        </div>
        <span>{bank.name}</span>
      </div>
    ),
    value: bank.key
  };
});

const languages = [
  { code: "ID", country: "Indonesia", value: "id" },
  { code: "GB", country: "English", value: "en" },
  { code: "DE", country: "German", value: "de" },
  { code: "ES", country: "Spanish", value: "es" },
  { code: "FR", country: "French", value: "fe" },
  { code: "JP", country: "Japanese", value: "ja" }
];

const languageOptions = languages.map((lang) => {
  return {
    label: (
      <LanguageItem>
        <ReactCountryFlag className="c-language__flag" code={lang.code} svg />
        <span> {lang.country}</span>
      </LanguageItem>
    ),
    value: lang.value
  };
});

const languageOptionsWithCustomProp = languages.map((lang) => {
  return {
    country: lang.country,
    label: (
      <LanguageItem>
        <ReactCountryFlag className="c-language__flag" code={lang.code} svg />
        <span> {lang.country}</span>
      </LanguageItem>
    ),
    value: lang.value
  };
});

function DropdownComp() {
  const [selectedSingle, setSelectedSingle] = useState(statusOptions[0]);
  const [selectedSingleInvalid, setSelectedSingleInvalid] = useState(statusOptions[0]);
  const [selectedSingleFull, setSelectedSingleFull] = useState(statusOptions[0]);
  const [selectedSingleCustom, setSelectedSingleCustom] = useState({});
  const [selectedSingleLabel, setSelectedSingleLabel] = useState(languageOptions[0]);
  const [selectedSingleRight, setSelectedSingleRight] = useState(statusOptions[0]);

  const [selectedSearch, setSelectedSearch] = useState(statusOptions[0]);
  const [selectedSearchCustom, setSelectedSearchCustom] = useState(languageOptionsWithCustomProp[0]);
  const [selectedSearchCustomFilter, setSelectedSearchCustomFilter] = useState(statusOptions[0]);

  const [selectedMulti, setSelectedMulti] = useState([...statusOptions]);
  const [selectedMultiFull, setSelectedMultiFull] = useState([...statusOptions]);
  const [selectedMultiInvalid, setSelectedMultiInvalid] = useState([]);
  const [selectedMultiCustom, setSelectedMultiCustom] = useState([]);
  const [selectedMultiLabel, setSelectedMultiLabel] = useState([]);
  const [selectedMultiRight, setSelectedMultiRight] = useState([]);
  const [selectedMultiSearch, setSelectedMultiSearch] = useState([]);
  const [selectedMultiImmediate, setSelectedMultiImmediate] = useState([]);
  const [selectedMultiInitialCount, setSelectedMultiInitialCount] = useState([]);

  function filterStatus(searchQuery, options) {
    return options.filter((item) => {
      const itemContent = item.label.toLowerCase();
      return itemContent.includes(searchQuery) || itemContent === "semua";
    });
  }

  return (
    <Container>
      <h1 className="u-font-bold">Dropdown</h1>

      {/**********************************************************/}
      {/******************** SINGLE SECTION **********************/}
      {/**********************************************************/}
      <Box>
        <h2 className="u-font-bold">Single</h2>

        <Row>
          <Col sm={4}>
            <h4 className="u-font-bold">Default</h4>
            <Dropdown
              options={statusOptions}
              selected={selectedSingle}
              placeholder="Pilih Status"
              onChange={(selected) => setSelectedSingle(selected)}
            />
            <div className="u-mt-1">Selected: {selectedSingle.label}</div>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Invalid</h4>
            <Dropdown
              options={statusOptions}
              selected={selectedSingleInvalid}
              placeholder="Pilih Status"
              onChange={(selected) => setSelectedSingleInvalid(selected)}
              isInvalid
            />
            <Dropdown.Feedback>Invalid message</Dropdown.Feedback>
            <div className="u-mt-1">Selected: {selectedSingleInvalid.label}</div>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Disabled</h4>
            <Dropdown options={statusOptions} placeholder="Pilih Status" disabled />
          </Col>
        </Row>

        <Row className="u-mt-8">
          <Col sm={4}>
            <h4 className="u-font-bold">Full Width</h4>
            <Dropdown
              options={statusOptions}
              selected={selectedSingleFull}
              placeholder="Pilih Status"
              onChange={(selected) => setSelectedSingleFull(selected)}
              fullWidth
            />
            <div className="u-mt-1">Selected: {selectedSingleFull.label}</div>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Menu Position Right</h4>
            <Dropdown
              options={statusOptions}
              selected={selectedSingleRight}
              placeholder="Pilih Status"
              menuPosition="right"
              onChange={(selected) => setSelectedSingleRight(selected)}
            />
            <div className="u-mt-1">Selected: {selectedSingleRight.label}</div>
          </Col>
        </Row>

        <Row className="u-mt-8">
          <Col sm={4}>
            <h4 className="u-font-bold">Custom Label</h4>
            <Dropdown
              options={banksOptions}
              selected={selectedSingleCustom}
              placeholder="Pilih Bank"
              itemLabel="bankLabel"
              onChange={(selected) => setSelectedSingleCustom(selected)}
            />
            <div className="u-mt-1">Selected: {selectedSingleCustom.label}</div>
            <Alert color="grey" className="u-mt-5">
              You can change the property that should be shown in the options (default: <code>label</code>). Use the{" "}
              <code>itemLabel</code> prop to change which property should be used as the label.
              <br />
              <br />
              For example: dropdown above use <code>bankLabel</code> as the custom label (
              <code>itemLabel="bankLabel"</code>)
            </Alert>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Label as Custom Element</h4>
            <Dropdown
              options={languageOptions}
              selected={selectedSingleLabel}
              placeholder="Pilih Bahasa"
              onChange={(selected) => setSelectedSingleLabel(selected)}
            />
            <div className="u-mt-1">Selected: {selectedSingleLabel.label}</div>
            <Alert color="grey" className="u-mt-5">
              It is possible for you to pass a Component in the <code>label</code> property instead just a string.
              <br />
              <br />
              For example: the <code>label</code> in dropdown above contains{" "}
              <code>{"<LanguageItem>...</LanguageItem>"}</code>
            </Alert>
          </Col>
        </Row>

        <Row className="u-mt-8">
          <Col sm={4}>
            <h4 className="u-font-bold">Searchable</h4>
            <Dropdown
              options={statusOptions}
              selected={selectedSearch}
              placeholder="Pilih Status"
              onChange={(selected) => setSelectedSearch(selected)}
              searchPlaceholder="Cari status"
              searchable
            />
            <div className="u-mt-1">Selected: {selectedSearch.label}</div>
            <Alert color="grey" className="u-mt-5">
              The filter function will use <code>label</code> to filter the options.
            </Alert>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Searchable: Specify Property</h4>
            <Dropdown
              options={languageOptionsWithCustomProp}
              selected={selectedSearchCustom}
              placeholder="Pilih Status"
              onChange={(selected) => setSelectedSearchCustom(selected)}
              searchPlaceholder="Cari status"
              searchLabel="country"
              searchable
            />
            <div className="u-mt-1">Selected: {selectedSearchCustom.label}</div>
            <Alert color="grey" className="u-mt-5">
              By default, the filter function will use <code>label</code> to filter the options. You can change it by
              passing which property should be used for the filter function via <code>searchLabel</code> prop.
              <br />
              <br />
              For example: dropdown above use <code>country</code> for filtering (<code>searchLabel="country"</code>)
            </Alert>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Searchable: Custom Filter Function</h4>
            <Dropdown
              options={statusOptions}
              selected={selectedSearchCustomFilter}
              placeholder="Pilih Status"
              onChange={(selected) => setSelectedSearchCustomFilter(selected)}
              searchPlaceholder="Cari status"
              searchFilter={filterStatus}
              searchable
            />
            <div className="u-mt-1">Selected: {selectedSearchCustomFilter.label}</div>
            <Alert color="grey" className="u-mt-5">
              By default, the filter function will do the filtering based on the value of <code>label</code>. If you
              need something different in your filtering function, you can create your own function and pass it via{" "}
              <code>searchFilter</code> prop. The <code>searchFilter</code> will pass 2 params for you (
              <code>searchQuery</code>, <code>options</code>) that can be used in the custom function.
              <br />
              <br />
              For example: dropdown above will always show the "Semua" options regardless the search query.
            </Alert>
          </Col>
        </Row>

        <Row className="u-mt-8">
          <Col sm={4}>
            <div className="u-bg-main-background u-rounded u-p-3">
              <h4 className="u-font-bold">Transparent</h4>
              <Dropdown
                options={statusOptions}
                selected={selectedSingle}
                placeholder="Pilih Status"
                onChange={(selected) => setSelectedSingle(selected)}
                transparent
              />
              <div className="u-mt-1">Selected: {selectedSingle.label}</div>
            </div>

            <Alert color="grey" className="u-mt-5">
              <code>transparent</code> vs <code>ghost</code>
              <br />
              <br />
              <code>transparent</code> changes the background to transparent and text color to dark smoke.{" "}
              <code>ghost</code> changes the background to transparent and text color (also border) to white. Both have
              bold text.
            </Alert>
          </Col>

          <Col sm={4}>
            <div className="u-bg-black-bekko u-rounded u-p-3">
              <h4 className="u-font-bold u-text-white">Ghost</h4>
              <Dropdown
                options={statusOptions}
                selected={selectedSingle}
                placeholder="Pilih Status"
                onChange={(selected) => setSelectedSingle(selected)}
                ghost
              />
              <div className="u-mt-1 u-text-white">Selected: {selectedSingle.label}</div>
            </div>
          </Col>
        </Row>

        <CodeBlock className="u-mt-7">
          <div>{'import { Dropdown } from "@flip-ui";'}</div>
          <br />
          <div>{"<Dropdown"}</div>
          <div>&nbsp;&nbsp;{'variat="single"'}</div>
          <div>&nbsp;&nbsp;{"options={...}"}</div>
          <div>&nbsp;&nbsp;{"selected={...}"}</div>
          <div>&nbsp;&nbsp;{"onChange={...}"}</div>
          <div>&nbsp;&nbsp;{"..."}</div>
          <div>{"/>"}</div>
          <div>{"<Dropdown.Feedback>Text</Dropdown.Feedback>"}</div>
        </CodeBlock>
      </Box>

      {/**********************************************************/}
      {/********************* MULTI SECTION **********************/}
      {/**********************************************************/}
      <Box className="u-mt-10">
        <h2 className="u-font-bold">Multi</h2>

        <Row>
          <Col sm={4}>
            <h4 className="u-font-bold">Default</h4>
            <Dropdown
              options={statusOptions}
              selected={selectedMulti}
              variant="multi"
              multiLabel="Status"
              placeholder="Pilih Status"
              onChange={(selectedItems) => setSelectedMulti(selectedItems)}
            />
            <div className="u-mt-1">Selected: {selectedMulti.map((item) => item.label).join(", ")}</div>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Invalid</h4>
            <Dropdown
              options={statusOptions}
              selected={selectedMultiInvalid}
              variant="multi"
              multiLabel="Status"
              placeholder="Pilih Status"
              onChange={(selectedItems) => setSelectedMultiInvalid(selectedItems)}
              isInvalid
            />
            <Dropdown.Feedback>Invalid message</Dropdown.Feedback>
            <div className="u-mt-1">Selected: {selectedMultiInvalid.map((item) => item.label).join(", ")}</div>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Disabled</h4>
            <Dropdown options={statusOptions} variant="multi" placeholder="Pilih Status" disabled />
          </Col>
        </Row>

        <Row className="u-mt-8">
          <Col sm={4}>
            <h4 className="u-font-bold">Full Width</h4>
            <Dropdown
              options={statusOptions}
              selected={selectedMultiFull}
              variant="multi"
              multiLabel="Status"
              placeholder="Pilih Status"
              onChange={(selectedItems) => setSelectedMultiFull(selectedItems)}
              fullWidth
            />
            <div className="u-mt-1">Selected: {selectedMultiFull.map((item) => item.label).join(", ")}</div>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Menu Position Right</h4>
            <Dropdown
              options={statusOptions}
              selected={selectedMultiRight}
              variant="multi"
              multiLabel="Status"
              placeholder="Pilih Status"
              menuPosition="right"
              onChange={(selectedItems) => setSelectedMultiRight(selectedItems)}
            />
            <div className="u-mt-1">Selected: {selectedMultiRight.map((item) => item.label).join(", ")}</div>
          </Col>
        </Row>

        <Row className="u-mt-8">
          <Col sm={4}>
            <h4 className="u-font-bold">Custom Label</h4>
            <Dropdown
              options={banksOptions}
              selected={selectedMultiCustom}
              variant="multi"
              multiLabel="Bank"
              itemLabel="bankLabel"
              placeholder="Pilih Bank"
              onChange={(selectedItems) => setSelectedMultiCustom(selectedItems)}
            />
            <div className="u-mt-1">Selected: {selectedMultiCustom.map((item) => item.label).join(", ")}</div>
            <Alert color="grey" className="u-mt-5">
              You can change the property that should be shown in the options (default: <code>label</code>). Use the{" "}
              <code>itemLabel</code> prop to change which property should be used as the label.
              <br />
              <br />
              For example: dropdown above use <code>bankLabel</code> as the custom label (
              <code>itemLabel="bankLabel"</code>)
            </Alert>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Label as Custom Element</h4>
            <Dropdown
              options={languageOptions}
              selected={selectedMultiLabel}
              variant="multi"
              placeholder="Pilih Bahasa"
              onChange={(selected) => setSelectedMultiLabel(selected)}
            />
            <div className="u-mt-1">Selected: {selectedMultiLabel.map((item) => item.id).join(", ")}</div>
            <Alert color="grey" className="u-mt-5">
              It is possible for you to pass a Component in the <code>label</code> property instead just a string.
              <br />
              <br />
              For example: the <code>label</code> in dropdown above contains{" "}
              <code>{"<LanguageItem>...</LanguageItem>"}</code>
            </Alert>
          </Col>
        </Row>

        <Row className="u-mt-8">
          <Col sm={4}>
            <h4 className="u-font-bold">Searchable</h4>
            <Dropdown
              options={banksOptions}
              selected={selectedMultiSearch}
              variant="multi"
              multiLabel="Bank"
              itemLabel="bankLabel"
              placeholder="Pilih Bank"
              onChange={(selectedItems) => setSelectedMultiSearch(selectedItems)}
              searchPlaceholder="Cari bank"
              searchable
            />
            <div className="u-mt-1">Selected: {selectedMultiSearch.map((item) => item.label).join(", ")}</div>
            <Alert color="grey" className="u-mt-5">
              All custom props for search is the same as single searchable dropdown.
            </Alert>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Immediate</h4>
            <Dropdown
              options={statusOptions}
              selected={selectedMultiImmediate}
              variant="multi"
              multiLabel="Status"
              placeholder="Pilih Status"
              immediate
              onChange={(selectedItems) => setSelectedMultiImmediate(selectedItems)}
            />
            <div className="u-mt-1">Selected: {selectedMultiImmediate.map((item) => item.label).join(", ")}</div>
            <Alert color="grey" className="u-mt-5">
              <b>Immediate</b> means that every change events in the options (check or uncheck) will be resulted in
              immediate callback action calling.
            </Alert>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Initial Count</h4>
            <Dropdown
              options={statusOptions}
              selected={selectedMultiInitialCount}
              variant="multi"
              multiLabel="Status"
              placeholder="Pilih Status"
              initialCount={3}
              onChange={(selectedItems) => setSelectedMultiInitialCount(selectedItems)}
            />
            <div className="u-mt-1">Selected: {selectedMultiInitialCount.map((item) => item.label).join(", ")}</div>

            <Alert color="grey" className="u-mt-5">
              By default if there is no option checked, the initial count of the multi dropdown is 0. You can set the
              initial value with <code>initialCount</code> property.
              <br />
              <br />
              For example: dropdown above set the initial count to 3 (<code>{"initialCount={3}"}</code>). Note that the{" "}
              <b>value passed should be number.</b>
            </Alert>
          </Col>
        </Row>

        <Row className="u-mt-8">
          <Col sm={4}>
            <div className="u-bg-main-background u-rounded u-p-3">
              <h4 className="u-font-bold">Transparent</h4>
              <Dropdown
                options={statusOptions}
                selected={selectedMulti}
                variant="multi"
                multiLabel="Status"
                placeholder="Pilih Status"
                onChange={(selectedItems) => setSelectedMulti(selectedItems)}
                transparent
              />
              <div className="u-mt-1">Selected: {selectedMulti.map((item) => item.label).join(", ")}</div>
            </div>
          </Col>

          <Col sm={4}>
            <div className="u-bg-black-bekko u-rounded u-p-3">
              <h4 className="u-font-bold u-text-white">Ghost</h4>
              <Dropdown
                options={statusOptions}
                selected={selectedMulti}
                variant="multi"
                multiLabel="Status"
                placeholder="Pilih Status"
                onChange={(selectedItems) => setSelectedMulti(selectedItems)}
                ghost
              />
              <div className="u-mt-1 u-text-white">Selected: {selectedMulti.map((item) => item.label).join(", ")}</div>
            </div>
          </Col>
        </Row>

        <CodeBlock className="u-mt-7">
          <div>{'import { Dropdown } from "@flip-ui";'}</div>
          <br />
          <div>{"<Dropdown"}</div>
          <div>&nbsp;&nbsp;{'variat="multi"'}</div>
          <div>&nbsp;&nbsp;{"options={...}"}</div>
          <div>&nbsp;&nbsp;{"selected={...}"}</div>
          <div>&nbsp;&nbsp;{"onChange={...}"}</div>
          <div>&nbsp;&nbsp;{"..."}</div>
          <div>{"/>"}</div>
          <div>{"<Dropdown.Feedback>Text</Dropdown.Feedback>"}</div>
        </CodeBlock>
      </Box>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Dropdown">
        <PropsTable.Prop
          name="options"
          type="array"
          desc={
            <>
              Array of objects of options. What need to be included in object options?
              <ul>
                <li>label [string] [Required!]: item label</li>
                <li>value [string, number, boolean] [Required!]: item value</li>
                <li>
                  <em>other label</em> [any]: additional property
                </li>
              </ul>
              Example:
              <br />
              <code>
                {"["}
                <br />
                &nbsp;&nbsp;{"{"}
                <br />
                &nbsp;&nbsp;&nbsp; label: "Berhasil",
                <br />
                &nbsp;&nbsp;&nbsp; value: "1"
                <br />
                &nbsp;&nbsp;{"},"}
                <br />
                &nbsp;&nbsp;{"{"}
                <br />
                &nbsp;&nbsp;&nbsp; label: "Sedang diproses",
                <br />
                &nbsp;&nbsp;&nbsp; value: "2"
                <br />
                &nbsp;&nbsp;{"}"}
                <br />
                {"]"}
              </code>
            </>
          }
          required
        />
        <PropsTable.Prop name="selected" type="object, array" desc="Set dropdown value" def="null" />
        <PropsTable.Prop name="variant" type="string" desc="Set dropdown variant ['single', 'multi']" def="'single'" />
        <PropsTable.Prop name="disabled" type="boolean" desc="Set dropdown to be disabled" def="false" />
        <PropsTable.Prop name="placeholder" type="string" desc="Set dropdown input placeholder" def="'Pilih...'" />
        <PropsTable.Prop
          name="multiLabel"
          type="string"
          desc="Set dropdown input label when multiple values are selcted"
          def="'Pilihan'"
        />
        <PropsTable.Prop
          name="itemLabel"
          type="string"
          desc={
            <>
              Set which property from options should be shown in menu
              <br />
              e.g:
              <br />
              <code>
                const opts = {"{"} label: ..., value: ..., customLabel: ... {"}"}
              </code>
              <br />
              then <code>itemLabel="customLabel"</code>
            </>
          }
          def="'label'"
        />
        <PropsTable.Prop
          name="fullWidth"
          type="boolean"
          desc="Set dropdown menu to be as the same width as input"
          def="false"
        />
        <PropsTable.Prop
          name="menuPosition"
          type="string"
          desc="Set dropdown menu to be aligned to the left or right of the input"
          def="'left'"
        />
        <PropsTable.Prop name="isInvalid" type="boolean" desc="Set dropdown is invalid" def="false" />
        <PropsTable.Prop
          name="onChange"
          type="function"
          desc="Callback function after item has been selected"
          def="() => {}"
        />
        <PropsTable.Prop name="searchable" type="boolean" desc="Set dropdown options can be searched" def="false" />
        <PropsTable.Prop
          name="searchLabel"
          type="string"
          desc="Set which property from options should be used in search"
          def="'label'"
        />
        <PropsTable.Prop
          name="searchPlaceholder"
          type="string"
          desc="Set placeholder for search bar"
          def="'Cari pilihan'"
        />
        <PropsTable.Prop
          name="immediate"
          type="boolean"
          desc="Set dropdown multi to trigger change immediately after an option is selected"
          def="false"
        />
        <PropsTable.Prop
          name="initialCount"
          type="number"
          desc="Set dropdown multi initial (default) count. So if none is selected, it will show this
          number"
          def="0"
        />
        <PropsTable.Prop
          name="transparent"
          type="boolean"
          desc="Set dropdown input background to transparent"
          def="false"
        />
        <PropsTable.Prop
          name="ghost"
          type="boolean"
          desc="Set dropdown input background to transparent and color & border to white"
          def="false"
        />
        <PropsTable.Prop name="inputClass" type="string" desc="Custom class to style DropdownInput" def="''" />
      </PropsTable>

      <PropsTable title="Dropdown.Feedback">
        <PropsTable.Prop name="children" type="any" desc="Set feedback text" def="''" />
      </PropsTable>
    </Container>
  );
}

export default DropdownComp;
