import React from "react";

import { Button, Panel } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import CodeBlock from "../utilities/CodeBlock";
import PropsTable from "../utilities/PropsTable";

function PanelComp() {
  return (
    <Container>
      <h1 className="u-font-bold">Panel</h1>

      <Box>
        <h4 className="u-font-bold">Default</h4>
        <div className="u-p-10 u-mb-3 u-bg-main-background u-rounded">
          <Panel>
            <Panel.Body>
              This is a content in panel. It should be wrapped in <b>Panel.Body</b>.
            </Panel.Body>
          </Panel>
        </div>
        <CodeBlock>
          <div>{'import { Panel } from "@flip-ui";'}</div>
          <br />
          <div>{"<Panel>"}</div>
          <div>&nbsp;&nbsp;{"<Panel.Body>"}</div>
          <div>&nbsp;&nbsp;&nbsp;&nbsp;Content</div>
          <div>&nbsp;&nbsp;{"</Panel.Body>"}</div>
          <div>{"</Panel>"}</div>
        </CodeBlock>

        <h4 className="u-font-bold u-mt-6">With Footer</h4>
        <div className="u-p-10 u-mb-3 u-bg-main-background u-rounded">
          <Panel>
            <Panel.Body>
              This is a content in panel. It should be wrapped in <b>Panel.Body</b>.
            </Panel.Body>
            <Panel.Footer className="u-text-right">
              <Button variant="secondary">OK</Button>
            </Panel.Footer>
          </Panel>
        </div>
        <CodeBlock>
          <div>{'import { Panel } from "@flip-ui";'}</div>
          <br />
          <div>{"<Panel>"}</div>
          <div>&nbsp;&nbsp;{"<Panel.Body>"}</div>
          <div>&nbsp;&nbsp;&nbsp;&nbsp;Body Content</div>
          <div>&nbsp;&nbsp;{"</Panel.Body>"}</div>
          <div>&nbsp;&nbsp;{"<Panel.Footer>"}</div>
          <div>&nbsp;&nbsp;&nbsp;&nbsp;Footer Content</div>
          <div>&nbsp;&nbsp;{"</Panel.Footer>"}</div>
          <div>{"</Panel>"}</div>
        </CodeBlock>

        <h4 className="u-font-bold u-mt-6">Bordered</h4>
        <div className="u-p-10 u-mb-3 u-bg-main-background u-rounded">
          <Panel bordered>
            <Panel.Body>
              This is a content in panel. It should be wrapped in <b>Panel.Body</b>.
            </Panel.Body>
          </Panel>
        </div>
        <CodeBlock>
          <div>{'import { Panel } from "@flip-ui";'}</div>
          <br />
          <div>{"<Panel bordered>"}</div>
          <div>&nbsp;&nbsp;{"<Panel.Body>"}</div>
          <div>&nbsp;&nbsp;&nbsp;&nbsp;Content</div>
          <div>&nbsp;&nbsp;{"</Panel.Body>"}</div>
          <div>{"</Panel>"}</div>
        </CodeBlock>

        <h4 className="u-font-bold u-mt-6">Shadowed</h4>
        <div className="u-p-10 u-mb-3 u-bg-main-background u-rounded">
          <Panel shadowed>
            <Panel.Body>
              This is a content in panel. It should be wrapped in <b>Panel.Body</b>.
            </Panel.Body>
          </Panel>
        </div>
      </Box>
      <CodeBlock>
        <div>{'import { Panel } from "@flip-ui";'}</div>
        <br />
        <div>{"<Panel shadowed>"}</div>
        <div>&nbsp;&nbsp;{"<Panel.Body>"}</div>
        <div>&nbsp;&nbsp;&nbsp;&nbsp;Content</div>
        <div>&nbsp;&nbsp;{"</Panel.Body>"}</div>
        <div>{"</Panel>"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Panel">
        <PropsTable.Prop name="bordered" type="boolean" desc="Set panel to be bordered" def="false" />
        <PropsTable.Prop name="shadowed" type="boolean" desc="Set panel to be shadowed" def="false" />
      </PropsTable>

      <PropsTable title="Panel.Body">
        <PropsTable.Prop
          name="fullHeight"
          type="boolean"
          desc="Set panel to be full height (to the bottom of page)"
          def="false"
        />
        <PropsTable.Prop
          name="fullHeightWithFooter"
          type="boolean"
          desc="Set panel to be full height (to the bottom of page) (for panel with footer
          only)"
          def="false"
        />
      </PropsTable>
    </Container>
  );
}

export default PanelComp;
