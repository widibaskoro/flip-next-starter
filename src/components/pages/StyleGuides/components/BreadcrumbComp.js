import React from "react";

import { Breadcrumb } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import CodeBlock from "../utilities/CodeBlock";

const BreadcrumbComp = () => {
  return (
    <Container>
      <h1 className="u-font-bold">Breadcrumb</h1>

      <Box>
        <div className="u-flex u-justify-between u-w-10/12">
          <div>
            <h4 className="u-font-bold">Default</h4>
            <Breadcrumb>
              <Breadcrumb.Item>Metode Input</Breadcrumb.Item>
              <Breadcrumb.Item>Buat Transaksi</Breadcrumb.Item>
              <Breadcrumb.Item>Metode Pembayaran</Breadcrumb.Item>
            </Breadcrumb>
          </div>

          <div>
            <h4 className="u-font-bold">Children are Anchors/Links</h4>
            <Breadcrumb>
              <Breadcrumb.Item>
                <a href="#breadcrumb">Metode Input</a>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
                <a href="#breadcrumb">Buat Transaksi</a>
              </Breadcrumb.Item>
              <Breadcrumb.Item>Metode Pembayaran</Breadcrumb.Item>
            </Breadcrumb>
          </div>
        </div>
      </Box>

      <CodeBlock className="u-my-5">
        <div>{'import { Breadcrumb } from "@flip-ui";'}</div>
        <br />
        <div>{"<Breadcrumb>"}</div>
        <div>&nbsp;&nbsp;{"<Breadcrumb.Item>Metode Input</Breadcrumb.Item>"}</div>
        <div>&nbsp;&nbsp;{"..."}</div>
        <div>{"</Breadcrumb>"}</div>
      </CodeBlock>
    </Container>
  );
};

export default BreadcrumbComp;
