import React from "react";

import { Tooltip } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function TooltipComp() {
  return (
    <Container>
      <h1 className="u-font-bold">Tooltip</h1>

      <Box>
        <h4 className="u-font-bold">Default</h4>
        <div className="u-pl-10">
          <div className="u-inline-block u-my-10 u-ml-20 u-pl-20">
            <Tooltip.Wrapper>
              Left
              <Tooltip position="left">Left Tooltip</Tooltip>
            </Tooltip.Wrapper>

            <Tooltip.Wrapper className="u-ml-10">
              Top
              <Tooltip position="top">Top Tooltip</Tooltip>
            </Tooltip.Wrapper>

            <Tooltip.Wrapper className="u-ml-10">
              Bottom
              <Tooltip position="bottom">Bottom Tooltip</Tooltip>
            </Tooltip.Wrapper>

            <Tooltip.Wrapper className="u-ml-10">
              Right
              <Tooltip position="right">Right Tooltip</Tooltip>
            </Tooltip.Wrapper>
          </div>
        </div>
      </Box>

      <Box>
        <h4 className="u-font-bold">Top variant</h4>
        <div className="u-pl-10">
          <div className="u-inline-block u-mt-10 u-ml-20 u-pl-20">
            <Tooltip.Wrapper>
              Top Right
              <Tooltip position="top-right">Top Right Tooltip</Tooltip>
            </Tooltip.Wrapper>

            <Tooltip.Wrapper className="u-ml-10">
              Top Left
              <Tooltip position="top-left">Top Right Tooltip</Tooltip>
            </Tooltip.Wrapper>
          </div>
        </div>
      </Box>

      <Box>
        <h4 className="u-font-bold">Bottom variant</h4>
        <div className="u-pl-10">
          <div className="u-inline-block u-mb-10 u-ml-20 u-pl-20">
            <Tooltip.Wrapper>
              Bottom Right
              <Tooltip position="bottom-right">Bottom Right Tooltip</Tooltip>
            </Tooltip.Wrapper>

            <Tooltip.Wrapper className="u-ml-10">
              Bottom Left
              <Tooltip position="bottom-left">Bottom Left Tooltip</Tooltip>
            </Tooltip.Wrapper>
          </div>
        </div>
      </Box>

      <CodeBlock className="u-mt-3">
        <div>{'import { Tooltip } from "@flip-ui";'}</div>
        <br />
        <div>{"<Tooltip.Wrapper>"}</div>
        <div>&nbsp;&nbsp;{'<Tooltip position="...">'}</div>
        <div>&nbsp;&nbsp;&nbsp;&nbsp;Content</div>
        <div>&nbsp;&nbsp;{"</Tooltip>"}</div>
        <div>{"</Tooltip.Wrapper>"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Tooltip">
        <PropsTable.Prop
          name="position"
          type="string"
          desc="Set tooltip position ('top', 'bottom', 'left', 'right', 'top-left', 'top-right', 'bottom-left', 'bottom-right')"
          def="'top'"
        />
      </PropsTable>

      <PropsTable title="Tooltip.Wrapper">
        <PropsTable.Prop name="children" type="object" desc="Tooltip component" required />
      </PropsTable>
    </Container>
  );
}

export default TooltipComp;
