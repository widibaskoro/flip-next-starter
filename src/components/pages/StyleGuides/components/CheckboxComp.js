import React, { useState } from "react";

import { Checkbox } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function CheckboxComp() {
  const [checked, setChecked] = useState(true);
  const [checked2, setChecked2] = useState(false);
  const [checked3, setChecked3] = useState(false);

  return (
    <Container>
      <h1 className="u-font-bold">Checkbox</h1>

      <Box className="u-flex u-w-7/12 u-justify-between">
        <div>
          <h5 className="u-font-bold">Checked</h5>
          <Checkbox checked={checked} onChange={(event) => setChecked(event.target.checked)}>
            Checked
          </Checkbox>
        </div>

        <div>
          <h5 className="u-font-bold">Unchecked</h5>
          <Checkbox checked={checked2} onChange={(event) => setChecked2(event.target.checked)}>
            Unchecked
          </Checkbox>
        </div>

        <div>
          <h5 className="u-font-bold">Disabled</h5>
          <Checkbox checked={checked3} onChange={(event) => setChecked3(event.target.checked)} disabled>
            Disabled
          </Checkbox>
        </div>
      </Box>

      <CodeBlock className="u-mt-5">
        <div>{'import { Checkbox } from "@flip-ui";'}</div>
        <br />
        <div>{"<Checkbox checked={...} onChange={...}>text</Checkbox>"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Checkbox" withOtherInfo>
        <PropsTable.Prop name="checked" type="boolean" desc="Set checkbox to be checked" def="false" />
        <PropsTable.Prop name="disabled" type="boolean" desc="Set checkbox to be disabled" def="false" />
      </PropsTable>
    </Container>
  );
}

export default CheckboxComp;
