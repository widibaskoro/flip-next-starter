import React from "react";

import { Button, Col, Field, FieldGroup, Row } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import CodeBlock from "../utilities/CodeBlock";
import FieldGroupGenerator from "../utilities/FieldGroupGenerator";

function FieldGroupComp() {
  return (
    <Container>
      <h1 className="u-font-bold">FieldGroup</h1>

      <Box>
        {/**********************************************************/}
        {/******************** DEFAULT SECTION *********************/}
        {/**********************************************************/}
        <h2 className="u-font-bold">Default</h2>
        <Row>
          <Col sm={4}>
            <FieldGroup>
              <Field placeholder="A placeholder" />
            </FieldGroup>
          </Col>
          <Col sm={8}>
            <CodeBlock>
              <div>{'import { FieldGroup, Field } from "@flip-ui";'}</div>
              <br />
              <div>{"<FieldGroup>"}</div>
              <div>&nbsp;&nbsp;{"<Field />"}</div>
              <div>{"</FieldGroup>"}</div>
            </CodeBlock>
          </Col>
        </Row>

        {/**********************************************************/}
        {/******************** PREPEND SECTION *********************/}
        {/**********************************************************/}
        <FieldGroupGenerator type="Prepend" />

        {/**********************************************************/}
        {/******************** PREPEND SECTION *********************/}
        {/**********************************************************/}
        <FieldGroupGenerator type="Append" />

        <h4 className="u-font-bold u-mt-5">Prepend + Append</h4>
        <Row>
          <Col sm={4}>
            <FieldGroup>
              <FieldGroup.Prepend>Rp</FieldGroup.Prepend>
              <FieldGroup.Append as={Button} variant="secondary">
                Cari
              </FieldGroup.Append>
              <Field placeholder="A placeholder" />
            </FieldGroup>

            <FieldGroup className="u-mt-4">
              <FieldGroup.Prepend>Rp</FieldGroup.Prepend>
              <FieldGroup.Append as={Button} variant="secondary" disabled>
                Cari
              </FieldGroup.Append>
              <Field placeholder="A placeholder" disabled />
            </FieldGroup>

            <FieldGroup className="u-mt-4">
              <FieldGroup.Prepend>Rp</FieldGroup.Prepend>
              <FieldGroup.Append as={Button} variant="secondary">
                Cari
              </FieldGroup.Append>
              <Field placeholder="A placeholder" isInvalid />
              <Field.Feedback>Invalid message</Field.Feedback>
            </FieldGroup>
          </Col>
          <Col sm={8}>
            <CodeBlock>
              <div>{'import { Button, FieldGroup, Field } from "@flip-ui";'}</div>
              <br />
              <div>{"<FieldGroup>"}</div>
              <div>&nbsp;&nbsp;{`<FieldGroup.Prepend>`}</div>
              <div>&nbsp;&nbsp;&nbsp;&nbsp;{"Text"}</div>
              <div>&nbsp;&nbsp;{`</FieldGroup.Prepend>`}</div>
              <div>&nbsp;&nbsp;{`<FieldGroup.Append as={Button}>`}</div>
              <div>&nbsp;&nbsp;&nbsp;&nbsp;{"Text"}</div>
              <div>&nbsp;&nbsp;{`</FieldGroup.Append>`}</div>
              <div>&nbsp;&nbsp;{"<Field />"}</div>
              <div>{"</FieldGroup>"}</div>
            </CodeBlock>
          </Col>
        </Row>
      </Box>
    </Container>
  );
}

export default FieldGroupComp;
