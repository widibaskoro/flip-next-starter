import React, { useState } from "react";

import { Alert, Col, TimePicker, FieldLabel, Form, Row } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function TimePickerComp() {
  const [timeValue, setTimeValue] = useState("");
  const [timeValue2, setTimeValue2] = useState("");

  return (
    <Container>
      <h1 className="u-font-bold">TimePicker</h1>

      <Box>
        <Row>
          <Col sm={4}>
            <h4 className="u-font-bold">Default</h4>
            <Form.Group>
              <FieldLabel>Normal TimePicker</FieldLabel>
              <TimePicker selectedTime={timeValue} onChangeTime={setTimeValue} />
            </Form.Group>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Invalid</h4>
            <Form.Group>
              <FieldLabel>Invalid TimePicker</FieldLabel>
              <TimePicker selectedTime={timeValue2} onChangeTime={setTimeValue2} isInvalid />
              <TimePicker.Feedback>Invalid message</TimePicker.Feedback>
            </Form.Group>
          </Col>

          <Col sm={4}>
            <h4 className="u-font-bold">Disabled</h4>
            <Form.Group>
              <FieldLabel>Disabled TimePicker</FieldLabel>
              <TimePicker selectedTime={timeValue} onChangeTime={setTimeValue} disabled />
            </Form.Group>
          </Col>
        </Row>
      </Box>

      <CodeBlock className="u-mt-5">
        <div>{'import { TimePicker } from "@flip-ui";'}</div>
        <br />
        <div>{"<TimePicker"}</div>
        <div>&nbsp;&nbsp;{"selectedTime={...}"}</div>
        <div>&nbsp;&nbsp;{"onChangeTime={...}"}</div>
        <div>&nbsp;&nbsp;{"..."}</div>
        <div>{"/>"}</div>
        <div>{"<TimePicker.Feedback>Text</TimePicker.Feedback>"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <Alert color="grey" className="u-mt-1 u-w-9/12">
        <span>Notes:</span>
        <br />
        <ul>
          <li>The result time of this component is in 24 hour format, but the display in input is 12 hour format</li>
        </ul>
      </Alert>

      <PropsTable title="TimePicker">
        <PropsTable.Prop name="disabled" type="boolean" desc="Set TimePicker disabled" def="false" />
        <PropsTable.Prop name="placeholder" type="string" desc="Set TimePicker input placeholder" def="''" />
        <PropsTable.Prop name="isInvalid" type="boolean" desc="Set TimePicker is invalid" def="false" />
        <PropsTable.Prop name="selectedTime" type="string" desc="Set TimePicker value" def="" />
        <PropsTable.Prop
          name="onChangeTime"
          type="function"
          desc="Callback after date has been selected (returning selected date)"
          def="() => {}"
        />
        <PropsTable.Prop
          name="position"
          type="string"
          desc="Set timepicker position ['left', 'right', 'right bottom']"
          def="'left'"
        />
        <PropsTable.Prop
          name="timeFormat"
          type="string"
          desc="Set timepicker display time format ['24h', '12h']"
          def="'12h'"
        />
      </PropsTable>

      <PropsTable title="TimePicker.Feedback">
        <PropsTable.Prop name="children" type="any" desc="Set feedback text" def="''" />
      </PropsTable>
    </Container>
  );
}

export default TimePickerComp;
