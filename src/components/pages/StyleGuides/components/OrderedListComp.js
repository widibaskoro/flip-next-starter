import React from "react";

import { OrderedList } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import CodeBlock from "../utilities/CodeBlock";

function OrderedListComp() {
  return (
    <Container>
      <h1 className="u-font-bold">OrderedList</h1>

      <Box>
        <OrderedList>
          <OrderedList.Item>
            <b>Rp322.372.000 akan diteruskan ke 20 tujuan.</b>
          </OrderedList.Item>
          <OrderedList.Item>
            <b>Total biaya transfer Rp108.000. </b>
          </OrderedList.Item>
          <OrderedList.Item>
            <b>Kode unik Rp43</b> untuk mempercepat proses pengecekan dan akan disimpan ke dalam deposit akun Anda yang
            dapat dicairkan.
          </OrderedList.Item>
          <OrderedList.Item>
            <b>Perhatian!</b>
          </OrderedList.Item>
          <OrderedList.Item>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
            industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
            scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
            electronic typesetting, remaining essentially unchanged.
          </OrderedList.Item>
          <OrderedList.Item>
            <b>Terima kasih!</b>
          </OrderedList.Item>
        </OrderedList>

        <CodeBlock className="u-mt-5">
          <div>{'import { OrderedList } from "@flip-ui";'}</div>
          <br />
          <div>{"<OrderedList>"}</div>
          <div>&nbsp;&nbsp;{"<OrderedList.Item>Text</OrderedList.Item>"}</div>
          <div>&nbsp;&nbsp;{"<OrderedList.Item>Text</OrderedList.Item>"}</div>
          <div>&nbsp;&nbsp;{"..."}</div>
          <div>{"</OrderedList>"}</div>
        </CodeBlock>
      </Box>
    </Container>
  );
}

export default OrderedListComp;
