import React, { useState } from "react";

import { Alert, Button, Col, Flash, Row } from "@flip-ui";
import { faExclamationCircle, faCheckCircle } from "@fortawesome/free-solid-svg-icons";

import { Container } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function FlashComp() {
  const [show, setShow] = useState(false);
  const [show1, setShow1] = useState(false);
  const [show2, setShow2] = useState(false);
  const [show3, setShow3] = useState(false);
  const [show4, setShow4] = useState(false);
  const [show5, setShow5] = useState(false);

  return (
    <Container>
      <h1 className="u-font-bold">Flash</h1>

      <Row>
        <Col sm={3}>
          <h4 className="u-font-bold">Plain (Default)</h4>
          <Button className="u-mr-3" variant="mini" onClick={() => setShow(true)}>
            Show
          </Button>
          <Button className="u-mr-3" variant="mini" onClick={() => setShow1(true)}>
            Show
          </Button>

          <Flash show={show} onClose={() => setShow(false)}>
            <div>
              Terdapat 4 transaksi yang memiliki <b>nama tidak sesuai dengan sistem bank.</b>
            </div>
          </Flash>
          <Flash color="grey" show={show1} onClose={() => setShow1(false)}>
            <div>
              <b>ID Perusahaan</b> Anda mengacu kepada nama usaha.
            </div>
          </Flash>
        </Col>

        <Col sm={3}>
          <h4 className="u-font-bold">Plain + Icon</h4>
          <Button className="u-mr-3" variant="mini" onClick={() => setShow2(true)}>
            Show
          </Button>
          <Button className="u-mr-3" variant="mini" onClick={() => setShow3(true)}>
            Show
          </Button>

          <Flash show={show2} color="yellow" icon={faExclamationCircle} onClose={() => setShow2(false)}>
            <div>
              Foto kartu identitas <b>harus jelas, lengkap/tidak terpotong, dan horizontal.</b>
            </div>
          </Flash>
          <Flash show={show3} color="blue" icon={faCheckCircle} onClose={() => setShow3(false)}>
            <div>
              Yay, Anda <b>hemat Rp110.000</b> dengan transfer menggunakan Big Flip!
            </div>
          </Flash>
        </Col>

        <Col sm={3}>
          <h4 className="u-font-bold">Plain + Emoticon</h4>
          <Button className="u-mr-3" variant="mini" onClick={() => setShow4(true)}>
            Show
          </Button>
          <Button className="u-mr-3" variant="mini" onClick={() => setShow5(true)}>
            Show
          </Button>
          <Flash show={show4} color="green" emoticon="🎉" onClose={() => setShow4(false)}>
            <div>
              <b>Verifikasi berhasil!</b> Silakan klik tombol “Lanjut ke Pembayaran" di bawah.
            </div>
          </Flash>
          <Flash show={show5} color="yellow" emoticon="📌" onClose={() => setShow5(false)}>
            <div>
              Foto kartu identitas <b>harus jelas, lengkap/tidak terpotong, dan horizontal.</b>
            </div>
          </Flash>
        </Col>
      </Row>

      <Alert color="grey" className="u-mt-10 u-w-6/12">
        <div>
          A <code>Flash</code> component will automatically disappear after 3 seconds.
        </div>
      </Alert>

      <CodeBlock className="u-mt-5">
        <div>{'import { Flash } from "@flip-ui";'}</div>
        <br />
        <div>{'<Flash color="..." show={...} onClose={...}>'}</div>
        <div>&nbsp;&nbsp;Flash Text</div>
        <div>{"</Flash>"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Flash">
        <PropsTable.Prop name="show" type="boolean" desc="Set flash to be shown" def="false" />
        <PropsTable.Prop
          name="color"
          type="string"
          desc="Set flash color ['blue', 'green', 'grey', 'red', 'yellow']"
          def="'red'"
        />
        <PropsTable.Prop name="emoticon" type="string" desc="Emoticon character" def="''" />
        <PropsTable.Prop name="icon" type="object" desc="Icon object from Font Awesome" def="null" />
        <PropsTable.Prop name="onClose" type="function" desc="Function for dismissing the flash" def="() => {}" />
      </PropsTable>
    </Container>
  );
}

export default FlashComp;
