import React from "react";

import { Shimmer } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function ShimmerComp() {
  return (
    <Container>
      <h1 className="u-font-bold">Shimmer</h1>

      <Box>
        <h4 className="u-font-bold">Default</h4>

        <div className="u-flex u-justify-between u-w-9/12">
          <div>
            <p className="u-m-0">Set height and width (px, %, vh, etc)</p>
            <Shimmer height={50} width={300} />
          </div>
          <div>
            <p className="u-m-0">Set height, width, radius</p>
            <Shimmer height={50} width={300} radius={20} />
          </div>
        </div>
      </Box>

      <CodeBlock className="u-mt-5">
        <div>{'import { Shimmer } from "@flip-ui";'}</div>
        <br />
        <div>{"<Shimmer height={...} width={...} radius={...} />"}</div>
      </CodeBlock>

      <h2 className="u-mt-10">API</h2>
      <PropsTable title="Shimmer">
        <PropsTable.Prop name="height" type="number, string" desc="Set shimmer height (px, %, vh, etc)" def="1" />
        <PropsTable.Prop name="width" type="number, string" desc="Set shimmer width (px, %, vh, etc)" def="1" />
        <PropsTable.Prop name="radius" type="number, string" desc="Set shimmer border radius" def="8" />
      </PropsTable>
    </Container>
  );
}

export default ShimmerComp;
