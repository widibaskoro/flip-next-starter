import React, { useState } from "react";

import { ButtonGroup, Col, Row } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

const ButtonGroupComp = () => {
  const [active, setActive] = useState("all");
  const [active2, setActive2] = useState("all");

  return (
    <Container>
      <h1 className="u-font-bold">ButtonGroup</h1>

      <Box>
        <h2 className="u-font-bold">Primary (Default)</h2>
        <Row>
          <Col sm={4}>
            <ButtonGroup>
              <ButtonGroup.Button active={active === "all"} onClick={() => setActive("all")}>
                Semua
              </ButtonGroup.Button>
              <ButtonGroup.Button active={active === "kredit"} onClick={() => setActive("kredit")}>
                Kredit
              </ButtonGroup.Button>
              <ButtonGroup.Button active={active === "debit"} onClick={() => setActive("debit")}>
                Debit
              </ButtonGroup.Button>
            </ButtonGroup>

            <p>Active key: "{active}"</p>
          </Col>

          <Col sm={4}>
            <ButtonGroup>
              <ButtonGroup.Button active={true}>Semua</ButtonGroup.Button>
              <ButtonGroup.Button>Kredit</ButtonGroup.Button>
              <ButtonGroup.Button disabled>Debit</ButtonGroup.Button>
            </ButtonGroup>
          </Col>
        </Row>
      </Box>

      <Box>
        <h2 className="u-font-bold">Mini</h2>
        <Row>
          <Col sm={4}>
            <ButtonGroup variant="mini">
              <ButtonGroup.Button active={active2 === "all"} onClick={() => setActive2("all")}>
                Semua
              </ButtonGroup.Button>
              <ButtonGroup.Button active={active2 === "kredit"} onClick={() => setActive2("kredit")}>
                Kredit
              </ButtonGroup.Button>
              <ButtonGroup.Button active={active2 === "debit"} onClick={() => setActive2("debit")}>
                Debit
              </ButtonGroup.Button>
            </ButtonGroup>

            <p>Active key: "{active2}"</p>
          </Col>

          <Col sm={4}>
            <ButtonGroup variant="mini">
              <ButtonGroup.Button active={true}>Semua</ButtonGroup.Button>
              <ButtonGroup.Button>Kredit</ButtonGroup.Button>
              <ButtonGroup.Button disabled>Debit</ButtonGroup.Button>
            </ButtonGroup>
          </Col>
        </Row>
      </Box>

      <CodeBlock className="u-my-5">
        <div>{'import { ButtonGroup } from "@flip-ui";'}</div>
        <br />
        <div>{'<ButtonGroup variant="...">'}</div>
        <div>&nbsp;&nbsp;{"<ButtonGroup.Button active={...} onClick={...}>Semua</ButtonGroup.Button>"}</div>
        <div>&nbsp;&nbsp;{"..."}</div>
        <div>{"</ButtonGroup>"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="ButtonGroup">
        <PropsTable.Prop name="variant" type="string" desc="Set button variant ['primary', 'mini']" def="primary" />
      </PropsTable>

      <PropsTable title="ButtonGroup.Button">
        <PropsTable.Prop name="active" type="boolean" desc="Set is active or not" def="false" />
      </PropsTable>
    </Container>
  );
};

export default ButtonGroupComp;
