import React, { useState } from "react";

import { Button, Pagination } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function PaginationComp() {
  const limit = 10;

  const [page, setPage] = useState(1);
  const [totalData, setTotalData] = useState(150);

  return (
    <Container>
      <h1 className="u-font-bold">Pagination</h1>

      <Box className="u-mt-4">
        <Pagination totalData={totalData} limit={limit} currentPage={page} onPageChange={(pg) => setPage(pg)} />

        <div className="u-mt-5">
          <div className="u-inline-block u-mr-5">
            Total data: <span className="u-font-bold">{totalData}</span>
          </div>
          <div className="u-inline-block u-mr-5">
            Limit per page: <span className="u-font-bold">{limit}</span>
          </div>
          <div className="u-inline-block u-mr-5">
            Current page: <span className="u-font-bold">{page}</span>
          </div>
        </div>

        <div className="u-font-bold u-mt-4">Change total data:</div>
        <Button className="u-mr-4" variant="mini" onClick={() => setTotalData(9)}>
          9 Data
        </Button>
        <Button className="u-mr-4" variant="mini" onClick={() => setTotalData(60)}>
          60 Data
        </Button>
        <Button className="u-mr-4" variant="mini" onClick={() => setTotalData(200)}>
          200 Data
        </Button>
        <Button variant="mini" onClick={() => setTotalData(50000)}>
          50000 Data
        </Button>

        <div className="u-font-bold u-mt-4">Change page via outer function:</div>
        <Button variant="mini" disabled={totalData < 10} onClick={() => setPage(2)}>
          Go to page 2
        </Button>
      </Box>

      <CodeBlock className="u-mt-5">
        <div>{'import { Pagination } from "@flip-ui";'}</div>
        <br />
        <div>{"<Pagination"}</div>
        <div>&nbsp;&nbsp;{"totalData={...}"}</div>
        <div>&nbsp;&nbsp;{"limit={...}"}</div>
        <div>&nbsp;&nbsp;{"currentPage={...}"}</div>
        <div>&nbsp;&nbsp;{"onPageChange={...}"}</div>
        <div>{"/>"}</div>
      </CodeBlock>

      <h2 className="u-mt-7 u-mb-0">API</h2>
      <PropsTable title="Pagination">
        <PropsTable.Prop name="totalData" type="number" desc="Set total data from all pages" def="1" />
        <PropsTable.Prop name="limit" type="number" desc="Set data to be shown in one page" def="1" />
        <PropsTable.Prop name="currentPage" type="number" desc="Set current active page" def="1" />
        <PropsTable.Prop
          name="onPageChange"
          type="function"
          desc="Callback after page has been changed"
          def="() => {}"
        />
      </PropsTable>
    </Container>
  );
}

export default PaginationComp;
