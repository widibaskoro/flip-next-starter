import React, { useState } from "react";

import { Tag } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function TagComp() {
  const [clicked, setClicked] = useState("");

  return (
    <Container>
      <h1 className="u-font-bold">Tag</h1>

      <Box>
        <div className="u-flex u-w-8/12 u-justify-between">
          <Tag onCloseClick={() => setClicked("first")}>First Tag</Tag>
          <Tag onCloseClick={() => setClicked("second")}>Second Tag</Tag>
          <Tag onCloseClick={() => setClicked("third")}>Third Tag</Tag>
          <Tag onCloseClick={() => setClicked("fourth")}>Fourth Tag</Tag>
        </div>

        <p className="u-mb-0 u-mt-3">Clicked tag: {clicked}</p>

        <CodeBlock className="u-mt-5">
          <div>{'import { Tag } from "@flip-ui";'}</div>
          <br />
          <div>{"<Tag onCloseClick={...}>Content</Tag>"}</div>
        </CodeBlock>

        <h2 className="u-mt-10 u-mb-0">API</h2>
        <PropsTable title="Tag">
          <PropsTable.Prop
            name="onCloseClick"
            type="function"
            desc="Callback after close button has been clicked"
            def="() => {}"
          />
        </PropsTable>
      </Box>
    </Container>
  );
}

export default TagComp;
