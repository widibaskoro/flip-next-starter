import React from "react";

import { DotSteps } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import CodeBlock from "../utilities/CodeBlock";

function DotStepsComp() {
  return (
    <Container>
      <h1 className="u-font-bold">DotSteps</h1>

      <Box>
        <h4 className="u-font-bold">Default</h4>
        <DotSteps>
          <DotSteps.Content>
            <b>Rp322.372.000 akan diteruskan ke 20 tujuan.</b>
          </DotSteps.Content>
          <DotSteps.Content>
            <b>Total biaya transfer Rp108.000. </b>
          </DotSteps.Content>
          <DotSteps.Content>
            <b>Kode unik Rp43</b> untuk mempercepat proses pengecekan dan akan disimpan ke dalam deposit akun Anda yang
            dapat dicairkan.
          </DotSteps.Content>
          <DotSteps.Content>
            <b>Perhatian!</b>
          </DotSteps.Content>
          <DotSteps.Content>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
            industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
            scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
            electronic typesetting, remaining essentially unchanged.
          </DotSteps.Content>
          <DotSteps.Content>
            <b>Terima kasih!</b>
          </DotSteps.Content>
        </DotSteps>
      </Box>

      <Box>
        <h4 className="u-font-bold">Only One Child</h4>
        <DotSteps>
          <DotSteps.Content>
            <b>Terima kasih!</b>
          </DotSteps.Content>
        </DotSteps>
      </Box>

      <CodeBlock className="u-my-5">
        <div>{'import { DotSteps } from "@flip-ui";'}</div>
        <br />
        <div>{"<DotSteps>"}</div>
        <div>&nbsp;&nbsp;{"<DotSteps.Content>Text</DotSteps.Content>"}</div>
        <div>&nbsp;&nbsp;{"<DotSteps.Content>Text</DotSteps.Content>"}</div>
        <div>&nbsp;&nbsp;{"..."}</div>
        <div>{"</DotSteps>"}</div>
      </CodeBlock>
    </Container>
  );
}

export default DotStepsComp;
