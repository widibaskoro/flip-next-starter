import React from "react";

import { Alert } from "@flip-ui";
import { faExclamationTriangle, faCheckCircle, faInfoCircle } from "@fortawesome/free-solid-svg-icons";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function AlertComp() {
  return (
    <Container>
      <h1 className="u-font-bold">Alert</h1>

      <Box className="u-w-7/12">
        <h2 className="u-font-bold">Plain (Default)</h2>

        <Alert color="red" className="u-mb-2">
          Terdapat 4 transaksi yang memiliki <b>nama tidak sesuai dengan sistem bank.</b>
        </Alert>
        <Alert color="yellow" className="u-mb-2">
          Foto kartu identitas <b>harus jelas, lengkap/tidak terpotong, dan horizontal.</b>
        </Alert>
        <Alert color="green" className="u-mb-2">
          <b>Verifikasi berhasil!</b> Silakan klik tombol “Lanjut ke Pembayaran" di bawah.
        </Alert>
        <Alert color="blue" className="u-mb-2">
          Yay, Anda <b>hemat Rp110.000</b> dengan transfer menggunakan Big Flip!
        </Alert>
        <Alert color="grey" className="u-mb-2">
          <b>ID Perusahaan</b> Anda mengacu kepada nama usaha.
        </Alert>

        <CodeBlock className="u-my-5">{'<Alert color="red"> ... </Alert>'}</CodeBlock>
      </Box>

      <Box className="u-w-7/12">
        <h2 className="u-font-bold">Plain Bordered</h2>

        <Alert color="red" className="u-mb-2" bordered>
          Terdapat 4 transaksi yang memiliki <b>nama tidak sesuai dengan sistem bank.</b>
        </Alert>
        <Alert color="yellow" className="u-mb-2" bordered>
          Foto kartu identitas <b>harus jelas, lengkap/tidak terpotong, dan horizontal.</b>
        </Alert>
        <Alert color="green" className="u-mb-2" bordered>
          <b>Verifikasi berhasil!</b> Silakan klik tombol “Lanjut ke Pembayaran" di bawah.
        </Alert>
        <Alert color="blue" className="u-mb-2" bordered>
          Yay, Anda <b>hemat Rp110.000</b> dengan transfer menggunakan Big Flip!
        </Alert>
        <Alert color="grey" className="u-mb-2" bordered>
          <b>ID Perusahaan</b> Anda mengacu kepada nama usaha.
        </Alert>

        <CodeBlock className="u-my-5">{'<Alert color="red" bordered> ... </Alert>'}</CodeBlock>
      </Box>

      <Box className="u-w-7/12 u-mt-10">
        <h2 className="u-font-bold">Plain + Icon</h2>

        <Alert color="red" icon={faExclamationTriangle} className="u-mb-2">
          Terdapat 4 transaksi yang memiliki <b>nama tidak sesuai dengan sistem bank.</b>
        </Alert>
        <Alert color="yellow" icon={faExclamationTriangle} className="u-mb-2">
          Foto kartu identitas <b>harus jelas, lengkap/tidak terpotong, dan horizontal.</b>
        </Alert>
        <Alert color="green" icon={faCheckCircle} className="u-mb-2">
          <b>Verifikasi berhasil!</b> Silakan klik tombol “Lanjut ke Pembayaran" di bawah.
        </Alert>
        <Alert color="blue" icon={faCheckCircle} className="u-mb-2">
          Yay, Anda <b>hemat Rp110.000</b> dengan transfer menggunakan Big Flip!
        </Alert>
        <Alert color="grey" icon={faInfoCircle} className="u-mb-2">
          <b>ID Perusahaan</b> Anda mengacu kepada nama usaha.
        </Alert>

        <CodeBlock className="u-my-5">{'<Alert color="..." icon={...}> ... </Alert>'}</CodeBlock>
      </Box>

      <Box className="u-w-7/12 u-mt-10">
        <h2 className="u-font-bold">Plain + Emoticon</h2>

        <Alert color="red" emoticon="📌" className="u-mb-2">
          Terdapat 4 transaksi yang memiliki <b>nama tidak sesuai dengan sistem bank.</b>
        </Alert>
        <Alert color="yellow" emoticon="📌" className="u-mb-2">
          Foto kartu identitas <b>harus jelas, lengkap/tidak terpotong, dan horizontal.</b>
        </Alert>
        <Alert color="green" emoticon="🎉" className="u-mb-2">
          <b>Verifikasi berhasil!</b> Silakan klik tombol “Lanjut ke Pembayaran" di bawah.
        </Alert>
        <Alert color="blue" emoticon="🎉" className="u-mb-2">
          Yay, Anda <b>hemat Rp110.000</b> dengan transfer menggunakan Big Flip!
        </Alert>
        <Alert color="grey" emoticon="📌" className="u-mb-2">
          <b>ID Perusahaan</b> Anda mengacu kepada nama usaha.
        </Alert>

        <CodeBlock className="u-my-5">{'<Alert color="..." emoticon="..."> ... </Alert>'}</CodeBlock>
      </Box>

      <Box className="u-w-7/12 u-mt-10">
        <h2 className="u-font-bold">Plain + Close Button</h2>

        <Alert color="red" withCloseButton className="u-mb-2">
          Terdapat 4 transaksi yang memiliki <b>nama tidak sesuai dengan sistem bank.</b>
        </Alert>
        <Alert color="yellow" withCloseButton className="u-mb-2">
          Foto kartu identitas <b>harus jelas, lengkap/tidak terpotong, dan horizontal.</b>
        </Alert>
        <Alert color="green" withCloseButton className="u-mb-2">
          <b>Verifikasi berhasil!</b> Silakan klik tombol “Lanjut ke Pembayaran" di bawah.
        </Alert>
        <Alert color="blue" withCloseButton className="u-mb-2">
          Yay, Anda <b>hemat Rp110.000</b> dengan transfer menggunakan Big Flip!
        </Alert>
        <Alert color="grey" withCloseButton className="u-mb-2">
          <b>ID Perusahaan</b> Anda mengacu kepada nama usaha.
        </Alert>

        <CodeBlock className="u-my-5">{'<Alert color="..." withCloseButton> ... </Alert>'}</CodeBlock>
      </Box>

      <Box className="u-w-7/12 u-mt-10">
        <h2 className="u-font-bold">Plain + Icon + Close Button</h2>

        <Alert color="red" icon={faExclamationTriangle} withCloseButton className="u-mb-2">
          Terdapat 4 transaksi yang memiliki <b>nama tidak sesuai dengan sistem bank.</b>
        </Alert>
        <Alert color="yellow" icon={faExclamationTriangle} withCloseButton className="u-mb-2">
          Foto kartu identitas <b>harus jelas, lengkap/tidak terpotong, dan horizontal.</b>
        </Alert>
        <Alert color="green" icon={faCheckCircle} withCloseButton className="u-mb-2">
          <b>Verifikasi berhasil!</b> Silakan klik tombol “Lanjut ke Pembayaran" di bawah.
        </Alert>
        <Alert color="blue" icon={faCheckCircle} withCloseButton className="u-mb-2">
          Yay, Anda <b>hemat Rp110.000</b> dengan transfer menggunakan Big Flip!
        </Alert>
        <Alert color="grey" icon={faInfoCircle} withCloseButton className="u-mb-2">
          <b>ID Perusahaan</b> Anda mengacu kepada nama usaha.
        </Alert>

        <CodeBlock className="u-my-5">{'<Alert color="..." icon={...} withCloseButton> ... </Alert>'}</CodeBlock>
      </Box>

      <CodeBlock className="u-mb-5">
        <div>{'import { Alert } from "@flip-ui";'}</div>
        <br />
        <div>{'<Alert color="..." emoticon="..." icon={...} bordered={...} withCloseButton={...}>text</Alert>'}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Alert">
        <PropsTable.Prop
          name="color"
          type="string"
          desc="Set alert color ['blue', 'green', 'grey', 'red', 'yellow']"
          def="'red'"
        />
        <PropsTable.Prop name="bordered" type="boolean" desc="Set alert with solid border color" def="false" />
        <PropsTable.Prop name="emoticon" type="string" desc="Emoticon character" def="''" />
        <PropsTable.Prop name="icon" type="object" desc="Icon object from Font Awesome" def="null" />
        <PropsTable.Prop
          name="onCloseClick"
          type="function"
          desc="Callback after close button has been clicked"
          def="() => {}"
        />
        <PropsTable.Prop name="withCloseButton" type="boolean" desc="Set alert with close button" def="false" />
      </PropsTable>
    </Container>
  );
}

export default AlertComp;
