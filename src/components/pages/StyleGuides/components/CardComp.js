import React from "react";

import { Card } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function CardComp() {
  return (
    <Container>
      <h1 className="u-font-bold">Card</h1>

      <Box className="u-w-7/12">
        <h2 className="u-font-bold">Default</h2>
        <Card>
          <p className="u-font-bold u-mb-2">Ini sebuah card</p>
          <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</div>
        </Card>
        <Card className="u-w-5/12 u-px-7 u-py-9 u-mt-5 u-text-center">
          <div className="u-inline-block u-w-16 u-h-16 u-mb-3 u-rounded-md u-bg-light-grey"></div>
          <p className="u-font-bold u-mb-2">Ini sebuah card</p>
          <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</div>
        </Card>
      </Box>

      <Box className="u-w-7/12 u-mt-10">
        <h2 className="u-font-bold">Active</h2>
        <Card active>
          <p className="u-font-bold u-mb-2">Ini sebuah card</p>
          <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</div>
        </Card>
        <Card active className="u-w-5/12 u-px-7 u-py-9 u-mt-5 u-text-center">
          <div className="u-inline-block u-w-16 u-h-16 u-mb-3 u-rounded-md u-bg-light-grey"></div>
          <p className="u-font-bold u-mb-2">Ini sebuah card</p>
          <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</div>
        </Card>
      </Box>

      <Box className="u-w-7/12 u-mt-10">
        <h2 className="u-font-bold">Hoverable</h2>
        <Card hoverable>
          <p className="u-font-bold u-mb-2">Ini sebuah card</p>
          <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</div>
        </Card>
        <Card hoverable className="u-w-5/12 u-px-7 u-py-9 u-mt-5 u-text-center">
          <div className="u-inline-block u-w-16 u-h-16 u-mb-3 u-rounded-md u-bg-light-grey"></div>
          <p className="u-font-bold u-mb-2">Ini sebuah card</p>
          <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</div>
        </Card>
      </Box>

      <Box className="u-w-7/12 u-mt-10">
        <h2 className="u-font-bold">Disabled</h2>
        <Card disabled>
          <p className="u-font-bold u-mb-2">Ini sebuah card</p>
          <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</div>
        </Card>
        <Card disabled className="u-w-5/12 u-px-7 u-py-9 u-mt-5 u-text-center">
          <div className="u-inline-block u-w-16 u-h-16 u-mb-3 u-rounded-md u-bg-light-grey"></div>
          <p className="u-font-bold u-mb-2">Ini sebuah card</p>
          <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</div>
        </Card>
      </Box>

      <CodeBlock className="u-mt-10">
        <div>{'import { Card } from "@flip-ui";'}</div>
        <br />
        <div>{"<Card active={...} hoverable={...}>text</Card>"}</div>
      </CodeBlock>

      <h2 className="u-mt-10 u-mb-0">API</h2>
      <PropsTable title="Card">
        <PropsTable.Prop name="active" type="boolean" desc="Set card is active" def="false" />
        <PropsTable.Prop name="hoverable" type="boolean" desc="Set card has hover state" def="false" />
      </PropsTable>
    </Container>
  );
}

export default CardComp;
