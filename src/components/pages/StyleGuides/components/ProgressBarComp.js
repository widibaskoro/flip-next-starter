import React from "react";

import { ProgressBar } from "@flip-ui";

import { Container, Box } from "../utilities/Boxes";
import PropsTable from "../utilities/PropsTable";
import CodeBlock from "../utilities/CodeBlock";

function ProgressBarComp() {
  return (
    <Container>
      <h1 className="u-font-bold">ProgressBar</h1>

      <Box>
        <h4 className="u-font-bold">Default</h4>
        <ProgressBar percent={10} />
        <br />
        <ProgressBar color="blue" percent={18} />
        <br />
        <ProgressBar color="green" percent={26} />
        <br />
        <ProgressBar color="grey" percent={37} />
        <br />
        <ProgressBar color="red" percent={50} />
        <br />
        <ProgressBar color="yellow" percent={62} />
        <br />
        <ProgressBar color="black" percent={80} />
      </Box>

      <CodeBlock className="u-mt-5">
        <div>{'import { ProgressBar } from "@flip-ui";'}</div>
        <br />
        <div>{'<ProgressBar color="..." percent={...} />'}</div>
      </CodeBlock>

      <Box>
        <h4 className="u-font-bold u-mt-6">Labeled</h4>
        <p>Label color is derived from the bar color. Unless when the percent is 0, it will be grey.</p>

        <ProgressBar percent={0} labeled />
        <br />
        <ProgressBar percent={10} labeled />
        <br />
        <ProgressBar color="blue" percent={18} labeled />
        <br />
        <ProgressBar color="green" percent={26} labeled />
      </Box>

      <CodeBlock className="u-mt-5">
        <div>{'import { ProgressBar } from "@flip-ui";'}</div>
        <br />
        <div>{'<ProgressBar color="..." percent={...} labeled />'}</div>
      </CodeBlock>

      <h2 className="u-mt-7 u-mb-0">API</h2>
      <PropsTable title="ProgressBar">
        <PropsTable.Prop
          name="color"
          type="string"
          desc="Set alert color ['orange', 'blue', 'green', 'grey', 'red', 'yellow', 'white', 'black']"
          def="'orange'"
        />
        <PropsTable.Prop
          name="percent"
          type="number, string"
          desc="Set current percentage of the bar [0 - 100]"
          def="0"
        />
        <PropsTable.Prop name="labeled" type="boolean" desc="Show or hide percentage label" def="false" />
      </PropsTable>
    </Container>
  );
}

export default ProgressBarComp;
