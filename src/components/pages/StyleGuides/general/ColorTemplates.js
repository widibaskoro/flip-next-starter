import React from "react";

import { colors } from "src/assets/styles/settings";

import { Container, Box } from "../utilities/Boxes";
import ColorBox from "../utilities/ColorBox";
import CodeBlock from "../utilities/CodeBlock";

function ColorTemplates() {
  return (
    <Container>
      <h1 className="u-font-bold">Colors</h1>

      <CodeBlock>{'import { colors } from "src/assets/styles/settings";'}</CodeBlock>

      <h2 className="u-font-bold u-mt-10">Primary Colors</h2>
      <Box>
        <ColorBox title="flip_orange" hex={colors.flip_orange} />
        <ColorBox title="black_bekko" hex={colors.black_bekko} />
        <ColorBox title="white" hex={colors.white} />
      </Box>

      <h2 className="u-font-bold u-mt-10">Secondary Colors</h2>
      <Box>
        <ColorBox title="ketchup_tomato" hex={colors.ketchup_tomato} />
        <ColorBox title="mango_yellow" hex={colors.mango_yellow} />
        <ColorBox title="intel_blue" hex={colors.intel_blue} />
        <ColorBox title="jade" hex={colors.jade} />
      </Box>

      <h2 className="u-font-bold u-mt-10">Shades and Tints</h2>
      <Box>
        <ColorBox title="cinnamon" hex={colors.cinnamon} />
        <ColorBox title="brick" hex={colors.brick} />
        <ColorBox title="flip_orange" hex={colors.flip_orange} />
        <ColorBox title="peach" hex={colors.peach} />
        <ColorBox title="anemia" hex={colors.anemia} />
      </Box>
      <Box>
        <ColorBox title="black_bekko" hex={colors.black_bekko} />
        <ColorBox title="dark_smoke" hex={colors.dark_smoke} />
        <ColorBox title="dark_grey" hex={colors.dark_grey} />
        <ColorBox title="light_grey" hex={colors.light_grey} />
        <ColorBox title="light_smoke" hex={colors.light_smoke} />
      </Box>
      <Box>
        <ColorBox title="red_wine" hex={colors.red_wine} />
        <ColorBox title="clown_red" hex={colors.clown_red} />
        <ColorBox title="ketchup_tomato" hex={colors.ketchup_tomato} />
        <ColorBox title="blush" hex={colors.blush} />
        <ColorBox title="sakura" hex={colors.sakura} />
      </Box>
      <Box>
        <ColorBox title="coin" hex={colors.coin} />
        <ColorBox title="goldenrod" hex={colors.goldenrod} />
        <ColorBox title="mango_yellow" hex={colors.mango_yellow} />
        <ColorBox title="blonde" hex={colors.blonde} />
        <ColorBox title="banana" hex={colors.banana} />
      </Box>
      <Box>
        <ColorBox title="space_blue" hex={colors.space_blue} />
        <ColorBox title="pepsi" hex={colors.pepsi} />
        <ColorBox title="intel_blue" hex={colors.intel_blue} />
        <ColorBox title="tie_dye" hex={colors.tie_dye} />
        <ColorBox title="bubble" hex={colors.bubble} />
      </Box>
      <Box>
        <ColorBox title="spinach" hex={colors.spinach} />
        <ColorBox title="pine" hex={colors.pine} />
        <ColorBox title="jade" hex={colors.jade} />
        <ColorBox title="slight_jade" hex={colors.slight_jade} />
        <ColorBox title="light_jade" hex={colors.light_jade} />
      </Box>

      <h2 className="u-font-bold u-mt-10">Punchy Colors</h2>
      <Box>
        <ColorBox title="warning" hex={colors.warning} />
        <ColorBox title="sunrise" hex={colors.sunrise} />
        <ColorBox title="sky_blue" hex={colors.sky_blue} />
        <ColorBox title="greeneon" hex={colors.greeneon} />
        <ColorBox title="catskill_white" hex={colors.catskill_white} />
      </Box>

      <h2 className="u-font-bold u-mt-10">Miscellaneous Colors</h2>
      <Box>
        <ColorBox title="table_stripe" hex={colors.table_stripe} />
        <ColorBox title="main_background" hex={colors.main_background} />
        <ColorBox title="disabled" hex={colors.disabled} />
        <ColorBox title="light_anemia" hex={colors.light_anemia} />
      </Box>

      <h2 className="u-font-bold u-mt-10">Gradient</h2>
      <Box>
        <ColorBox title="flip_gradient" hex={colors.flip_gradient} displayHex="#FD6542 - #D42E52" />
        <ColorBox title="green_lantern" hex={colors.green_lantern} displayHex="#00C1A5 - #22D48C" />
        <ColorBox title="loading" hex={colors.loading} displayHex="#DADADA - #EBEBEB" />
      </Box>
    </Container>
  );
}

export default ColorTemplates;
