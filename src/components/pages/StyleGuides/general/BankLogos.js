import React from "react";

import { Alert } from "@flip-ui";
import { Container, OneThirdBox } from "../utilities/Boxes";
import CodeBlock from "../utilities/CodeBlock";

import { SUPPORTED_BANKS } from "src/consts/banks";

function BankLogos() {
  return (
    <Container>
      <h1 className="u-font-bold">Bank Logos</h1>

      <OneThirdBox>
        <h2 className="u-font-bold">Default</h2>

        <Alert color="grey" className="u-mb-3">
          <p className="u-m-0">
            Logo size: <b>128px x 55px</b>
          </p>
        </Alert>

        <CodeBlock className="u-mb-3">
          {'<span className="c-bank-logo c-bank-logo--[bankname]" />'}
          <br />
          e.g:
          <br />
          {'<span className="c-bank-logo c-bank-logo--mandiri" />'}
        </CodeBlock>

        <table>
          <tbody>
            {Object.keys(SUPPORTED_BANKS).map((key, index) => {
              return (
                <tr className="u-mb-2" key={index}>
                  <td>
                    <span className={`c-bank-logo c-bank-logo--${key}`} />
                  </td>
                  <td className="u-pl-3">
                    <code>{key}</code>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </OneThirdBox>

      <OneThirdBox>
        <h2 className="u-font-bold">Small</h2>

        <Alert color="grey" className="u-mb-3">
          <p className="u-m-0">
            Logo size: <b>70px x 30px</b>
          </p>
        </Alert>

        <CodeBlock className="u-mb-3">
          {'<span className="c-bank-logo c-bank-logo--small c-bank-logo--[bankname]" />'}
          <br />
          e.g:
          <br />
          {'<span className="c-bank-logo c-bank-logo--small c-bank-logo--mandiri" />'}
        </CodeBlock>

        <table>
          <tbody>
            {Object.keys(SUPPORTED_BANKS).map((key, index) => {
              return (
                <tr className="u-mb-2" key={index}>
                  <td>
                    <span className={`c-bank-logo c-bank-logo--small c-bank-logo--${key}`} />
                  </td>
                  <td className="u-pl-3">
                    <code>{key}</code>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </OneThirdBox>

      <OneThirdBox>
        <h2 className="u-font-bold">Tiny</h2>

        <Alert color="grey" className="u-mb-3">
          <p className="u-m-0">
            Logo size: <b>32px x 14px</b>
          </p>
        </Alert>

        <CodeBlock className="u-mb-3">
          {'<span className="c-bank-logo c-bank-logo--small c-bank-logo--[bankname]" />'}
          <br />
          e.g:
          <br />
          {'<span className="c-bank-logo c-bank-logo--tiny c-bank-logo--mandiri" />'}
        </CodeBlock>

        <table>
          <tbody>
            {Object.keys(SUPPORTED_BANKS).map((key, index) => {
              return (
                <tr className="u-mb-2" key={index}>
                  <td>
                    <span className={`c-bank-logo c-bank-logo--tiny c-bank-logo--${key}`} />
                  </td>
                  <td className="u-pl-3">
                    <code>{key}</code>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </OneThirdBox>
    </Container>
  );
}

export default BankLogos;
