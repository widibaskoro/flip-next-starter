import React from "react";

import { Alert } from "@flip-ui";
import { Container } from "../utilities/Boxes";
 
function StyleUtilities() {
  return (
    <Container>
      <h1 className="u-font-bold">Utilities</h1>

      <Alert className="u-block u-text-left u-mb-3" color="blue">
        We use <b>Tailwind</b> for style utilities.
      </Alert>

      <p>
        It still uses <code>u-</code> prefix for the class name.
        <br />
        E.g:
        <code>.u-text-left</code>, <code>.u-text-flip-orange</code>, <code>.u-mt-3</code>, etc. For more documentations,
        please see:
      </p>
      <ul>
        <li>
          <a href="https://wiki.flip.id/en/Frontend/Technicals/Tailwind">
            https://wiki.flip.id/en/Frontend/Technicals/Tailwind
          </a>
        </li>
        <li>
          <a href="https://tailwindcss.com/docs/">https://tailwindcss.com/docs/</a>
        </li>
      </ul>
    </Container>
  );
}

export default StyleUtilities;
