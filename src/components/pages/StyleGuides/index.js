import Head from "next/head"
import { Col, Container, Row } from "@flip-ui";
import { BodyContainer, MenuWrapper, Menus, Title, Separator } from "./utilities/Boxes";

import BankLogos from "./general/BankLogos";
import ColorTemplates from "./general/ColorTemplates";
import StyleUtilities from "./general/StyleUtilities";

import AccordionComp from "./components/AccordionComp";
import AlertComp from "./components/AlertComp";
import BreadcrumbComp from "./components/BreadcrumbComp";
import ButtonComp from "./components/ButtonComp";
import ButtonGroupComp from "./components/ButtonGroupComp";
import ButtonSwitchComp from "./components/ButtonSwitchComp";
import CardComp from "./components/CardComp";
import CheckboxComp from "./components/CheckboxComp";
import CopyComp from "./components/CopyComp";
import DatePickerComp from "./components/DatePickerComp";
import DotComp from "./components/DotComp";
import DotStepsComp from "./components/DotStepsComp";
import DropdownComp from "./components/DropdownComp";
import FieldComp from "./components/FieldComp";
import FieldGroupComp from "./components/FieldGroupComp";
import FieldLabelComp from "./components/FieldLabelComp";
import FileInputComp from "./components/FileInputComp";
import FormComp from "./components/FormComp";
import FlashComp from "./components/FlashComp";
import IconComp from "./components/IconComp";
import LabelComp from "./components/LabelComp";
import LoaderComp from "./components/LoaderComp";
import MenuBarComp from "./components/MenuBarComp";
import ModalComp from "./components/ModalComp";
import OrderedListComp from "./components/OrderedListComp";
import PaginationComp from "./components/PaginationComp";
import PanelComp from "./components/PanelComp";
import ProgressBarComp from "./components/ProgressBarComp";
import RadioButtonComp from "./components/RadioButtonComp";
import RangeDatePickerComp from "./components/RangeDatePickerComp";
import ShimmerComp from "./components/ShimmerComp";
import StepsComp from "./components/StepsComp";
import TableComp from "./components/TableComp";
import TabsComp from "./components/TabsComp";
import TagComp from "./components/TagComp";
import TimePickerComp from "./components/TimePickerComp";
import ToggleComp from "./components/ToggleComp";
import TooltipComp from "./components/TooltipComp";

const StyleGuides = () => {
  const generalMenu = [
    ["Colors", "colors", <ColorTemplates />],
    ["Bank Logos", "banklogos", <BankLogos />],
    ["Style Utilities", "utilities", <StyleUtilities />]
  ];

  const componentMenus = [
    ["Accordion", "accordion", <AccordionComp />],
    ["Alert", "alert", <AlertComp />],
    ["Breadcrumb", "breadcrumb", <BreadcrumbComp />],
    ["Button", "button", <ButtonComp />],
    ["Button Group", "button-group", <ButtonGroupComp />],
    ["Button Switch", "button-switch", <ButtonSwitchComp />],
    ["Card", "card", <CardComp />],
    ["Checkbox", "checkbox", <CheckboxComp />],
    ["Copy", "Copy", <CopyComp />],
    ["Date Picker", "datepicker", <DatePickerComp />],
    ["Dot", "dot", <DotComp />],
    ["Dot Steps", "dotsteps", <DotStepsComp />],
    ["Dropdown", "dropdown", <DropdownComp />],
    ["Field", "field", <FieldComp />],
    ["Field Group", "field-group", <FieldGroupComp />],
    ["Field Label", "field-label", <FieldLabelComp />],
    ["File Input", "file-input", <FileInputComp />],
    ["Flash", "flash", <FlashComp />],
    ["Form", "form", <FormComp />],
    ["Icon", "icon", <IconComp />],
    ["Label", "label", <LabelComp />],
    ["Loader", "loader", <LoaderComp />],
    ["Menu Bar", "menubar", <MenuBarComp />],
    ["Modal", "modal", <ModalComp />],
    ["Ordered List", "ordered-list", <OrderedListComp />],
    ["Pagination", "pagination", <PaginationComp />],
    ["Panel", "panel", <PanelComp />],
    ["Progress Bar", "progress-bar", <ProgressBarComp />],
    ["Radio Button", "radio-button", <RadioButtonComp />],
    ["Range Date Picker", "range-datepicker", <RangeDatePickerComp />],
    ["Shimmer", "shimmer", <ShimmerComp />],
    ["Steps", "steps", <StepsComp />],
    ["Table", "table", <TableComp />],
    ["Tabs", "tabs", <TabsComp />],
    ["Tag", "tag", <TagComp />],
    ["Time Picker", "timepicker", <TimePickerComp />],
    ["Toggle", "toggle", <ToggleComp />],
    ["Tooltip", "tooltip", <TooltipComp />]
  ];

  return (
    <BodyContainer>
      <Head>
        <title>Style Guide</title>
      </Head>

      <Container fluid>
        <Row>
          <Col md={2}>
            <MenuWrapper>
              <h4 className="u-font-bold">General</h4>
              <Menus>
                {generalMenu.map((menu) => {
                  return (
                    <a key={menu[1]} href={`#${menu[1]}`}>
                      <span>{menu[0]}</span>
                    </a>
                  );
                })}
              </Menus>

              <h4 className="u-font-bold u-mt-5">Components</h4>
              <Menus>
                {componentMenus.map((menu) => {
                  return (
                    <a key={menu[1]} href={`#${menu[1]}`}>
                      <span>{menu[0]}</span>
                    </a>
                  );
                })}
              </Menus>
            </MenuWrapper>
          </Col>
          <Col md={10}>
            <Title>General</Title>
            {generalMenu.map((menu) => {
              return (
                <div key={menu[1]} id={menu[1]}>
                  {menu[2]}
                  <Separator />
                </div>
              );
            })}

            <Title>Components</Title>
            {componentMenus.map((menu) => {
              return (
                <div key={menu[1]} id={menu[1]}>
                  {menu[2]}
                  <Separator />
                </div>
              );
            })}
          </Col>
        </Row>
      </Container>
    </BodyContainer>
  );
};

export default StyleGuides;
