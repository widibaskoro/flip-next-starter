import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { StyledCard } from "./Card.styled";

const propTypes = {
  /**
   * Set card is active
   *
   */
  active: PropTypes.bool,

  /**
   * Set card has hover state
   */
  hoverable: PropTypes.bool
};

const defaultProps = {
  active: false,
  hoverable: false
};

const Card = React.forwardRef((props, ref) => {
  const { children, className, active, hoverable, disabled, ...restProps } = props;
  const cardClass = classNames(
    "c-card",
    active && "is-active",
    hoverable && "is-hoverable",
    disabled && "is-disabled",
    className
  );

  return (
    <StyledCard data-testid="qa-card" ref={ref} className={cardClass} {...restProps}>
      {children}
    </StyledCard>
  );
});

Card.propTypes = propTypes;
Card.defaultProps = defaultProps;

export default Card;
