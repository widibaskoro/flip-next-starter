import React from "react";
import { render, screen } from "@testing-library/react";
import { rgba } from "polished";

import { colors, typography } from "src/assets/styles/settings";
import { checkProps } from "src/helpers/test";
import Card from "./index";

describe("Card component", () => {
  describe("Card rendering", () => {
    it("should render Breadcrumb without error", () => {
      render(<Card />);
      expect(screen.getByTestId("qa-card")).toBeInTheDocument();
    });
  });

  describe("Card propTypes", () => {
    it("should not throw warning when expected props provided", () => {
      const defaultProps = {
        active: true,
        hoverable: false
      };

      checkProps(Card, defaultProps, "Card");
    });
  });

  describe("Card props", () => {
    it("should have custom className when provided", () => {
      render(<Card className="u-border-r-2" />);
      expect(screen.getByTestId("qa-card").className).toContain("u-border-r-2");
    });
  });

  describe("Card style", () => {
    it("should render expected style when active props is true", () => {
      render(<Card active />);
      const expectedStyle = {
        "border-color": colors.flip_orange,
        "box-shadow": `0px 8px 16px ${rgba(colors.anemia, 0.32)}`,
        color: colors.black_bekko,
        "font-weight": typography.font_weight.normal,
        "text-decoration": "none"
      };
      expect(screen.getByTestId("qa-card")).toHaveStyle(expectedStyle);
    });

    it("should render expected style when hoverable props is true and component is being hovered", () => {
      render(<Card hoverable />);
      // check hover - this approach is not best practice because we just get the css state when hover is true
      // do this only to get coverage of hoverable props
      expect(screen.getByTestId("qa-card")).toHaveStyleRule("border-color", colors.flip_orange, {
        modifier: "&.is-hoverable:hover"
      });
      expect(screen.getByTestId("qa-card")).toHaveStyleRule("box-shadow", `0px 8px 16px ${rgba(colors.anemia, 0.32)}`, {
        modifier: "&.is-hoverable:hover"
      });
    });

    it("Should render expected style if disabled props true", () => {
      render(<Card disabled />);
      const expectedStyle = {
        "border-color": colors.light_grey,
        color: colors.dark_grey,
        cursor: "not-allowed",
        "box-shadow": "none",
        "text-decoration": "none",
        transform: "translateY(0)"
      };
      expect(screen.getByTestId("qa-card")).toHaveStyle(expectedStyle);
    });
  });
});
