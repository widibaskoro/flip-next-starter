import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";
import { rgba } from "polished";

export const StyledCard = styled.div`
  background: ${colors.white};
  border: 1px solid ${colors.main_background};
  box-sizing: border-box;
  box-shadow: 0px 2px 6px ${rgba(colors.light_grey, 0.48)};
  border-radius: 8px;
  color: ${colors.black_bekko};
  display: block;
  font-weight: ${typography.font_weight.normal};
  padding: 20px;
  text-decoration: none;
  transform: translateY(0);
  transition: all 0.15s ease-in-out;

  &.is-active,
  &.is-hoverable:hover {
    border-color: ${colors.flip_orange};
    box-shadow: 0px 8px 16px ${rgba(colors.anemia, 0.32)};
    color: ${colors.black_bekko};
    font-weight: ${typography.font_weight.normal};
    text-decoration: none;

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      color: ${colors.flip_orange};
    }
  }

  &.is-hoverable:hover {
    cursor: pointer;
    transform: translateY(-4px);
    transition: all 0.15s ease-in-out;
  }

  &[href]:hover {
    color: ${colors.black_bekko};
    cursor: pointer;
    font-weight: ${typography.font_weight.normal};
    text-decoration: none;
  }

  &.is-disabled,
  &.is-disabled:hover,
  &.is-disabled.is-hoverable,
  &.is-disabled.is-active {
    border-color: ${colors.light_grey};
    box-shadow: none;
    color: ${colors.dark_grey};
    cursor: not-allowed;
    text-decoration: none;
    transform: translateY(0);

    *:not(.c-button),
    p,
    span,
    a {
      color: ${colors.dark_grey};
    }
  }
`;
