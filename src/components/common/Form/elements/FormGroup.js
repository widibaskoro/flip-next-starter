import React from "react";
import classNames from "classnames";

import { StyledFormGroup } from "../Form.styled";

const FormGroup = React.forwardRef((props, ref) => {
  const { className, ...restProps } = props;
  const formGroupClass = classNames("c-form__group", className);

  return <StyledFormGroup ref={ref} className={formGroupClass} {...restProps} />;
});

export default FormGroup;
