import styled from "styled-components";

export const StyledForm = styled.form`
  display: block;
`;

export const StyledFormGroup = styled.div`
  margin-bottom: 24px;
`;
