import React from "react";
import classNames from "classnames";

import FormGroup from "./elements/FormGroup";
import { StyledForm } from "./Form.styled";

const Form = React.forwardRef((props, ref) => {
  const { className, ...restProps } = props;
  const labelClass = classNames("c-form", className);

  return <StyledForm ref={ref} className={labelClass} {...restProps} />;
});

Form.Group = FormGroup;

export default Form;
