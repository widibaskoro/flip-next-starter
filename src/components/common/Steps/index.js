import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { StepContainer, StepItem, StepBar, StepLabel } from "./Steps.styled";

const propTypes = {
  /**
   * Array of objects of steps
   *
   * What need to be included in object step?
   *  - label (string) -> step label
   *  - status (string) -> step status ('active', 'done', 'none')
   *
   * e.g:
   * {
   *   label: "First Step",
   *   status: "done"
   * },
   * {
   *   label: "Second Step",
   *   status: "active"
   * },
   */
  steps: PropTypes.array
};

const defaultProps = {
  steps: []
};

const Steps = React.forwardRef((props, ref) => {
  const { className, steps, ...restProps } = props;
  const stepsClass = classNames("c-steps", className);

  return (
    <StepContainer ref={ref} className={stepsClass} {...restProps} data-testid="qa-steps-container">
      {steps.map((step, index) => (
        <StepItem
          key={index}
          className={classNames("c-steps__item", { "is-done": step.status === "done" || step.status === "active" })}
          data-testid="qa-steps-item"
        >
          <StepLabel data-testid="qa-steps-label">
            {index + 1}. {step.label}
          </StepLabel>
          <StepBar data-testid="qa-steps-bar" />
        </StepItem>
      ))}
    </StepContainer>
  );
});

Steps.propTypes = propTypes;
Steps.defaultProps = defaultProps;

export default Steps;
