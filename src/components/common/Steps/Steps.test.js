import React from "react";
import { render, screen } from "@testing-library/react";

import Steps from "./index";
import { checkProps } from "src/helpers/test";
import { colors } from "src/assets/styles/settings";

const DUMMY_STEPS = [
  {
    label: "Metode Pembayaran",
    status: "done"
  },
  {
    label: "Kirim",
    status: "active"
  },
  {
    label: "Selesai",
    status: ""
  }
];

describe("Steps component", () => {
  describe("Steps rendering", () => {
    it("Should render without error if props is given", () => {
      render(<Steps steps={DUMMY_STEPS} />);
      expect(screen.getByTestId("qa-steps-container")).toBeInTheDocument();
      expect(screen.queryAllByTestId("qa-steps-item")).toHaveLength(3);
      expect(screen.queryAllByTestId("qa-steps-label")).toHaveLength(3);
      expect(screen.queryAllByTestId("qa-steps-label")[0]).toHaveTextContent("Metode Pembayaran");
      expect(screen.queryAllByTestId("qa-steps-label")[1]).toHaveTextContent("Kirim");
      expect(screen.queryAllByTestId("qa-steps-label")[2]).toHaveTextContent("Selesai");
    });

    it("Should render without error if no props is given", () => {
      render(<Steps />);
      expect(screen.getByTestId("qa-steps-container")).toBeInTheDocument();
      expect(screen.queryAllByTestId("qa-steps-item")).toHaveLength(0);
      expect(screen.queryAllByTestId("qa-steps-label")).toHaveLength(0);
    });
  });

  describe("Steps propTypes", () => {
    it("Should not throw warning when expected array are provided", () => {
      checkProps(Steps, { steps: DUMMY_STEPS }, "Steps");
    });
  });

  describe("Steps props", () => {
    it("should have custom className when provided", () => {
      render(<Steps className="u-mr-2" />);
      expect(screen.getByTestId("qa-steps-container").className).toContain("u-mr-2");
    });

    it("should show Metode Pembayaran text in label", () => {
      render(<Steps steps={DUMMY_STEPS} />);
      expect(screen.queryAllByTestId("qa-steps-label")[0]).toHaveTextContent("Metode Pembayaran");
    });

    it("should show Kirim text in label", () => {
      render(<Steps steps={DUMMY_STEPS} />);
      expect(screen.queryAllByTestId("qa-steps-label")[1]).toHaveTextContent("Kirim");
    });

    it("should show Selesai text in label", () => {
      render(<Steps steps={DUMMY_STEPS} />);
      expect(screen.queryAllByTestId("qa-steps-label")[2]).toHaveTextContent("Selesai");
    });
  });

  describe("Steps styles", () => {
    it("should have light_grey background-color step bar if status is none", () => {
      render(<Steps steps={DUMMY_STEPS} />);
      expect(screen.queryAllByTestId("qa-steps-bar")[2]).toHaveStyle({ "background-color": colors.light_grey });
    });

    it("should have jade background-color step bar if status is done", () => {
      render(<Steps steps={DUMMY_STEPS} />);
      expect(screen.queryAllByTestId("qa-steps-bar")[0]).toHaveStyle({ "background-color": colors.jade });
    });

    it("should have jade background-color step bar if status is active", () => {
      render(<Steps steps={DUMMY_STEPS} />);
      expect(screen.queryAllByTestId("qa-steps-bar")[1]).toHaveStyle({ "background-color": colors.jade });
    });

    it("should have dark_grey color step bar if status is none", () => {
      render(<Steps steps={DUMMY_STEPS} />);
      expect(screen.queryAllByTestId("qa-steps-label")[2]).toHaveStyle({ color: colors.dark_grey });
    });

    it("should have dark_smoke color step bar if status is done", () => {
      render(<Steps steps={DUMMY_STEPS} />);
      expect(screen.queryAllByTestId("qa-steps-label")[0]).toHaveStyle({ color: colors.dark_smoke });
    });

    it("should have dark_smoke color step bar if status is active", () => {
      render(<Steps steps={DUMMY_STEPS} />);
      expect(screen.queryAllByTestId("qa-steps-label")[1]).toHaveStyle({ color: colors.dark_smoke });
    });
  });
});
