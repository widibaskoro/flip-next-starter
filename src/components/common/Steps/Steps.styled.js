import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

export const StepContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  text-align: left;
  width: 100%;
`;

export const StepItem = styled.div`
  display: inline-block;
  margin-right: 8px;
  position: relative;
  width: 100%;

  &:last-child {
    margin-right: 0;
  }
`;

export const StepBar = styled.div`
  background-color: ${colors.light_grey};
  border-radius: 8px;
  height: 5px;

  .is-done > & {
    background-color: ${colors.jade};
  }
`;

export const StepLabel = styled.div`
  color: ${colors.dark_grey};
  font-size: ${typography.font_size.small};
  font-weight: ${typography.font_weight.bold};
  margin-bottom: 4px;
  position: relative;
  text-align: left;

  .is-done > & {
    color: ${colors.dark_smoke};
  }
`;
