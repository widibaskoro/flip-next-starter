import React from "react";
import { render, screen } from "@testing-library/react";

import Label from "./index";
import { checkProps } from "src/helpers/test";
import { colors } from "src/assets/styles/settings";

describe("Label component", () => {
  describe("Label rendering", () => {
    it("Should render without error", () => {
      render(<Label />);
      expect(screen.getByTestId("qa-label")).toBeInTheDocument();
    });
  });

  describe("Label propTypes", () => {
    it("Should not throw warning when expected colors (enum) are provided", () => {
      checkProps(Label, { color: "orange" }, "Label");
      checkProps(Label, { color: "blue" }, "Label");
      checkProps(Label, { color: "green" }, "Label");
      checkProps(Label, { color: "grey" }, "Label");
      checkProps(Label, { color: "red" }, "Label");
      checkProps(Label, { color: "yellow" }, "Label");
      checkProps(Label, { color: "dark-yellow" }, "Label");
    });

    it("Should not throw warning when expected size are provided", () => {
      checkProps(Label, { size: "default" }, "Label");
      checkProps(Label, { size: "large" }, "Label");
    });

    it("should show children text", () => {
      render(<Label>Label</Label>);
      expect(screen.getByTestId("qa-label")).toHaveTextContent("Label");
    });
  });

  describe("Label props", () => {
    it("should have custom className when provided", () => {
      render(<Label className="u-mr-2" />);
      expect(screen.getByTestId("qa-label").className).toContain("u-mr-2");
    });
  });

  describe("Label color", () => {
    it("Should have background flip_orange and color white if no color prop is given", () => {
      render(<Label />);
      const expectedStyle = { background: colors.flip_orange, color: colors.white };
      expect(screen.getByTestId("qa-label")).toHaveStyle(expectedStyle);
    });

    it("Should have background flip_orange and color white when given color: orange", () => {
      render(<Label color="orange" />);
      const expectedStyle = { background: colors.flip_orange, color: colors.white };
      expect(screen.getByTestId("qa-label")).toHaveStyle(expectedStyle);
    });

    it("Should have background intel_blue and color white when given color: blue", () => {
      render(<Label color="blue" />);
      const expectedStyle = { background: colors.intel_blue, color: colors.white };
      expect(screen.getByTestId("qa-label")).toHaveStyle(expectedStyle);
    });

    it("Should have background jade and color white when given color: green", () => {
      render(<Label color="green" />);
      const expectedStyle = { background: colors.jade, color: colors.white };
      expect(screen.getByTestId("qa-label")).toHaveStyle(expectedStyle);
    });

    it("Should have background dark_grey and color white when given color: grey", () => {
      render(<Label color="grey" />);
      const expectedStyle = { background: colors.dark_grey, color: colors.white };
      expect(screen.getByTestId("qa-label")).toHaveStyle(expectedStyle);
    });

    it("Should have background ketchup_tomato and color white when given color: red", () => {
      render(<Label color="red" />);
      const expectedStyle = { background: colors.ketchup_tomato, color: colors.white };
      expect(screen.getByTestId("qa-label")).toHaveStyle(expectedStyle);
    });

    it("Should have background mango_yellow and color white when given color: yellow", () => {
      render(<Label color="yellow" />);
      const expectedStyle = { background: colors.mango_yellow, color: colors.white };
      expect(screen.getByTestId("qa-label")).toHaveStyle(expectedStyle);
    });

    it("Should have background goldenrod and color white when given color: dark-yellow", () => {
      render(<Label color="dark-yellow" />);
      const expectedStyle = { background: colors.goldenrod, color: colors.white };
      expect(screen.getByTestId("qa-label")).toHaveStyle(expectedStyle);
    });
  });
});
