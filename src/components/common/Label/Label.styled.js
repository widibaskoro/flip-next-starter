import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

const typeColors = {
  orange: colors.flip_orange,
  blue: colors.intel_blue,
  green: colors.jade,
  grey: colors.dark_grey,
  red: colors.ketchup_tomato,
  yellow: colors.mango_yellow,
  "dark-yellow": colors.goldenrod
};

const BaseLabel = styled.div`
  background: ${({ color }) => typeColors[color]};
  box-sizing: border-box;
  color: ${colors.white};
  display: inline-block;
  font-weight: ${typography.font_weight.bold};
  line-height: ${typography.line_height};
  text-transform: uppercase;
  white-space: nowrap;
`;

export const DefaultLabel = styled(BaseLabel)`
  border-radius: 20px;
  font-size: ${typography.font_size.label};
  line-height: 9px;
  height: 14px;
  padding: 2px 4px;
`;

export const LargeLabel = styled(BaseLabel)`
  border-radius: 40px;
  font-size: ${typography.font_size.semi};
  font-weight: ${typography.font_weight.normal};
  height: 25px;
  padding: 3px 8px;
`;
