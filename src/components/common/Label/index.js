import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { DefaultLabel, LargeLabel } from "./Label.styled";

const propTypes = {
  /**
   * Set label color
   *
   * @type {"orange" | "blue" | "green" | "grey" | "red" | "yellow" | "dark-yellow"}
   */
  color: PropTypes.oneOf(["orange", "blue", "green", "grey", "red", "yellow", "dark-yellow"]),

  /**
   * Set label size
   *
   * @type {"default" | "large"}
   */
  size: PropTypes.oneOf(["default", "large"])
};

const defaultProps = {
  color: "orange",
  size: "default"
};

const LabelComponent = {
  default: DefaultLabel,
  large: LargeLabel
};

const Label = React.forwardRef((props, ref) => {
  const { children, className, color, size, ...restProps } = props;
  const dotClass = classNames("c-label", className);

  const Comp = LabelComponent[size];

  return (
    <Comp data-testid="qa-label" ref={ref} className={dotClass} color={color} {...restProps}>
      {children}
    </Comp>
  );
});

Label.propTypes = propTypes;
Label.defaultProps = defaultProps;

export default Label;
