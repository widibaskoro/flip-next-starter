import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import ButtonSwitch from "./index";
import { checkProps } from "src/helpers/test";
import { colors, typography } from "src/assets/styles/settings";

describe("ButtonSwitch Components", () => {
  describe("ButtonSwitch rendering", () => {
    it("Should render normally when more than one contents is given", () => {
      render(
        <ButtonSwitch active="button-1">
          <ButtonSwitch.Button key="button-1"></ButtonSwitch.Button>
          <ButtonSwitch.Button key="button-2"></ButtonSwitch.Button>
        </ButtonSwitch>
      );
      expect(screen.getByTestId("qa-button-switch-wrapper")).toBeInTheDocument();
      expect(screen.queryAllByTestId("qa-button-switch")).toHaveLength(2);
    });

    it("Should render normally when no content/child is given", () => {
      render(<ButtonSwitch active="button-1"></ButtonSwitch>);
      expect(screen.getByTestId("qa-button-switch-wrapper")).toBeInTheDocument();
    });

    it("Should throw error if one content is given", () => {
      jest.spyOn(console, "error");
      console.error.mockImplementation(() => {});
      expect(() =>
        render(
          <ButtonSwitch active="button-1">
            <ButtonSwitch.Button key="button-1">button</ButtonSwitch.Button>
          </ButtonSwitch>
        )
      ).toThrow();
      console.error.mockRestore();
    });
  });

  describe("ButtonSwitch functionality", () => {
    it("should calls function provided when clicked", () => {
      const spy = jest.fn();
      render(
        <ButtonSwitch active="button-1">
          <ButtonSwitch.Button key="button-1" onClick={() => spy("x")}></ButtonSwitch.Button>
          <ButtonSwitch.Button key="button-2" onClick={() => spy("x")}></ButtonSwitch.Button>
        </ButtonSwitch>
      );
      fireEvent.click(screen.getAllByTestId("qa-button-switch")[0]);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith("x");
    });
  });

  describe("ButtonSwitch props", () => {
    it("should have custom className when provided", () => {
      render(
        <ButtonSwitch active="button-1" className="u-m-1">
          <ButtonSwitch.Button key="button-1"></ButtonSwitch.Button>
          <ButtonSwitch.Button key="button-2"></ButtonSwitch.Button>
        </ButtonSwitch>
      );
      expect(screen.getByTestId("qa-button-switch-wrapper").className).toContain("u-m-1");
    });

    it("should show children text", () => {
      render(
        <ButtonSwitch active="button-1">
          <ButtonSwitch.Button key="button-1">All</ButtonSwitch.Button>
          <ButtonSwitch.Button key="button-2">Single</ButtonSwitch.Button>
        </ButtonSwitch>
      );
      expect(screen.getAllByTestId("qa-button-switch")[0]).toHaveTextContent("All");
      expect(screen.getAllByTestId("qa-button-switch")[1]).toHaveTextContent("Single");
    });

    it("should be disabled when disabled props is true", () => {
      render(
        <ButtonSwitch active="button-1">
          <ButtonSwitch.Button key="button-1" disabled></ButtonSwitch.Button>
          <ButtonSwitch.Button key="button-2"></ButtonSwitch.Button>
        </ButtonSwitch>
      );
      expect(screen.getAllByTestId("qa-button-switch")[0]).toBeDisabled();
    });
  });

  describe("ButtonSwitch style", () => {
    it("should have expected style when active", () => {
      render(
        <ButtonSwitch active="button-2">
          <ButtonSwitch.Button key="button-1"></ButtonSwitch.Button>
          <ButtonSwitch.Button key="button-2"></ButtonSwitch.Button>
        </ButtonSwitch>
      );
      const expectedStyle = {
        transform: "translateX(100%)"
      };
      expect(screen.getByTestId("qa-button-switch-highlighter")).toHaveStyle(expectedStyle);
    });

    it("should have expected style when disabled", () => {
      render(
        <ButtonSwitch active="button-1">
          <ButtonSwitch.Button key="button-1"></ButtonSwitch.Button>
          <ButtonSwitch.Button key="button-2" disabled></ButtonSwitch.Button>
        </ButtonSwitch>
      );
      const expectedStyle = {
        color: colors.light_grey
      };
      expect(screen.getAllByTestId("qa-button-switch")[1]).toHaveStyle(expectedStyle);
    });
  });
});
