import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { Wrapper, Highlighter, Switch, Button } from "./ButtonSwitch.styled";

const propTypes = {
  /**
   * Child component <ButtonSwitch.Button>
   */
  children: PropTypes.array.isRequired,

  /**
   * Current active key, will be used to highlight button with same key
   */
  active: PropTypes.string.isRequired
};

const defaultProps = {
  children: []
};

const ButtonSwitch = React.forwardRef((props, ref) => {
  const { active, children, className, ...resProps } = props;
  const buttonClass = classNames("c-button-switch", className);

  function isActive(active, children) {
    const index = children.findIndex((item) => item.key === active);
    return index > -1 ? index : 0;
  }

  function renderOneButton({ props: childProps, key }) {
    return (
      <Switch
        data-testid="qa-button-switch"
        className="c-button-switch__button"
        type="button"
        key={key}
        active={key === active}
        disabled={childProps.disabled}
        onClick={childProps.onClick}
      >
        {childProps.children}
      </Switch>
    );
  }

  function renderButtons() {
    return children.map((child) => renderOneButton(child));
  }

  return (
    <Wrapper data-testid="qa-button-switch-wrapper" className={buttonClass} ref={ref} {...resProps}>
      <Highlighter
        data-testid="qa-button-switch-highlighter"
        className="c-button-switch__highlighter"
        active={isActive(active, children)}
        length={children.length}
      />
      {renderButtons()}
    </Wrapper>
  );
});

ButtonSwitch.defaultProps = defaultProps;
ButtonSwitch.propTypes = propTypes;

ButtonSwitch.Button = Button;

export default ButtonSwitch;
