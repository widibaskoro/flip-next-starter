import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

export const Wrapper = styled.div`
  background: ${colors.light_smoke};
  border-radius: 8px;
  box-sizing: border-box;
  display: flex;
  height: 47px;
  padding: 6px;
  position: relative;
`;

export const Highlighter = styled.div`
  background: ${colors.white};
  border-radius: 6px;
  box-shadow: 0px 2px 6px rgba(222, 222, 222, 0.48);
  color: ${colors.black_bekko};
  font-weight: ${typography.font_weight.normal};
  height: calc(100% - 12px);
  line-height: ${typography.line_height};
  position: absolute;
  transition: transform 0.4s ease;
  transform: ${({ active }) => `translateX(${active ? active + "00%" : 0})`};
  width: ${({ length }) => `calc((100% - 12px) / ${length})`};
`;

export const Switch = styled.button`
  align-items: center;
  background: transparent;
  border-radius: 4px;
  border-width: 0;
  color: ${({ active, disabled }) => (disabled ? colors.light_grey : active ? colors.black_bekko : colors.dark_grey)};
  cursor: pointer;
  display: flex;
  flex-grow: 1;
  font-size: ${typography.font_size.base};
  font-weight: ${typography.font_weight.normal};
  line-height: ${typography.line_height};
  justify-content: center;
  height: 100%;
  z-index: 1;

  :focus,
  :active {
    outline: none;
  }
`;

export const Button = styled.button``;
