import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import Dropdown from "./index";
import { resizeMenu } from "./utils";

import { checkProps } from "src/helpers/test";
import { colors } from "src/assets/styles/settings";

const DUMMY_STATUS_OPTIONS = [
  {
    label: "All",
    value: "0"
  },
  {
    label: "Success",
    value: "1"
  },
  {
    label: "Failed",
    value: "2"
  }
];

const DUMMY_CUSTOM_LABEL_OPTIONS = [
  {
    label: "BCA",
    bankLabel: "Custom BCA Label",
    value: "0"
  },
  {
    label: "Mandiri",
    bankLabel: "Custom Mandiri Label",
    value: "1"
  },
  {
    label: "BRI",
    bankLabel: "Custom BRI Label",
    value: "2"
  }
];

const DUMMY_SEARCH_LABEL_OPTIONS = [
  {
    label: "Bank Central Asia",
    keyword: "BCA",
    value: "0"
  },
  {
    label: "Mandiri",
    keyword: "Mandiri",
    value: "1"
  },
  {
    label: "Bank Rakyat Indonesia",
    keyword: "BRI",
    value: "2"
  }
];

describe("Dropdown component", () => {
  describe("Dropdown rendering", () => {
    it("Should render without error and show the input with triangle", () => {
      render(<Dropdown options={[]} />);
      expect(screen.getByTestId("qa-dropdown")).toBeInTheDocument();
      expect(screen.getByTestId("qa-dropdown__input")).toBeInTheDocument();
      expect(screen.getByTestId("qa-dropdown__input-triangle")).toBeInTheDocument();
      expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("Pilih...");
    });
  });

  describe("Dropdown propTypes", () => {
    it("Should not throw warning when expected variant (enum) are provided", () => {
      checkProps(<Dropdown options={[]} />, { variant: "single" }, "Dropdown");
      checkProps(<Dropdown options={[]} />, { variant: "multi" }, "Dropdown");
    });

    it("Should not throw warning when expected menuPosition (enum) are provided", () => {
      checkProps(<Dropdown options={[]} />, { menuPosition: "left" }, "Dropdown");
      checkProps(<Dropdown options={[]} />, { menuPosition: "right" }, "Dropdown");
    });

    it("Should not throw warning when all expected props types provided", () => {
      const defaultProps = {
        selected: {},
        disabled: false,
        variant: "single",
        placeholder: "Select",
        multiLabel: "Selections",
        itemLabel: "label",
        fullWidth: false,
        menuPosition: "left",
        isInvalid: false,
        searchable: false,
        searchLabel: "label",
        searchPlaceholder: "Search options",
        searchFilter: null,
        immediate: false,
        initialCount: 0,
        ghost: false,
        transparent: false,
        inputClass: "",
        onChange: () => {}
      };
      checkProps(<Dropdown options={[]} />, defaultProps, "Dropdown");
    });
  });

  describe("Dropdown placeholder", () => {
    it("Should show the correct placeholder text given", () => {
      render(<Dropdown options={[]} placeholder="Pilih Status" />);
      expect(screen.getByTestId("qa-dropdown__input")).toBeInTheDocument();
      expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("Pilih Status");
    });
  });

  describe("Dropdown initial input value", () => {
    describe("Variant: 'single'", () => {
      it("Should show 'All' as the initial displayed value (selected: first option)", () => {
        const selectedOptions = DUMMY_STATUS_OPTIONS[0];
        render(<Dropdown selected={selectedOptions} options={DUMMY_STATUS_OPTIONS} />);
        expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("All");
      });
      it("Should show 'Pilih...' as the initial displayed value (selected: empty object)", () => {
        const selectedOptions = {};
        render(<Dropdown selected={selectedOptions} options={DUMMY_STATUS_OPTIONS} />);
        expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("Pilih...");
      });
    });
    describe("Variant: 'multi'", () => {
      it("Should show 'Semua Pilihan' as the initial displayed value (selected: all options)", () => {
        const selectedOptions = [...DUMMY_STATUS_OPTIONS];
        render(<Dropdown variant="multi" selected={selectedOptions} options={DUMMY_STATUS_OPTIONS} />);
        expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("Semua Pilihan");
      });
      it("Should show 'Pilih...' as the initial displayed value (selected: empty array)", () => {
        const selectedOptions = [];
        render(<Dropdown variant="multi" selected={selectedOptions} options={DUMMY_STATUS_OPTIONS} />);
        expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("Pilih...");
      });
    });
  });

  describe("Dropdown rendering menu", () => {
    describe("Variant: 'single'", () => {
      it("Should open the menu when the input is clicked", () => {
        render(<Dropdown options={[]} />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__menu-list")).toBeInTheDocument();
        expect(screen.queryByTestId("qa-dropdown__menu-multi")).not.toBeInTheDocument();
      });

      it("Should show no data info when options is empty array", () => {
        render(<Dropdown options={[]} />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu-list")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__menu-empty")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__menu-empty")).toHaveTextContent("Tidak ada pilihan");
      });

      it("Should show menu with correct options label", () => {
        render(<Dropdown options={DUMMY_STATUS_OPTIONS} />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu-list")).toBeInTheDocument();
        expect(screen.getAllByTestId("qa-dropdown__menu-item")).toHaveLength(3);
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[0]).toHaveTextContent("All");
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[1]).toHaveTextContent("Success");
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[2]).toHaveTextContent("Failed");
      });
    });

    describe("Variant: 'multi'", () => {
      it("Should open the menu when the input is clicked", () => {
        render(<Dropdown options={[]} variant="multi" />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu-multi")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__menu-multi-list")).toBeInTheDocument();
        expect(screen.queryByTestId("qa-dropdown__menu")).not.toBeInTheDocument();
      });

      it("Should show no data info when options is empty array", () => {
        render(<Dropdown options={[]} variant="multi" />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu-multi-list")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__menu-multi-empty")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__menu-multi-empty")).toHaveTextContent("Tidak ada pilihan");
      });

      it("Should show menu with correct options label", () => {
        render(<Dropdown options={DUMMY_STATUS_OPTIONS} variant="multi" />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu-multi-list")).toBeInTheDocument();
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")).toHaveLength(3);
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[0]).toHaveTextContent("All");
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[1]).toHaveTextContent("Success");
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[2]).toHaveTextContent("Failed");
      });
    });
  });

  describe("Dropdown rendering menu with initial selected value", () => {
    it("Should render item with correct 'is-selected' class name -> variant: 'single'", () => {
      const selectedOptions = DUMMY_STATUS_OPTIONS[0];
      render(<Dropdown selected={selectedOptions} options={DUMMY_STATUS_OPTIONS} />);
      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      expect(screen.getAllByTestId("qa-dropdown__menu-item")[0]).toHaveClass("is-selected");
      expect(screen.getAllByTestId("qa-dropdown__menu-item")[1]).not.toHaveClass("is-selected");
      expect(screen.getAllByTestId("qa-dropdown__menu-item")[2]).not.toHaveClass("is-selected");
      expect(screen.getAllByTestId("qa-dropdown__menu-item")[0]).toHaveStyle({
        background: colors.flip_orange,
        color: colors.white
      });
    });

    it("Should render item with correct 'is-selected' class name -> variant: 'multi'", () => {
      const selectedOptions = [DUMMY_STATUS_OPTIONS[0], DUMMY_STATUS_OPTIONS[1]];
      render(<Dropdown variant="multi" selected={selectedOptions} options={DUMMY_STATUS_OPTIONS} />);
      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[0]).toHaveClass("is-selected");
      expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[1]).toHaveClass("is-selected");
      expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[2]).not.toHaveClass("is-selected");
    });
  });

  describe("Dropdown select functionalities", () => {
    describe("Variant: 'single'", () => {
      it("Should return the correct selected value, close menu, and show the value in the input", () => {
        const spy = jest.fn();
        render(<Dropdown options={DUMMY_STATUS_OPTIONS} onChange={(value) => spy(value)} />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        fireEvent.click(screen.getByText("Success"));
        expect(spy).toHaveBeenCalledWith(DUMMY_STATUS_OPTIONS[1]);
        expect(screen.queryByTestId("qa-dropdown__menu")).not.toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("Success");
      });
    });

    describe("Variant: 'multi' (returns the correct selected value then close menu when blanket clicked and show the value in the input)", () => {
      it("Should do the expected result: 1 option selected -> show 'Success' as value", () => {
        const spy = jest.fn();
        render(<Dropdown variant="multi" options={DUMMY_STATUS_OPTIONS} onChange={(value) => spy(value)} />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        const dropdownItems = screen.getAllByTestId("qa-dropdown__menu-multi-item-checkbox");

        fireEvent.click(dropdownItems[1]);
        expect(spy).not.toHaveBeenCalled();
        expect(dropdownItems[0].checked).toEqual(false);
        expect(dropdownItems[1].checked).toEqual(true);
        expect(dropdownItems[2].checked).toEqual(false);
        expect(screen.queryByTestId("qa-dropdown__menu-multi")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("Success");

        fireEvent.click(screen.getByTestId("qa-dropdown__blanket"));
        expect(spy).toHaveBeenCalledWith([DUMMY_STATUS_OPTIONS[1]]);
      });

      it("Should do the expected result: 2 options selected -> show '2 Pilihan' as value", () => {
        const spy = jest.fn();
        render(<Dropdown variant="multi" options={DUMMY_STATUS_OPTIONS} onChange={(value) => spy(value)} />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        const dropdownItems = screen.getAllByTestId("qa-dropdown__menu-multi-item-checkbox");

        fireEvent.click(dropdownItems[1]);
        expect(dropdownItems[0].checked).toEqual(false);
        expect(dropdownItems[1].checked).toEqual(true);
        expect(dropdownItems[2].checked).toEqual(false);
        expect(screen.queryByTestId("qa-dropdown__menu-multi")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("Success");
        expect(spy).not.toHaveBeenCalled();

        fireEvent.click(dropdownItems[2]);
        expect(dropdownItems[0].checked).toEqual(false);
        expect(dropdownItems[1].checked).toEqual(true);
        expect(dropdownItems[2].checked).toEqual(true);
        expect(screen.queryByTestId("qa-dropdown__menu-multi")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("2 Pilihan");
        expect(spy).not.toHaveBeenCalled();

        fireEvent.click(screen.getByTestId("qa-dropdown__blanket"));
        expect(spy).toHaveBeenCalledWith([DUMMY_STATUS_OPTIONS[1], DUMMY_STATUS_OPTIONS[2]]);
      });

      it("Should do the expected result: select 2 then remove 1 -> show 'Success' as value", () => {
        const spy = jest.fn();
        render(<Dropdown variant="multi" options={DUMMY_STATUS_OPTIONS} onChange={(value) => spy(value)} />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        const dropdownItems = screen.getAllByTestId("qa-dropdown__menu-multi-item-checkbox");

        fireEvent.click(dropdownItems[1]);
        fireEvent.click(dropdownItems[2]);
        expect(dropdownItems[0].checked).toEqual(false);
        expect(dropdownItems[1].checked).toEqual(true);
        expect(dropdownItems[2].checked).toEqual(true);
        expect(screen.queryByTestId("qa-dropdown__menu-multi")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("2 Pilihan");
        expect(spy).not.toHaveBeenCalled();

        fireEvent.click(dropdownItems[2]);
        expect(dropdownItems[0].checked).toEqual(false);
        expect(dropdownItems[1].checked).toEqual(true);
        expect(dropdownItems[2].checked).toEqual(false);
        expect(screen.queryByTestId("qa-dropdown__menu-multi")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("Success");
        expect(spy).not.toHaveBeenCalled();

        fireEvent.click(screen.getByTestId("qa-dropdown__blanket"));
        expect(spy).toHaveBeenCalledWith([DUMMY_STATUS_OPTIONS[1]]);
      });
    });

    describe("Button action -> variant: 'multi'", () => {
      it("Should check all options", () => {
        const spy = jest.fn();
        render(<Dropdown variant="multi" options={DUMMY_STATUS_OPTIONS} onChange={(value) => spy(value)} />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        const dropdownItems = screen.getAllByTestId("qa-dropdown__menu-multi-item-checkbox");

        expect(screen.getByTestId("qa-dropdown__menu-multi-action-button")).toHaveTextContent("Centang Semua");
        expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("Pilih...");
        expect(dropdownItems[0].checked).toEqual(false);
        expect(dropdownItems[1].checked).toEqual(false);
        expect(dropdownItems[2].checked).toEqual(false);
        expect(spy).not.toHaveBeenCalled();
        expect(screen.queryByTestId("qa-dropdown__menu-multi")).toBeInTheDocument();

        fireEvent.click(screen.getByTestId("qa-dropdown__menu-multi-action-button"));
        expect(screen.getByTestId("qa-dropdown__menu-multi-action-button")).toHaveTextContent("Hapus Semua Centang");
        expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("Semua Pilihan");
        expect(dropdownItems[0].checked).toEqual(true);
        expect(dropdownItems[1].checked).toEqual(true);
        expect(dropdownItems[2].checked).toEqual(true);

        fireEvent.click(screen.getByTestId("qa-dropdown__blanket"));
        expect(spy).toHaveBeenCalledWith(DUMMY_STATUS_OPTIONS);
      });

      it("Should check all options initially, then uncheck all options when the button is clicked", () => {
        const selectedOptions = [...DUMMY_STATUS_OPTIONS];
        const spy = jest.fn();
        render(
          <Dropdown
            variant="multi"
            selected={selectedOptions}
            options={DUMMY_STATUS_OPTIONS}
            onChange={(value) => spy(value)}
          />
        );
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        const dropdownItems = screen.getAllByTestId("qa-dropdown__menu-multi-item-checkbox");
        expect(screen.getByTestId("qa-dropdown__menu-multi-action-button")).toHaveTextContent("Hapus Semua Centang");
        expect(dropdownItems[0].checked).toEqual(true);
        expect(dropdownItems[1].checked).toEqual(true);
        expect(dropdownItems[2].checked).toEqual(true);

        fireEvent.click(screen.getByTestId("qa-dropdown__menu-multi-action-button"));
        expect(screen.getByTestId("qa-dropdown__menu-multi-action-button")).toHaveTextContent("Centang Semua");
        expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("Pilih...");
        expect(dropdownItems[0].checked).toEqual(false);
        expect(dropdownItems[1].checked).toEqual(false);
        expect(dropdownItems[2].checked).toEqual(false);

        fireEvent.click(screen.getByTestId("qa-dropdown__blanket"));
        expect(spy).toHaveBeenCalledWith([]);
      });

      it("Should check all options initially, then change the button text when all options are unchecked", () => {
        const selectedOptions = [...DUMMY_STATUS_OPTIONS];
        const spy = jest.fn();
        render(
          <Dropdown
            variant="multi"
            selected={selectedOptions}
            options={DUMMY_STATUS_OPTIONS}
            onChange={(value) => spy(value)}
          />
        );
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu-multi-action-button")).toHaveTextContent("Hapus Semua Centang");

        fireEvent.click(screen.getAllByTestId("qa-dropdown__menu-multi-item-checkbox")[0]);
        expect(screen.getByTestId("qa-dropdown__menu-multi-action-button")).toHaveTextContent("Hapus Semua Centang");

        fireEvent.click(screen.getAllByTestId("qa-dropdown__menu-multi-item-checkbox")[1]);
        fireEvent.click(screen.getAllByTestId("qa-dropdown__menu-multi-item-checkbox")[2]);
        expect(screen.getByTestId("qa-dropdown__menu-multi-action-button")).toHaveTextContent("Centang Semua");
      });
    });
  });

  describe("Dropdown state -> itemLabel: 'bankLabel'", () => {
    describe("Variant: 'single'", () => {
      it("Should show the bankLabel contents as the option items and return the correct option when selected", () => {
        const spy = jest.fn();
        render(
          <Dropdown itemLabel="bankLabel" options={DUMMY_CUSTOM_LABEL_OPTIONS} onChange={(value) => spy(value)} />
        );
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getAllByTestId("qa-dropdown__menu-item")).toHaveLength(3);
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[0]).toHaveTextContent("Custom BCA Label");
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[1]).toHaveTextContent("Custom Mandiri Label");
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[2]).toHaveTextContent("Custom BRI Label");

        fireEvent.click(screen.getByText("Custom Mandiri Label"));
        expect(spy).toHaveBeenCalledWith(DUMMY_CUSTOM_LABEL_OPTIONS[1]);
        expect(screen.queryByTestId("qa-dropdown__menu")).not.toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("Mandiri");
      });
    });

    describe("Variant: 'multi'", () => {
      it("Should show the bankLabel contents as the option items and return the correct option when selected", () => {
        const spy = jest.fn();
        render(
          <Dropdown
            variant="multi"
            itemLabel="bankLabel"
            options={DUMMY_CUSTOM_LABEL_OPTIONS}
            onChange={(value) => spy(value)}
          />
        );
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")).toHaveLength(3);
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[0]).toHaveTextContent("Custom BCA Label");
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[1]).toHaveTextContent("Custom Mandiri Label");
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[2]).toHaveTextContent("Custom BRI Label");

        fireEvent.click(screen.getAllByTestId("qa-dropdown__menu-multi-item-checkbox")[1]);
        fireEvent.click(screen.getByTestId("qa-dropdown__blanket"));
        expect(spy).toHaveBeenCalledWith([DUMMY_CUSTOM_LABEL_OPTIONS[1]]);
        expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("Mandiri");
      });
    });
  });

  describe("Dropdown state and functionalities -> searchable", () => {
    describe("Variant: 'single'", () => {
      it("Should do the filter based on label and show correct results", () => {
        render(<Dropdown options={DUMMY_SEARCH_LABEL_OPTIONS} searchable />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__search-input")).toBeInTheDocument();
        expect(screen.getAllByTestId("qa-dropdown__menu-item")).toHaveLength(3);
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[0]).toHaveTextContent("Bank Central Asia");
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[1]).toHaveTextContent("Mandiri");
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[2]).toHaveTextContent("Bank Rakyat Indonesia");

        fireEvent.change(screen.getByTestId("qa-dropdown__search-input"), { target: { value: "Bank" } });
        expect(screen.getAllByTestId("qa-dropdown__menu-item")).toHaveLength(2);
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[0]).toHaveTextContent("Bank Central Asia");
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[1]).toHaveTextContent("Bank Rakyat Indonesia");
      });

      it("Should clear the search input and reset the options to all items when clear button is clicked", () => {
        render(<Dropdown options={DUMMY_SEARCH_LABEL_OPTIONS} searchable />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__search-input")).toBeInTheDocument();

        fireEvent.change(screen.getByTestId("qa-dropdown__search-input"), { target: { value: "Bank" } });
        expect(screen.getAllByTestId("qa-dropdown__menu-item")).toHaveLength(2);
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[0]).toHaveTextContent("Bank Central Asia");
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[1]).toHaveTextContent("Bank Rakyat Indonesia");

        fireEvent.click(screen.getByTestId("qa-dropdown__search-clear"));
        expect(screen.getByTestId("qa-dropdown__search-input").value).toBe("");
        expect(screen.getAllByTestId("qa-dropdown__menu-item")).toHaveLength(3);
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[0]).toHaveTextContent("Bank Central Asia");
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[1]).toHaveTextContent("Mandiri");
        expect(screen.getAllByTestId("qa-dropdown__menu-item")[2]).toHaveTextContent("Bank Rakyat Indonesia");
      });

      it("Should show the empty option info if search query matches no options", () => {
        render(<Dropdown options={DUMMY_SEARCH_LABEL_OPTIONS} searchable />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__search-input")).toBeInTheDocument();

        fireEvent.change(screen.getByTestId("qa-dropdown__search-input"), { target: { value: "Lorem ipsum" } });
        expect(screen.queryAllByTestId("qa-dropdown__menu-item")).toHaveLength(0);
        expect(screen.getByTestId("qa-dropdown__menu-empty")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__menu-empty")).toHaveTextContent("Tidak ada pilihan");
      });
    });

    describe("Variant: 'multi'", () => {
      it("Should do the filter based on label and show correct results", () => {
        render(<Dropdown variant="multi" options={DUMMY_SEARCH_LABEL_OPTIONS} searchable />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__search-input")).toBeInTheDocument();
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")).toHaveLength(3);
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[0]).toHaveTextContent("Bank Central Asia");
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[1]).toHaveTextContent("Mandiri");
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[2]).toHaveTextContent("Bank Rakyat Indonesia");

        fireEvent.change(screen.getByTestId("qa-dropdown__search-input"), { target: { value: "Bank" } });
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")).toHaveLength(2);
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[0]).toHaveTextContent("Bank Central Asia");
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[1]).toHaveTextContent("Bank Rakyat Indonesia");
      });

      it("Should clear the search input and reset the options to all items when clear button is clicked", () => {
        render(<Dropdown variant="multi" options={DUMMY_SEARCH_LABEL_OPTIONS} searchable />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__search-input")).toBeInTheDocument();

        fireEvent.change(screen.getByTestId("qa-dropdown__search-input"), { target: { value: "Bank" } });
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")).toHaveLength(2);
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[0]).toHaveTextContent("Bank Central Asia");
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[1]).toHaveTextContent("Bank Rakyat Indonesia");

        fireEvent.click(screen.getByTestId("qa-dropdown__search-clear"));
        expect(screen.getByTestId("qa-dropdown__search-input").value).toBe("");
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")).toHaveLength(3);
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[0]).toHaveTextContent("Bank Central Asia");
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[1]).toHaveTextContent("Mandiri");
        expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[2]).toHaveTextContent("Bank Rakyat Indonesia");
      });

      it("Should show the empty option info if search query matches no options", () => {
        render(<Dropdown variant="multi" options={DUMMY_SEARCH_LABEL_OPTIONS} searchable />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__search-input")).toBeInTheDocument();

        fireEvent.change(screen.getByTestId("qa-dropdown__search-input"), { target: { value: "Lorem ipsum" } });
        expect(screen.queryAllByTestId("qa-dropdown__menu-multi-item")).toHaveLength(0);
        expect(screen.getByTestId("qa-dropdown__menu-multi-empty")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__menu-multi-empty")).toHaveTextContent("Tidak ada pilihan");
      });
    });
  });

  describe("Dropdown state and functionalities -> searchable (with searchLabel: 'keyword')", () => {
    it("Should do the filter based on 'keyword' property and show correct results -> variant: 'single'", () => {
      render(<Dropdown options={DUMMY_SEARCH_LABEL_OPTIONS} searchLabel="keyword" searchable />);
      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      expect(screen.getByTestId("qa-dropdown__search-input")).toBeInTheDocument();
      expect(screen.getAllByTestId("qa-dropdown__menu-item")).toHaveLength(3);
      expect(screen.getAllByTestId("qa-dropdown__menu-item")[0]).toHaveTextContent("Bank Central Asia");
      expect(screen.getAllByTestId("qa-dropdown__menu-item")[1]).toHaveTextContent("Mandiri");
      expect(screen.getAllByTestId("qa-dropdown__menu-item")[2]).toHaveTextContent("Bank Rakyat Indonesia");

      fireEvent.change(screen.getByTestId("qa-dropdown__search-input"), { target: { value: "Bank" } });
      expect(screen.queryAllByTestId("qa-dropdown__menu-item")).toHaveLength(0);

      fireEvent.change(screen.getByTestId("qa-dropdown__search-input"), { target: { value: "BCA" } });
      expect(screen.queryAllByTestId("qa-dropdown__menu-item")).toHaveLength(1);
      expect(screen.getAllByTestId("qa-dropdown__menu-item")[0]).toHaveTextContent("Bank Central Asia");
    });

    it("Should do the filter based on 'keyword' property and show correct results -> variant: 'multi'", () => {
      render(<Dropdown variant="multi" options={DUMMY_SEARCH_LABEL_OPTIONS} searchLabel="keyword" searchable />);
      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      expect(screen.getByTestId("qa-dropdown__search-input")).toBeInTheDocument();
      expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")).toHaveLength(3);
      expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[0]).toHaveTextContent("Bank Central Asia");
      expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[1]).toHaveTextContent("Mandiri");
      expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[2]).toHaveTextContent("Bank Rakyat Indonesia");

      fireEvent.change(screen.getByTestId("qa-dropdown__search-input"), { target: { value: "Bank" } });
      expect(screen.queryAllByTestId("qa-dropdown__menu-multi-item")).toHaveLength(0);

      fireEvent.change(screen.getByTestId("qa-dropdown__search-input"), { target: { value: "BCA" } });
      expect(screen.queryAllByTestId("qa-dropdown__menu-multi-item")).toHaveLength(1);
      expect(screen.getAllByTestId("qa-dropdown__menu-multi-item")[0]).toHaveTextContent("Bank Central Asia");
    });
  });

  describe("Dropdown state and functionalities -> searchable (with searchFilter: spy function)", () => {
    it("Should return the given filter function with correct searchQuery and current options -> variant: 'single'", () => {
      const spy = jest.fn(() => []);
      render(<Dropdown options={DUMMY_SEARCH_LABEL_OPTIONS} searchFilter={spy} searchable />);
      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      fireEvent.change(screen.getByTestId("qa-dropdown__search-input"), { target: { value: "Bank" } });
      expect(spy).toHaveBeenCalledWith("bank", DUMMY_SEARCH_LABEL_OPTIONS);
    });
  });

  describe("Dropdown state -> immediate (only for variant: 'multi')", () => {
    it("Should call the onChange function after one option is clicked", () => {
      const spy = jest.fn();
      render(<Dropdown variant="multi" options={DUMMY_STATUS_OPTIONS} onChange={(value) => spy(value)} immediate />);
      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      const dropdownItems = screen.getAllByTestId("qa-dropdown__menu-multi-item-checkbox");

      fireEvent.click(dropdownItems[1]);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith([DUMMY_STATUS_OPTIONS[1]]);
      expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("Success");
      expect(dropdownItems[0].checked).toEqual(false);
      expect(dropdownItems[1].checked).toEqual(true);
      expect(dropdownItems[2].checked).toEqual(false);
      expect(screen.queryByTestId("qa-dropdown__menu-multi")).toBeInTheDocument();

      fireEvent.click(screen.getByTestId("qa-dropdown__blanket"));
      expect(spy).not.toHaveBeenCalledTimes(2);
    });
  });

  describe("Dropdown state -> initialCount: 3 (only for variant: 'multi')", () => {
    it("Should show '3 Pilihan' as the initial text in the input then change accordingly", () => {
      render(<Dropdown variant="multi" options={DUMMY_STATUS_OPTIONS} initialCount={3} />);
      expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("3 Pilihan");

      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      fireEvent.click(screen.getAllByTestId("qa-dropdown__menu-multi-item-checkbox")[1]);
      expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("4 Pilihan");
    });
  });

  describe("Dropdown state -> multiLabel: 'Status' (only for variant: 'multi')", () => {
    it("Should show '3 Status' as the initial text in the input then change accordingly", () => {
      render(<Dropdown variant="multi" options={DUMMY_STATUS_OPTIONS} multiLabel="Status" />);
      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      fireEvent.click(screen.getAllByTestId("qa-dropdown__menu-multi-item-checkbox")[0]);
      fireEvent.click(screen.getAllByTestId("qa-dropdown__menu-multi-item-checkbox")[1]);
      expect(screen.getByTestId("qa-dropdown__input")).toHaveTextContent("2 Status");
    });
  });

  describe("Dropdown state -> disabled", () => {
    it("Should NOT open the menu when the input is clicked -> variant: 'single'", () => {
      render(<Dropdown options={[]} disabled />);
      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      expect(screen.queryByTestId("qa-dropdown__menu")).not.toBeInTheDocument();
      expect(screen.queryByTestId("qa-dropdown__menu-list")).not.toBeInTheDocument();
    });

    it("Should NOT open the menu when the input is clicked -> variant: 'multi'", () => {
      render(<Dropdown options={[]} variant="multi" disabled />);
      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      expect(screen.queryByTestId("qa-dropdown__menu-multi")).not.toBeInTheDocument();
      expect(screen.queryByTestId("qa-dropdown__menu-multi-list")).not.toBeInTheDocument();
    });
  });

  describe("Dropdown state -> isInvalid", () => {
    it("Should show the red border", () => {
      render(<Dropdown options={[]} isInvalid />);
      expect(screen.getByTestId("qa-dropdown__input")).toHaveStyle({ "border-color": colors.ketchup_tomato });
    });
  });

  describe("Dropdown state -> fullWidth", () => {
    it("Should show the full width menu -> variant: 'single'", () => {
      render(<Dropdown options={[]} fullWidth />);
      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      expect(screen.getByTestId("qa-dropdown__menu")).toBeInTheDocument();
      expect(screen.getByTestId("qa-dropdown__menu")).toHaveStyle({ width: "100%" });
    });

    it("Should show the full width menu -> variant: 'multi'", () => {
      render(<Dropdown options={[]} variant="multi" fullWidth />);
      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      expect(screen.getByTestId("qa-dropdown__menu-multi")).toBeInTheDocument();
      expect(screen.getByTestId("qa-dropdown__menu-multi")).toHaveStyle({ width: "100%" });
    });
  });

  describe("Dropdown state -> menuPosition", () => {
    describe("menuPosition: undefined", () => {
      it("Should show the full width menu -> variant: 'single'", () => {
        render(<Dropdown options={[]} />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__menu")).toHaveStyle({ left: 0, right: "unset" });
      });

      it("Should show the full width menu -> variant: 'multi'", () => {
        render(<Dropdown options={[]} variant="multi" />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu-multi")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__menu-multi")).toHaveStyle({ left: 0, right: "unset" });
      });
    });

    describe("menuPosition: 'left'", () => {
      it("Should show the full width menu -> variant: 'single'", () => {
        render(<Dropdown options={[]} menuPosition="left" />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__menu")).toHaveStyle({ left: 0, right: "unset" });
      });

      it("Should show the full width menu -> variant: 'multi'", () => {
        render(<Dropdown options={[]} variant="multi" menuPosition="left" />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu-multi")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__menu-multi")).toHaveStyle({ left: 0, right: "unset" });
      });
    });

    describe("menuPosition: 'right'", () => {
      it("Should show the full width menu -> variant: 'single'", () => {
        render(<Dropdown options={[]} menuPosition="right" />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__menu")).toHaveStyle({ left: "unset", right: 0 });
      });

      it("Should show the full width menu -> variant: 'multi'", () => {
        render(<Dropdown options={[]} variant="multi" menuPosition="right" />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu-multi")).toBeInTheDocument();
        expect(screen.getByTestId("qa-dropdown__menu-multi")).toHaveStyle({ left: "unset", right: 0 });
      });
    });
  });

  describe("Dropdown state -> transparent", () => {
    it("Should change the input background to TRANSPARENT and color to DARK SMOKE -> variant: 'single'", () => {
      render(<Dropdown options={[]} transparent />);
      expect(screen.getByTestId("qa-dropdown__input")).toHaveStyle({
        background: "transparent",
        color: colors.dark_smoke
      });
      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      expect(screen.getByTestId("qa-dropdown__menu")).toHaveStyle({ background: colors.white });
    });

    it("Should change the input background to TRANSPARENT and color to DARK SMOKE -> variant: 'multi'", () => {
      render(<Dropdown options={[]} variant="multi" transparent />);
      expect(screen.getByTestId("qa-dropdown__input")).toHaveStyle({
        background: "transparent",
        color: colors.dark_smoke
      });
      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      expect(screen.getByTestId("qa-dropdown__menu-multi")).toHaveStyle({ background: colors.white });
    });
  });

  describe("Dropdown state -> ghost", () => {
    it("Should change the input background to TRANSPARENT and color to WHITE -> variant: 'single'", () => {
      render(<Dropdown options={[]} ghost />);
      expect(screen.getByTestId("qa-dropdown__input")).toHaveStyle({
        background: "transparent",
        color: colors.white
      });
      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      expect(screen.getByTestId("qa-dropdown__menu")).toHaveStyle({ background: colors.white });
    });

    it("Should change the input background to TRANSPARENT and color to WHITE -> variant: 'multi'", () => {
      render(<Dropdown options={[]} variant="multi" ghost />);
      expect(screen.getByTestId("qa-dropdown__input")).toHaveStyle({
        background: "transparent",
        color: colors.white
      });
      fireEvent.click(screen.getByTestId("qa-dropdown__input"));
      expect(screen.getByTestId("qa-dropdown__menu-multi")).toHaveStyle({ background: colors.white });
    });
  });

  describe("Dropdown resize menu utility function", () => {
    const MAX_HEIGHT_THRESHOLD = 400;
    const realInnerHeight = global.innerHeight;

    afterAll(() => {
      global.innerHeight = realInnerHeight;
    });

    describe("Max height greater than threshold (700px)", () => {
      const maxHeight = MAX_HEIGHT_THRESHOLD + 300; // 700
      const expectedHeight = MAX_HEIGHT_THRESHOLD; // 400

      it("Should set max height to 400px -> variant: 'single'", () => {
        global.innerHeight = maxHeight;
        render(<Dropdown options={[]} />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu")).toHaveStyle({ "max-height": expectedHeight + "px" });
      });

      it("Should set max height to 400px -> variant: 'multi'", () => {
        global.innerHeight = maxHeight;
        render(<Dropdown variant="multi" options={[]} />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu-multi")).toHaveStyle({ "max-height": expectedHeight + "px" });
      });
    });

    describe("Max height as the same as the threshold (400px)", () => {
      const maxHeight = MAX_HEIGHT_THRESHOLD; // 400
      const expectedHeight = MAX_HEIGHT_THRESHOLD - 10; // 390

      it("Should set max height to 390px -> variant: 'single'", () => {
        global.innerHeight = maxHeight;
        render(<Dropdown options={[]} />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu")).toHaveStyle({ "max-height": expectedHeight + "px" });
      });

      it("Should set max height to 390px -> variant: 'multi'", () => {
        global.innerHeight = maxHeight;
        render(<Dropdown variant="multi" options={[]} />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu-multi")).toHaveStyle({ "max-height": expectedHeight + "px" });
      });
    });

    describe("Max height less than threshold (300px)", () => {
      const maxHeight = MAX_HEIGHT_THRESHOLD - 100; // 300
      const expectedHeight = maxHeight - 10; // 290

      it("Should set max height to 290px -> variant: 'single'", () => {
        global.innerHeight = maxHeight;
        render(<Dropdown options={[]} />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu")).toHaveStyle({ "max-height": expectedHeight + "px" });
      });

      it("Should set max height to 290px -> variant: 'multi'", () => {
        global.innerHeight = maxHeight;
        render(<Dropdown variant="multi" options={[]} />);
        fireEvent.click(screen.getByTestId("qa-dropdown__input"));
        expect(screen.getByTestId("qa-dropdown__menu-multi")).toHaveStyle({ "max-height": expectedHeight + "px" });
      });
    });

    describe("resizeMenu function when getBoundingClientRect function is undefined", () => {
      const maxHeight = MAX_HEIGHT_THRESHOLD - 100; // 300
      const clientRectTop = 50;

      it("Should change the maxHeight value to 240", () => {
        global.innerHeight = maxHeight;
        const expectedHeight = maxHeight - clientRectTop - 10; // 240
        const menuRef = {
          current: {
            style: { maxHeight: 10 },
            getBoundingClientRect() {
              return { top: clientRectTop };
            }
          }
        };
        resizeMenu(menuRef);
        expect(menuRef.current.style.maxHeight).toBe(expectedHeight + "px");
      });

      it("Should NOT change the maxHeight value", () => {
        global.innerHeight = maxHeight;
        const menuRef = {
          current: {
            style: { maxHeight: 10 }
          }
        };
        resizeMenu(menuRef);
        expect(menuRef.current.style.maxHeight).toBe(10);
      });
    });
  });
});
