import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import DropdownInput from "./components/DropdownInput";
import DropdownMenu from "./components/DropdownMenu";
import DropdownMenuMulti from "./components/DropdownMenuMulti";
import { DropdownWrapper, DropdownBlanket, DropdownFeedback } from "./Dropdown.styled";

import { getTranslationManually } from "src/helpers/locale";

const propTypes = {
  /**
   * set dropdown options
   */
  options: PropTypes.array.isRequired,

  /**
   * set dropdown value
   */
  selected: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

  /**
   * set dropdown variant
   *
   * @type {"single" | "multi"}
   */
  variant: PropTypes.oneOf(["single", "multi"]),

  /**
   * set dropdown to be disabled
   */
  disabled: PropTypes.bool,

  /**
   * set dropdown input placeholder
   */
  placeholder: PropTypes.string,

  /**
   * set dropdown input label when multiple values are selcted
   * eg: 2 Status, 3 Bank
   */
  multiLabel: PropTypes.string,

  /**
   * set which property from options should be shown in menu
   * eg:
   * const opts = { label: ..., value: ..., customLabel: ... }
   * then itemLabel="customLabel"
   *
   * default: label
   */
  itemLabel: PropTypes.string,

  /**
   * set dropdown menu to be as the same width as input
   */
  fullWidth: PropTypes.bool,

  /**
   * set dropdown menu to be aligned to the left or right of the input
   *
   * @type {"left" | "right"}
   */
  menuPosition: PropTypes.oneOf(["left", "right"]),

  /**
   * set dropdown is invalid
   */
  isInvalid: PropTypes.bool,

  /**
   * set dropdown options can be searched
   */
  searchable: PropTypes.bool,

  /**
   * set which property from options should be used in search
   */
  searchLabel: PropTypes.string,

  /**
   * set placeholder for search bar
   */
  searchPlaceholder: PropTypes.string,

  /**
   * callback function after item has been selected
   */
  searchFilter: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),

  /**
   * set dropdown multi to trigger change immediately after an option is selected
   */
  immediate: PropTypes.bool,

  /**
   * set dropdown multi initial (default) count. So if none is selected, it will show this number
   */
  initialCount: PropTypes.number,

  /**
   * set dropdown input color and border to white
   */
  ghost: PropTypes.bool,

  /**
   * set dropdown input background to transparent
   */
  transparent: PropTypes.bool,

  /**
   * callback function after item has been selected
   */
  onChange: PropTypes.func,

  /**
   * custom class for DropdownInput
   */
  inputClass: PropTypes.string
};

const defaultProps = {
  selected: null,
  disabled: false,
  variant: "single",
  placeholder: getTranslationManually("Pilih...", "Select..."),
  multiLabel: getTranslationManually("Pilihan", "Selections"),
  itemLabel: "label",
  fullWidth: false,
  menuPosition: "left",
  isInvalid: false,
  searchable: false,
  searchLabel: "label",
  searchPlaceholder: getTranslationManually("Cari pilihan", "Search options"),
  searchFilter: null,
  immediate: false,
  initialCount: 0,
  ghost: false,
  transparent: false,
  inputClass: "",
  onChange: () => {}
};

const Dropdown = React.forwardRef((props, ref) => {
  const {
    className,
    options,
    selected,
    variant,
    disabled,
    placeholder,
    multiLabel,
    itemLabel,
    fullWidth,
    menuPosition,
    isInvalid,
    searchable,
    searchLabel,
    searchPlaceholder,
    searchFilter,
    immediate,
    initialCount,
    ghost,
    transparent,
    onChange,
    inputClass,
    ...restProps
  } = props;
  const dropdownClass = classNames("c-dropdown", { "is-invalid": isInvalid }, className);

  const [isOpen, setIsOpen] = useState(false);
  const [displayedValue, setDisplayedValue] = useState(getInitialDisplayed());
  const [menuOptions, setMenuOptions] = useState([]);

  const [selectedSingle, setSelectedSingle] = useState({});
  const [selectedMulti, setSelectedMulti] = useState([]);
  const [selectedValuesMulti, setSelectedValuesMulti] = useState([]);

  useEffect(() => {
    if (selected) {
      if (variant === "multi") {
        if (selected.length > 0) {
          setSelectedMulti(selected);
          setSelectedValuesMulti(selected.map((item) => item.value));
          setDisplayedValue(getDisplayedValue(selected));
        } else {
          setSelectedMulti([]);
          setSelectedValuesMulti([]);
          setDisplayedValue(getInitialDisplayed());
        }
      } else {
        if (Object.keys(selected).length > 0) {
          setSelectedSingle(selected);
          setDisplayedValue(selected.label);
        } else {
          setSelectedSingle({});
          setDisplayedValue(placeholder);
        }
      }
    }
  }, [selected, placeholder]);

  function getInitialDisplayed() {
    return variant === "multi" && initialCount > 0 ? `${initialCount} ${multiLabel}` : placeholder;
  }

  function getDisplayedValue(items) {
    let displayed = placeholder;
    if (items.length === 1) displayed = items[0].label;
    if (items.length > 1) displayed = `${items.length} ${multiLabel}`;
    if (initialCount > 0) displayed = `${initialCount + items.length} ${multiLabel}`;
    if (items.length === options.length) displayed = `${getTranslationManually("Semua", "All")} ${multiLabel}`;
    return displayed;
  }

  function handleItemSelected(item) {
    setSelectedSingle(item);
    setDisplayedValue(item.label);
    onChange(item);
    setIsOpen(false);
  }

  function handleItemSelectedMulti(items, values) {
    setSelectedMulti(items);
    setSelectedValuesMulti(values);
    setDisplayedValue(getDisplayedValue(items));
    if (immediate) onChange(items);
  }

  function handleBlanketClick() {
    if (variant === "multi" && !immediate) {
      onChange(selectedMulti);
    }
    setIsOpen(false);
  }

  useEffect(() => {
    setMenuOptions(options);
  }, [options]);

  return (
    <DropdownWrapper ref={ref} className={dropdownClass} data-testid="qa-dropdown" {...restProps}>
      <DropdownInput
        disabled={disabled || typeof options === "undefined"}
        className={inputClass}
        value={displayedValue}
        setDropdownOpen={setIsOpen}
        ghost={ghost}
        transparent={transparent}
      />
      {isOpen && variant === "single" && (
        <DropdownMenu
          options={menuOptions}
          selected={selectedSingle}
          onItemClicked={handleItemSelected}
          toggleMenu={setIsOpen}
          itemLabel={itemLabel}
          fullWidth={fullWidth}
          menuPosition={menuPosition}
          searchable={searchable}
          searchLabel={searchLabel}
          searchFilter={searchFilter}
          searchPlaceholder={searchPlaceholder}
        />
      )}
      {isOpen && variant === "multi" && (
        <DropdownMenuMulti
          data-testid="qa-dropdown__menu-multi"
          options={menuOptions}
          selected={selectedMulti}
          selectedValues={selectedValuesMulti}
          onItemClicked={handleItemSelectedMulti}
          toggleMenu={setIsOpen}
          itemLabel={itemLabel}
          fullWidth={fullWidth}
          menuPosition={menuPosition}
          searchable={searchable}
          searchLabel={searchLabel}
          searchFilter={searchFilter}
          searchPlaceholder={searchPlaceholder}
        />
      )}
      {isOpen && <DropdownBlanket data-testid="qa-dropdown__blanket" onClick={handleBlanketClick} />}
    </DropdownWrapper>
  );
});

Dropdown.defaultProps = defaultProps;
Dropdown.propTypes = propTypes;

Dropdown.Feedback = DropdownFeedback;

export default Dropdown;
