export function resizeMenu(menuRef) {
  const thisMenu = menuRef.current;
  if (thisMenu && typeof thisMenu.getBoundingClientRect !== "undefined") {
    const maxHeight = window.innerHeight - thisMenu.getBoundingClientRect().top - 10;
    if (maxHeight < 400) {
      thisMenu.style.maxHeight = maxHeight + "px";
    }
  }
}
