import styled from "styled-components";

import { colors } from "src/assets/styles/settings";

export const DropdownSearchWrapper = styled.div`
  padding: 8px 16px 12px;
  position: relative;
`;

export const DropdownSearchClearButton = styled.div`
  color: ${colors.black_bekko};
  cursor: pointer;
  display: inline-block;
  position: absolute;
  right: 28px;
  top: calc(50% - 4px);
  transform: translate(0, calc(-50% + 2px));
`;
