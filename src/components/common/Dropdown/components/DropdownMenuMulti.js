import React, { useRef, useState, useEffect } from "react";
import classNames from "classnames";
import Button from "../../Button";
import Checkbox from "../../Checkbox";
import {
  StyledDropdownMenu,
  DropdownActionButtonWrapper,
  DropdownMenuList,
  DropdownMenuItem,
  DropdownSearchEmpty
} from "./DropdownMenu.styled";
import DropdownSearch from "./DropdownSearch";

import { getTranslationManually } from "src/helpers/locale";
import { resizeMenu } from "../utils";

function DropdownMenuMulti(props) {
  const {
    options,
    selected,
    selectedValues,
    onItemClicked,
    itemLabel,
    fullWidth,
    menuPosition,
    searchable,
    searchLabel,
    searchPlaceholder,
    searchFilter
  } = props;

  const menuRef = useRef(null);
  const menuClass = classNames(
    "c-dropdown__menu",
    fullWidth && "c-dropdown__menu--full",
    `c-dropdown__menu--${menuPosition}`
  );
  const [currentOptions, setCurrentOptions] = useState([]);

  function handleSelectedItems(isChecked, item) {
    const newSelected = [...selected];
    const newSelectedValues = [...selectedValues];

    const valueIndex = newSelectedValues.indexOf(item.value);
    if (isChecked) {
      if (valueIndex === -1) {
        newSelected.push(item);
        newSelectedValues.push(item.value);
      }
    } else {
      if (valueIndex > -1) {
        newSelected.splice(valueIndex, 1);
        newSelectedValues.splice(valueIndex, 1);
      }
    }
    onItemClicked(newSelected, newSelectedValues);
  }

  function handleButtonClick() {
    if (selectedValues.length === 0) {
      onItemClicked(
        [...options],
        options.map((item) => item.value)
      );
    } else {
      onItemClicked([], []);
    }
  }

  useEffect(() => {
    setCurrentOptions(options);
    resizeMenu(menuRef);
  }, [options]);

  return (
    <StyledDropdownMenu ref={menuRef} className={menuClass} data-testid="qa-dropdown__menu-multi">
      {searchable && (
        <DropdownSearch
          options={options}
          searchLabel={searchLabel}
          searchPlaceholder={searchPlaceholder}
          searchFilter={searchFilter}
          setOptions={setCurrentOptions}
        />
      )}
      {currentOptions.length === options.length && (
        <DropdownActionButtonWrapper>
          <Button
            data-testid="qa-dropdown__menu-multi-action-button"
            className="c-dropdown__menu-action-button"
            variant="secondary"
            color="grey"
            onClick={handleButtonClick}
            outline
            block
          >
            {selectedValues.length === 0
              ? getTranslationManually("Centang Semua", "Select All")
              : getTranslationManually("Hapus Semua Centang", "Clear Selection")}
          </Button>
        </DropdownActionButtonWrapper>
      )}
      <DropdownMenuList className="c-dropdown__list" data-testid="qa-dropdown__menu-multi-list">
        {currentOptions.length > 0 ? (
          currentOptions.map((item, index) => {
            const itemClass = classNames(
              "c-dropdown__item-multi",
              selectedValues.includes(item.value) && "is-selected"
            );
            return (
              <DropdownMenuItem
                data-testid="qa-dropdown__menu-multi-item"
                className={itemClass}
                key={index}
                value={item.value}
              >
                <Checkbox
                  data-testid="qa-dropdown__menu-multi-item-checkbox"
                  checked={selectedValues.includes(item.value)}
                  onChange={(event) => handleSelectedItems(event.target.checked, item)}
                >
                  {item[itemLabel]}
                </Checkbox>
              </DropdownMenuItem>
            );
          })
        ) : (
          <DropdownSearchEmpty data-testid="qa-dropdown__menu-multi-empty">
            {getTranslationManually("Tidak ada pilihan", "No options")}
          </DropdownSearchEmpty>
        )}
      </DropdownMenuList>
    </StyledDropdownMenu>
  );
}

export default DropdownMenuMulti;
