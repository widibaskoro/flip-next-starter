import React from "react";
import classNames from "classnames";

import { StyledDropdownInput, DropdownValue, Triangle } from "./DropdownInput.styled";

function DropdownInput(props) {
  const { disabled, value, setDropdownOpen, ghost, transparent, className } = props;
  const inputClass = classNames("c-dropdown__input", disabled && "is-disabled", className);
  const triangleClass = classNames(
    "c-dropdown__triangle",
    disabled && "is-disabled",
    ghost && "is-ghost",
    transparent && "is-transparent"
  );

  function handleInputClicked() {
    if (!disabled) {
      setDropdownOpen(true);
    }
  }

  return (
    <StyledDropdownInput
      className={inputClass}
      ghost={ghost}
      transparent={transparent}
      data-testid="qa-dropdown__input"
      onClick={handleInputClicked}
    >
      <DropdownValue className="c-dropdown__input-value" data-testid="qa-dropdown__input-value">
        {value}
      </DropdownValue>
      <Triangle className={triangleClass} data-testid="qa-dropdown__input-triangle" />
    </StyledDropdownInput>
  );
}

export default DropdownInput;
