import styled from "styled-components";

import { ReactComponent as TriangleFile } from "../assets/triangle.svg";

import { colors, typography } from "src/assets/styles/settings";

function textColor({ ghost, transparent }) {
  if (transparent) return colors.dark_smoke;
  if (ghost) return colors.white;
  return colors.black_bekko;
}

export const StyledDropdownInput = styled.div`
  align-items: center;
  background: ${({ ghost, transparent }) => (ghost || transparent ? "transparent" : colors.white)};
  border: 1px solid ${({ ghost }) => (ghost ? colors.white : colors.light_grey)};
  border-radius: 4px;
  box-sizing: border-box;
  color: ${(props) => textColor(props)};
  cursor: pointer;
  display: flex;
  font-weight: ${({ ghost, transparent }) =>
    ghost || transparent ? typography.font_weight.bold : typography.font_weight.normal};
  height: 40px;
  justify-content: space-between;
  overflow: hidden;
  padding: 8px 12px;
  position: relative;
  order: 1;

  &.is-disabled {
    background: ${colors.disabled};
    color: ${colors.dark_grey};
    cursor: not-allowed;

    &:after {
      color: ${colors.dark_grey};
    }
  }

  .is-invalid & {
    border-color: ${colors.ketchup_tomato};
  }
`;

export const DropdownValue = styled.div`
  display: inline-block;
  overflow: hidden;
  max-width: calc(100% - 24px);
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export const Triangle = styled(TriangleFile)`
  fill: ${colors.black_bekko};

  &.is-ghost {
    fill: ${colors.white};
  }
  &.is-transparent {
    fill: ${colors.dark_smoke};
  }
  &.is-disabled {
    fill: ${colors.dark_grey};
  }
`;

export const TriangleWrapper = styled.span`
  position: absolute;
  padding: 0 12px;
`;
