import styled from "styled-components";

import { colors, typography, zIndex } from "src/assets/styles/settings";
import { rgba } from "polished";

export const StyledDropdownMenu = styled.div`
  background: ${colors.white};
  border-radius: 4px;
  box-shadow: 0px 6px 20px rgba(222, 222, 222, 0.48);
  max-height: 400px;
  min-width: 200px;
  overflow-y: auto;
  padding: 8px 0;
  position: absolute;
  top: 40px;
  text-align: left;
  width: max-content;
  z-index: ${zIndex.dropdown};
  -webkit-overflow-scrolling: touch;

  &.c-dropdown__menu--full {
    width: 100%;
  }

  &.c-dropdown__menu--left {
    left: 0;
  }

  &.c-dropdown__menu--right {
    right: 0;
  }
`;

export const DropdownSearchEmpty = styled.div`
  color: ${colors.dark_grey};
  font-size: ${typography.font_size.small};
  padding: 4px 12px 8px;
  text-align: center;
`;

export const DropdownActionButtonWrapper = styled.div`
  padding: 8px 16px;
`;

export const DropdownMenuList = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
`;

export const DropdownMenuItem = styled.li`
  color: ${colors.black_bekko};
  font-weight: ${typography.font_weight.normal};
  font-size: ${typography.font_size.base};
  line-height: ${typography.line_height};
  margin: 0;

  &:hover {
    background: ${rgba(colors.flip_orange, 0.12)};
    cursor: pointer;
  }

  &.c-dropdown__item {
    padding: 12px 16px;

    &.is-selected {
      background: ${colors.flip_orange};
      color: ${colors.white};
    }
  }

  &.c-dropdown__item-multi .c-checkbox {
    margin: 0;
    width: 100%;
    padding: 12px 16px;
  }
`;
