import React, { useRef, useState, useEffect } from "react";
import classNames from "classnames";

import { StyledDropdownMenu, DropdownMenuList, DropdownMenuItem, DropdownSearchEmpty } from "./DropdownMenu.styled";
import DropdownSearch from "./DropdownSearch";

import { getTranslationManually } from "src/helpers/locale";
import { resizeMenu } from "../utils";

function DropdownMenu(props) {
  const {
    options,
    selected,
    onItemClicked,
    itemLabel,
    fullWidth,
    menuPosition,
    searchable,
    searchLabel,
    searchPlaceholder,
    searchFilter
  } = props;

  const menuRef = useRef(null);
  const menuClass = classNames(
    "c-dropdown__menu",
    fullWidth && "c-dropdown__menu--full",
    `c-dropdown__menu--${menuPosition}`
  );
  const [currentOptions, setCurrentOptions] = useState([]);

  useEffect(() => {
    setCurrentOptions(options);
    resizeMenu(menuRef);
  }, [options]);

  return (
    <StyledDropdownMenu ref={menuRef} className={menuClass} data-testid="qa-dropdown__menu">
      <DropdownMenuList className="c-dropdown__list" data-testid="qa-dropdown__menu-list">
        {searchable && (
          <DropdownSearch
            options={options}
            searchLabel={searchLabel}
            searchPlaceholder={searchPlaceholder}
            searchFilter={searchFilter}
            setOptions={setCurrentOptions}
          />
        )}
        {currentOptions.length > 0 ? (
          currentOptions.map((item, index) => {
            const itemClass = classNames("c-dropdown__item", item.value === selected.value && "is-selected");
            return (
              <DropdownMenuItem
                data-testid="qa-dropdown__menu-item"
                className={itemClass}
                key={index}
                value={item.value}
                onClick={() => onItemClicked(item)}
              >
                {item[itemLabel]}
              </DropdownMenuItem>
            );
          })
        ) : (
          <DropdownSearchEmpty data-testid="qa-dropdown__menu-empty">
            {getTranslationManually("Tidak ada pilihan", "No options")}
          </DropdownSearchEmpty>
        )}
      </DropdownMenuList>
    </StyledDropdownMenu>
  );
}

export default DropdownMenu;
