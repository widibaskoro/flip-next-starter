import React, { useState, useEffect, useRef } from "react";

import Field from "../../Field";
import Icon from "../../Icon";
import { DropdownSearchWrapper, DropdownSearchClearButton } from "./DropdownSearch.styled";

import { faTimes } from "@fortawesome/free-solid-svg-icons";

function DropdownSearch(props) {
  const { options, searchLabel, searchPlaceholder, searchFilter, setOptions } = props;
  const [filterQuery, setFilterQuery] = useState("");

  const inputRef = useRef(null);

  function filterItems(query) {
    const searchQuery = query.toLowerCase();

    if (searchFilter && typeof searchFilter === "function") {
      return searchFilter(searchQuery, options);
    }

    return options.filter((item) => {
      const itemContent = item[searchLabel].toLowerCase();
      return itemContent.includes(searchQuery);
    });
  }

  function doFilter(event) {
    setFilterQuery(event.target.value);
    setOptions(filterItems(event.target.value));
  }

  function clearFilter() {
    setFilterQuery("");
    setOptions(options);
    inputRef.current.focus();
  }

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  return (
    <DropdownSearchWrapper>
      <Field
        ref={inputRef}
        data-testid="qa-dropdown__search-input"
        className="c-dropdown__search-input u-pr-8"
        type="text"
        value={filterQuery}
        placeholder={searchPlaceholder}
        onChange={doFilter}
      />
      {filterQuery !== "" && (
        <DropdownSearchClearButton data-testid="qa-dropdown__search-clear" onClick={clearFilter}>
          <Icon icon={faTimes} />
        </DropdownSearchClearButton>
      )}
    </DropdownSearchWrapper>
  );
}

export default DropdownSearch;
