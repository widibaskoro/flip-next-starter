import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

export const Tab = styled.div`
  padding: 0;
  margin: 0;
`;

export const TabNavs = styled.ul`
  border-bottom: none;
  display: flex;
  flex-wrap: wrap;
  list-style: none;
  margin-bottom: 0px;
  padding-left: 0px;
  position: relative;
  width: 100%;
`;

export const TabNavItem = styled.li`
  background: #fafafa;
  border-radius: 8px 8px 0 0;
  color: ${colors.dark_grey};
  display: inline-block;
  flex-grow: ${({ justify }) => (justify ? 1 : 0)};
  font-weight: ${typography.font_weight.normal};
  font-size: ${typography.font_size.base};
  line-height: ${typography.line_height};
  list-style: none;
  margin-right: 8px;
  padding: 16px 36px;
  text-align: center;

  &.is-active {
    background-color: white;
    color: ${colors.black_bekko};
    font-weight: ${typography.font_weight.bold};
  }

  &:hover {
    background-color: white;
    color: ${colors.flip_orange};
    cursor: pointer;

    &.is-active {
      color: ${colors.black_bekko};
    }
  }

  &:last-child {
    margin-right: 0;
  }
`;

export const TabContent = styled.div`
  background: ${colors.white};
  border-radius: ${({ justify }) => (justify ? "0 0 8px 8px" : "0 8px 8px 8px")};
  padding: 16px;
`;
