import React, { useState, useRef, useEffect } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { Tab, TabNavs, TabNavItem, TabContent } from "./Tabs.styled";

const propTypes = {
  /**
   * Set tab nav key
   */
  activeTabKey: PropTypes.string.isRequired,

  /**
   * Set tab nav being equal with and full
   */
  justify: PropTypes.bool,

  /**
   * Set tab to be full height (to the bottom of page)
   */
  fullHeight: PropTypes.bool,

  /**
   * Tab nav clicked callback function
   */
  onChange: PropTypes.func
};

const defaultProps = {
  activeTabKey: "",
  justify: false,
  fullHeight: false,
  onChange: () => {}
};

const Tabs = React.forwardRef((props, ref) => {
  const { children, activeTabKey, justify, fullHeight, onChange } = props;
  const [activeTab, setActiveTab] = useState(activeTabKey);

  const defaultBodyPadding = 20;
  const tabContentRef = useRef(null);

  function onClickTabItem(tabKey) {
    if (activeTab === tabKey) return;
    setActiveTab(tabKey);
    onChange(tabKey);
  }

  function resizeTabs() {
    const thisTab = tabContentRef.current;
    if (thisTab && typeof thisTab.getBoundingClientRect !== "undefined") {
      const minHeight = window.innerHeight - thisTab.getBoundingClientRect().top - defaultBodyPadding;
      thisTab.style.minHeight = minHeight + "px";
    }
  }

  function renderNavs() {
    return children.map(child => {
      const { label, tabKey } = child.props;
      const tabNavClass = classNames("c-tabs__nav-item", { "is-active": activeTab === tabKey });

      return (
        <TabNavItem key={tabKey} className={tabNavClass} justify={justify} onClick={() => onClickTabItem(tabKey)}>
          {label}
        </TabNavItem>
      );
    });
  }

  useEffect(() => {
    setActiveTab(activeTabKey);
  }, [activeTabKey]);

  useEffect(() => {
    if (fullHeight) resizeTabs();
  }, []);

  return (
    <div ref={ref} className="c-tabs">
      <TabNavs className="c-tabs__navs">{renderNavs()}</TabNavs>
      <TabContent ref={tabContentRef} className="c-tabs__content" justify={justify}>
        {children.map(child => {
          if (child.props.tabKey !== activeTab) return undefined;
          return child.props.children;
        })}
      </TabContent>
    </div>
  );
});

Tabs.Tab = Tab;

Tabs.propTypes = propTypes;
Tabs.defaultProps = defaultProps;

export default Tabs;
