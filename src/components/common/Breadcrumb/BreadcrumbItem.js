import React from "react";
import classNames from "classnames";

import { BreadcrumbItem as StyledBreadcrumbItem } from "./Breadcrumb.styled";

const BreadcrumbItem = React.forwardRef((props, ref) => {
  const { className, ...restProps } = props;
  const breadcrumbItemClass = classNames("c-breadcrumb__item", className);

  return (
    <StyledBreadcrumbItem data-testid="qa-breadcrumb-item" ref={ref} className={breadcrumbItemClass} {...restProps} />
  );
});

export default BreadcrumbItem;
