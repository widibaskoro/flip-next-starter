import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

export const BreadcrumbList = styled.ol`
  color: ${colors.dark_grey};
  display: flex;
  flex-wrap: wrap;
  list-style: none;
  margin: 0;
  padding: 0;
`;

export const BreadcrumbItem = styled.li`
  &,
  & a {
    color: ${colors.peach};
    font-weight: ${typography.font_weight.normal};
    font-size: ${typography.font_size.semi};
    line-height: ${typography.line_height};
    text-decoration: none;
  }

  & a:hover {
    color: ${colors.peach};
    cursor: pointer;
    text-decoration: underline;
  }

  & + .c-breadcrumb__item {
    padding-left: 8px;
  }

  & + .c-breadcrumb__item::before {
    border-top: 2px solid ${colors.dark_grey};
    border-right: 2px solid ${colors.dark_grey};
    content: "";
    display: inline-block;
    height: 8px;
    width: 8px;
    margin-right: 8px;
    transform: rotate(45deg);
  }

  &:last-child {
    color: ${colors.dark_grey};
  }
`;
