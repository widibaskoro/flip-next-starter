import React from "react";
import { render, screen } from "@testing-library/react";

import { colors } from "src/assets/styles/settings";

import Breadcrumb from "./index";
import BreadcrumbItem from "./BreadcrumbItem";

describe("Breadcrumb component", () => {
  describe("Breadcrumb rendering", () => {
    it("should render Breadcrumb without error", () => {
      render(<Breadcrumb />);
      expect(screen.getByTestId("qa-breadcrumb-list")).toBeInTheDocument();
    });

    it("should render BreadcrumbItem children", () => {
      render(
        <Breadcrumb>
          <BreadcrumbItem>One</BreadcrumbItem>
          <BreadcrumbItem>Two</BreadcrumbItem>
        </Breadcrumb>
      );

      expect(screen.getByText(/one/i)).toBeInTheDocument();
      expect(screen.getByText(/two/i)).toBeInTheDocument();
    });
  });

  describe("Breadcrumb props", () => {
    it("should have custom className when provided", () => {
      render(<Breadcrumb className="u-border-r-2" />);
      expect(screen.getByTestId("qa-breadcrumb-list").className).toContain("u-border-r-2");
    });
  });
});

describe("BreadcrumbItem component", () => {
  describe("BreadcrumbItem rendering", () => {
    it("should render without error", () => {
      render(<BreadcrumbItem />);
      expect(screen.getByTestId("qa-breadcrumb-item")).toBeInTheDocument();
    });
  });

  describe("BreadcrumbItem props", () => {
    it("should have custom className when provided", () => {
      render(<BreadcrumbItem className="u-border-r-2" />);
      expect(screen.getByTestId("qa-breadcrumb-item").className).toContain("u-border-r-2");
    });

    it("should show children text", () => {
      render(<BreadcrumbItem>One</BreadcrumbItem>);
      expect(screen.getByTestId("qa-breadcrumb-item")).toHaveTextContent(/one/i);
    });
  });

  describe("BreadcrumbItem style", () => {
    it("should have padding left 8px when component is child number 2 or more", () => {
      render(
        <Breadcrumb>
          <BreadcrumbItem>One</BreadcrumbItem>
          <BreadcrumbItem>Two</BreadcrumbItem>
          <BreadcrumbItem>Three</BreadcrumbItem>
        </Breadcrumb>
      );

      expect(screen.getByText(/two/i)).toHaveStyle("padding-left: 8px");
      expect(screen.getByText(/three/i)).toHaveStyle("padding-left: 8px");
    });

    it("should have grey color when component is last child", () => {
      render(
        <Breadcrumb>
          <BreadcrumbItem>One</BreadcrumbItem>
          <BreadcrumbItem>Two</BreadcrumbItem>
        </Breadcrumb>
      );

      expect(screen.getByText(/two/i)).toHaveStyle(`color: ${colors.dark_grey}`);
    });
  });
});
