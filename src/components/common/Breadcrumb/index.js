import React from "react";
import classNames from "classnames";

import { BreadcrumbList } from "./Breadcrumb.styled";
import BreadcrumbItem from "./BreadcrumbItem";

const Breadcrumb = React.forwardRef((props, ref) => {
  const { className, ...restProps } = props;
  const breadcrumbClass = classNames("c-breadcrumb", className);

  return <BreadcrumbList data-testid="qa-breadcrumb-list" ref={ref} className={breadcrumbClass} {...restProps} />;
});

Breadcrumb.Item = BreadcrumbItem;

export default Breadcrumb;
