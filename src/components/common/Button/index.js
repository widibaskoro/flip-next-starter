import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { StyledButton, ButtonSpinner } from "./Button.styled";

const propTypes = {
  /**
   * Set button spinner to be loading state with spinner
   */
  activated: PropTypes.bool,

  /**
   * Set button color
   *
   * @type {"orange" | "green" | "grey" | "red"}
   */
  color: PropTypes.oneOf(["orange", "green", "grey", "red"]),

  /**
   * Set button icon
   */
  icon: PropTypes.object,

  /**
   * Set button style to outline mode (white inside, only border)
   */
  outline: PropTypes.bool,

  /**
   * Set button variant
   *
   * @type {"primary" | "secondary" | "tertiary" | "mini"}
   */
  variant: PropTypes.oneOf(["primary", "secondary", "tertiary", "mini"]),

  /**
   * Set button to be full width
   */
  block: PropTypes.bool,

  /**
   * Set button type
   */
  type: PropTypes.string
};

const defaultProps = {
  activated: false,
  color: "orange",
  icon: null,
  outline: false,
  variant: "primary",
  block: false,
  type: "button"
};

const Button = React.forwardRef((props, ref) => {
  const { children, className, activated, color, icon, outline, variant, block, type, disabled, ...restProps } = props;
  const buttonClass = classNames("c-button", { "is-only-icon": icon !== null && children === undefined }, className);

  return (
    <StyledButton
      data-testid="qa-button"
      ref={ref}
      className={buttonClass}
      color={color}
      outline={outline}
      variant={variant}
      block={block}
      type={type}
      disabled={disabled}
      {...restProps}
    >
      {icon || activated ? (
        <>
          <span>
            {activated && (
              <ButtonSpinner
                data-testid="qa-button-spinner"
                className="c-button-spinner"
                color={color}
                outline={outline}
                variant={variant}
                disabled={disabled}
              />
            )}
            {icon && !activated && (
              <FontAwesomeIcon icon={icon} data-testid="qa-button-icon" className="c-button__icon" />
            )}
          </span>
          {children && <span>{children}</span>}
        </>
      ) : (
        children
      )}
    </StyledButton>
  );
});

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;

export default Button;
