import styled, { keyframes } from "styled-components";

import { typography } from "src/assets/styles/settings";

import {
  getBackground,
  getBorder,
  getBorderWidth,
  getFontSize,
  getFontWeight,
  getFontColor,
  getTextTransform,
  getDisbaledBackground,
  getDisbaledFontColor,
  getPadding,
  getOnlyIconPadding,
  getOnlyIconSize,
  getHeight,
  getWidth,
  getDisplay,
  getSpinnerDimension,
  getSpinnerImage
} from "./Button.settings";

const rotation = keyframes`
  from {
		transform: rotate(0deg);
  }
  to {
    transform: rotate(359deg);
  }
`;

export const StyledButton = styled.button`
  background: ${(props) => getBackground(props)};
  border-color: ${(props) => getBorder(props)};
  border-style: solid;
  border-width: ${getBorderWidth()};
  color: ${(props) => getFontColor(props)};
  cursor: pointer;
  border-radius: 8px;
  display: ${(props) => getDisplay(props)};
  font-weight: ${(props) => getFontWeight(props)};
  font-size: ${(props) => getFontSize(props)};
  line-height: ${typography.line_height};
  height: ${(props) => getHeight(props)};
  margin: 0;
  opacity: 1;
  padding: ${(props) => getPadding(props)};
  text-align: center;
  text-decoration: none;
  text-transform: ${(props) => getTextTransform(props)};
  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out;
  user-select: none;
  vertical-align: middle;
  width: ${(props) => getWidth(props)};
  white-space: nowrap;

  &:active,
  &:hover {
    background: ${(props) => getBackground({ ...props, hover: true })};
    border-color: ${(props) => getBorder({ ...props, hover: true })};
    color: ${(props) => getFontColor({ ...props, hover: true })};
    text-decoration: none;
  }

  &:focus {
    outline: 0;
    text-decoration: none;
  }

  &:disabled {
    cursor: not-allowed !important;
    background: ${(props) => getDisbaledBackground(props)};
    border-color: ${(props) => getDisbaledBackground(props)};
    color: ${(props) => getDisbaledFontColor(props)};

    &:active,
    &:hover {
      background: ${(props) => getDisbaledBackground(props)};
      border-color: ${(props) => getDisbaledBackground(props)};
      color: ${(props) => getDisbaledFontColor(props)};
      text-decoration: none;
    }
  }

  & > span:first-child {
    margin-right: 8px;
  }

  & > span:only-child {
    margin: 0;
  }

  &.is-only-icon {
    padding: ${(props) => getOnlyIconPadding(props)};
    width: ${(props) => getHeight(props)};

    .c-button__icon {
      font-size: ${(props) => getOnlyIconSize(props)};
    }
  }
`;

export const ButtonSpinner = styled.div`
  animation: ${rotation} 1s infinite linear;
  background-image: url(${(props) => getSpinnerImage(props)});
  background-repeat: no-repeat;
  background-size: ${(props) => getSpinnerDimension(props)};
  display: inline-block;
  height: ${(props) => getSpinnerDimension(props)};
  width: ${(props) => getSpinnerDimension(props)};
  vertical-align: sub;

  .c-button:hover & {
    background-image: url(${(props) => getSpinnerImage({ ...props, forceWhite: true })});
  }
`;
