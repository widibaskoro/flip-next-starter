import { colors, typography } from "src/assets/styles/settings";

import SpinnerOrange from "./assets/spinner-orange.png";
import SpinnerGreen from "./assets/spinner-green.png";
import SpinnerGrey from "./assets/spinner-grey.png";
import SpinnerRed from "./assets/spinner-red.png";
import SpinnerWhite from "./assets/spinner-white.png";

const settings = {
  /************** BORDERS **************/
  border_width: 1,

  /************** COLORS **************/
  colors: {
    orange: colors.flip_orange,
    green: colors.jade,
    grey: colors.dark_grey,
    red: colors.ketchup_tomato
  },
  hover_colors: {
    orange: colors.brick,
    green: colors.pine,
    grey: colors.dark_smoke,
    red: colors.clown_red
  },
  mini_hover_colors: {
    orange: colors.light_anemia,
    green: colors.light_jade,
    grey: colors.light_smoke,
    red: colors.sakura
  },

  /************** TEXTS **************/
  font_sizes: {
    primary: typography.font_size.base,
    secondary: typography.font_size.semi,
    tertiary: typography.font_size.semi,
    mini: typography.font_size.semi
  },
  only_icon_sizes: {
    primary: "26px",
    secondary: "20px",
    tertiary: "20px",
    mini: "12px"
  },
  font_weight: {
    primary: typography.font_weight.bold,
    secondary: typography.font_weight.bold,
    tertiary: typography.font_weight.bold,
    mini: typography.font_weight.normal
  },
  text_transform: {
    primary: "uppercase",
    secondary: "uppercase",
    tertiary: "uppercase",
    mini: "none"
  },

  /************** DIMENSIONS **************/
  height: {
    primary: "53px",
    secondary: "40px",
    tertiary: "40px",
    mini: "24px"
  },
  paddings: {
    primary: [16, 32],
    secondary: [10, 20],
    tertiary: [10, 20],
    mini: [2, 8]
  },
  only_icon_paddings: {
    primary: "13px",
    secondary: "10px",
    tertiary: "10px",
    mini: "2px"
  },

  /************** SPINNERS **************/
  spinner_dimension: {
    primary: "1.0625rem", // 17px
    secondary: "0.9375rem", // 15px
    tertiary: "0.9375rem", // 15px
    mini: "0.75rem" // 12px
  },
  spinner_image: {
    orange: SpinnerOrange,
    green: SpinnerGreen,
    grey: SpinnerGrey,
    red: SpinnerRed,
    white: SpinnerWhite
  }
};

/********************* BORDERS *********************/
export function getBorder({ color, outline, variant, hover = false }) {
  if (variant === "tertiary") return "transparent";
  if (variant === "mini" || outline) return settings.colors[color];
  return hover ? settings.hover_colors[color] : settings.colors[color];
}
export function getBorderWidth() {
  return settings.border_width + "px";
}

/********************* COLORS *********************/
export function getBackground({ color, outline, variant, hover = false }) {
  if (variant === "tertiary") return "transparent";
  if (variant === "mini") return hover ? settings.mini_hover_colors[color] : colors.white;
  if (outline) return hover ? settings.colors[color] : colors.white;
  return hover ? settings.hover_colors[color] : settings.colors[color];
}
export function getFontColor({ color, outline, variant, hover = false }) {
  if (variant === "tertiary") return hover ? settings.hover_colors[color] : settings.colors[color];
  if (variant === "mini" || (outline && !hover)) return settings.colors[color];
  return colors.white;
}
export function getDisbaledBackground({ variant }) {
  return variant === "tertiary" ? "transparent" : colors.light_grey;
}
export function getDisbaledFontColor({ variant }) {
  return variant === "tertiary" ? colors.light_grey : colors.white;
}

/********************* TEXTS *********************/
export function getFontSize({ variant }) {
  return settings.font_sizes[variant];
}
export function getFontWeight({ variant }) {
  return settings.font_weight[variant];
}
export function getTextTransform({ variant }) {
  return settings.text_transform[variant];
}
export function getOnlyIconSize({ variant }) {
  return settings.only_icon_sizes[variant];
}

/********************* DIMENSIONS *********************/
export function getDisplay({ block }) {
  return block ? "block" : "inline-block";
}
export function getPadding({ variant }) {
  const [vertical, horizontal] = settings.paddings[variant];
  const vPad = vertical - settings.border_width;
  const hPad = horizontal - settings.border_width;
  return `${vPad}px ${hPad}px`;
}
export function getOnlyIconPadding({ variant }) {
  return settings.only_icon_paddings[variant];
}
export function getHeight({ variant }) {
  return settings.height[variant];
}
export function getWidth({ block }) {
  return block ? "100%" : "auto";
}

/********************* SPINNERS *********************/
export function getSpinnerDimension({ variant }) {
  return settings.spinner_dimension[variant];
}
export function getSpinnerImage({ variant, color, outline, disabled, forceWhite = false }) {
  if (variant === "tertiary") return disabled ? settings.spinner_image.grey : settings.spinner_image[color];
  if (variant === "mini") return disabled ? settings.spinner_image.white : settings.spinner_image[color];
  if (outline && !disabled && !forceWhite) return settings.spinner_image[color];
  return settings.spinner_image.white;
}
