import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";

import Button from "./index";
import { checkProps } from "src/helpers/test";
import { colors, typography } from "src/assets/styles/settings";

describe("Button component", () => {
  describe("button rendering", () => {
    it("should render without error", () => {
      render(<Button />);
      expect(screen.getByTestId("qa-button")).toBeInTheDocument();
    });
  });

  describe("button functionality", () => {
    it("should calls function provided when clicked", () => {
      const spy = jest.fn();
      render(<Button onClick={() => spy("x")} />);
      fireEvent.click(screen.getByTestId("qa-button"));
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith("x");
    });
  });

  describe("button propTypes", () => {
    it("should not throw warning when expected color props (enum) provided", () => {
      const colorsEnum = ["orange", "green", "grey", "red"];
      colorsEnum.forEach((color) => checkProps(Button, { color }, "Button"));
    });

    it("should not throw warning when expected button types props (enum) provided", () => {
      const variantEnum = ["primary", "secondary", "tertiary", "mini"];
      variantEnum.forEach((variant) => checkProps(Button, { variant }, "Button"));
    });

    it("should not throw warning when all expected props types provided", () => {
      const defaultProps = {
        activated: false,
        color: "orange",
        icon: null,
        outline: false,
        variant: "primary",
        block: false,
        type: "button"
      };

      checkProps(Button, defaultProps, "Button");
    });
  });

  describe("button props", () => {
    it("should have custom className when provided", () => {
      render(<Button className="u-border-r-2" />);
      expect(screen.getByTestId("qa-button").className).toContain("u-border-r-2");
    });

    it("should show children text", () => {
      render(<Button>Submit</Button>);
      expect(screen.getByTestId("qa-button")).toHaveTextContent("Submit");
    });

    it("should be disabled when disabled props is true", () => {
      render(<Button disabled />);
      expect(screen.getByTestId("qa-button")).toBeDisabled();
    });

    it("should show spinner when activated props is true", () => {
      render(<Button activated />);
      expect(screen.getByTestId("qa-button-spinner")).toBeInTheDocument();
    });

    it("should show correct icon when icon props is a valid svg icons", () => {
      render(<Button icon={faExclamationTriangle} />);
      expect(screen.getByTestId("qa-button-icon")).toBeInTheDocument();
      expect(screen.getByTestId("qa-button-icon")).toHaveClass(
        "svg-inline--fa fa-exclamation-triangle fa-w-18 c-button__icon"
      );
    });

    it("should show children text when icon props is provided", () => {
      render(<Button icon={faExclamationTriangle}>Submit</Button>);
      expect(screen.getByTestId("qa-button")).toHaveTextContent("Submit");
    });
  });

  describe("button border and width", () => {
    it("should have border around button when outline props is true", () => {
      render(<Button outline />);
      const expectedStyle = { "border-color": colors.flip_orange, color: colors.flip_orange, background: colors.white };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });

    it("should have auto width and display inline-block when block props is false", () => {
      render(<Button block={false} />);
      const expectedStyle = { width: "auto", display: "inline-block" };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });

    it("should have full width and display block when block props is true", () => {
      render(<Button block />);
      const expectedStyle = { width: "100%", display: "block" };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });

    it("should have 1px border width", () => {
      render(<Button />);
      const expectedStyle = { borderWidth: "1px" };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });
  });

  describe("button color", () => {
    it("should have orange theme when outline props is true and color is orange", () => {
      render(<Button outline color="orange" />);
      const expectedStyle = { background: colors.white, "border-color": colors.flip_orange, color: colors.flip_orange };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });

    it("should have green theme when outline props is true and color is green", () => {
      render(<Button outline color="green" />);
      const expectedStyle = { background: colors.white, "border-color": colors.jade, color: colors.jade };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });

    it("should have grey theme when outline props is true and color is grey", () => {
      render(<Button outline color="grey" />);
      const expectedStyle = { background: colors.white, "border-color": colors.dark_grey, color: colors.dark_grey };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });

    it("should have red theme when outline props is true and color is red", () => {
      render(<Button outline color="red" />);
      const expectedStyle = {
        background: colors.white,
        "border-color": colors.ketchup_tomato,
        color: colors.ketchup_tomato
      };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });

    it("should have jade theme color when color props is green", () => {
      render(<Button color="green" />);
      const expectedStyle = { background: colors.jade, "border-color": colors.jade, color: colors.white };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });

    it("should have flip_orange theme color when color props is orange (default)", () => {
      render(<Button />);
      const expectedStyle = { background: colors.flip_orange, "border-color": colors.flip_orange, color: colors.white };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });

    it("should have dark_grey theme color when color props is grey", () => {
      render(<Button color="grey" />);
      const expectedStyle = { background: colors.dark_grey, "border-color": colors.dark_grey, color: colors.white };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });

    it("should have ketchup_tomato theme color when color props is red", () => {
      render(<Button color="red" />);
      const expectedStyle = {
        background: colors.ketchup_tomato,
        "border-color": colors.ketchup_tomato,
        color: colors.white
      };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });

    it("should have light_grey background and white text color when button is disabled", () => {
      render(<Button disabled />);
      const expectedStyle = { background: colors.light_grey, color: colors.white };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });

    it("should have transparent background and light_grey text color when button is disabled and variant is tertiary", () => {
      render(<Button disabled variant="tertiary" />);
      const expectedStyle = { background: "transparent", color: colors.light_grey };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });
  });

  describe("button variant", () => {
    it("should have expected style when variant props is primary", () => {
      render(<Button />);
      const expectedStyle = {
        "font-size": typography.font_size.base,
        "font-weight": typography.font_weight.bold,
        "text-transform": "uppercase",
        background: colors.flip_orange,
        color: colors.white,
        padding: "15px 31px",
        height: "53px"
      };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });

    it("should have expected style when variant props is secondary", () => {
      render(<Button variant="secondary" />);
      const expectedStyle = {
        "font-size": typography.font_size.semi,
        "font-weight": typography.font_weight.bold,
        "text-transform": "uppercase",
        background: colors.flip_orange,
        color: colors.white,
        padding: "9px 19px",
        height: "40px"
      };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });

    it("should have expected style when variant props is tertiary", () => {
      render(<Button variant="tertiary" />);
      const expectedStyle = {
        "font-size": typography.font_size.semi,
        "font-weight": typography.font_weight.bold,
        "text-transform": "uppercase",
        background: "transparent",
        color: colors.flip_orange,
        padding: "9px 19px",
        height: "40px"
      };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });

    it("should have expected style when variant props is mini", () => {
      render(<Button variant="mini" />);
      const expectedStyle = {
        "font-size": typography.font_size.semi,
        "font-weight": typography.font_weight.normal,
        "text-transform": "none",
        background: colors.white,
        color: colors.flip_orange,
        padding: "1px 7px",
        height: "24px"
      };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedStyle);
    });
  });

  describe("button spinner", () => {
    it("should render white spinner and correct dimension when activated props is true and variant is primary", () => {
      render(<Button activated variant="primary" />);
      const expectedStyle = {
        "background-image": "url(spinner-white.png)",
        height: "1.0625rem",
        width: "1.0625rem",
        "background-size": "1.0625rem"
      };
      expect(screen.getByTestId("qa-button-spinner")).toHaveStyle(expectedStyle);
    });

    it("should render white spinner and correct dimension when activated props is true and variant is secondary", () => {
      render(<Button activated variant="secondary" />);
      const expectedStyle = {
        "background-image": "url(spinner-white.png)",
        height: "0.9375rem",
        width: "0.9375rem",
        "background-size": "0.9375rem"
      };
      expect(screen.getByTestId("qa-button-spinner")).toHaveStyle(expectedStyle);
    });

    it("should render orange spinner and correct dimension when props activated is true, color is orange and variant is tertiary", () => {
      render(<Button activated variant="tertiary" color="orange" />);
      const expectedStyle = {
        "background-image": "url(spinner-orange.png)",
        height: "0.9375rem",
        width: "0.9375rem",
        "background-size": "0.9375rem"
      };
      expect(screen.getByTestId("qa-button-spinner")).toHaveStyle(expectedStyle);
    });

    it("should render green spinner when props activated is true, color is green and variant is tertiary", () => {
      render(<Button activated variant="tertiary" color="green" />);
      const expectedStyle = { "background-image": "url(spinner-green.png)" };
      expect(screen.getByTestId("qa-button-spinner")).toHaveStyle(expectedStyle);
    });

    it("should render red spinner when props activated is true, color is red and variant is tertiary", () => {
      render(<Button activated variant="tertiary" color="red" />);
      const expectedStyle = { "background-image": "url(spinner-red.png)" };
      expect(screen.getByTestId("qa-button-spinner")).toHaveStyle(expectedStyle);
    });

    it("should render grey spinner when props activated is true, color is grey and variant is tertiary", () => {
      render(<Button activated variant="tertiary" color="grey" />);
      const expectedStyle = { "background-image": "url(spinner-grey.png)" };
      expect(screen.getByTestId("qa-button-spinner")).toHaveStyle(expectedStyle);
    });

    it("should render grey spinner when props activated is true, disabled is true and variant is tertiary", () => {
      render(<Button activated variant="tertiary" disabled />);
      const expectedStyle = { "background-image": "url(spinner-grey.png)" };
      expect(screen.getByTestId("qa-button-spinner")).toHaveStyle(expectedStyle);
    });

    it("should render orange spinner and correct dimension when props activated is true, color is orange and variant is mini", () => {
      render(<Button activated variant="mini" color="orange" />);
      const expectedStyle = {
        "background-image": "url(spinner-orange.png)",
        height: "0.75rem",
        width: "0.75rem",
        "background-size": "0.75rem"
      };
      expect(screen.getByTestId("qa-button-spinner")).toHaveStyle(expectedStyle);
    });

    it("should render green spinner when props activated is true, color is green and variant is mini", () => {
      render(<Button activated variant="mini" color="green" />);
      const expectedStyle = { "background-image": "url(spinner-green.png)" };
      expect(screen.getByTestId("qa-button-spinner")).toHaveStyle(expectedStyle);
    });

    it("should render red spinner when props activated is true, color is red and variant is mini", () => {
      render(<Button activated variant="mini" color="red" />);
      const expectedStyle = { "background-image": "url(spinner-red.png)" };
      expect(screen.getByTestId("qa-button-spinner")).toHaveStyle(expectedStyle);
    });

    it("should render grey spinner when props activated is true, color is grey and variant is mini", () => {
      render(<Button activated variant="mini" color="grey" />);
      const expectedStyle = { "background-image": "url(spinner-grey.png)" };
      expect(screen.getByTestId("qa-button-spinner")).toHaveStyle(expectedStyle);
    });

    it("should render grey spinner when props activated is true, disabled is true and variant is mini", () => {
      render(<Button activated variant="mini" disabled />);
      const expectedStyle = { "background-image": "url(spinner-white.png)" };
      expect(screen.getByTestId("qa-button-spinner")).toHaveStyle(expectedStyle);
    });

    it("should render orange spinner when activated and outline props is true", () => {
      render(<Button activated outline />);
      expect(screen.getByTestId("qa-button-spinner")).toHaveStyle("background-image: url(spinner-orange.png)");
    });
  });

  describe("button with only icon", () => {
    it("should have correct button and icon size when icon provided with no children text and variant is primary", () => {
      render(<Button icon={faExclamationTriangle} variant="primary" />);
      const expectedButtonStyle = { padding: "13px", width: "53px" };
      const expectedIconStyle = { "font-size": "26px" };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedButtonStyle);
      expect(screen.getByTestId("qa-button-icon")).toHaveStyle(expectedIconStyle);
    });

    it("should have correct button and icon size when icon provided with no children text and variant is secondary", () => {
      render(<Button icon={faExclamationTriangle} variant="secondary" />);
      const expectedButtonStyle = { padding: "10px", width: "40px" };
      const expectedIconStyle = { "font-size": "20px" };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedButtonStyle);
      expect(screen.getByTestId("qa-button-icon")).toHaveStyle(expectedIconStyle);
    });

    it("should have correct button and icon size when icon provided with no children text and variant is tertiary", () => {
      render(<Button icon={faExclamationTriangle} variant="tertiary" />);
      const expectedButtonStyle = { padding: "10px", width: "40px" };
      const expectedIconStyle = { "font-size": "20px" };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedButtonStyle);
      expect(screen.getByTestId("qa-button-icon")).toHaveStyle(expectedIconStyle);
    });

    it("should have correct button and icon size when icon provided with no children text and variant is mini", () => {
      render(<Button icon={faExclamationTriangle} variant="mini" />);
      const expectedButtonStyle = { padding: "2px", width: "24px" };
      const expectedIconStyle = { "font-size": "12px" };
      expect(screen.getByTestId("qa-button")).toHaveStyle(expectedButtonStyle);
      expect(screen.getByTestId("qa-button-icon")).toHaveStyle(expectedIconStyle);
    });
  });
});
