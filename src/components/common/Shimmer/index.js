import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { Shimmered } from "./Shimmer.styled";

const propTypes = {
  /**
   * Set shimmer height
   */
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  /**
   * Set shimmer width
   */
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  /**
   * Set shimmer border radius
   */
  radius: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

const defaultProps = {
  height: 1,
  width: 1,
  radius: 8
};

const Shimmer = React.forwardRef((props, ref) => {
  const { className, height, width, radius, ...restProps } = props;
  const cardClass = classNames("c-shimmer", className);

  return (
    <Shimmered
      data-testid="qa-shimmer"
      ref={ref}
      className={cardClass}
      height={height}
      width={width}
      radius={radius}
      {...restProps}
    />
  );
});

Shimmer.propTypes = propTypes;
Shimmer.defaultProps = defaultProps;

export default Shimmer;
