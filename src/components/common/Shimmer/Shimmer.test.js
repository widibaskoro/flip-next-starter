import React from "react";
import { render, screen } from "@testing-library/react";

import Shimmer from "./index";
import { checkProps } from "src/helpers/test";

describe("Shimmer component", () => {
  describe("Shimmer rendering", () => {
    it("Should render without error", () => {
      render(<Shimmer />);
      expect(screen.getByTestId("qa-shimmer")).toBeInTheDocument();
    });
  });

  describe("Shimmer propTypes", () => {
    it("Should not throw warning when expected height are provided", () => {
      checkProps(Shimmer, { height: "24px" }, "Shimmer");
      checkProps(Shimmer, { height: 50 }, "Shimmer");
    });

    it("Should not throw warning when expected width are provided", () => {
      checkProps(Shimmer, { width: "24px" }, "Shimmer");
      checkProps(Shimmer, { width: 50 }, "Shimmer");
    });

    it("Should not throw warning when expected radius are provided", () => {
      checkProps(Shimmer, { radius: "24px" }, "Shimmer");
      checkProps(Shimmer, { radius: 50 }, "Shimmer");
    });
  });

  describe("Shimmer props", () => {
    it("should have custom className when provided", () => {
      render(<Shimmer className="u-mr-2" />);
      expect(screen.getByTestId("qa-shimmer").className).toContain("u-mr-2");
    });
  });

  describe("Shimmer styles", () => {
    it("should have 64px height when height props is 64 in number type", () => {
      render(<Shimmer height={64} />);
      const expectedStyle = { height: "64px" };
      expect(screen.getByTestId("qa-shimmer")).toHaveStyle(expectedStyle);
    });

    it("should have 64px height when height props is 64 in string type", () => {
      render(<Shimmer height="64" />);
      const expectedStyle = { height: "64px" };
      expect(screen.getByTestId("qa-shimmer")).toHaveStyle(expectedStyle);
    });

    it("should have 64px height when height props is 64px", () => {
      render(<Shimmer height="64px" />);
      const expectedStyle = { height: "64px" };
      expect(screen.getByTestId("qa-shimmer")).toHaveStyle(expectedStyle);
    });

    it("should have 64vh height when height props is 64vh", () => {
      render(<Shimmer height="64vh" />);
      const expectedStyle = { height: "64vh" };
      expect(screen.getByTestId("qa-shimmer")).toHaveStyle(expectedStyle);
    });

    it("should have 64% height when height props is 64%", () => {
      render(<Shimmer height="64%" />);
      const expectedStyle = { height: "64%" };
      expect(screen.getByTestId("qa-shimmer")).toHaveStyle(expectedStyle);
    });

    it("should have 64px width when width props is 64 in number type", () => {
      render(<Shimmer width={64} />);
      const expectedStyle = { width: "64px" };
      expect(screen.getByTestId("qa-shimmer")).toHaveStyle(expectedStyle);
    });

    it("should have 64px width when width props is 64 in string type", () => {
      render(<Shimmer width="64" />);
      const expectedStyle = { width: "64px" };
      expect(screen.getByTestId("qa-shimmer")).toHaveStyle(expectedStyle);
    });

    it("should have 64px width when width props is 64px", () => {
      render(<Shimmer width="64px" />);
      const expectedStyle = { width: "64px" };
      expect(screen.getByTestId("qa-shimmer")).toHaveStyle(expectedStyle);
    });

    it("should have 64px radius when radius props is 64 in number type", () => {
      render(<Shimmer radius={64} />);
      const expectedStyle = { "border-radius": "64px" };
      expect(screen.getByTestId("qa-shimmer")).toHaveStyle(expectedStyle);
    });

    it("should have 64px radius when radius props is 64 in string type", () => {
      render(<Shimmer radius="64" />);
      const expectedStyle = { "border-radius": "64px" };
      expect(screen.getByTestId("qa-shimmer")).toHaveStyle(expectedStyle);
    });

    it("should have 64px radius when radius props is 64px", () => {
      render(<Shimmer radius="64px" />);
      const expectedStyle = { "border-radius": "64px" };
      expect(screen.getByTestId("qa-shimmer")).toHaveStyle(expectedStyle);
    });
  });
});
