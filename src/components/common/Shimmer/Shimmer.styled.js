import styled, { keyframes } from "styled-components";

import { colors } from "src/assets/styles/settings";

const backgroundMove = keyframes`
  0% {
    background-position: 100%;
  }

  100% {
    background-position: -100%;
  }
`;

export const Shimmered = styled.div`
  animation: ${backgroundMove} 1s infinite;
  background: ${colors.loading};
  background-size: 200%;

  border-radius: ${({ radius }) => (+radius ? `${radius}px` : radius)};
  height: ${({ height }) => (+height ? `${height}px` : height)};
  width: ${({ width }) => (+width ? `${width}px` : width)};
`;
