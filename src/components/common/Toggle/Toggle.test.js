import React from "react";
import { shallow, mount } from "enzyme";

import { findByTestAttr, hexToRGBString } from "src/helpers/test";
import { colors } from "src/assets/styles/settings";

import Toggle from "./index";

describe("Toggle Components", () => {
  describe("Components rendering", () => {
    it("Should render without error", () => {
      const wrapper = shallow(<Toggle />);
      expect(wrapper.length).toBe(1);
    });
  });

  describe("Components toggle background color", () => {
    it("Should render with correct color when disabled=false", () => {
      const wrapper = mount(<Toggle onChange={() => {}} />);
      const label = findByTestAttr(wrapper, "qa-toggle__switch").first();

      const style = getComputedStyle(label.getDOMNode());
      expect(style.backgroundColor).toBe(hexToRGBString(colors.dark_grey));

      wrapper.setProps({ checked: true });
      const styleActive = getComputedStyle(label.getDOMNode());
      expect(styleActive.backgroundColor).toBe(hexToRGBString(colors.jade));
    });

    it("Should render with ligth grey when disabled=true", () => {
      const wrapper = mount(<Toggle disabled onChange={() => {}} />);
      const label = findByTestAttr(wrapper, "qa-toggle__switch").first();

      const style = getComputedStyle(label.getDOMNode());
      expect(style.backgroundColor).toBe(hexToRGBString(colors.light_grey));

      wrapper.setProps({ checked: true });
      const styleActive = getComputedStyle(label.getDOMNode());
      expect(styleActive.backgroundColor).toBe(hexToRGBString(colors.slight_jade));
    });
  });

  describe("Toggle labels", () => {
    it("Should render with correct colors", () => {
      const wrapper = mount(<Toggle labels={["No", "Yes"]} onChange={() => {}} />);
      const textFalse = findByTestAttr(wrapper, "qa-toggle__label-false").first();
      const textTrue = findByTestAttr(wrapper, "qa-toggle__label-true").first();

      const textFalseStyle = getComputedStyle(textFalse.getDOMNode());
      expect(textFalse.text()).toEqual("No");
      expect(textFalseStyle.color).toBe(hexToRGBString(colors.black_bekko));

      const textTrueStyle = getComputedStyle(textTrue.getDOMNode());
      expect(textTrue.text()).toEqual("Yes");
      expect(textTrueStyle.color).toBe(hexToRGBString(colors.dark_grey));

      wrapper.setProps({ checked: true });

      const textFalseStyleNext = getComputedStyle(textFalse.getDOMNode());
      const textTrueStyleNext = getComputedStyle(textTrue.getDOMNode());
      expect(textFalseStyleNext.color).toBe(hexToRGBString(colors.dark_grey));
      expect(textTrueStyleNext.color).toBe(hexToRGBString(colors.black_bekko));
    });
  });
});
