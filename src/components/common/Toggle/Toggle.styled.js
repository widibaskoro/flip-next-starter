import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

const config = {
  big: {
    height: "28px",
    width: "52px",
    switch: "22px",
    gutter: "3px",
    translateX: "24px"
  },
  small: {
    height: "18px",
    width: "32px",
    switch: "14px",
    gutter: "2px",
    translateX: "14px"
  }
};

export const ToggleWrapper = styled.div`
  display: inline-flex;
  align-items: center;
`;

export const ToggleComp = styled.div`
  position: relative;
  display: inline-flex;
  height: ${({ size }) => config[size].height};
  width: ${({ size }) => config[size].width}; ;
`;

export const ToggleCheckbox = styled.input.attrs({ type: "checkbox" })`
  display: none;
`;

export const ToggleLabel = styled.label`
  display: block;
  margin: 0;
  overflow: hidden;
`;

export const ToggleSwitch = styled.span`
  position: absolute;
  background: ${({ checked }) => (checked ? colors.jade : colors.dark_grey)};
  border-radius: 14px;
  cursor: ${({ disabled }) => (disabled ? "not-allowed" : "pointer")};
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  transition: 0.4s;

  &:before {
    position: absolute;
    background-color: ${colors.white};
    border-radius: 50%;
    box-shadow: 0px 2px 2px rgba(6, 96, 76, 0.4);
    content: "";
    height: ${({ size }) => config[size].switch};
    width: ${({ size }) => config[size].switch};
    top: ${({ size }) => config[size].gutter};
    left: ${({ size }) => config[size].gutter};
    transition: 0.4s;
  }

  .is-disabled & {
    background: ${({ checked }) => (checked ? colors.slight_jade : colors.light_grey)};

    &:before {
      background-color: ${colors.disabled};
      box-shadow: none;
    }
  }

  ${ToggleCheckbox}:checked + &:before {
    transform: translateX(${({ size }) => config[size].translateX});
  }
`;

const ToggleText = styled.span`
  font-size: ${typography.font_size.small};
  font-weight: ${typography.font_weight.bold};
`;

export const ToggleTextFalse = styled(ToggleText)`
  color: ${({ checked, disabled }) => (checked || disabled ? colors.dark_grey : colors.black_bekko)};
  margin-right: 8px;
`;

export const ToggleTextTrue = styled(ToggleText)`
  color: ${({ checked, disabled }) => (checked && !disabled ? colors.black_bekko : colors.dark_grey)};
  margin-left: 8px;
`;
