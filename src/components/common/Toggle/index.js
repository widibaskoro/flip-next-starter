import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import {
  ToggleWrapper,
  ToggleComp,
  ToggleCheckbox,
  ToggleLabel,
  ToggleSwitch,
  ToggleTextFalse,
  ToggleTextTrue
} from "./Toggle.styled";

const propTypes = {
  /**
   * Set toggle to be checked
   */
  checked: PropTypes.bool,

  /**
   * Set toggle to be disabled
   */
  disabled: PropTypes.bool,

  /**
   * Set toggle labels (array of strings of labels)
   */
  labels: PropTypes.array,

  /**
   * Set toggle size
   *
   * @type {"big" | "small"}
   */
  size: PropTypes.oneOf(["big", "small"])
};

const defaultProps = {
  checked: false,
  disabled: false,
  labels: [],
  size: "big"
};

const Toggle = React.forwardRef((props, ref) => {
  const { className, checked, disabled, labels, size, ...restProps } = props;
  const toggleClass = classNames("c-toggle", disabled && "is-disabled", className);

  if (labels.length > 0) {
    return (
      <ToggleWrapper ref={ref} className={toggleClass}>
        <ToggleTextFalse data-testid="qa-toggle__label-false" checked={checked} disabled={disabled}>
          {labels[0]}
        </ToggleTextFalse>
        <ToggleComp size={size}>
          <ToggleLabel data-testid="qa-toggle__label" className="c-toggle__label">
            <ToggleCheckbox
              type="checkbox"
              className="c-toggle__checkbox"
              data-testid="qa-toggle__checkbox"
              checked={checked}
              disabled={disabled}
              {...restProps}
            />
            <ToggleSwitch
              size={size}
              data-testid="qa-toggle__switch"
              className="c-toggle__switch"
              checked={checked}
              disabled={disabled}
            />
          </ToggleLabel>
        </ToggleComp>
        <ToggleTextTrue data-testid="qa-toggle__label-true" checked={checked} disabled={disabled}>
          {labels[1]}
        </ToggleTextTrue>
      </ToggleWrapper>
    );
  }

  return (
    <ToggleComp ref={ref} className={toggleClass} size={size}>
      <ToggleLabel data-testid="qa-toggle__label" className="c-toggle__label">
        <ToggleCheckbox
          type="checkbox"
          className="c-toggle__checkbox"
          data-testid="qa-toggle__checkbox"
          checked={checked}
          disabled={disabled}
          {...restProps}
        />
        <ToggleSwitch
          size={size}
          data-testid="qa-toggle__switch"
          className="c-toggle__switch"
          checked={checked}
          disabled={disabled}
        />
      </ToggleLabel>
    </ToggleComp>
  );
});

Toggle.defaultProps = defaultProps;
Toggle.propTypes = propTypes;

export default Toggle;
