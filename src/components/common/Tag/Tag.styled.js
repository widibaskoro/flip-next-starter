import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

export const TagWrapper = styled.div`
  background: ${colors.catskill_white};
  border-radius: 40px;
  display: inline-flex;
  padding: 8px 16px;
  position: relative;
`;

export const TagContent = styled.div`
  color: ${colors.black_bekko};
  display: inline-block;
  font-size: ${typography.font_size.base};
  font-weight: ${typography.font_weight.normal};
  line-height: ${typography.line_height};
`;

export const TagClose = styled.button`
  background: transparent;
  border: none;
  color: ${colors.dark_grey};
  display: inline-block;
  line-height: normal;
  font-size: ${typography.font_size.base};
  margin-left: 8px;
  padding: 0;

  &:focus {
    outline: none;
  }
`;
