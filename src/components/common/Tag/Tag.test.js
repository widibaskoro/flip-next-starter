import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import Tag from "./index";
import { checkProps } from "src/helpers/test";

describe("Tag component", () => {
  describe("Tag rendering", () => {
    it("should render without error", () => {
      render(<Tag />);
      expect(screen.getByTestId("qa-tag")).toBeInTheDocument();
    });
  });

  describe("Tag Functionality", () => {
    it("should trigger onCloseClick props function when close button is clicked", () => {
      const spy = jest.fn();
      render(<Tag onCloseClick={spy} />);
      fireEvent.click(screen.getByTestId("qa-tag-close-button"));
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });

  describe("Tag propTypes", () => {
    it("should not throw warning when expected onCloseClick props are provided", () => {
      checkProps(Tag, { onCloseClick: () => {} }, "Tag");
    });
  });

  describe("Tag props", () => {
    it("should have custom className when provided", () => {
      render(<Tag className="u-border-r-2" />);
      expect(screen.getByTestId("qa-tag").className).toContain("u-border-r-2");
    });

    it("should have children text when provided", () => {
      render(<Tag>Coba</Tag>);
      expect(screen.getByTestId("qa-tag-content")).toHaveTextContent("Coba");
    });
  });
});
