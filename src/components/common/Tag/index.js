import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

import { TagWrapper, TagClose, TagContent } from "./Tag.styled";

const propTypes = {
  /**
   * Callback after close button has been clicked
   */
  onCloseClick: PropTypes.func
};

const defaultProps = {
  onCloseClick: () => {}
};

const Tag = React.forwardRef((props, ref) => {
  const { children, className, onCloseClick } = props;
  const tagClass = classNames("c-tag", className);

  return (
    <TagWrapper ref={ref} className={tagClass} data-testid="qa-tag">
      <TagContent data-testid="qa-tag-content">{children}</TagContent>
      <TagClose data-testid="qa-tag-close-button" type="button" onClick={onCloseClick}>
        <FontAwesomeIcon icon={faTimes} />
      </TagClose>
    </TagWrapper>
  );
});

Tag.defaultProps = defaultProps;
Tag.propTypes = propTypes;

export default Tag;
