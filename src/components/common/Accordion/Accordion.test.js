import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import Accordion from "./index";

describe("Accordion component", () => {
  describe("Accordion rendering", () => {
    it("Should render without error", () => {
      render(<Accordion />);
      expect(screen.getByTestId("qa-accordion")).toBeInTheDocument();
    });
  });

  describe("Accordion functionality", () => {
    it("Should have content when the header is clicked", () => {
      render(<Accordion title="A title">Lorem ipsum</Accordion>);
      fireEvent.click(screen.getByTestId("qa-accordion-header"));
      expect(screen.getByTestId("qa-accordion-content-wrapper")).toHaveTextContent("Lorem ipsum");
    });

    it("Should keep the content when the header is clicked twice", () => {
      render(<Accordion title="A title">Lorem ipsum</Accordion>);
      fireEvent.click(screen.getByTestId("qa-accordion-header"));
      fireEvent.click(screen.getByTestId("qa-accordion-header"));
      expect(screen.getByTestId("qa-accordion-content-wrapper")).toHaveTextContent("Lorem ipsum");
    });
  });

  describe("Accordion title and helper text", () => {
    it("Should show correct title and helper text", () => {
      render(
        <Accordion title="A title" helperText="A helper text">
          Lorem ipsum
        </Accordion>
      );
      expect(screen.getByTestId("qa-accordion-title")).toHaveTextContent("A title");
      expect(screen.getByTestId("qa-accordion-helper-text")).toHaveTextContent("A helper text");
      expect(screen.getByTestId("qa-accordion-content-wrapper")).toHaveTextContent("Lorem ipsum");
    });
  });

  describe("Accordion image", () => {
    it("Should show shimmer when image is still loading", () => {
      render(
        <Accordion title="A title" helperImageSrc="image">
          Lorem ipsum
        </Accordion>
      );
      expect(screen.getByTestId("qa-accordion-img-shimmer")).toBeInTheDocument();
      expect(screen.getByTestId("qa-accordion-img")).toHaveAttribute("src", "image");
    });

    it("Should remove shimmer when image has finished loading", () => {
      render(
        <Accordion title="A title" helperImageSrc="image">
          Lorem ipsum
        </Accordion>
      );
      fireEvent.load(screen.getByTestId("qa-accordion-img"));
      expect(screen.queryByTestId("qa-accordion-img-shimmer")).toBeNull();
      expect(screen.getByTestId("qa-accordion-img")).toHaveAttribute("src", "image");
    });
  });
});
