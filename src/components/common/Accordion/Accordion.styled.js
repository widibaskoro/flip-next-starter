import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";
import { rgba } from "polished";

export const Wrapper = styled.div`
  background: ${colors.white};
  border: 1px solid transparent;
  box-shadow: 0px 2px 6px ${rgba(colors.light_grey, 0.48)};
  border-radius: 8px;
  display: flex;
  flex-direction: column;

  &.is-active {
    border-color: ${colors.anemia};
    box-shadow: 0px 8px 16px ${rgba(colors.anemia, 0.32)};
  }
`;

export const Header = styled.div`
  align-items: center;
  border: 8px;
  cursor: pointer;
  display: flex;
  padding: 20px;
  position: relative;
`;

export const TitleWrapper = styled.div`
  max-width: calc(100% - 92px);
`;

export const Title = styled.p`
  color: ${colors.black_bekko};
  font-size: ${typography.font_size.medium};
  font-weight: ${typography.font_weight.bold};
  line-height: ${typography.line_height};
  margin: 0 0 4px;
`;

export const HelpText = styled.div`
  color: ${colors.dark_smoke};
  font-size: ${typography.font_size.small};
  font-weight: ${typography.font_weight.normal};
  line-height: ${typography.line_height};
`;

export const HelpImage = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 24px;
  min-width: 40px;
  width: 40px;

  img {
    max-width: 100%;
    max-height: 100%;
  }
`;

export const ArrowWrapper = styled.div`
  color: ${colors.flip_orange};
  font-size: 19px;
  margin: -2px 20px 0 8px;
  position: absolute;
  transition: transform 0.2s ease;
  right: 0;
  width: 20px;

  .is-active & {
    transform: rotate(180deg);
  }
`;

export const ContentWrapper = styled.div`
  overflow-y: hidden;
  transition: max-height 0.3s ease;
`;

export const Content = styled.div`
  padding: 0 20px 20px;
  position: relative;
`;
