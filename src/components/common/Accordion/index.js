import React, { useState, useRef } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import Shimmer from "../Shimmer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Wrapper,
  Header,
  TitleWrapper,
  Title,
  HelpText,
  HelpImage,
  ContentWrapper,
  Content,
  ArrowWrapper
} from "./Accordion.styled";

import { faChevronDown } from "@fortawesome/free-solid-svg-icons";

const propTypes = {
  /**
   * Set accordion title
   */
  title: PropTypes.string,

  /**
   * Set helper text bellow the title
   */
  helperText: PropTypes.string,

  /**
   * Set helper image on the left of title
   */
  helperImageSrc: PropTypes.string
};

const defaultProps = {
  title: "",
  helperText: "",
  helperImageSrc: ""
};

const Accordion = React.forwardRef((props, ref) => {
  const { children, className, title, helperText, helperImageSrc } = props;

  const [height, setHeight] = useState("0px");
  const [isActive, setIsActive] = useState(false);
  const [isImageLoaded, setIsImageLoaded] = useState(false);

  const contentRef = useRef(null);
  const accordionClass = classNames("c-accordion", isActive && "is-active", className);

  function toggleAccordion() {
    const currentActive = isActive;
    setIsActive(!currentActive);
    setHeight(currentActive ? "0px" : `${contentRef.current.scrollHeight}px`);
  }

  return (
    <Wrapper data-testid="qa-accordion" className={accordionClass} ref={ref}>
      <Header data-testid="qa-accordion-header" onClick={toggleAccordion}>
        {helperImageSrc && (
          <HelpImage>
            {!isImageLoaded && <Shimmer data-testid="qa-accordion-img-shimmer" height={30} width={50} />}
            <img
              data-testid="qa-accordion-img"
              src={helperImageSrc}
              alt="Help"
              style={{ display: isImageLoaded ? "block" : "none" }}
              onLoad={() => setIsImageLoaded(true)}
            />
          </HelpImage>
        )}
        <TitleWrapper>
          <Title data-testid="qa-accordion-title">{title}</Title>
          {helperText && <HelpText data-testid="qa-accordion-helper-text">{helperText}</HelpText>}
        </TitleWrapper>
        <ArrowWrapper>
          <FontAwesomeIcon icon={faChevronDown} />
        </ArrowWrapper>
      </Header>
      <ContentWrapper data-testid="qa-accordion-content-wrapper" ref={contentRef} style={{ maxHeight: `${height}` }}>
        <Content>{children}</Content>
      </ContentWrapper>
    </Wrapper>
  );
});

Accordion.propTypes = propTypes;
Accordion.defaultProps = defaultProps;

export default Accordion;
