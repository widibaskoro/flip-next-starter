import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { TooltipWrapper, StyledTooltip, TooltipLabel, TooltipArrow } from "./Tooltip.styled";

const propTypes = {
  /**
   * Set tooltip position
   *
   * @type {"top" | "bottom" | "left" | "right" | "top-left" | "top-right" | "bottom-left" | "bottom-right"}
   */
  position: PropTypes.oneOf(["top", "bottom", "left", "right", "top-left", "top-right", "bottom-left", "bottom-right"])
};

const defaultProps = {
  position: "top"
};

const Tooltip = React.forwardRef((props, ref) => {
  const { children, className, position, ...restProps } = props;
  const tooltipClass = classNames("c-tooltip", `c-tooltip--${position}`, className);

  return (
    <StyledTooltip ref={ref} className={tooltipClass} {...restProps}>
      <TooltipArrow className="c-tooltip__arrow"></TooltipArrow>
      <TooltipLabel className="c-tooltip__label">{children}</TooltipLabel>
    </StyledTooltip>
  );
});

Tooltip.propTypes = propTypes;
Tooltip.defaultProps = defaultProps;

Tooltip.Wrapper = TooltipWrapper;

export default Tooltip;
