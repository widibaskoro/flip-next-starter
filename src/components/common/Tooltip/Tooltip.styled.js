import styled from "styled-components";

import { colors, typography, zIndex } from "src/assets/styles/settings";
import { triangle } from "polished";

const triangleDimension = {
  top: ["12px", "8px"],
  right: ["8px", "12px"],
  bottom: ["12px", "8px"],
  left: ["8px", "12px"]
};

function getTriange(direction) {
  return triangle({
    pointingDirection: direction,
    width: triangleDimension[direction][0],
    height: triangleDimension[direction][1],
    foregroundColor: colors.black_bekko
  });
}

export const TooltipWrapper = styled.div`
  position: relative;
  display: inline-block;
`;

export const StyledTooltip = styled.div`
  position: absolute;
  background: ${colors.black_bekko};
  border-radius: 4px;
  color: ${colors.white};
  display: inline-block;
  font-style: normal;
  font-size: ${typography.font_size.semi};
  font-weight: ${typography.font_weight.normal};
  line-height: ${typography.line_height};
  opacity: 0.75;
  text-align: left;
  text-align: start;
  text-shadow: none;
  text-transform: none;
  white-space: normal;
  word-break: normal;
  word-spacing: normal;
  word-wrap: normal;
  width: max-content;
  max-width: 180px;
  z-index: ${zIndex.tooltip};

  &.c-tooltip--right {
    top: 50%;
    right: -8px;
    transform: translate3d(100%, -50%, 0);
  }

  &.c-tooltip--left {
    top: 50%;
    left: -8px;
    transform: translate3d(-100%, -50%, 0);
  }

  &.c-tooltip--top {
    top: -8px;
    left: 50%;
    transform: translate3d(-50%, -100%, 0);
  }

  &.c-tooltip--top-left {
    top: -8px;
    left: 50%;
    transform: translate3d(-13%, -100%, 0);
  }

  &.c-tooltip--top-right {
    top: -8px;
    left: 50%;
    transform: translate3d(-87%, -100%, 0);
  }

  &.c-tooltip--bottom {
    bottom: -8px;
    left: 50%;
    transform: translate3d(-50%, 100%, 0);
  }

  &.c-tooltip--bottom-left {
    bottom: -8px;
    left: 50%;
    transform: translate3d(-13%, 100%, 0);
  }

  &.c-tooltip--bottom-right {
    bottom: -8px;
    left: 50%;
    transform: translate3d(-87%, 100%, 0);
  }
`;

export const TooltipLabel = styled.div`
  border-radius: 4px;
  color: ${colors.white};
  width: 100%;
  padding: 4px 8px;
  text-align: center;
`;

export const TooltipArrow = styled.div`
  position: absolute;

  .c-tooltip--right & {
    ${getTriange("left")}
    top: 50%;
    left: auto;
    margin-left: -7px;
    margin-top: -6px;
  }

  .c-tooltip--left & {
    ${getTriange("right")}
    top: 50%;
    left: auto;
    right: -7px;
    margin-top: -6px;
  }

  .c-tooltip--top & {
    ${getTriange("bottom")}
    top: auto;
    left: 50%;
    bottom: -7px;
    margin-left: -4px;
  }

  .c-tooltip--top-left & {
    ${getTriange("bottom")}
    top: auto;
    left: 15%;
    bottom: -7px;
    margin-left: -4px;
  }

  .c-tooltip--top-right & {
    ${getTriange("bottom")}
    top: auto;
    left: 85%;
    bottom: -7px;
    margin-left: -4px;
  }

  .c-tooltip--bottom & {
    ${getTriange("top")}
    top: 0;
    left: 50%;
    margin-top: -7px;
    margin-left: -4px;
  }

  .c-tooltip--bottom-left & {
    ${getTriange("top")}
    top: 0;
    left: 15%;
    margin-top: -7px;
    margin-left: -4px;
  }

  .c-tooltip--bottom-right & {
    ${getTriange("top")}
    top: 0;
    left: 85%;
    margin-top: -7px;
    margin-left: -4px;
  }
`;
