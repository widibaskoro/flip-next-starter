import styled from "styled-components";

import { colors, typography, zIndex } from "src/assets/styles/settings";
import { rgba } from "polished";

const config = {
  height: {
    primary: "36px",
    mini: "28px"
  },
  padding: {
    // In Figma, the border width is counted as it is inside the button, so we need to adjust the padding instead
    primary: "7px 16px",
    mini: "2px 16px 3px"
  }
};

export const StyledButtonGroup = styled.button`
  background: ${({ active }) => (active ? rgba(colors.flip_orange, 0.08) : colors.white)};
  border-color: ${({ active }) => (active ? colors.anemia : colors.light_grey)};
  border-style: solid;
  border-width: 1px;
  color: ${({ active }) => (active ? colors.flip_orange : colors.dark_grey)};
  cursor: pointer;
  border-radius: 4px;
  display: inline-block;
  flex: 1 1 auto;
  font-weight: ${typography.font_weight.normal};
  font-size: ${typography.font_size.base};
  height: ${({ variant }) => config.height[variant]};
  line-height: ${typography.line_height};
  margin: 0;
  opacity: 1;
  padding: ${({ variant }) => config.padding[variant]};
  position: relative;
  text-align: center;
  text-decoration: none;
  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out;
  user-select: none;
  vertical-align: middle;
  z-index: ${({ active }) => (active ? zIndex.buttonActive : zIndex.default)};

  &:hover {
    background: ${colors.flip_orange};
    border-color: ${colors.flip_orange};
    color: ${colors.white};
    text-decoration: none;
    z-index: ${zIndex.buttonHover};
  }

  &:focus {
    outline: 0;
  }

  &:disabled {
    cursor: not-allowed !important;
    background: ${colors.light_grey};
    border-color: ${colors.light_grey};
    color: ${colors.white};

    &:active,
    &:hover,
    &:focus {
      background: ${colors.light_grey};
      border-color: ${colors.light_grey};
      color: ${colors.white};
      text-decoration: none;
    }
  }

  &:not(:first-child) {
    margin-left: -1px;
  }

  &:not(:last-child) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }

  &:not(:first-child) {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
  }
`;

export const ButtonGroupWrapper = styled.div`
  position: relative;
  display: inline-flex;
  vertical-align: middle;
`;

export const ButtonGroupButton = styled.button``;
