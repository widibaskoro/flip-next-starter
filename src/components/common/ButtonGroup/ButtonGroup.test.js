import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import ButtonGroup from "./index";
import { checkProps } from "src/helpers/test";
import { colors, zIndex } from "src/assets/styles/settings";
import { rgba } from "polished";

describe("ButtonGroup component", () => {
  describe("ButtonGroup rendering", () => {
    it("Should render normally when one content is given", () => {
      render(
        <ButtonGroup>
          <ButtonGroup.Button></ButtonGroup.Button>
        </ButtonGroup>
      );
      expect(screen.getByTestId("qa-button-group-wrapper")).toBeInTheDocument();
      expect(screen.getByTestId("qa-button-group")).toBeInTheDocument();
    });

    it("Should render normally when more than one contents is given", () => {
      render(
        <ButtonGroup>
          <ButtonGroup.Button></ButtonGroup.Button>
          <ButtonGroup.Button></ButtonGroup.Button>
          <ButtonGroup.Button></ButtonGroup.Button>
        </ButtonGroup>
      );
      expect(screen.getByTestId("qa-button-group-wrapper")).toBeInTheDocument();
      expect(screen.queryAllByTestId("qa-button-group")).toHaveLength(3);
    });

    it("Should throw error if no content/child is given", () => {
      jest.spyOn(console, "error");
      console.error.mockImplementation(() => {});
      expect(() => render(<ButtonGroup />)).toThrow();
      console.error.mockRestore();
    });
  });

  describe("ButtonGroup functionality", () => {
    it("should calls function provided when clicked", () => {
      const spy = jest.fn();
      render(
        <ButtonGroup>
          <ButtonGroup.Button onClick={() => spy("x")}></ButtonGroup.Button>
        </ButtonGroup>
      );
      fireEvent.click(screen.getByTestId("qa-button-group"));
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith("x");
    });
  });

  describe("ButtonGroup propTypes", () => {
    it("Should not throw warning when expected variant are provided", () => {
      checkProps(ButtonGroup, { variant: "primary" }, "ButtonGroup");
      checkProps(ButtonGroup, { variant: "mini" }, "ButtonGroup");
    });

    it("Should not throw warning when all expected props types provided", () => {
      const defaultProps = {
        variant: "primary"
      };
      checkProps(ButtonGroup, defaultProps, "ButtonGroup");
    });
  });

  describe("ButtonGroup props", () => {
    it("should have custom className when provided", () => {
      render(
        <ButtonGroup className="u-m-1">
          <ButtonGroup.Button></ButtonGroup.Button>
        </ButtonGroup>
      );
      expect(screen.getByTestId("qa-button-group-wrapper").className).toContain("u-m-1");
    });

    it("should show children text", () => {
      render(
        <ButtonGroup>
          <ButtonGroup.Button>All</ButtonGroup.Button>
        </ButtonGroup>
      );
      expect(screen.getByTestId("qa-button-group")).toHaveTextContent("All");
    });

    it("should be disabled when disabled props is true", () => {
      render(
        <ButtonGroup>
          <ButtonGroup.Button disabled></ButtonGroup.Button>
        </ButtonGroup>
      );
      expect(screen.getByTestId("qa-button-group")).toBeDisabled();
    });
  });

  describe("ButtonGroup style", () => {
    it("should have expected style when active", () => {
      render(
        <ButtonGroup>
          <ButtonGroup.Button active></ButtonGroup.Button>
        </ButtonGroup>
      );
      const expectedStyle = {
        background: rgba(colors.flip_orange, 0.08),
        "border-color": colors.anemia,
        color: colors.flip_orange,
        "z-index": zIndex.buttonActive
      };
      expect(screen.getByTestId("qa-button-group")).toHaveStyle(expectedStyle);
    });

    it("should have expected style when disabled", () => {
      render(
        <ButtonGroup>
          <ButtonGroup.Button disabled></ButtonGroup.Button>
        </ButtonGroup>
      );
      const expectedStyle = {
        cursor: "not-allowed",
        background: colors.light_grey,
        "border-color": colors.light_grey,
        color: colors.white
      };
      expect(screen.getByTestId("qa-button-group")).toHaveStyle(expectedStyle);
    });
  });

  describe("ButtonGroup content", () => {
    it("Should contain correct text when one content is given", () => {
      render(
        <ButtonGroup>
          <ButtonGroup.Button>Button</ButtonGroup.Button>
        </ButtonGroup>
      );
      expect(screen.getByTestId("qa-button-group")).toHaveTextContent("Button");
    });

    it("Should contain correct texts when more than one contents are given", () => {
      render(
        <ButtonGroup>
          <ButtonGroup.Button>1st Button</ButtonGroup.Button>
          <ButtonGroup.Button>2nd Button</ButtonGroup.Button>
          <ButtonGroup.Button>3rd Button</ButtonGroup.Button>
        </ButtonGroup>
      );
      expect(screen.queryAllByTestId("qa-button-group")).toHaveLength(3);
      expect(screen.queryAllByTestId("qa-button-group")[0]).toHaveTextContent("1st Button");
      expect(screen.queryAllByTestId("qa-button-group")[1]).toHaveTextContent("2nd Button");
      expect(screen.queryAllByTestId("qa-button-group")[2]).toHaveTextContent("3rd Button");
    });
  });
});
