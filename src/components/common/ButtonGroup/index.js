import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { StyledButtonGroup, ButtonGroupWrapper, ButtonGroupButton } from "./ButtonGroup.styled";

const propTypes = {
  /**
   * Set button variant
   *
   * @type {"primary" | "mini"}
   */
  variant: PropTypes.oneOf(["primary", "mini"])
};

const defaultProps = {
  variant: "primary"
};

const ButtonGroup = React.forwardRef((props, ref) => {
  const { children, className, variant, ...restProps } = props;
  const buttonClass = classNames("c-button-group", className);

  function renderOneButton({ children, active, disabled, ...restChildProps }, index) {
    return (
      <StyledButtonGroup
        data-testid="qa-button-group"
        className="c-button-group__button"
        key={index}
        active={active}
        disabled={disabled}
        variant={variant}
        {...restChildProps}
      >
        {children}
      </StyledButtonGroup>
    );
  }

  function renderButtons() {
    if (Array.isArray(children)) {
      return children.map((child, index) => renderOneButton(child.props, index));
    }
    return renderOneButton(children.props);
  }

  return (
    <ButtonGroupWrapper data-testid="qa-button-group-wrapper" ref={ref} className={buttonClass} {...restProps}>
      {renderButtons()}
    </ButtonGroupWrapper>
  );
});

ButtonGroup.propTypes = propTypes;
ButtonGroup.defaultProps = defaultProps;

ButtonGroup.Button = ButtonGroupButton;

export default ButtonGroup;
