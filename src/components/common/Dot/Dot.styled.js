import styled from "styled-components";

import { colors } from "src/assets/styles/settings";

const typeColors = {
  orange: colors.flip_orange,
  blue: colors.intel_blue,
  green: colors.jade,
  grey: colors.dark_grey,
  red: colors.warning,
  yellow: colors.mango_yellow,
  "light-grey": colors.light_grey
};

export const StyledDot = styled.div`
  background: ${({ color }) => typeColors[color]};
  border: none;
  border-radius: 50%;
  box-sizing: border-box;
  display: inline-block;
  height: 10px;
  width: 10px;
`;
