import React from "react";
import { render, screen } from "@testing-library/react";

import Dot from "./index";
import { checkProps } from "src/helpers/test";
import { colors } from "src/assets/styles/settings";

describe("Dot component", () => {
  describe("Dot rendering", () => {
    it("Should render without error", () => {
      render(<Dot />);
      expect(screen.getByTestId("qa-dot")).toBeInTheDocument();
    });
  });

  describe("Dot propTypes", () => {
    it("Should not throw warning when expected colors (enum) are provided", () => {
      checkProps(Dot, { color: "orange" }, "Dot");
      checkProps(Dot, { color: "blue" }, "Dot");
      checkProps(Dot, { color: "green" }, "Dot");
      checkProps(Dot, { color: "grey" }, "Dot");
      checkProps(Dot, { color: "red" }, "Dot");
      checkProps(Dot, { color: "yellow" }, "Dot");
      checkProps(Dot, { color: "light-grey" }, "Dot");
    });
  });

  describe("Dot color", () => {
    it("Should have flip_orange color if no color prop is given", () => {
      render(<Dot />);
      expect(screen.getByTestId("qa-dot")).toHaveStyle({ background: colors.flip_orange });
    });

    it("Should have flip_orange color if given color: orange", () => {
      render(<Dot color="orange" />);
      expect(screen.getByTestId("qa-dot")).toHaveStyle({ background: colors.flip_orange });
    });

    it("Should have intel_blue color if given color: blue", () => {
      render(<Dot color="blue" />);
      expect(screen.getByTestId("qa-dot")).toHaveStyle({ background: colors.intel_blue });
    });

    it("Should have jade color if given color: green", () => {
      render(<Dot color="green" />);
      expect(screen.getByTestId("qa-dot")).toHaveStyle({ background: colors.jade });
    });

    it("Should have dark_grey color if given color: grey", () => {
      render(<Dot color="grey" />);
      expect(screen.getByTestId("qa-dot")).toHaveStyle({ background: colors.dark_grey });
    });

    it("Should have warning color if given color: red", () => {
      render(<Dot color="red" />);
      expect(screen.getByTestId("qa-dot")).toHaveStyle({ background: colors.warning });
    });

    it("Should have mango_yellow color if given color: yellow", () => {
      render(<Dot color="yellow" />);
      expect(screen.getByTestId("qa-dot")).toHaveStyle({ background: colors.mango_yellow });
    });

    it("Should have light_grey color if given color: light-grey", () => {
      render(<Dot color="light-grey" />);
      expect(screen.getByTestId("qa-dot")).toHaveStyle({ background: colors.light_grey });
    });
  });
});
