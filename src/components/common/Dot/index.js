import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { StyledDot } from "./Dot.styled";

const propTypes = {
  /**
   * Set dot color
   *
   * @type {"orange" | "blue" | "green" | "grey" | "red" | "yellow" | "light-grey"}
   */
  color: PropTypes.oneOf(["orange", "blue", "green", "grey", "red", "yellow", "light-grey"])
};

const defaultProps = {
  color: "orange"
};

const Dot = React.forwardRef((props, ref) => {
  const { className, color, ...restProps } = props;
  const dotClass = classNames("c-dot", className);

  return <StyledDot data-testid="qa-dot" ref={ref} className={dotClass} color={color} {...restProps} />;
});

Dot.propTypes = propTypes;
Dot.defaultProps = defaultProps;

export default Dot;
