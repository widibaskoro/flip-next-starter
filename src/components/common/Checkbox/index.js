import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import {
  CheckboxWrapper,
  CheckboxContainer,
  CheckboxIcon,
  HiddenCheckbox,
  StyledCheckbox,
  LabelText
} from "./Checkbox.styled";
import { paths } from "src/assets/styles/settings";

const propTypes = {
  /**
   * Set checkbox to be checked
   */
  checked: PropTypes.bool,

  /**
   * Set checkbox to be disabled
   */
  disabled: PropTypes.bool
};

const defaultProps = {
  checked: false,
  disabled: false
};

const Checkbox = React.forwardRef((props, ref) => {
  const { className, children, checked, disabled, ...restProps } = props;
  const checkboxClass = classNames("c-checkbox", className);

  return (
    <CheckboxWrapper data-testid="qa-checkbox" ref={ref} className={checkboxClass}>
      <CheckboxContainer className="c-checkbox__wrapper">
        <HiddenCheckbox
          data-testid="qa-checkbox-input"
          className="c-checkbox__input"
          checked={checked}
          disabled={disabled}
          {...restProps}
        />
        <StyledCheckbox data-testid="qa-checkbox-box" className="c-checkbox__box">
          <CheckboxIcon data-testid="qa-checkbox-icon" className="c-checkbox__icon" viewBox="0 0 24 24">
            <path d={paths.check_icon} />
          </CheckboxIcon>
        </StyledCheckbox>
      </CheckboxContainer>
      <LabelText disabled={disabled}>{children}</LabelText>
    </CheckboxWrapper>
  );
});

Checkbox.propTypes = propTypes;
Checkbox.defaultProps = defaultProps;

export default Checkbox;
