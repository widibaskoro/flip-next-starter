import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import Checkbox from "./index";
import { colors } from "src/assets/styles/settings";

describe("Checkbox component", () => {
  describe("Checkbox rendering", () => {
    it("Should render without error", () => {
      render(<Checkbox onChange={() => {}} />);
      expect(screen.getByTestId("qa-checkbox")).toBeInTheDocument();
    });
  });

  describe("Checkbox functionality", () => {
    it("Should return true for the checked value when clicked", () => {
      const spy = jest.fn();
      render(
        <Checkbox checked={false} onChange={(e) => spy(e.target.checked)}>
          A label
        </Checkbox>
      );
      expect(screen.getByTestId("qa-checkbox-input").checked).toEqual(false);
      fireEvent.click(screen.getByTestId("qa-checkbox"));
      expect(spy).toHaveBeenCalledWith(true);
    });

    it("Should return false for the checked value when clicked (initial checked is true)", () => {
      const spy = jest.fn();
      render(
        <Checkbox checked={true} onChange={(e) => spy(e.target.checked)}>
          A label
        </Checkbox>
      );
      expect(screen.getByTestId("qa-checkbox-input").checked).toEqual(true);
      fireEvent.click(screen.getByTestId("qa-checkbox"));
      expect(spy).toHaveBeenCalledWith(false);
    });
  });

  describe("Checkbox disabled functionality", () => {
    it("Should do nothing if clicked when disabled", () => {
      const spy = jest.fn();
      render(
        <Checkbox checked={false} onChange={(e) => spy(e.target.checked)} disabled>
          A label
        </Checkbox>
      );
      expect(screen.getByTestId("qa-checkbox-input").checked).toEqual(false);
      fireEvent.click(screen.getByTestId("qa-checkbox"));
      expect(spy).not.toHaveBeenCalled();
    });
  });

  describe("Checkbox style", () => {
    it("Should has no background and no checkbox when checked is false", () => {
      render(
        <Checkbox checked={false} onChange={() => {}}>
          A label
        </Checkbox>
      );
      expect(screen.getByTestId("qa-checkbox-box")).toHaveStyle({ background: colors.white });
      expect(screen.getByTestId("qa-checkbox-icon")).toHaveStyle({ visibility: "hidden" });
    });

    it("Should has orange background and visible checkbox when checked is true", () => {
      render(
        <Checkbox checked={true} onChange={() => {}}>
          A label
        </Checkbox>
      );
      expect(screen.getByTestId("qa-checkbox-box")).toHaveStyle({ background: colors.flip_orange });
      expect(screen.getByTestId("qa-checkbox-icon")).toHaveStyle({ visibility: "visible" });
    });

    it("Should has grey background and no checkbox when checked is false", () => {
      render(
        <Checkbox checked={false} onChange={() => {}} disabled>
          A label
        </Checkbox>
      );
      expect(screen.getByTestId("qa-checkbox-box")).toHaveStyle({ background: colors.light_grey });
      expect(screen.getByTestId("qa-checkbox-icon")).toHaveStyle({ visibility: "hidden" });
    });

    it("Should has orange background and visible checkbox when checked is true", () => {
      render(
        <Checkbox checked={true} onChange={() => {}} disabled>
          A label
        </Checkbox>
      );
      expect(screen.getByTestId("qa-checkbox-box")).toHaveStyle({ background: colors.light_grey });
      expect(screen.getByTestId("qa-checkbox-icon")).toHaveStyle({ visibility: "visible" });
      expect(screen.getByTestId("qa-checkbox-icon")).toHaveStyle({ fill: colors.dark_grey });
    });
  });
});
