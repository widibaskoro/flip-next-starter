import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

export const CheckboxWrapper = styled.label`
  align-items: center;
  display: inline-flex;
  margin-bottom: 12px;

  &:hover {
    cursor: pointer;

    .c-checkbox__box {
      border-color: ${colors.flip_orange};
    }
  }
`;

export const CheckboxContainer = styled.div`
  display: inline-block;
  height: 18px;
  text-align: left;
  vertical-align: middle;
`;

export const CheckboxIcon = styled.svg`
  fill: ${colors.white};
  position: absolute;
  margin: 3px 2px;
  top: 0;
  visibility: hidden;
  width: 12px;
`;

export const StyledCheckbox = styled.div`
  background: ${colors.white};
  border: 1px solid ${colors.dark_grey};
  border-radius: 5px;
  display: inline-block;
  height: 18px;
  position: relative;
  transition: all 150ms;
  width: 18px;
`;

export const HiddenCheckbox = styled.input.attrs({ type: "checkbox" })`
  border: 0;
  clip: rect(0 0 0 0);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;

  &:checked + ${StyledCheckbox} {
    background: ${colors.flip_orange};
    border-color: ${colors.flip_orange};

    & > ${CheckboxIcon} {
      visibility: visible;
    }
  }

  &:disabled + ${StyledCheckbox} {
    background: ${colors.light_grey};
    border-color: ${colors.dark_grey};

    & > ${CheckboxIcon} {
      fill: ${colors.dark_grey};
    }
  }
`;

export const LabelText = styled.span`
  color: ${({ disabled }) => (disabled ? colors.dark_grey : colors.black_bekko)};
  font-weight: ${typography.font_weight.normal};
  font-size: ${typography.font_size.base};
  line-height: ${typography.line_height};
  margin-left: 8px;
`;
