import React, { useState } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { useRouter } from "next/router";
import { faEllipsisV, faChevronUp, faChevronDown } from "@fortawesome/free-solid-svg-icons";

import {
  MenuList,
  MenuItem,
  MenuItemLink,
  MenuItemIconBase,
  MenuItemIconHover,
  MenuItemText,
  MenuItemLinkExtended,
  MenuItemTextExtended,
  MenuItemTextWrapper,
  MenuItemChildWrapperCollapsed,
  MenuItemTextExtendedChild,
  MenuItemChildWrapperMain,
  MenuItemWrapper,
  IconWrapper
} from "./MenuBar.styled";
import { Icon } from "src/components/common";

import { getTranslationManually } from "src/helpers/locale";
import "./assets/menubar-icons.module.scss";

const propTypes = {
  /**
   * set menu bar menus
   */
  menus: PropTypes.array,

  /**
   * set menu bar to be collapsed mode
   */
  collapsed: PropTypes.bool,

  /**
   * current location path to determine which menu is active
   */
  currentPath: PropTypes.string
};

const defaultProps = {
  menus: [],
  collapsed: false,
  currentPath: "/home"
};

const MenuBar = React.forwardRef((props, ref) => {
  const { className, menus, collapsed, currentPath, ...restProps } = props;
  const location = useRouter();

  const [dropdownActive, setDropdownActive] = useState(location.pathname);
  const [activeToggle, setActiveToggle] = useState(true);

  const menubarClass = classNames("c-menubar", collapsed && "is-collapsed");

  function isActive(prefixes) {
    return prefixes.includes(currentPath);
  }

  function isChildActive(activePath) {
    let result = false;
    const currentPath = location.pathname.split("/");

    activePath.forEach((path) => {
      const menuPath = path.split("/");
      result = (currentPath[1] === menuPath[1] && currentPath[2] === menuPath[2]) || result;
    });

    return result;
  }

  return (
    <MenuList ref={ref} className={menubarClass} {...restProps}>
      {menus.map((menu, index) => {
        if (!menu.show) return null;
        const isMenuActive = isActive(menu.active_url_prefixes);
        const hasChild = !!menu.childs;
        const isDropdownActive = dropdownActive === menu.to;

        return (
          <div key={menu.to}>
            {menu.break && <hr className="u-my-3 u-border-light-grey" />}
            <MenuItem className={classNames("c-menu__item", { "is-active": isMenuActive })}>
              <MenuItemWrapper
                className={classNames({ "c-menu-active": isMenuActive ? activeToggle : isDropdownActive })}
              >
                <MenuItemLink as={menu.as} to={menu.to} className="c-menubar__link">
                  <MenuItemIconBase className="c-menubar__icon">
                    {isMenuActive ? (
                      <span className={classNames("c-menubar-icon", `c-menubar-icon--${menu.icon}-white`)} />
                    ) : (
                      <span className={classNames("c-menubar-icon", `c-menubar-icon--${menu.icon}`)} />
                    )}
                  </MenuItemIconBase>
                  <MenuItemIconHover className="c-menubar__icon">
                    <span className={classNames("c-menubar-icon", `c-menubar-icon--${menu.icon}-white`)} />
                  </MenuItemIconHover>
                  <MenuItemText className="c-menubar__text">
                    {getTranslationManually(menu.label[0], menu.label[1])}
                  </MenuItemText>
                </MenuItemLink>
                {hasChild && (
                  <IconWrapper
                    className={classNames("c-icon__wrapper", { "c-icon-active": isDropdownActive })}
                    onClick={() => {
                      isMenuActive
                        ? setActiveToggle((current) => !current)
                        : setDropdownActive((current) => (current === menu.to ? "" : menu.to));
                    }}
                  >
                    <Icon
                      className="u-text-semi"
                      icon={(isMenuActive ? activeToggle : isDropdownActive) ? faChevronUp : faChevronDown}
                    />
                  </IconWrapper>
                )}
                {hasChild && (
                  <MenuItemChildWrapperMain>
                    {menu.childs.map((child) => (
                      <MenuItemTextExtendedChild
                        key={child.path[0]}
                        as={menu.as}
                        to={child.path[0]}
                        className={isChildActive(child.path) && "u-text-flip-orange"}
                      >
                        {getTranslationManually(child.label[0], child.label[1])}
                      </MenuItemTextExtendedChild>
                    ))}
                  </MenuItemChildWrapperMain>
                )}
              </MenuItemWrapper>
              {collapsed && hasChild && (
                <Icon
                  className="u-text-semi u-text-dark-grey u-absolute u-right-0 u-top-0"
                  style={{ marginRight: "-12px", marginTop: "15px" }}
                  icon={faEllipsisV}
                />
              )}
              {collapsed && (
                <MenuItemLinkExtended haschild={hasChild ? 1 : 0} as={!hasChild && menu.as} to={menu.to} key={index}>
                  <MenuItemTextWrapper haschild={hasChild ? 1 : 0}>
                    <MenuItemTextExtended>{getTranslationManually(menu.label[0], menu.label[1])}</MenuItemTextExtended>
                  </MenuItemTextWrapper>
                  {hasChild && (
                    <MenuItemChildWrapperCollapsed>
                      {menu.childs.map((child) => (
                        <MenuItemTextExtendedChild
                          key={child.path[0]}
                          as={menu.as}
                          to={child.path[0]}
                          className={isChildActive(child.path) && "u-text-flip-orange"}
                        >
                          {getTranslationManually(child.label[0], child.label[1])}
                        </MenuItemTextExtendedChild>
                      ))}
                    </MenuItemChildWrapperCollapsed>
                  )}
                </MenuItemLinkExtended>
              )}
            </MenuItem>
          </div>
        );
      })}
    </MenuList>
  );
});

MenuBar.defaultProps = defaultProps;
MenuBar.propTypes = propTypes;

export default MenuBar;
