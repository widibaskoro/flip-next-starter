import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

export const MenuWrapper = styled.div`
  position: relative;
`;

export const MenuList = styled.div`
  background: ${colors.white};
  float: none;
  margin: 0;
  padding: 0;
  position: relative;
`;

export const MenuItem = styled.div`
  display: block;
  min-height: 40px;
  margin-bottom: 4px;
  position: relative;
`;

export const IconWrapper = styled.div`
  position: absolute;
  height: 40px;
  width: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  color: ${colors.black_bekko};
  right: 0;
  top: 0;
  transition: opacity ease;
  transition-delay: 0.2s;
  border-left: 2px solid ${colors.white};

  .is-collapsed & {
    display: none;
  }

  &:hover,
  .is-active & {
    color: ${colors.flip_orange};
    background-color: ${colors.light_anemia};
    border-radius: 0 8px 8px 0;
  }
`;

export const MenuItemWrapper = styled.div`
  overflow: hidden;
  max-height: 40px;
  transition: max-height 0.4s ease;

  .is-collapsed & {
    max-height: 40px !important;
    transition: none !important;
  }

  &.c-menu-active {
    max-height: 300px;
  }
`;

export const MenuItemLink = styled.div`
  align-items: center;
  background: ${colors.white};
  border: none;
  border-radius: 8px;
  display: flex;
  height: 40px;
  padding: 10px 12px;
  text-decoration: none;
  width: 100%;
  overflow: hidden;
  transition: width 0.3s ease;

  &:hover,
  .is-active &,
  .is-collapsed .c-menu__item:hover & {
    background: ${colors.light_anemia};
    text-decoration: none;
  }

  &:hover + .c-icon__wrapper {
    color: ${colors.flip_orange};
  }

  &:focus {
    outline: 0;
    text-decoration: none;
  }

  .is-collapsed & {
    width: 41px;
    text-decoration: none;
    transition: width 0s;
  }

  .is-collapsed .c-menu__item:hover & {
    border-radius: 8px 0 0 8px;
  }
`;

export const MenuItemIcon = styled.span`
  display: inline-block;
  height: 18px;
  line-height: 1;
`;

export const MenuItemIconBase = styled(MenuItemIcon)`
  display: inline-block;

  .c-menubar__link:hover &,
  .is-collapsed .c-menu__item:hover & {
    display: none;
  }
`;

export const MenuItemIconHover = styled(MenuItemIcon)`
  display: none;

  .c-menubar__link:hover &,
  .is-collapsed .c-menu__item:hover & {
    display: inline-block;
  }
`;

export const MenuItemText = styled.p`
  color: ${colors.black_bekko};
  display: inline-block;
  font-style: normal;
  font-weight: ${typography.font_weight.bold};
  font-size: ${typography.font_size.base};
  line-height: ${typography.line_height};
  margin: 0 0 0 16px;
  vertical-align: top;
  white-space: nowrap;

  .is-active &,
  .c-menubar__link:hover & {
    color: ${colors.flip_orange};
  }
`;

export const MenuItemChildWrapperMain = styled.div`
  width: 100%;
  padding: 8px 0px 12px 48px;
`;

export const MenuItemLinkExtended = styled.div`
  align-items: center;
  border: none;
  left: 41px;
  position: absolute;
  top: 0;
  width: 0;
  height: 0;
  overflow: hidden;
  text-decoration: none;
  cursor: default;

  .is-collapsed .c-menu__item:hover & {
    width: ${({ haschild }) => (haschild ? "187px" : "max-content")};
    height: auto;
    text-decoration: none;
  }
`;

export const MenuItemTextWrapper = styled.div`
  border-radius: ${({ haschild }) => (haschild ? "0px 8px 0px 0px" : "0px 8px 8px 0px")};
  height: 40px;
  background: ${colors.light_anemia};
  width: 100%;
  padding: 10px 12px 10px 4px;
`;

export const MenuItemTextExtended = styled.p`
  color: ${colors.flip_orange};
  font-style: normal;
  font-weight: ${typography.font_weight.bold};
  font-size: ${typography.font_size.base};
  line-height: ${typography.line_height};
  margin: 0;
`;

export const MenuItemChildWrapperCollapsed = styled.div`
  width: 168px;
  margin-left: 19px;
  background: ${colors.white};
  border-radius: 0px 0px 8px 8px;
  max-height: 0px;
  box-shadow: -1px 21px 34px -24px rgb(0, 0, 0, 0.89);
  transition: max-height 0.5s linear;
  padding: 12px 0px 12px 12px;
  border: 0.5px solid ${colors.light_smoke};

  .is-collapsed .c-menu__item:hover & {
    max-height: 300px;
  }
`;

export const MenuItemTextExtendedChild = styled.p`
  color: ${colors.dark_smoke};
  display: inline-block;
  font-style: normal;
  font-weight: ${typography.font_weight.bold};
  font-size: ${typography.font_size.base};
  line-height: 21px;
  margin-bottom: 12px;
  cursor: pointer;
  text-decoration: none;
  width: 100%;
  white-space: nowrap;
  &:last-child {
    margin-bottom: 0;
  }

  &:hover {
    color: ${colors.flip_orange};
    text-decoration: none;
  }
`;
