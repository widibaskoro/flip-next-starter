import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { StyledTable } from "./Table.styled";

const propTypes = {
  /**
   * Set rows to be having striped style
   */
  zebra: PropTypes.bool
};

const defaultProps = {
  zebra: false
};

const Table = React.forwardRef((props, ref) => {
  const { children, className, zebra, ...restProps } = props;
  const cardClass = classNames("c-table", zebra && "c-table--zebra", className);

  return (
    <StyledTable data-testid="qa-table" ref={ref} className={cardClass} {...restProps}>
      {children}
    </StyledTable>
  );
});

Table.propTypes = propTypes;
Table.defaultProps = defaultProps;

export default Table;
