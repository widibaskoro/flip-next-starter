import React from "react";
import { render, screen } from "@testing-library/react";

import Table from "./index";
import { rgba } from "polished";
import { checkProps } from "src/helpers/test";
import { colors, typography } from "src/assets/styles/settings";

const DUMMY_HEADS = ["#", "Id Transaksi", "Nama Penerima", "Bank Penerima", "Nomor Rekening", "Waktu Dibuat"];
const DUMMY_CONTENT = [
  ["1", "12755093", "Bambang Kurniawan", "BNI/BNI Syariah", "127383928372321", "08 Jul 2020 17:01:11"],
  ["2", "12755061", "Kunto Coroko", "Bank Syariah Mandiri", "31329892444", "08 Jul 2020 17:01:11"],
  ["3", "12755738", "Asmiranda Budiman", "BCA", "888372823", "08 Jul 2020 17:01:11"]
];

function RenderTable({ zebra = false, className } = {}) {
  return (
    <Table zebra={zebra} className={className}>
      <thead>
        <tr>
          {DUMMY_HEADS.map((item, i) => (
            <th key={i}>{item}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {DUMMY_CONTENT.map((item, i) => (
          <tr key={i}>
            {item.map((col, j) => (
              <td key={j}>{col}</td>
            ))}
          </tr>
        ))}
      </tbody>
    </Table>
  );
}

describe("Table component", () => {
  describe("Table rendering", () => {
    it("Should render without error", () => {
      render(<Table></Table>);
      expect(screen.getByTestId("qa-table")).toBeInTheDocument();
    });
  });

  describe("Table propTypes", () => {
    it("Should not throw warning when all expected props types provided", () => {
      const defaultProps = {
        zebra: false
      };
      checkProps(Table, defaultProps, "Table");
    });
  });

  describe("Table props", () => {
    it("should have custom className when provided", () => {
      render(RenderTable({ className: "u-m-1" }));
      expect(screen.getByTestId("qa-table").className).toContain("u-m-1");
    });
  });

  describe("Table content", () => {
    it("Should contain correct content when one content is given", () => {
      render(RenderTable());
      expect(screen.getByTestId("qa-table")).toHaveTextContent("Id Transaksi");
    });
  });

  describe("Table style", () => {
    it("should have expected style", () => {
      render(RenderTable());
      const expectedStyle = {
        color: colors.black_bekko,
        "border-collapse": "collapse",
        "max-width": "100%",
        width: "100%"
      };
      expect(screen.getByTestId("qa-table")).toHaveStyle(expectedStyle);
    });

    it("should have expected style for table heading", () => {
      render(RenderTable());
      const expectedStyle = {
        color: colors.dark_grey,
        "font-size": typography.font_size.small,
        "font-weight": typography.font_weight.bold,
        "line-height": typography.line_height,
        padding: "8px 12px",
        "text-align": "left",
        "text-transform": "uppercase",
        "vertical-align": "bottom"
      };
      expect(screen.getByText("Id Transaksi")).toHaveStyle(expectedStyle);
    });

    it("should have expected style for table zebra", () => {
      render(RenderTable({ zebra: true }));
      expect(screen.getAllByRole("row")[1]).toHaveStyle({
        "background-color": rgba(colors.catskill_white, 0.32)
      });
    });
  });
});
