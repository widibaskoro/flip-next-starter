import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";
import { rgba } from "polished";

export const StyledTable = styled.table`
  color: ${colors.black_bekko};
  border-collapse: collapse;
  max-width: 100%;
  width: 100%;

  thead > tr > th {
    color: ${colors.dark_grey};
    font-size: ${typography.font_size.small};
    font-weight: ${typography.font_weight.bold};
    line-height: ${typography.line_height};
    padding: 8px 12px;
    text-align: left;
    text-transform: uppercase;
    vertical-align: bottom;
  }

  td {
    padding: 16px 12px;
  }

  &.c-table--zebra {
    tbody tr {
      &:nth-of-type(odd) {
        background-color: ${rgba(colors.catskill_white, 0.32)};
      }
    }
  }
`;
