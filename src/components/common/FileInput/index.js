import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { Button, Loader } from "src/components/common";
import {
  FileInputComponent,
  FileInputField,
  FileInputBox,
  FileInputContent,
  FileInputBoxDragging,
  FileInputPlaceholder,
  FileInputSeparatorText,
  FileInputSizeText,
  InvalidText,
  LoadingWrapper,
  LoadingText
} from "./FileInput.styled";

import { getTranslationManually } from "src/helpers/locale";
import { bytesToSize } from "src/helpers/number";

const propTypes = {
  /**
   * Field placeholder
   */
  placeholder: PropTypes.string,

  /**
   * Set accepted file types (file extension separated by coma)
   * e.g: ".png, .jpg"
   */
  fileTypes: PropTypes.string,

  /**
   * Set max file size (in bytes)
   */
  maxFileSize: PropTypes.number,

  /**
   * Set field to be uploading state
   */
  isUploading: PropTypes.bool,

  /**
   * Set field to be disabled
   */
  disabled: PropTypes.bool,

  /**
   * Callback after file accepted
   */
  onChange: PropTypes.func,

  /**
   * if true, reset file to null
   */
  resetFile: PropTypes.bool
};

const defaultProps = {
  placeholder: getTranslationManually("Taruh di sini", "Drop here"),
  fileTypes: "",
  maxFileSize: 2097152,
  isUploading: false,
  disabled: false,
  resetFile: false,
  onChange: () => {}
};

function FileInput(props) {
  const {
    className,
    placeholder,
    fileTypes,
    maxFileSize,
    isUploading,
    resetFile,
    disabled,
    onChange,
    ...restProps
  } = props;

  const [fileName, setFileName] = useState("");
  const [isDragging, setIsDragging] = useState(false);
  const [dragCounter, setDragCounter] = useState(0);
  const [invalidText, setInvalidText] = useState("");

  const fileInputClass = classNames("c-file-input", disabled && "is-disabled", className);
  const fileInputRef = useRef(null);
  const texts = {
    uploading: getTranslationManually("Sedang mengunggah file", "Uploading file"),
    or: getTranslationManually("atau", "or"),
    button: getTranslationManually("Pilih file", "Choose file"),
    maxSize: getTranslationManually("Ukuran maksimal file yang boleh diunggah", "Maximum allowed file size"),
    drop: getTranslationManually("Taruh di sini", "Drop here")
  };

  function processFile(file) {
    const fileExt = `.${file.name.split(".").pop().toLowerCase()}`;

    if (file.size > maxFileSize) {
      const oversizeText = getTranslationManually(
        "Ukuran file melebihi batas maksimal!",
        "File size exceeds maximum allowed size!"
      );
      setInvalidText(oversizeText);
    } else if (fileTypes !== "" && !fileTypes.includes(fileExt)) {
      setInvalidText(getTranslationManually("Tipe file tidak sesuai!", "File format invalid!"));
    } else {
      setInvalidText("");
      setFileName(file.name);
      onChange(file);
    }
  }

  function handleChange(event) {
    const file = event.target.files[0];
    if (file) {
      processFile(file);
    }
  }

  function onDragEnter(event) {
    event.preventDefault();
    event.stopPropagation();

    if (!disabled) {
      setDragCounter(dragCounter + 1);
      if (event.dataTransfer.items && event.dataTransfer.items.length > 0) {
        setIsDragging(true);
      }
    }
  }

  function onDragLeave(event) {
    event.preventDefault();
    event.stopPropagation();

    if (!disabled) {
      const drc = dragCounter - 1;
      setDragCounter(drc);
      if (drc === 0) {
        setIsDragging(false);
      }
    }
  }

  function onDragOver(event) {
    event.preventDefault();
    event.stopPropagation();
  }

  function onDrop(event) {
    event.preventDefault();
    event.stopPropagation();

    if (!disabled) {
      setIsDragging(false);
      if (event.dataTransfer.files && event.dataTransfer.files.length > 0) {
        const file = event.dataTransfer.files[0];
        processFile(file);
        event.dataTransfer.clearData();
        setDragCounter(0);
      }
    }
  }

  useEffect(() => {
    if (resetFile) {
      fileInputRef.current.value = null;
      setInvalidText("");
    }
  }, [resetFile]);

  return (
    <FileInputComponent className={fileInputClass} data-testid="qa-file-input" {...restProps}>
      {isUploading ? (
        <LoadingWrapper>
          <Loader />
          <LoadingText data-testid="qa-file-input-loading-text">
            {texts.uploading} <b>{fileName}...</b>
          </LoadingText>
        </LoadingWrapper>
      ) : (
        <>
          <FileInputField
            data-testid="qa-file-input-field"
            ref={fileInputRef}
            type="file"
            accept={fileTypes}
            onChange={handleChange}
            disabled={disabled}
          />
          <FileInputBox
            data-testid="qa-file-input-box"
            className={classNames(disabled && "is-disabled")}
            onDragEnter={onDragEnter}
            onDragLeave={onDragLeave}
            onDragOver={onDragOver}
            onDrop={onDrop}
          >
            <FileInputContent className={classNames(isDragging && "is-hidden")}>
              <FileInputPlaceholder>{placeholder}</FileInputPlaceholder>
              <FileInputSeparatorText>{texts.or}</FileInputSeparatorText>
              <Button
                data-testid="qa-file-input-button"
                variant="secondary"
                disabled={disabled}
                onClick={() => fileInputRef.current.click()}
              >
                {texts.button}
              </Button>
              <FileInputSizeText>
                {texts.maxSize}: <b>{bytesToSize(maxFileSize)}</b>
              </FileInputSizeText>
              {invalidText !== "" && <InvalidText data-testid="qa-file-input-invalid-text">{invalidText}</InvalidText>}
            </FileInputContent>
            {isDragging && (
              <FileInputBoxDragging data-testid="qa-file-input-dragging-box">{texts.drop}...</FileInputBoxDragging>
            )}
          </FileInputBox>
        </>
      )}
    </FileInputComponent>
  );
}

FileInput.propTypes = propTypes;
FileInput.defaultProps = defaultProps;

export default FileInput;
