import styled from "styled-components";

import { colors, typography, zIndex } from "src/assets/styles/settings";
import { rgba } from "polished";

export const FileInputComponent = styled.div`
  background-color: ${colors.light_smoke};
  border-radius: 12px;
  color: ${colors.dark_grey};
  padding: 20px;
  position: relative;

  &.is-disabled {
    background: ${colors.disabled};
  }
`;

export const FileInputField = styled.input.attrs({ type: "file" })`
  border: 0;
  height: 1px;
  opacity: 0;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`;

export const FileInputBox = styled.div`
  border: 1px dashed ${colors.flip_orange};
  border-radius: 8px;
  display: block;
  padding: 28px 0;
  margin-bottom: 0;
  position: relative;
  text-align: center;
  width: 100%;

  &.is-disabled {
    border-color: ${colors.light_grey};
  }
`;

export const FileInputContent = styled.div`
  visibility: visible;

  &.is-hidden {
    visibility: hidden;
  }
`;

export const FileInputBoxDragging = styled.div`
  background: ${rgba(colors.flip_orange, 0.12)};
  border-radius: 8px;
  color: ${colors.flip_orange};
  font-size: ${typography.font_size.huge};
  font-weight: ${typography.font_weight.bold};
  padding: 88px;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  text-align: center;
  vertical-align: middle;
  z-index: ${zIndex.fileInput};
`;

export const FileInputPlaceholder = styled.p`
  color: ${colors.flip_orange};
  font-weight: ${typography.font_weight.bold};

  .is-disabled & {
    color: ${colors.light_grey};
  }
`;

export const FileInputSeparatorText = styled.p`
  color: ${colors.dark_grey};

  .is-disabled & {
    color: ${colors.light_grey};
  }
`;

export const FileInputSizeText = styled.p`
  color: ${colors.dark_grey};
  margin: 28px 0 0 0;

  .is-disabled & {
    color: ${colors.light_grey};
  }
`;

export const InvalidText = styled.p`
  color: ${colors.ketchup_tomato};
  margin: 0;
`;

export const LoadingWrapper = styled.div`
  border: 1px dashed ${colors.flip_orange};
  border-radius: 5px;
  color: ${colors.flip_orange};
  display: block;
  padding: 72px 0;
  text-align: center;
  margin: 0;
  width: 100%;
`;

export const LoadingText = styled.p`
  color: ${colors.flip_orange};
  margin: 16px 0 0 0;
`;
