import React from "react";
import { render, screen, createEvent, fireEvent } from "@testing-library/react";
import user from "@testing-library/user-event";

import FileInput from "./index";

describe("FileInput component", () => {
  describe("FileInput rendering", () => {
    it("Should render without error", () => {
      render(<FileInput />);
      expect(screen.getByTestId("qa-file-input")).toBeInTheDocument();
    });
  });

  describe("FileInput functionalities", () => {
    it("Should upload the file and call the onChange callback function with correct file", () => {
      const spy = jest.fn();
      render(<FileInput onChange={spy} />);

      const file = new File(["hello"], "hello.png");
      const input = screen.getByTestId("qa-file-input-field");
      user.upload(input, file);

      expect(input.files[0]).toStrictEqual(file);
      expect(input.files).toHaveLength(1);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(file);
    });

    it("Should NOT call the onChange callback function when no file is uploaded", () => {
      const spy = jest.fn();
      render(<FileInput onChange={spy} />);

      const input = screen.getByTestId("qa-file-input-field");
      user.upload(input);

      expect(spy).toHaveBeenCalledTimes(0);
    });

    it("Should NOT call the onChange function and have no file when uploaded a file and state is DISABLED", () => {
      const spy = jest.fn();
      render(<FileInput onChange={spy} disabled />);

      const input = screen.getByTestId("qa-file-input-field");
      const file = new File(["hello"], "hello.png");
      user.upload(input, file);

      expect(spy).toHaveBeenCalledTimes(0);
      expect(input.files[0]).toStrictEqual(undefined);
      expect(input.files).toHaveLength(0);
    });
  });

  describe("FileInput file size", () => {
    it("Should show size invalid message when file size is larger than default allowed size: 2097152", () => {
      const fileContent = "a".repeat(2097153);
      const file = new File([fileContent], "hello.png");
      render(<FileInput />);

      const input = screen.getByTestId("qa-file-input-field");
      user.upload(input, file);

      expect(screen.queryByTestId("qa-file-input-invalid-text")).toBeInTheDocument();
      expect(screen.getByTestId("qa-file-input-invalid-text")).toHaveTextContent(
        "Ukuran file melebihi batas maksimal!"
      );
    });

    it("Should success when file size is smaller than given allowed size: 3097152", () => {
      const fileContent = "a".repeat(2097153);
      const file = new File([fileContent], "hello.png");
      render(<FileInput maxFileSize={3097152} />);

      const input = screen.getByTestId("qa-file-input-field");
      user.upload(input, file);

      expect(screen.queryByTestId("qa-file-input-invalid-text")).toBeNull();
    });

    it("Should show size invalid message when file size is larger than given allowed size: 3097152", () => {
      const fileContent = "a".repeat(3097153);
      const file = new File([fileContent], "hello.png");
      render(<FileInput maxFileSize={3097152} />);

      const input = screen.getByTestId("qa-file-input-field");
      user.upload(input, file);

      expect(screen.queryByTestId("qa-file-input-invalid-text")).toBeInTheDocument();
      expect(screen.getByTestId("qa-file-input-invalid-text")).toHaveTextContent(
        "Ukuran file melebihi batas maksimal!"
      );
    });
  });

  describe("FileInput file type", () => {
    it("Should success when upload jpg file when given allowed file type is 'png, jpg, jpeg'", () => {
      const file = new File(["hello"], "hello.png");
      render(<FileInput fileTypes="png, jpg, jpeg" />);

      const input = screen.getByTestId("qa-file-input-field");
      user.upload(input, file);

      expect(input.files[0]).toStrictEqual(file);
      expect(input.files).toHaveLength(1);
    });

    it('Should success when upload jpg file when given allowed file type is ".png, .jpg, .jpeg"', () => {
      const file = new File(["hello"], "hello.png");
      render(<FileInput fileTypes=".png, .jpg, .jpeg" />);

      const input = screen.getByTestId("qa-file-input-field");
      user.upload(input, file);

      expect(input.files[0]).toStrictEqual(file);
      expect(input.files).toHaveLength(1);
    });

    it('Should show invalid file type message when given a pdf file and allowed file type is ".png, .jpg, .jpeg"', () => {
      const file = new File(["hello"], "hello.pdf");
      render(<FileInput fileTypes=".png, .jpg, .jpeg" />);

      const input = screen.getByTestId("qa-file-input-field");
      user.upload(input, file);

      expect(screen.queryByTestId("qa-file-input-invalid-text")).toBeInTheDocument();
      expect(screen.getByTestId("qa-file-input-invalid-text")).toHaveTextContent("Tipe file tidak sesuai!");
    });
  });

  describe("FileInput drag behavior", () => {
    it("Should show the placeholder text when a file is dragged in to the dropzone", () => {
      const spy = jest.fn();
      render(<FileInput onChange={spy} />);

      const file = new File(["hello"], "hello.png");
      const dropZone = screen.getByTestId("qa-file-input-box");
      const fileDragEnterEvent = createEvent.dragEnter(dropZone);
      Object.defineProperty(fileDragEnterEvent, "dataTransfer", {
        value: {
          files: [file],
          items: [file]
        }
      });

      fireEvent(dropZone, fileDragEnterEvent);
      expect(screen.getByTestId("qa-file-input-dragging-box")).toHaveTextContent("Taruh di sini...");
    });

    it("Should NOT show the placeholder text when a file is dragged in to the dropzone when state is DISABLED", () => {
      const spy = jest.fn();
      render(<FileInput onChange={spy} disabled />);

      const file = new File(["hello"], "hello.png");
      const dropZone = screen.getByTestId("qa-file-input-box");
      const fileDragEnterEvent = createEvent.dragEnter(dropZone);
      Object.defineProperty(fileDragEnterEvent, "dataTransfer", {
        value: {
          files: [file],
          items: [file]
        }
      });

      fireEvent(dropZone, fileDragEnterEvent);
      expect(screen.queryByTestId("qa-file-input-dragging-box")).toBeNull();
    });

    it("Should NOT show the placeholder text when a file is dragged in to the dropzone when no file is carried", () => {
      const spy = jest.fn();
      render(<FileInput onChange={spy} />);

      const dropZone = screen.getByTestId("qa-file-input-box");
      const fileDragEnterEvent = createEvent.dragEnter(dropZone);
      Object.defineProperty(fileDragEnterEvent, "dataTransfer", {
        value: {}
      });

      fireEvent(dropZone, fileDragEnterEvent);
      expect(screen.queryByTestId("qa-file-input-dragging-box")).toBeNull();
    });

    it("Should show and hide the placeholder text when a file is dragged in to and out of the dropzone", () => {
      const spy = jest.fn();
      render(<FileInput onChange={spy} />);

      const file = new File(["hello"], "hello.png");
      const dropZone = screen.getByTestId("qa-file-input-box");
      const fileDragEnterEvent = createEvent.dragEnter(dropZone);
      const fileDragLeaveEvent = createEvent.dragLeave(dropZone);
      const fileDragOverEvent = createEvent.dragOver(dropZone);
      Object.defineProperty(fileDragEnterEvent, "dataTransfer", {
        value: {
          files: [file],
          items: [file]
        }
      });

      fireEvent(dropZone, fileDragEnterEvent);
      expect(screen.getByTestId("qa-file-input-dragging-box")).toHaveTextContent("Taruh di sini...");

      fireEvent(dropZone, fileDragLeaveEvent);
      fireEvent(dropZone, fileDragOverEvent);
      expect(screen.queryByTestId("qa-file-input-dragging-box")).toBeNull();
    });

    it("Should NOT show the placeholder text when a file is dragged in to and out of the dropzone when state is DISABLED", () => {
      const spy = jest.fn();
      render(<FileInput onChange={spy} disabled />);

      const file = new File(["hello"], "hello.png");
      const dropZone = screen.getByTestId("qa-file-input-box");
      const fileDragEnterEvent = createEvent.dragEnter(dropZone);
      const fileDragLeaveEvent = createEvent.dragLeave(dropZone);
      const fileDragOverEvent = createEvent.dragOver(dropZone);
      Object.defineProperty(fileDragEnterEvent, "dataTransfer", {
        value: {
          files: [file],
          items: [file]
        }
      });

      fireEvent(dropZone, fileDragEnterEvent);
      expect(screen.queryByTestId("qa-file-input-dragging-box")).toBeNull();

      fireEvent(dropZone, fileDragLeaveEvent);
      fireEvent(dropZone, fileDragOverEvent);
      expect(screen.queryByTestId("qa-file-input-dragging-box")).toBeNull();
    });

    it("Should NOT show the placeholder when dragLeave event is performed", () => {
      const spy = jest.fn();
      render(<FileInput onChange={spy} />);

      const dropZone = screen.getByTestId("qa-file-input-box");
      const fileDragLeaveEvent = createEvent.dragLeave(dropZone);

      fireEvent(dropZone, fileDragLeaveEvent);
      expect(screen.queryByTestId("qa-file-input-dragging-box")).toBeNull();
    });
  });

  describe("FileInput drop behavior", () => {
    it("Should return correct file when a file is dropped in the dropzone", () => {
      const spy = jest.fn();
      render(<FileInput onChange={spy} />);

      const file = new File(["hello"], "hello.png");
      const dropZone = screen.getByTestId("qa-file-input-box");
      const fileDropEvent = createEvent.drop(dropZone);
      Object.defineProperty(fileDropEvent, "dataTransfer", {
        value: {
          files: [file],
          clearData: () => {}
        }
      });

      fireEvent(dropZone, fileDropEvent);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(file);
    });

    it("Should NOT call the onChange function and have no file when dropped a file and the state is DISABLED", () => {
      const spy = jest.fn();
      render(<FileInput onChange={spy} disabled />);

      const file = new File(["hello"], "hello.png");
      const dropZone = screen.getByTestId("qa-file-input-box");
      const fileDropEvent = createEvent.drop(dropZone);
      Object.defineProperty(fileDropEvent, "dataTransfer", {
        value: {
          files: [file],
          clearData: () => {}
        }
      });

      fireEvent(dropZone, fileDropEvent);
      expect(spy).toHaveBeenCalledTimes(0);
      expect(spy).not.toHaveBeenCalledWith(file);
    });

    it("Should NOT call the onChange function when drop event is performed but no file is carried", () => {
      const spy = jest.fn();
      render(<FileInput onChange={spy} />);

      const dropZone = screen.getByTestId("qa-file-input-box");
      const fileDropEvent = createEvent.drop(dropZone);
      Object.defineProperty(fileDropEvent, "dataTransfer", {
        value: {
          files: [],
          clearData: () => {}
        }
      });

      fireEvent(dropZone, fileDropEvent);
      expect(spy).toHaveBeenCalledTimes(0);
    });
  });

  describe("FileInput button click", () => {
    it('Should click the input field when the "Pilih File" button is clicked', () => {
      render(<FileInput />);

      const input = screen.getByTestId("qa-file-input-field");
      const spy = jest.spyOn(input, "click");

      fireEvent.click(screen.getByTestId("qa-file-input-button"));
      expect(spy).toHaveBeenCalled();
    });
  });

  describe("FileInput isUploading prop", () => {
    it("Should show the uploading loading text and hide the input when the isUploading prop is true", () => {
      render(<FileInput isUploading={true} />);

      expect(screen.queryByTestId("qa-file-input-loading-text")).toBeInTheDocument();
      expect(screen.getByTestId("qa-file-input-loading-text")).toHaveTextContent("Sedang mengunggah file");

      expect(screen.queryByTestId("qa-file-input-field")).toBeNull();
      expect(screen.queryByTestId("qa-file-input-box")).toBeNull();
    });
  });
});
