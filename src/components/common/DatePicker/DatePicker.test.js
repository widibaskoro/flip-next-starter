import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import { checkProps } from "src/helpers/test";
import { format } from "date-fns";
import { getCurrentLocale } from "src/helpers/locale";
import DatePicker from "./index";
import Cookies from "js-cookie";

const DEFAULT_DATE_FORMAT = "dd MMM yyyy";
const CURRENT_YEAR = new Date().getFullYear();
const CURRENT_MONTH = new Date().getMonth();
const CURRENT_DAY = new Date().getDay();

const createAriaLabel = ({ day = CURRENT_DAY, month = CURRENT_MONTH, year = CURRENT_YEAR } = {}) => {
  const choosenDate = new Date(year, month, day);
  const formatedDate = "Choose " + format(choosenDate, "EEEE, MMMM do, yyyy");
  return formatedDate;
};

const getMonthString = (monthNum) => {
  return new Date(CURRENT_YEAR, monthNum).toLocaleString(getCurrentLocale(), { month: "short" });
};

describe("DatePicker component", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  describe("DatePicker rendering", () => {
    it("should render without error", () => {
      render(<DatePicker />);
      expect(screen.getByTestId("qa-date-picker")).toBeInTheDocument();
    });
  });

  describe("DatePicker functionality", () => {
    it("should show DatePicker modal when input is clicked", () => {
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      expect(screen.getByTestId("qa-date-picker-selected-date-display")).toBeInTheDocument();
    });

    it("should hide DatePicker modal when simpan button is clicked", () => {
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      fireEvent.click(screen.getByText(/simpan|apply/i));
      expect(screen.queryByTestId("qa-date-picker-selected-date-display")).not.toBeInTheDocument();
    });

    it("should hide DatePicker modal when reset button is clicked", () => {
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      fireEvent.click(screen.getByText(/reset/i));
      expect(screen.queryByTestId("qa-date-picker-selected-date-display")).not.toBeInTheDocument();
    });

    it("should hide DatePicker modal when click outside the modal", () => {
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      userEvent.click(document.body);
      expect(screen.queryByTestId("qa-date-picker-selected-date-display")).not.toBeInTheDocument();
    });

    it("should change date day in display header when certain day is clicked", () => {
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      const day = screen.getByRole("button", {
        name: createAriaLabel({ day: 5 })
      });
      fireEvent.click(day);
      expect(screen.getByTestId("qa-date-picker-selected-date-display")).toHaveTextContent(
        `05 ${getMonthString(CURRENT_MONTH)} ${CURRENT_YEAR}`
      );
    });

    it("should change month with ID format in display header when choose a month in options", () => {
      Cookies.get = jest.fn().mockImplementation(() => "id");
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      userEvent.selectOptions(screen.getByTestId("qa-date-picker-calendar-month-options"), "Oktober");
      const day = screen.getByRole("button", {
        name: createAriaLabel({ day: 10, month: 9 })
      });
      fireEvent.click(day);
      expect(screen.getByTestId("qa-date-picker-selected-date-display")).toHaveTextContent(
        `10 ${getMonthString(9)} ${CURRENT_YEAR}`
      );
    });

    it("should change month with ID format in display header by current month + 1 when pressing next button", () => {
      Cookies.get = jest.fn().mockImplementation(() => "id");
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      fireEvent.click(screen.getByTestId("qa-date-picker-calendar-month-button-next"));
      const day = screen.getByRole("button", {
        name: createAriaLabel({ day: 11, month: CURRENT_MONTH + 1 })
      });
      fireEvent.click(day);
      expect(screen.getByTestId("qa-date-picker-selected-date-display")).toHaveTextContent(
        `11 ${getMonthString(CURRENT_MONTH + 1)} ${CURRENT_YEAR}`
      );
    });

    it("should change month with ID format in display header by current month - 1 when pressing previous button", () => {
      Cookies.get = jest.fn().mockImplementation(() => "id");
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      fireEvent.click(screen.getByTestId("qa-date-picker-calendar-month-button-prev"));
      const day = screen.getByRole("button", {
        name: createAriaLabel({ day: 2, month: CURRENT_MONTH - 1 })
      });
      fireEvent.click(day);
      expect(screen.getByTestId("qa-date-picker-selected-date-display")).toHaveTextContent(
        `02 ${getMonthString(CURRENT_MONTH - 1)} ${CURRENT_YEAR}`
      );
    });

    it("should change month with EN format in display header when choose a month in options", () => {
      Cookies.get = jest.fn().mockImplementation(() => "en");
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      userEvent.selectOptions(screen.getByTestId("qa-date-picker-calendar-month-options"), "October");
      const day = screen.getByRole("button", {
        name: createAriaLabel({ day: 10, month: 9 })
      });
      fireEvent.click(day);
      expect(screen.getByTestId("qa-date-picker-selected-date-display")).toHaveTextContent(
        `10 ${getMonthString(9)} ${CURRENT_YEAR}`
      );
    });

    it("should change month with EN format in display header by current month + 1 when pressing next button", () => {
      Cookies.get = jest.fn().mockImplementation(() => "en");
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      fireEvent.click(screen.getByTestId("qa-date-picker-calendar-month-button-next"));
      const day = screen.getByRole("button", {
        name: createAriaLabel({ day: 11, month: CURRENT_MONTH + 1 })
      });
      fireEvent.click(day);
      expect(screen.getByTestId("qa-date-picker-selected-date-display")).toHaveTextContent(
        `11 ${getMonthString(CURRENT_MONTH + 1)} ${CURRENT_YEAR}`
      );
    });

    it("should change month with EN format in display header by current month - 1 when pressing previous button", () => {
      Cookies.get = jest.fn().mockImplementation(() => "en");
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      fireEvent.click(screen.getByTestId("qa-date-picker-calendar-month-button-prev"));
      const day = screen.getByRole("button", {
        name: createAriaLabel({ day: 2, month: CURRENT_MONTH - 1 })
      });
      fireEvent.click(day);
      expect(screen.getByTestId("qa-date-picker-selected-date-display")).toHaveTextContent(
        `02 ${getMonthString(CURRENT_MONTH - 1)} ${CURRENT_YEAR}`
      );
    });

    it("should change year in display header when choose a year in options", () => {
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      userEvent.selectOptions(screen.getByTestId("qa-date-picker-calendar-year-options"), "2018");
      const day = screen.getByRole("button", {
        name: createAriaLabel({ day: 5, year: 2018 })
      });
      fireEvent.click(day);
      expect(screen.getByTestId("qa-date-picker-selected-date-display")).toHaveTextContent(
        `5 ${getMonthString(CURRENT_MONTH)} 2018`
      );
    });

    it("should change year in display header by current year + 1 when pressing next button", () => {
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      fireEvent.click(screen.getByTestId("qa-date-picker-calendar-year-button-next"));
      const day = screen.getByRole("button", {
        name: createAriaLabel({ day: 11, year: CURRENT_YEAR + 1 })
      });
      fireEvent.click(day);
      expect(screen.getByTestId("qa-date-picker-selected-date-display")).toHaveTextContent(
        `11 ${getMonthString(CURRENT_MONTH)} ${CURRENT_YEAR + 1}`
      );
    });

    it("should change year in display header by current year - 1 when pressing previous button", () => {
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      fireEvent.click(screen.getByTestId("qa-date-picker-calendar-year-button-prev"));
      const day = screen.getByRole("button", {
        name: createAriaLabel({ day: 11, year: CURRENT_YEAR - 1 })
      });
      fireEvent.click(day);
      expect(screen.getByTestId("qa-date-picker-selected-date-display")).toHaveTextContent(
        `11 ${getMonthString(CURRENT_MONTH)} ${CURRENT_YEAR - 1}`
      );
    });

    it("should call onChangeDate function props by date when simpan button is clicked", () => {
      const date = new Date();
      const spy = jest.fn();
      render(<DatePicker onChangeDate={() => spy(date)} />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      fireEvent.click(screen.getByText(/simpan|apply/i));
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(date);
    });

    it("should set date in DatePicker input when simpan button is clicked", () => {
      const currentDate = new Date().getDate();
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      fireEvent.click(screen.getByText(/simpan|apply/i));
      expect(screen.getByTestId("qa-date-picker-input-field")).toHaveValue(
        `${currentDate.toString().padStart(2, "0")} ${getMonthString(CURRENT_MONTH)} ${CURRENT_YEAR}`
      );
    });

    it("should set date in DatePicker input to empty string when reset button is clicked", () => {
      render(<DatePicker />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      fireEvent.click(screen.getByText(/reset/i));
      expect(screen.getByTestId("qa-date-picker-input-field")).toHaveValue("");
    });
  });

  describe("DatePicker propTypes", () => {
    it("should not throw warning when selectedDate props type is string", () => {
      checkProps(DatePicker, { selectedDate: "" }, "DatePicker");
    });

    it("should not throw warning when selectedDate props type is object", () => {
      checkProps(DatePicker, { selectedDate: {} }, "DatePicker");
    });

    it("should not throw warning when selectedDate props type is Date", () => {
      checkProps(DatePicker, { selectedDate: new Date() }, "DatePicker");
    });

    it("should not throw warning when expected props is provided", () => {
      const expectedProps = {
        onChangeDate: () => {},
        inputPlaceholder: "placeholder",
        disabled: false,
        isInvalid: false
      };
      checkProps(DatePicker, expectedProps, "DatePicker");
    });
  });

  describe("DatePicker props", () => {
    it("should show correctly formated date at DatePicker input when selectedDate props is provided", () => {
      const date = new Date(2019, 10, 25);
      const formatedDate = format(date, DEFAULT_DATE_FORMAT);
      render(<DatePicker selectedDate={date} />);
      expect(screen.getByTestId("qa-date-picker-input-field")).toHaveValue(formatedDate);
    });

    it("should show correctly formated date at DatePicker header display when selectedDate props is provided", () => {
      const date = new Date(2019, 10, 25);
      const formatedDate = format(date, DEFAULT_DATE_FORMAT);
      render(<DatePicker selectedDate={date} />);
      fireEvent.click(screen.getByTestId("qa-date-picker-input-field"));
      expect(screen.getByTestId("qa-date-picker-selected-date-display")).toHaveTextContent(formatedDate);
    });

    it("should show placeholder when inputPlaceholder is provided", () => {
      render(<DatePicker inputPlaceholder="Select options" />);
      expect(screen.getByTestId("qa-date-picker-input-field").placeholder).toBe("Select options");
    });

    it("should be disabled when disabled props is true", () => {
      render(<DatePicker disabled />);
      expect(screen.getByTestId("qa-date-picker-input-field")).toBeDisabled();
    });

    it("should set date value to empty string when selectedDate props type other than Date", () => {
      render(<DatePicker selectedDate="non date" />);
      expect(screen.getByTestId("qa-date-picker-input-field")).toHaveValue("");
    });
  });
});
