import React, { useRef, useState, useEffect } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import DatePickerComponent from "./components/DatePickerComponent";
import DatePickerInput from "./components/DatePickerInput";
import { DatePickerContainer, DatepickerFeedback } from "./DatePicker.styled";

import { format } from "date-fns";
import { id, enGB } from "date-fns/locale";
import { getCurrentLocale } from "src/helpers/locale";
import { useOutsideClick } from "src/hooks/outside-click";

const propTypes = {
  /**
   * Set date value
   */
  selectedDate: PropTypes.oneOfType([PropTypes.string, PropTypes.object, PropTypes.instanceOf(Date)]),

  /**
   * Callback after date has been selected
   */
  onChangeDate: PropTypes.func,

  /**
   * Set datepicker input placeholder
   */
  inputPlaceholder: PropTypes.string,

  /*
   * Set datepicker disabled
   */
  disabled: PropTypes.bool,

  /**
   * set datepicker is invalid
   */
  isInvalid: PropTypes.bool,

  /**
   * position of timepicker modal
   *
   * @type { "right" | "left" | "bottom" }
   */
  position: PropTypes.oneOf(["right", "left", "bottom"])
};

const defaultProps = {
  selectedDate: new Date(),
  onChangeDate: () => {},
  inputPlaceholder: undefined,
  disabled: false,
  isInvalid: false,
  position: "left"
};

const DatePicker = React.forwardRef((props, ref) => {
  const {
    className,
    datepickerClass,
    selectedDate,
    onChangeDate,
    inputPlaceholder,
    disabled,
    isInvalid,
    position,
    ...restProps
  } = props;
  const defaultFormat = "dd MMM yyyy";
  const containerClass = classNames("c-datepicker__container", { "is-invalid": isInvalid }, className);

  const [isOpen, setIsOpen] = useState(false);
  const [selectedDateValue, setSelectedDateValue] = useState("");

  const datepickerCompRef = useRef(null);
  useOutsideClick(datepickerCompRef, () => {
    setIsOpen(false);
  });

  useEffect(() => {
    if (selectedDate && selectedDate instanceof Date) {
      setSelectedDateValue(format(selectedDate, defaultFormat, { locale: getCurrentLocale() === "en" ? enGB : id }));
    } else {
      setSelectedDateValue("");
    }
  }, [selectedDate]);

  function handleApplyDate(date) {
    setSelectedDateValue(date !== "" ? format(date, defaultFormat) : date);
    onChangeDate(date);
    setIsOpen(false);
  }

  function openDatePicker() {
    if (!disabled) setIsOpen(true);
  }

  return (
    <DatePickerContainer data-testid="qa-date-picker" ref={ref} className={containerClass}>
      <DatePickerInput
        value={selectedDateValue}
        onInputClick={openDatePicker}
        inputPlaceholder={inputPlaceholder}
        disabled={disabled}
      />
      {isOpen ? (
        <DatePickerComponent
          ref={datepickerCompRef}
          className={datepickerClass}
          selectedDate={selectedDate}
          defaultFormat={defaultFormat}
          onApplyDate={handleApplyDate}
          position={position}
          {...restProps}
        />
      ) : null}
    </DatePickerContainer>
  );
});

DatePicker.defaultProps = defaultProps;
DatePicker.propTypes = propTypes;

DatePicker.Feedback = DatepickerFeedback;

export default DatePicker;
