import styled from "styled-components";

import { colors, typography, zIndex } from "src/assets/styles/settings";

function getPosition(pos) {
  if (pos === "bottom") {
    return "bottom: 40px";
  }

  return `${pos}: 0`;
}

export const DatePickerWrapper = styled.div`
  background: ${colors.white};
  box-shadow: rgba(13, 22, 38, 0.1) 0px 2px 5px 1px, rgba(13, 22, 38, 0.1) 0px 4px 20px;
  border-radius: 8px;
  left: 40px;
  padding: 16px 4px 8px;
  position: absolute;
  text-align: center;
  z-index: ${zIndex.dropdown};
  ${({ position }) => getPosition(position)};
`;

export const SelectedDate = styled.div`
  background: ${colors.flip_gradient};
  border-radius: 8px 8px 0 0;
  color: ${colors.white};
  font-size: ${typography.font_size.huge};
  font-weight: ${typography.font_weight.bold};
  height: 90px;
  margin: -16px -4px 12px -4px;
  padding-top: 24px;
  padding-bottom: 24px;
  text-align: center;
  white-space: nowrap;
  width: calc(100% + 8px);
`;

export const CalendarFooter = styled.div`
  padding: 0 16px 12px 0;
  text-align: right;
  width: 100%;
`;
