import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight, faAngleLeft } from "@fortawesome/free-solid-svg-icons";

import {
  CalendarHeaderContent,
  CalendarNavButtonPrev,
  CalendarNavButtonNext,
  DropdownSelect,
  DropdownWrapperMonth,
  DropdownWrapperYear
} from "./CalendarHeader.styled";

import { range } from "lodash";
import { getMonth, getYear } from "date-fns";
import { getCurrentLocale } from "src/helpers/locale";
import { getLocalizeMonth } from "src/helpers/date";

function CalendarHeader(props) {
  const {
    date,
    changeYear,
    changeMonth,
    decreaseMonth,
    increaseMonth,
    decreaseYear,
    increaseYear,
    prevMonthButtonDisabled,
    nextMonthButtonDisabled,
    prevYearButtonDisabled,
    nextYearButtonDisabled
  } = props;

  const months = getLocalizeMonth(getCurrentLocale());
  const years = range(1950, getYear(new Date()) + 1, 1);

  function handleChangeMonth(evt) {
    changeMonth(months.indexOf(evt.target.value));
  }

  function handleChangeYear(evt) {
    changeYear(evt.target.value);
  }

  return (
    <CalendarHeaderContent data-testid="qa-date-picker-calendar-header" className="c-datepicker__header">
      <CalendarNavButtonPrev
        data-testid="qa-date-picker-calendar-month-button-prev"
        type="button"
        onClick={decreaseMonth}
        disabled={prevMonthButtonDisabled}
      >
        <FontAwesomeIcon icon={faAngleLeft} />
      </CalendarNavButtonPrev>
      <DropdownWrapperMonth>
        <DropdownSelect
          data-testid="qa-date-picker-calendar-month-options"
          value={months[getMonth(date)]}
          onChange={handleChangeMonth}
        >
          {months.map((option) => (
            <option key={option} value={option}>
              {option}
            </option>
          ))}
        </DropdownSelect>
      </DropdownWrapperMonth>
      <CalendarNavButtonNext
        data-testid="qa-date-picker-calendar-month-button-next"
        type="button"
        onClick={increaseMonth}
        disabled={nextMonthButtonDisabled}
      >
        <FontAwesomeIcon icon={faAngleRight} />
      </CalendarNavButtonNext>

      <CalendarNavButtonPrev
        data-testid="qa-date-picker-calendar-year-button-prev"
        type="button"
        onClick={decreaseYear}
        disabled={prevYearButtonDisabled}
      >
        <FontAwesomeIcon icon={faAngleLeft} />
      </CalendarNavButtonPrev>
      <DropdownWrapperYear>
        <DropdownSelect
          data-testid="qa-date-picker-calendar-year-options"
          value={getYear(date)}
          onChange={handleChangeYear}
        >
          {years.map((option) => (
            <option key={option} value={option}>
              {option}
            </option>
          ))}
        </DropdownSelect>
      </DropdownWrapperYear>
      <CalendarNavButtonNext
        data-testid="qa-date-picker-calendar-year-button-next"
        type="button"
        onClick={increaseYear}
        disabled={nextYearButtonDisabled}
      >
        <FontAwesomeIcon icon={faAngleRight} />
      </CalendarNavButtonNext>
    </CalendarHeaderContent>
  );
}

export default CalendarHeader;
