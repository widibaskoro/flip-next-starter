import React, { useState, useEffect } from "react";
import classNames from "classnames";

import ReactDatePicker, { registerLocale } from "react-datepicker";
import { DatePickerWrapper, SelectedDate, CalendarFooter } from "./DatePickerComponent.styled";
import Button from "../../Button";
import CalendarHeader from "./CalendarHeader";

import { format } from "date-fns";
import { id, enGB } from "date-fns/locale";
import { getCurrentLocale, getTranslationManually } from "src/helpers/locale";

import "./DatePicker.module.scss";
registerLocale("id", id);
registerLocale("en-GB", enGB);

const DatePickerComponent = React.forwardRef((props, ref) => {
  const { className, selectedDate, defaultFormat, onApplyDate, position, ...restProps } = props;
  const datepickerClass = classNames("c-datepicker", className);
  const localeString = getCurrentLocale() === "en" ? "en-GB" : "id";

  const [currentSelectedDate, setCurrentSelectedDate] = useState(
    selectedDate instanceof Date ? selectedDate : new Date()
  );
  const [currentSelectedDateValue, setCurrentSelectedDateValue] = useState("");

  function applyDate() {
    onApplyDate(currentSelectedDate);
  }

  function resetDate() {
    onApplyDate("");
  }

  useEffect(() => {
    if (currentSelectedDate && currentSelectedDate instanceof Date) {
      setCurrentSelectedDateValue(
        format(currentSelectedDate, defaultFormat, { locale: getCurrentLocale() === "en" ? enGB : id })
      );
    }
  }, [currentSelectedDate]);

  return (
    <DatePickerWrapper data-testid="qa-date-picker-component" ref={ref} className={datepickerClass} position={position}>
      <SelectedDate data-testid="qa-date-picker-selected-date-display" className="c-datepicker__selected-date">
        {currentSelectedDateValue}
      </SelectedDate>
      <ReactDatePicker
        calendarClassName="c-datepicker"
        renderCustomHeader={CalendarHeader}
        locale={localeString}
        selected={currentSelectedDate}
        onChange={(date) => setCurrentSelectedDate(date)}
        inline
        {...restProps}
      />
      <CalendarFooter className="c-datepicker__footer">
        <Button className="u-mr-2" variant="secondary" color="grey" onClick={resetDate} outline>
          Reset
        </Button>
        <Button variant="secondary" onClick={applyDate}>
          {getTranslationManually("Simpan", "Apply")}
        </Button>
      </CalendarFooter>
    </DatePickerWrapper>
  );
});

export default DatePickerComponent;
