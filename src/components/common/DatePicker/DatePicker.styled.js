import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

export const DatePickerContainer = styled.div`
  position: relative;
`;

export const DatepickerFeedback = styled.div`
  color: ${colors.ketchup_tomato};
  display: none;
  font-size: ${typography.font_size.small};
  margin-top: 4px;
  order: 4;
  width: 100%;

  .c-datepicker.is-invalid ~ &,
  .c-datepicker__container.is-invalid ~ & {
    display: block;
  }
`;
