import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";

import Alert from "./index";
import { checkProps } from "src/helpers/test";
import { colors } from "src/assets/styles/settings";

describe("Alert component", () => {
  describe("Alert rendering", () => {
    it("Should render without error", () => {
      render(<Alert />);
      expect(screen.getByTestId("qa-alert")).toBeInTheDocument();
    });
  });

  describe("Alert functionality", () => {
    it("Should calls provided function when close button is clicked", () => {
      const spy = jest.fn();
      render(<Alert onCloseClick={() => spy("x")} withCloseButton />);
      fireEvent.click(screen.getByTestId("qa-alert-close"));
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith("x");
    });
  });

  describe("Alert propTypes", () => {
    it("Should not throw warning when expected colors (enum) are provided", () => {
      checkProps(Alert, { color: "blue" }, "Alert");
      checkProps(Alert, { color: "green" }, "Alert");
      checkProps(Alert, { color: "grey" }, "Alert");
      checkProps(Alert, { color: "red" }, "Alert");
      checkProps(Alert, { color: "yellow" }, "Alert");
    });

    it("Should not throw warning when all expected props types provided", () => {
      const defaultProps = {
        color: "red",
        icon: null,
        emoticon: null,
        bordered: false,
        withCloseButton: false,
        onCloseClick: () => {}
      };
      checkProps(Alert, defaultProps, "Alert");
    });
  });

  describe("Alert props", () => {
    it("Should have custom className when provided", () => {
      render(<Alert className="u-border-r-2" />);
      expect(screen.getByTestId("qa-alert").className).toContain("u-border-r-2");
    });

    it("Should show given text", () => {
      render(<Alert>Content passed...</Alert>);
      expect(screen.getByTestId("qa-alert")).toHaveTextContent("Content passed...");
    });
  });

  describe("Alert color", () => {
    it("Should have sakura and red_wine theme if no color prop is given", () => {
      render(<Alert />);
      const expectedStyle = { background: colors.sakura, border: "none", color: colors.red_wine };
      expect(screen.getByTestId("qa-alert")).toHaveStyle(expectedStyle);
    });

    it("Should have sakura and red_wine theme when given color: red", () => {
      render(<Alert color="red" />);
      const expectedStyle = { background: colors.sakura, border: "none", color: colors.red_wine };
      expect(screen.getByTestId("qa-alert")).toHaveStyle(expectedStyle);
    });

    it("Should have banana and coint theme when given color: yellow", () => {
      render(<Alert color="yellow" />);
      const expectedStyle = { background: colors.banana, border: "none", color: colors.coin };
      expect(screen.getByTestId("qa-alert")).toHaveStyle(expectedStyle);
    });

    it("Should have light_jade and spinach theme when given color: green", () => {
      render(<Alert color="green" />);
      const expectedStyle = { background: colors.light_jade, border: "none", color: colors.spinach };
      expect(screen.getByTestId("qa-alert")).toHaveStyle(expectedStyle);
    });

    it("Should have bubble and space blue theme when given color: blue", () => {
      render(<Alert color="blue" />);
      const expectedStyle = { background: colors.bubble, border: "none", color: colors.space_blue };
      expect(screen.getByTestId("qa-alert")).toHaveStyle(expectedStyle);
    });

    it("Should have light_smoke and dark_somke theme when given color: grey", () => {
      render(<Alert color="grey" />);
      const expectedStyle = { background: colors.light_smoke, border: "none", color: colors.dark_smoke };
      expect(screen.getByTestId("qa-alert")).toHaveStyle(expectedStyle);
    });
  });

  describe("Alert border style and color", () => {
    it("Should have blush border color if no color prop is given", () => {
      render(<Alert bordered />);
      const expectedStyle = {
        "border-width": "1px",
        "border-style": "solid",
        "border-radius": "8px",
        "border-color": colors.blush
      };
      expect(screen.getByTestId("qa-alert")).toHaveStyle(expectedStyle);
    });

    it("Should have blush border color when given color: red", () => {
      render(<Alert color="red" bordered />);
      expect(screen.getByTestId("qa-alert")).toHaveStyle({ "border-color": colors.blush });
    });

    it("Should have blonde border color when given color: yellow", () => {
      render(<Alert color="yellow" bordered />);
      expect(screen.getByTestId("qa-alert")).toHaveStyle({ "border-color": colors.blonde });
    });

    it("Should have slight_jade border color when given color: green", () => {
      render(<Alert color="green" bordered />);
      expect(screen.getByTestId("qa-alert")).toHaveStyle({ "border-color": colors.slight_jade });
    });

    it("Should have tie_dye border color when given color: blue", () => {
      render(<Alert color="blue" bordered />);
      expect(screen.getByTestId("qa-alert")).toHaveStyle({ "border-color": colors.tie_dye });
    });

    it("Should have light_grey border color when given color: grey", () => {
      render(<Alert color="grey" bordered />);
      expect(screen.getByTestId("qa-alert")).toHaveStyle({ "border-color": colors.light_grey });
    });
  });

  describe("Alert close button and its color", () => {
    it("Should show correct close button icon when withCloseButton is true", () => {
      render(<Alert withCloseButton />);
      expect(screen.getByTestId("qa-alert-close-icon")).toBeInTheDocument();
      expect(screen.getByTestId("qa-alert-close-icon")).toHaveClass("svg-inline--fa fa-times");
    });

    it("Should have red_wine button color if no color prop is given", () => {
      render(<Alert withCloseButton />);
      expect(screen.getByTestId("qa-alert-close")).toHaveStyle({ color: colors.red_wine });
    });

    it("Should have red_wine button color when given color: red", () => {
      render(<Alert color="red" withCloseButton />);
      expect(screen.getByTestId("qa-alert-close")).toHaveStyle({ color: colors.red_wine });
    });

    it("Should have coin button color when given color: yellow", () => {
      render(<Alert color="yellow" withCloseButton />);
      expect(screen.getByTestId("qa-alert-close")).toHaveStyle({ color: colors.coin });
    });

    it("Should have spinach button color when given color: green", () => {
      render(<Alert color="green" withCloseButton />);
      expect(screen.getByTestId("qa-alert-close")).toHaveStyle({ color: colors.spinach });
    });

    it("Should have space_blue button color when given color: blue", () => {
      render(<Alert color="blue" withCloseButton />);
      expect(screen.getByTestId("qa-alert-close")).toHaveStyle({ color: colors.space_blue });
    });

    it("Should have dark_smoke button color when given color: grey", () => {
      render(<Alert color="grey" withCloseButton />);
      expect(screen.getByTestId("qa-alert-close")).toHaveStyle({ color: colors.dark_smoke });
    });
  });

  describe("Alert icon", () => {
    it("Should show correct icon when given a valid svg icons", () => {
      render(<Alert icon={faExclamationTriangle} />);
      expect(screen.getByTestId("qa-alert-icon-wrapper")).toBeInTheDocument();
      expect(screen.getByTestId("qa-alert-icon")).toBeInTheDocument();
      expect(screen.getByTestId("qa-alert-icon")).toHaveClass("svg-inline--fa fa-exclamation-triangle");
    });

    it("Should show children text when icon is given", () => {
      render(<Alert icon={faExclamationTriangle}>Content passed...</Alert>);
      expect(screen.getByTestId("qa-alert")).toHaveTextContent("Content passed...");
    });
  });

  describe("Alert emoticon", () => {
    it("Should show correct emoticon when given one", () => {
      render(<Alert emoticon="🎉" />);
      expect(screen.getByTestId("qa-alert-icon-wrapper")).toBeInTheDocument();
      expect(screen.getByTestId("qa-alert-icon-wrapper")).toHaveTextContent("🎉");
    });

    it("Should show children text when emoticon is given", () => {
      render(<Alert emoticon="🎉">Content passed...</Alert>);
      expect(screen.getByTestId("qa-alert")).toHaveTextContent("Content passed...");
    });
  });
});
