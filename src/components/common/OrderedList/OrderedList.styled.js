import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

export const OrderedListWrapper = styled.ol`
  list-style: none;
  counter-reset: section;
  padding-left: 28px;
`;

export const OrderedListItem = styled.li.attrs({
  "data-testid": "qa-ordered-list-item"
})`
  counter-increment: section;
  margin-bottom: 12px;
  position: relative;

  &::before {
    background: ${colors.flip_orange};
    border-radius: 50%;
    color: ${colors.white};
    content: counter(section);
    display: inline-block;
    font-size: ${typography.font_size.base};
    position: absolute;
    left: -28px;
    line-height: 20px;
    height: 20px;
    width: 20px;
    text-align: center;
  }
`;
