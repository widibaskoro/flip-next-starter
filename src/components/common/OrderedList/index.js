import React from "react";
import classNames from "classnames";

import { OrderedListWrapper, OrderedListItem } from "./OrderedList.styled";

const OrderedList = React.forwardRef((props, ref) => {
  const { className, ...restProps } = props;
  const oderedListClass = classNames("c-ordered-list", className);

  return <OrderedListWrapper data-testid="qa-ordered-list" ref={ref} className={oderedListClass} {...restProps} />;
});

OrderedList.Item = OrderedListItem;

export default OrderedList;
