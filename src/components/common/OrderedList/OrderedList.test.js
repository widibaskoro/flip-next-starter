import React from "react";
import { render, screen } from "@testing-library/react";

import OrderedList from "./index";

describe("OrderedList component", () => {
  describe("OrderedList and OrderedList.Item rendering", () => {
    it("should render OrderedList without error", () => {
      render(<OrderedList />);
      expect(screen.getByTestId("qa-ordered-list")).toBeInTheDocument();
    });

    it("should render OrderedList.Item without error", () => {
      render(<OrderedList.Item />);
      expect(screen.getByTestId("qa-ordered-list-item")).toBeInTheDocument();
    });
  });

  describe("OrderedList props", () => {
    it("should have custom className when provided", () => {
      render(<OrderedList className="u-mr-2" />);
      expect(screen.getByTestId("qa-ordered-list").className).toContain("u-mr-2");
    });
  });
});
