import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { rgba } from "polished";

import Pagination from "./index";
import { checkProps } from "src/helpers/test";
import { colors, zIndex } from "src/assets/styles/settings";

describe("Pagination component", () => {
  describe("Pagination rendering", () => {
    it("Should render without error", () => {
      render(<Pagination />);
      expect(screen.getByTestId("qa-pagination")).toBeInTheDocument();
    });
  });

  describe("Pagination functionality", () => {
    it("should have disabled previous button when on the first page", () => {
      render(<Pagination totalData={456} limit={30} />); // on page 1/16
      expect(screen.getByTestId("qa-pagination-previous-button")).toHaveClass("is-disabled");
    });

    it("should have disabled previous button when there is only one page", () => {
      render(<Pagination totalData={456} limit={456} />); // on page 1/1
      expect(screen.getByTestId("qa-pagination-previous-button")).toHaveClass("is-disabled");
    });

    it("should have active previous button when not on the first page", () => {
      render(<Pagination totalData={456} limit={30} />);
      fireEvent.click(screen.getByTestId("qa-pagination-page-link-3")); // go to page 3/16
      expect(screen.getByTestId("qa-pagination-previous-button")).not.toHaveClass("is-disabled");
    });

    it("should have disabled next button when there is only one page", () => {
      render(<Pagination totalData={456} limit={456} />); // on page 1/1
      expect(screen.getByTestId("qa-pagination-next-button")).toHaveClass("is-disabled");
    });

    it("should have active next button when not on the last page", () => {
      render(<Pagination totalData={456} limit={30} />);
      fireEvent.click(screen.getByTestId("qa-pagination-page-link-3")); // go to page 3/16
      expect(screen.getByTestId("qa-pagination-next-button")).not.toHaveClass("is-disabled");
    });

    it("should have disabled next button when on the last page", async () => {
      render(<Pagination totalData={456} limit={30} />);
      fireEvent.click(screen.getByTestId("qa-pagination-page-link-16")); // go to page 16/16
      expect(screen.getByTestId("qa-pagination-next-button")).toHaveClass("is-disabled");
    });

    it("should move to next page when next button clicked", async () => {
      render(<Pagination totalData={456} limit={30} />);
      fireEvent.click(screen.getByTestId("qa-pagination-next-button-link")); // on page 1/16 to 2/16
      expect(screen.getByTestId("qa-pagination-page-button-2")).toHaveClass("is-current");
    });

    it("should move to previous page when previous button clicked", async () => {
      render(<Pagination totalData={456} limit={30} />);
      fireEvent.click(screen.getByTestId("qa-pagination-page-link-4")); // go to page 4/16
      fireEvent.click(screen.getByTestId("qa-pagination-previous-button-link")); // back to 3/16
      expect(screen.getByTestId("qa-pagination-page-button-3")).toHaveClass("is-current");
    });

    it("should go to desired page when certain page button clicked", async () => {
      const spy = jest.fn();
      render(<Pagination totalData={456} limit={30} onPageChange={spy} />);
      fireEvent.click(screen.getByTestId("qa-pagination-page-link-5")); // go to page 5/16
      expect(screen.getByTestId("qa-pagination-page-button-5")).toHaveClass("is-current");
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(5);
    });

    it("should not go anywhere when ... button is pressed", () => {
      const spy = jest.fn();
      render(<Pagination totalData={456} limit={30} onPageChange={spy} />);
      fireEvent.click(screen.getByTestId("qa-pagination-page-link-...")); // not goind anywhere
      expect(spy).toHaveBeenCalledTimes(0);
    });

    it("should not go anywhere when currentpage button is pressed", () => {
      const spy = jest.fn();
      render(<Pagination totalData={456} limit={30} onPageChange={spy} />);
      fireEvent.click(screen.getByTestId("qa-pagination-page-link-1")); // not goind anywhere
      expect(spy).toHaveBeenCalledTimes(0);
    });

    it("should have expected last pagination page button of 16 when total page 16", () => {
      // ceil(456 / 30) = 16
      render(<Pagination totalData={456} limit={30} />);
      const paginationChild = screen.getByTestId("qa-pagination").children;
      const paginationLength = paginationChild.length;
      // get second last button, the last button is next arrow >
      expect(paginationChild[paginationLength - 2]).toHaveTextContent("16");
      expect(screen.getByTestId("qa-pagination-page-button-16")).toBeInTheDocument();
    });

    it("should have expected last pagination page button of 15 when total page is 15", () => {
      render(<Pagination totalData={750} limit={50} />); // 750/50 = 15
      const paginationChild = screen.getByTestId("qa-pagination").children;
      const paginationLength = paginationChild.length;
      expect(paginationChild[paginationLength - 2]).toHaveTextContent("15");
      expect(screen.getByTestId("qa-pagination-page-button-15")).toBeInTheDocument();
    });

    it("should have pagination page button 1-7 (first 7), ..., 16 (last page) when total page > 8", () => {
      render(<Pagination totalData={456} limit={30} />);
      expect(screen.getByTestId("qa-pagination-page-button-1")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-2")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-3")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-4")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-5")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-6")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-7")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-...")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-16")).toBeInTheDocument();
    });

    it("should have pagination page button 1, ..., 10-16 (last 7) when total page > 8 and on the last page", () => {
      render(<Pagination totalData={456} limit={30} />);
      fireEvent.click(screen.getByTestId("qa-pagination-page-link-16")); // go to page 16/16
      expect(screen.getByTestId("qa-pagination-page-button-1")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-...")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-10")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-11")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-12")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-13")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-14")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-15")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-16")).toBeInTheDocument();
    });

    it("should have pagination page button 1, ..., 3-7 (left 2, right 2 from current page), ... , 16 when total page > 8 and on 5th page", () => {
      render(<Pagination totalData={456} limit={30} />);
      fireEvent.click(screen.getByTestId("qa-pagination-page-link-5")); // go to page 5/16
      expect(screen.getByTestId("qa-pagination-page-button-1")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-3")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-4")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-5")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-6")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-7")).toBeInTheDocument();
      expect(screen.getByTestId("qa-pagination-page-button-16")).toBeInTheDocument();
      expect(screen.getAllByTestId("qa-pagination-page-button-...")).toHaveLength(2); // two ... button
    });

    it("should not have pagination page button of 8 (over limit of 7) when total page > 8 and on the first page", () => {
      render(<Pagination totalData={456} limit={30} />);
      expect(screen.queryByTestId("qa-pagination-page-button-8")).toBeFalsy();
    });

    it("should not have pagination page button of 9 (over limit of 7, downwards) when total page > 8 and on the last page", () => {
      render(<Pagination totalData={456} limit={30} />);
      fireEvent.click(screen.getByTestId("qa-pagination-page-link-16")); // go to page 16/16
      expect(screen.queryByTestId("qa-pagination-page-button-9")).toBeFalsy();
    });
  });

  describe("Pagination propTypes", () => {
    it("Should not throw warning when expected totalData props are provided", () => {
      checkProps(Pagination, { totalData: 1 }, "Pagination");
    });

    it("Should not throw warning when expected limit props are provided", () => {
      checkProps(Pagination, { limit: 1 }, "Pagination");
    });

    it("Should not throw warning when expected currentPage props are provided", () => {
      checkProps(Pagination, { currentPage: 1 }, "Pagination");
    });

    it("Should not throw warning when expected onPageChange props are provided", () => {
      checkProps(Pagination, { onPageChange: () => {} }, "Pagination");
    });
  });

  describe("Pagination props", () => {
    it("should have custom className when provided", () => {
      render(<Pagination className="u-mr-2" />);
      expect(screen.getByTestId("qa-pagination").className).toContain("u-mr-2");
    });
  });

  describe("Pagination style", () => {
    it("Should have disabled style when previous button is disabled", () => {
      render(<Pagination totalData={456} limit={30} />);
      const expectedStyle = {
        "background-color": colors.white,
        color: colors.light_grey,
        cursor: "default",
        "pointer-events": "none",
        "z-index": zIndex.default
      };
      expect(screen.getByTestId("qa-pagination-previous-button-link")).toHaveStyle(expectedStyle);
    });

    it("Should have disabled style when next button is disabled", () => {
      render(<Pagination totalData={456} limit={30} />);
      const expectedStyle = {
        "background-color": colors.white,
        color: colors.light_grey,
        cursor: "default",
        "pointer-events": "none",
        "z-index": zIndex.default
      };
      fireEvent.click(screen.getByTestId("qa-pagination-page-link-16")); // go to page 16/16
      expect(screen.getByTestId("qa-pagination-next-button-link")).toHaveStyle(expectedStyle);
    });
  });
});
