import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { PaginationList, PaginationItem, PaginationLink } from "./Pagination.styled";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft, faChevronRight } from "@fortawesome/free-solid-svg-icons";

const propTypes = {
  /**
   * Set total data from all pages
   */
  totalData: PropTypes.number,

  /**
   * Set data to be shown in one page
   */
  limit: PropTypes.number,

  /**
   * Set current active page
   */
  currentPage: PropTypes.number,

  /**
   * Callback after page has been changed
   */
  onPageChange: PropTypes.func
};

const defaultProps = {
  totalData: 1,
  limit: 1,
  currentPage: 1,
  onPageChange: () => {}
};

const Pagination = React.forwardRef((props, ref) => {
  const { className, totalData, limit, currentPage, onPageChange } = props;
  const paginationClass = classNames("c-pagination", className);

  const [totalPage, setTotalPage] = useState(Math.ceil(totalData / limit));
  const [activePage, setActivePage] = useState(currentPage);
  const [links, setLinks] = useState([]);

  function range(from, to, step = 1) {
    let i = from;
    const range = [];

    while (i <= to) {
      range.push(i);
      i += step;
    }
    return range;
  }

  function buildLinks(nextPage) {
    const currPage = nextPage || currentPage;
    const neighbours = 2;
    const totalNeighbours = neighbours * 2;

    if (totalPage < totalNeighbours + 4) {
      setLinks(range(1, totalPage));
    } else if (currPage < totalNeighbours + 1) {
      const pages = range(1, totalNeighbours + 3);
      setLinks([...pages, "...", totalPage]);
    } else if (currPage > totalPage - totalNeighbours) {
      const pages = range(totalPage - totalNeighbours - 2, totalPage);
      setLinks([1, "...", ...pages]);
    } else {
      const pages = range(currPage - neighbours, currPage + neighbours);
      setLinks([1, "...", ...pages, "...", totalPage]);
    }
  }

  function changePage(page) {
    if (page === "...") return;
    if (page === activePage) return;

    let newPage = page;
    if (page === "next") {
      newPage = activePage < totalPage ? activePage + 1 : totalPage;
    } else if (page === "prev") {
      newPage = activePage > 1 ? activePage - 1 : 1;
    }

    setActivePage(newPage);
    buildLinks(newPage);
    onPageChange(newPage);
  }

  useEffect(() => {
    buildLinks();
  }, []);

  useEffect(() => {
    buildLinks();
    changePage(1);
  }, [totalPage]);

  useEffect(() => {
    setTotalPage(Math.ceil(totalData / limit));
  }, [totalData]);

  useEffect(() => {
    if (currentPage && currentPage !== activePage) {
      changePage(currentPage);
    }
  }, [currentPage]);

  return (
    <PaginationList ref={ref} className={paginationClass} data-testid="qa-pagination">
      <PaginationItem
        className={classNames("c-pagination__item", activePage === 1 && "is-disabled")}
        data-testid="qa-pagination-previous-button"
      >
        <PaginationLink
          className="c-pagination__link"
          onClick={() => changePage("prev")}
          data-testid="qa-pagination-previous-button-link"
        >
          <FontAwesomeIcon icon={faChevronLeft} data-testid="qa-pagination-previous-button-icon" />
        </PaginationLink>
      </PaginationItem>

      {links.map((page, index) => {
        const itemClass = classNames(
          "c-pagination__item",
          page === activePage && "is-current",
          page === "..." && "is-disabled"
        );
        return (
          <PaginationItem key={index} className={itemClass} data-testid={`qa-pagination-page-button-${page}`}>
            <PaginationLink
              className="c-pagination__link"
              onClick={() => changePage(page)}
              data-testid={`qa-pagination-page-link-${page}`}
            >
              {page}
            </PaginationLink>
          </PaginationItem>
        );
      })}

      <PaginationItem
        className={classNames("c-pagination__item", activePage === totalPage && "is-disabled")}
        data-testid="qa-pagination-next-button"
      >
        <PaginationLink
          className="c-pagination__link"
          onClick={() => changePage("next")}
          data-testid="qa-pagination-next-button-link"
        >
          <FontAwesomeIcon icon={faChevronRight} data-testid="qa-pagination-next-button-icon" />
        </PaginationLink>
      </PaginationItem>
    </PaginationList>
  );
});

Pagination.propTypes = propTypes;
Pagination.defaultProps = defaultProps;

export default Pagination;
