import styled from "styled-components";

import { colors, typography, zIndex } from "src/assets/styles/settings";
import { rgba } from "polished";

export const PaginationList = styled.ul`
  display: flex;
  list-style: none;
  margin: 0;
  padding: 0;
`;

export const PaginationItem = styled.li`
  &.is-current .c-pagination__link {
    background-color: ${rgba(colors.anemia, 0.36)};
    color: ${colors.flip_orange};
    pointer-events: none;
    z-index: ${zIndex.paginationActive};
  }
`;

export const PaginationLink = styled.button`
  align-items: center;
  background-color: ${colors.white};
  border-radius: 8px;
  color: ${colors.flip_orange};
  cursor: pointer;
  display: flex;
  font-size: ${typography.font_size.semi};
  font-weight: ${typography.font_weight.bold};
  justify-content: center;
  text-align: center;
  height: 32px;
  width: 32px;

  &:hover {
    background-color: ${colors.flip_orange};
    color: ${colors.white};
    z-index: ${zIndex.paginationActive};
  }

  &:focus {
    outline: 0;
  }

  .is-disabled & {
    background-color: ${colors.white};
    color: ${colors.light_grey};
    cursor: default;
    pointer-events: none;
    z-index: ${zIndex.default};
  }
`;
