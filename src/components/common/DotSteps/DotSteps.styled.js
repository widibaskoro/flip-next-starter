import styled from "styled-components";

import { colors } from "src/assets/styles/settings";

export const StyledDotStep = styled.div`
  margin: 0;
  padding: 0 20px;
  position: relative;
`;

export const StepDotWrapper = styled.div`
  position: relative;
  padding: 0;
  height: 100%;

  &:first-child {
    padding-top: 17px;
  }

  &:last-child:not(:first-child) .c-dot-steps__bar {
    display: none;
  }

  &:last-child:first-child .c-dot-steps__bar {
    height: 50%;
  }
`;

export const StepBar = styled.div`
  position: absolute;
  top: 1px;
  left: 8px;
  width: 4px;
  height: 100%;
  display: inline-block;
  content: "";
  background: ${colors.main_background};
`;

export const StepCircle = styled.div`
  background: ${colors.main_background};
  display: inline-block;
  vertical-align: top;
  height: 20px;
  width: 20px;
  position: relative;
  border-radius: 50%;

  &::after {
    content: "";
    background: ${colors.flip_orange};
    position: absolute;
    width: 14px;
    height: 14px;
    border-radius: 50%;
    margin: 3px;
  }
`;

export const StepContent = styled.div`
  display: inline-block;
  padding: 0 0 12px 8px;
  position: relative;
  width: calc(100% - 20px);
`;

export const StepChild = styled.div`
  position: relative;
  top: -1px;
`;
