import React from "react";
import classNames from "classnames";

import { StyledDotStep, StepDotWrapper, StepBar, StepCircle, StepContent, StepChild } from "./DotSteps.styled";

const DotSteps = React.forwardRef((props, ref) => {
  const { children, className, ...restProps } = props;
  const dotStepsClass = classNames("c-dot-steps", className);

  return (
    <StyledDotStep data-testid="qa-dotsteps" ref={ref} className={dotStepsClass} {...restProps}>
      {children.length && children.length > 1 ? (
        children.map((child, index) => {
          return (
            <StepDotWrapper data-testid="qa-dotsteps-step" key={index} className="c-dot-steps__wrapper">
              <StepBar className="c-dot-steps__bar" />
              <StepCircle className="c-dot-steps__circle" />
              <StepContent className="c-dot-steps__content">{child}</StepContent>
            </StepDotWrapper>
          );
        })
      ) : (
        <StepDotWrapper data-testid="qa-dotsteps-step" className="c-dot-steps__wrapper">
          <StepBar className="c-dot-steps__bar" />
          <StepCircle className="c-dot-steps__circle" />
          <StepContent className="c-dot-steps__content">{children}</StepContent>
        </StepDotWrapper>
      )}
    </StyledDotStep>
  );
});

DotSteps.Content = StepChild;

export default DotSteps;
