import React from "react";
import { render, screen } from "@testing-library/react";

import DotSteps from "./index";

describe("DotSteps component", () => {
  describe("DotSteps rendering", () => {
    it("Should throw error if no content/child is given", () => {
      jest.spyOn(console, "error");
      console.error.mockImplementation(() => {});
      expect(() => render(<DotSteps />)).toThrow();
      console.error.mockRestore();
    });

    it("Should render normally when one content is given", () => {
      render(
        <DotSteps>
          <DotSteps.Content>Content here</DotSteps.Content>
        </DotSteps>
      );
      expect(screen.getByTestId("qa-dotsteps")).toBeInTheDocument();
      expect(screen.getByTestId("qa-dotsteps-step")).toBeInTheDocument();
    });

    it("Should render normally when more than one contents are given", () => {
      render(
        <DotSteps>
          <DotSteps.Content>Content here</DotSteps.Content>
          <DotSteps.Content>Content here</DotSteps.Content>
          <DotSteps.Content>Content here</DotSteps.Content>
        </DotSteps>
      );
      expect(screen.getByTestId("qa-dotsteps")).toBeInTheDocument();
      expect(screen.queryAllByTestId("qa-dotsteps-step")).toHaveLength(3);
    });
  });

  describe("DotSteps content", () => {
    it("Should contain correct text when one content is given", () => {
      render(
        <DotSteps>
          <DotSteps.Content>Content here</DotSteps.Content>
        </DotSteps>
      );
      expect(screen.getByTestId("qa-dotsteps-step")).toHaveTextContent("Content here");
    });

    it("Should contain correct texts when more than one contents are given", () => {
      render(
        <DotSteps>
          <DotSteps.Content>1st Content here</DotSteps.Content>
          <DotSteps.Content>2nd Content here</DotSteps.Content>
          <DotSteps.Content>3rd Content here</DotSteps.Content>
        </DotSteps>
      );
      expect(screen.queryAllByTestId("qa-dotsteps-step")).toHaveLength(3);
      expect(screen.queryAllByTestId("qa-dotsteps-step")[0]).toHaveTextContent("1st Content here");
      expect(screen.queryAllByTestId("qa-dotsteps-step")[1]).toHaveTextContent("2nd Content here");
      expect(screen.queryAllByTestId("qa-dotsteps-step")[2]).toHaveTextContent("3rd Content here");
    });
  });
});
