import styled from "styled-components";

import { colors, typography, zIndex } from "src/assets/styles/settings";

export const CalendarHeaderContent = styled.div`
  margin: 0 16px 4px;
  display: flex;
  justify-content: space-between;
`;

export const CalendarNavButton = styled.button`
  background: ${colors.white};
  border-radius: 4px;
  border: 1px solid transparent;
  cursor: pointer;
  font-size: ${typography.font_size.large};
  height: 32px;
  padding-top: 1px;
  text-align: center;
  white-space: nowrap;
  width: 32px;

  &:focus {
    outline: none;
  }

  &:hover {
    color: ${colors.flip_orange};
  }

  &:disabled {
    color: ${colors.light_grey};
    cursor: default;

    &:hover {
      color: ${colors.light_grey};
    }
  }
`;

export const CalendarNavButtonPrev = styled(CalendarNavButton)`
  padding-right: 9px;
`;

export const CalendarNavButtonNext = styled(CalendarNavButton)`
  padding-left: 9px;
`;

export const DropdownWrapper = styled.div`
  height: 32px;
  overflow: hidden;
  position: relative;

  &:after {
    color: ${colors.light_grey};
    content: "▼";
    font-size: ${typography.font_size.small};
    height: 100%;
    padding-top: 8px;
    position: absolute;
    pointer-events: none;
    right: 8px;
    text-align: center;
    top: 0;
    width: 10%;
    z-index: ${zIndex.default};
  }
`;

export const DropdownWrapperMonth = styled(DropdownWrapper)`
  width: 70%;
`;

export const DropdownWrapperYear = styled(DropdownWrapper)`
  width: 50%;
`;

export const DropdownSelect = styled.select`
  appearance: none;
  background-color: ${colors.white};
  box-sizing: border-box;
  border: none;
  color: ${colors.black_bekko};
  display: block;
  font-size: ${typography.font_size.base};
  font-weight: ${typography.font_weight.normal};
  height: 32px;
  line-height: 1.5;
  margin: 0;
  padding-left: 8px;
  width: 100%;

  &::-ms-expand {
    display: none;
  }

  &:hover {
    cursor: pointer;
  }

  &:focus {
    outline: none;
  }

  & option {
    font-weight: ${typography.font_weight.bold};
  }
`;
