import React, { useState, useEffect } from "react";
import classNames from "classnames";

import {
  CalendarTabWrapper,
  CalendarTabNavWrapper,
  CalendarTabNav,
  CalendarWrapper,
  TabArrow
} from "./CalendarTab.styled";
import { faAngleRight } from "@fortawesome/free-solid-svg-icons";

import { format } from "date-fns";
import { getTranslationManually } from "src/helpers/locale";

function CalendarTab(props) {
  const { startCalendar, endCalendar, startDate, endDate, locale, changeToEnd, onTabChange } = props;
  const [activeTab, setActiveTab] = useState("tabstart");

  function formatDate(date) {
    if (!date) return "";
    return format(date, "dd MMM yyyy", { locale });
  }

  function handleTabChange(tabKey) {
    setActiveTab(tabKey);
    onTabChange(tabKey);
  }

  useEffect(() => {
    if (changeToEnd) setActiveTab("tabend");
  }, [changeToEnd]);

  return (
    <CalendarTabWrapper className="c-range-datepicker__tab">
      <CalendarTabNavWrapper>
        <CalendarTabNav
          className={classNames("c-range-datepicker__tab-nav", activeTab === "tabstart" && "is-active")}
          onClick={() => handleTabChange("tabstart")}
        >
          <label>{getTranslationManually("Dari", "From")}</label>
          <div>{formatDate(startDate)}</div>
          <TabArrow icon={faAngleRight} />
        </CalendarTabNav>
        <CalendarTabNav
          className={classNames("c-range-datepicker__tab-nav", activeTab === "tabend" && "is-active")}
          onClick={() => handleTabChange("tabend")}
        >
          <label>{getTranslationManually("Sampai", "To")}</label>
          <div>{formatDate(endDate)}</div>
        </CalendarTabNav>
      </CalendarTabNavWrapper>

      <CalendarWrapper
        className={classNames("c-range-datepicker__tab-content", activeTab === "tabstart" && "is-active")}
      >
        {startCalendar}
      </CalendarWrapper>
      <CalendarWrapper className={classNames("c-range-datepicker__tab-content", activeTab === "tabend" && "is-active")}>
        {endCalendar}
      </CalendarWrapper>
    </CalendarTabWrapper>
  );
}

export default CalendarTab;
