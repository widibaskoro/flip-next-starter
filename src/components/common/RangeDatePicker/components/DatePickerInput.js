import React from "react";

import FieldGroup from "../../FieldGroup";
import Icon from "../../Icon";
import { CustomField } from "./DatePickerInput.styled";

import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { getTranslationManually } from "src/helpers/locale";

const defaultProps = {
  inputPlaceholder: getTranslationManually("Pilih tanggal", "Select date"),
  onInputClick: () => {}
};

function DatePickerInput(props) {
  const { onInputClick, inputPlaceholder, ...restProps } = props;

  return (
    <FieldGroup className="c-range-datepicker__input-group">
      <FieldGroup.Prepend>
        <Icon icon={faCalendarAlt} />
      </FieldGroup.Prepend>
      <CustomField
        className="c-range-datepicker__input"
        onClick={onInputClick}
        placeholder={inputPlaceholder}
        readOnly
        {...restProps}
      />
    </FieldGroup>
  );
}

DatePickerInput.defaultProps = defaultProps;

export default DatePickerInput;
