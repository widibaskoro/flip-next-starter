import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

export const CustomDateWrapper = styled.div`
  display: inline-block;
  height: 100%;
  padding: 12px 16px;
  text-align: left;
  width: 132px;
  vertical-align: top;
`;

export const CustomDateTitle = styled.p`
  color: ${colors.black_bekko};
  font-weight: ${typography.font_weight.bold};
  font-size: ${typography.font_size.semi};
  margin: 4px 0 12px 0;
  line-height: 150%;
  text-transform: uppercase;
`;

export const CustomDateList = styled.ul`
  list-style: none;
  margin: 8px 0 0 0;
  padding: 0;

  li {
    color: ${colors.dark_smoke};
    cursor: pointer;
    font-size: ${typography.font_size.small};
    margin-bottom: 6px;

    &:hover {
      color: ${colors.flip_orange};
    }

    &.is-active {
      color: ${colors.flip_orange};
      font-weight: ${typography.font_weight.bold};
    }
  }
`;
