import styled from "styled-components";

import { colors } from "src/assets/styles/settings";

export const CalendarFooterWrapper = styled.div`
  border-left: 2px solid ${colors.main_background};
  display: inline-block;
  margin-left: 132px;
  padding: 0 16px 16px 0;
  text-align: right;
  width: 334px;
`;
