import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight, faAngleLeft } from "@fortawesome/free-solid-svg-icons";

import {
  CalendarHeaderContent,
  CalendarNavButtonPrev,
  CalendarNavButtonNext,
  DropdownSelect,
  DropdownWrapperMonth,
  DropdownWrapperYear
} from "./CalendarHeader.styled";

import { range } from "lodash";
import { getMonth, getYear } from "date-fns";
import { getCurrentLocale } from "src/helpers/locale";
import { getLocalizeMonth } from "src/helpers/date";

function CalendarHeader(props) {
  const {
    date,
    changeYear,
    changeMonth,
    decreaseMonth,
    increaseMonth,
    decreaseYear,
    increaseYear,
    prevMonthButtonDisabled,
    nextMonthButtonDisabled,
    prevYearButtonDisabled,
    nextYearButtonDisabled
  } = props;

  const months = getLocalizeMonth(getCurrentLocale());
  const years = range(1990, getYear(new Date()) + 1, 1);

  function handleChangeMonth(evt) {
    changeMonth(months.indexOf(evt.target.value));
  }

  function handleChangeYear(evt) {
    changeYear(evt.target.value);
  }

  return (
    <CalendarHeaderContent className="c-range-datepicker__header">
      <CalendarNavButtonPrev type="button" onClick={decreaseMonth} disabled={prevMonthButtonDisabled}>
        <FontAwesomeIcon icon={faAngleLeft} />
      </CalendarNavButtonPrev>
      <DropdownWrapperMonth>
        <DropdownSelect value={months[getMonth(date)]} onChange={handleChangeMonth}>
          {months.map(option => (
            <option key={option} value={option}>
              {option}
            </option>
          ))}
        </DropdownSelect>
      </DropdownWrapperMonth>
      <CalendarNavButtonNext type="button" onClick={increaseMonth} disabled={nextMonthButtonDisabled}>
        <FontAwesomeIcon icon={faAngleRight} />
      </CalendarNavButtonNext>

      <CalendarNavButtonPrev type="button" onClick={decreaseYear} disabled={prevYearButtonDisabled}>
        <FontAwesomeIcon icon={faAngleLeft} />
      </CalendarNavButtonPrev>
      <DropdownWrapperYear>
        <DropdownSelect value={getYear(date)} onChange={handleChangeYear}>
          {years.map(option => (
            <option key={option} value={option}>
              {option}
            </option>
          ))}
        </DropdownSelect>
      </DropdownWrapperYear>
      <CalendarNavButtonNext type="button" onClick={increaseYear} disabled={nextYearButtonDisabled}>
        <FontAwesomeIcon icon={faAngleRight} />
      </CalendarNavButtonNext>
    </CalendarHeaderContent>
  );
}

export default CalendarHeader;
