import styled from "styled-components";

import Field from "../../Field";

import { colors } from "src/assets/styles/settings";

export const CustomField = styled(Field)`
  &[readonly] {
    background-color: ${colors.white};
    border: 1px solid ${colors.light_grey};
    color: ${colors.black_bekko};
    cursor: default;
    opacity: 1;

    &:hover {
      cursor: default;
    }

    &:focus {
      border: 1px solid ${colors.light_grey};
      box-shadow: none;
      outline: 0;
    }
  }

  &:disabled {
    background-color: ${colors.disabled};
    color: ${colors.dark_grey};
    cursor: not-allowed;
    opacity: 1;

    &:hover {
      cursor: not-allowed;
    }
  }

  .is-invalid & {
    border-color: ${colors.ketchup_tomato};
  }
`;
