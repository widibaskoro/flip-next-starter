import React, { useState } from "react";

import { CustomDateWrapper, CustomDateTitle, CustomDateList } from "./CustomRange.styled";

import { subDays, startOfWeek, subWeeks, endOfWeek, startOfMonth, subMonths, endOfMonth } from "date-fns";
import { getTranslationManually } from "src/helpers/locale";

function CustomRange(props) {
  const { setStartDate, setEndDate } = props;
  const [activeLink, setActiveLink] = useState("");

  function handleToday() {
    const today = new Date();

    setStartDate(today);
    setEndDate(today);
    setActiveLink("today");
  }

  function handleYesterday() {
    const today = new Date();
    const yesterday = subDays(today, 1);

    setStartDate(yesterday);
    setEndDate(yesterday);
    setActiveLink("yesterday");
  }

  function handleLast7Days() {
    const today = new Date();
    const last7Days = subDays(today, 6);

    setStartDate(last7Days);
    setEndDate(today);
    setActiveLink("last7");
  }

  function handleLast14Days() {
    const today = new Date();
    const last14Days = subDays(today, 13);

    setStartDate(last14Days);
    setEndDate(today);
    setActiveLink("last14");
  }

  function handleLast30Days() {
    const today = new Date();
    const last30Days = subDays(today, 29);

    setStartDate(last30Days);
    setEndDate(today);
    setActiveLink("last30");
  }

  function handleThisWeek() {
    const today = new Date();
    const thisWeekStart = startOfWeek(today, { weekStartsOn: 1 });

    setStartDate(thisWeekStart);
    setEndDate(today);
    setActiveLink("thisweek");
  }

  function handleLastWeek() {
    const today = new Date();
    const lastWeekStart = startOfWeek(today, { weekStartsOn: 1 });
    const lastWeek = subWeeks(lastWeekStart, 1);
    const lastWeekEnd = endOfWeek(lastWeek, { weekStartsOn: 1 });

    setStartDate(lastWeek);
    setEndDate(lastWeekEnd);
    setActiveLink("lastweek");
  }

  function handleThisMonth() {
    const today = new Date();
    const thisMonthStart = startOfMonth(today);

    setStartDate(thisMonthStart);
    setEndDate(today);
    setActiveLink("thismonth");
  }

  function handleLastMonth() {
    const today = new Date();
    const thisMonthStart = startOfMonth(today);
    const lastMonth = subMonths(thisMonthStart, 1);
    const lastMonthEnd = endOfMonth(lastMonth);

    setStartDate(lastMonth);
    setEndDate(lastMonthEnd);
    setActiveLink("lastmonth");
  }

  function getActiveClass(key) {
    return activeLink === key ? "is-active" : "";
  }

  return (
    <CustomDateWrapper className="c-range-datepicker__custom-range">
      <CustomDateTitle>{getTranslationManually("Pilih Waktu", "Set Date")}</CustomDateTitle>
      <CustomDateList className="c-range-datepicker__custom-range-list">
        <li className={getActiveClass("today")} onClick={handleToday}>
          {getTranslationManually("Hari ini", "Today")}
        </li>
        <li className={getActiveClass("yesterday")} onClick={handleYesterday}>
          {getTranslationManually("Kemarin", "Yesterday")}
        </li>
        <li className={getActiveClass("last7")} onClick={handleLast7Days}>
          {getTranslationManually("7 hari terakhir", "Last 7 days")}
        </li>
        <li className={getActiveClass("last14")} onClick={handleLast14Days}>
          {getTranslationManually("14 hari terakhir", "Last 14 days")}
        </li>
        <li className={getActiveClass("last30")} onClick={handleLast30Days}>
          {getTranslationManually("30 hari terakhir", "Last 30 days")}
        </li>
        <li className={getActiveClass("thisweek")} onClick={handleThisWeek}>
          {getTranslationManually("Minggu ini", "This week")}
        </li>
        <li className={getActiveClass("lastweek")} onClick={handleLastWeek}>
          {getTranslationManually("Minggu lalu", "Last week")}
        </li>
        <li className={getActiveClass("thismonth")} onClick={handleThisMonth}>
          {getTranslationManually("Bulan ini", "This month")}
        </li>
        <li className={getActiveClass("lastmonth")} onClick={handleLastMonth}>
          {getTranslationManually("Bulan lalu", "Last month")}
        </li>
      </CustomDateList>
    </CustomDateWrapper>
  );
}

export default CustomRange;
