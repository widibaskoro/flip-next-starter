import React, { useState } from "react";
import classNames from "classnames";

import ReactDatePicker, { registerLocale } from "react-datepicker";
import CalendarHeader from "./CalendarHeader";
import CustomRange from "./CustomRange";
import CalendarTab from "./CalendarTab";
import CalendarFooter from "./CalendarFooter";
import { DatePickerWrapper } from "./DatePickerComponent.styled";

import { compareAsc } from "date-fns";
import { id, enGB } from "date-fns/locale";
import { getCurrentLocale } from "src/helpers/locale";

import "./DatePicker.module.scss";
registerLocale("id", id);
registerLocale("en-GB", enGB);

function DatePickerComponent(props) {
  const {
    className,
    startDateValue,
    endDateValue,
    startDateReset,
    endDateReset,
    onApplyDate,
    position,
    ...restProps
  } = props;
  const datepickerClass = classNames("c-range-datepicker", className);
  const localeString = getCurrentLocale() === "en" ? "en-GB" : "id";
  const locale = getCurrentLocale() === "en" ? enGB : id;
  const today = new Date();

  const [startDate, setStartDate] = useState(startDateValue instanceof Date ? startDateValue : new Date());
  const [endDate, setEndDate] = useState(endDateValue instanceof Date ? endDateValue : new Date());
  const [isStartClicked, setIsStartClicked] = useState(false);

  function handleChangeStart(date) {
    setStartDate(date);
    if (compareAsc(date, endDate) === 1) {
      setEndDate(date);
    }
    setIsStartClicked(true);
  }

  function handleChangeEnd(date) {
    setEndDate(date);
    if (compareAsc(startDate, date) === 1) {
      setStartDate(date);
    }
    setIsStartClicked(false);
  }

  function checkTabChange(tabKey) {
    if (tabKey === "tabstart") {
      setIsStartClicked(false);
    }
  }

  function applyDate() {
    onApplyDate(startDate, endDate, "apply");
  }

  function resetDate() {
    setStartDate(startDateReset);
    setEndDate(endDateReset);
    onApplyDate(startDateReset, endDateReset, "reset");
  }

  return (
    <DatePickerWrapper position={position} className={datepickerClass}>
      <CustomRange setStartDate={setStartDate} setEndDate={setEndDate} />

      <CalendarTab
        locale={locale}
        startDate={startDate}
        endDate={endDate}
        changeToEnd={isStartClicked}
        onTabChange={checkTabChange}
        startCalendar={
          <ReactDatePicker
            calendarClassName="c-range-datepicker c-range-datepicker__start"
            renderCustomHeader={CalendarHeader}
            locale={localeString}
            selected={startDate}
            startDate={startDate}
            endDate={endDate}
            maxDate={today}
            onChange={handleChangeStart}
            selectsStart
            inline
            {...restProps}
          />
        }
        endCalendar={
          <ReactDatePicker
            calendarClassName="c-range-datepicker c-range-datepicker__end"
            renderCustomHeader={CalendarHeader}
            locale={localeString}
            selected={endDate}
            startDate={startDate}
            endDate={endDate}
            maxDate={today}
            onChange={handleChangeEnd}
            selectsEnd
            inline
            {...restProps}
          />
        }
      />

      <CalendarFooter onResetDate={resetDate} onApplyDate={applyDate} />
    </DatePickerWrapper>
  );
}

export default DatePickerComponent;
