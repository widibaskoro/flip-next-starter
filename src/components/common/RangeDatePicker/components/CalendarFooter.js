import React from "react";

import Button from "../../Button";
import { CalendarFooterWrapper } from "./CalendarFooter.styled";

function CalendarFooter(props) {
  const { onResetDate, onApplyDate } = props;

  return (
    <CalendarFooterWrapper className="c-range-datepicker__footer">
      <Button className="u-mr-2" variant="secondary" color="grey" onClick={onResetDate} outline>
        Reset
      </Button>
      <Button variant="secondary" onClick={onApplyDate}>
        Apply
      </Button>
    </CalendarFooterWrapper>
  );
}

export default CalendarFooter;
