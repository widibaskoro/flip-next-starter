import styled from "styled-components";

import { colors, zIndex } from "src/assets/styles/settings";

export const DatePickerWrapper = styled.div`
  background: ${colors.white};
  box-shadow: rgba(13, 22, 38, 0.1) 0px 2px 5px 1px, rgba(13, 22, 38, 0.1) 0px 4px 20px;
  border-radius: 8px;
  ${({ position }) => (position === "left" ? "left: 40px;" : "right: 0px;")}
  position: absolute;
  text-align: center;
  width: 466px;
  z-index: ${zIndex.dropdown};
`;
