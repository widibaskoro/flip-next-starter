import styled from "styled-components";

import Icon from "../../Icon";
import { colors, typography } from "src/assets/styles/settings";

export const CalendarTabWrapper = styled.div`
  border-left: 2px solid ${colors.main_background};
  display: inline-block;
  height: 100%;
  width: 334px;
`;

export const CalendarTabNavWrapper = styled.div`
  background: ${colors.flip_gradient};
  border-radius: 0 8px 0 0;
`;

export const CalendarTabNav = styled.div`
  color: ${colors.anemia};
  cursor: pointer;
  display: inline-block;
  line-height: 150%;
  padding: 16px 20px 20px;
  position: relative;
  text-align: left;
  width: 50%;

  label {
    font-weight: ${typography.font_weight.bold};
    font-size: ${typography.font_size.semi};
    margin-bottom: 0;
    margin-right: 12px;
    text-transform: uppercase;
  }

  div {
    font-weight: ${typography.font_weight.normal};
    font-size: 1.3125rem;
  }

  &.is-active {
    color: ${colors.white};

    div {
      color: ${colors.white};
      font-weight: ${typography.font_weight.bold};
    }
  }
`;

export const TabArrow = styled(Icon)`
  color: ${colors.white};
  font-size: 1.3125rem;
  position: absolute;
  top: calc(50% - 1px);
  right: 0;
`;

export const CalendarWrapper = styled.div`
  display: none;
  padding: 12px 12px 0 12px;

  &.is-active {
    display: block;
  }
`;
