import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import DatePickerComponent from "./components/DatePickerComponent";
import DatePickerInput from "./components/DatePickerInput";
import { DatePickerContainer, DatePickerBlanket, DatepickerFeedback } from "./RangeDatePicker.styled";

import { format, subDays } from "date-fns";

const propTypes = {
  /**
   * Set date picker start date value
   */
  startDateValue: PropTypes.oneOfType([PropTypes.string, PropTypes.object, PropTypes.instanceOf(Date)]),

  /**
   * Set date picker end date value
   */
  endDateValue: PropTypes.oneOfType([PropTypes.string, PropTypes.object, PropTypes.instanceOf(Date)]),

  /**
   * Set date picker to specific start date when reset
   */
  startDateReset: PropTypes.oneOfType([PropTypes.string, PropTypes.object, PropTypes.instanceOf(Date)]),

  /**
   * Set date picker to specific end date when reset
   */
  endDateReset: PropTypes.oneOfType([PropTypes.string, PropTypes.object, PropTypes.instanceOf(Date)]),

  /**
   * Handler when date picker has been applied or reset
   */
  onChangeDate: PropTypes.func.isRequired,

  /**
   * Set datepicker input placeholder
   */
  inputPlaceholder: PropTypes.string,

  /**
   * Set datepicker disabled
   */
  disabled: PropTypes.bool,

  /**
   * set datepicker is invalid
   */
  isInvalid: PropTypes.bool,

  /**
   * set dropdown menu to be aligned to the left or right of the input
   */
  datePickerPosition: PropTypes.oneOf(["left", "right"])
};

const defaultProps = {
  startDateValue: subDays(new Date(), 7),
  endDateValue: new Date(),
  startDateReset: "",
  endDateReset: "",
  onChangeDate: () => {},
  inputPlaceholder: undefined,
  disabled: false,
  isInvalid: false,
  datePickerPosition: "left"
};

const RangeDatePicker = React.forwardRef((props, ref) => {
  const {
    className,
    datepickerClass,
    startDateValue,
    endDateValue,
    startDateReset,
    endDateReset,
    onChangeDate,
    inputPlaceholder,
    disabled,
    isInvalid,
    datePickerPosition,
    ...restProps
  } = props;
  const defaultFormat = "dd MMM yyyy";
  const containerClass = classNames("c-range-datepicker__container", { "is-invalid": isInvalid }, className);

  const [isOpen, setIsOpen] = useState(false);
  const [selectedRange, setSelectedRange] = useState("");

  useEffect(() => {
    if (startDateValue && endDateValue && startDateValue instanceof Date && endDateValue instanceof Date) {
      setSelectedRange(getFormattedDate(startDateValue, endDateValue));
    } else {
      setSelectedRange("");
    }
  }, [startDateValue, endDateValue]);

  function handleApplyDate(startDate, endDate, actionType) {
    let defaultEndDate;

    // add behavior for rangeDatepicker to auto pick endDate to current day
    // if startDate value is provided and is a date instance
    if (!endDate && startDate && startDate instanceof Date) {
      defaultEndDate = new Date();
    } else {
      defaultEndDate = endDate;
    }

    if (startDate && defaultEndDate && startDate instanceof Date && defaultEndDate instanceof Date) {
      setSelectedRange(getFormattedDate(startDate, defaultEndDate));
    }
    onChangeDate(startDate, defaultEndDate, actionType);
    setIsOpen(false);
  }

  function getFormattedDate(startDate, endDate) {
    const formatedEndDate = format(endDate, defaultFormat);
    if (startDate.getFullYear() !== endDate.getFullYear()) {
      return `${format(startDate, defaultFormat)} - ${formatedEndDate}`;
    }
    if (startDate.getMonth() !== endDate.getMonth()) {
      return `${format(startDate, "dd MMM")} - ${formatedEndDate}`;
    }
    return `${format(startDate, "dd")} - ${formatedEndDate}`;
  }

  function openDatePicker() {
    if (!disabled) setIsOpen(true);
  }

  return (
    <DatePickerContainer ref={ref} className={containerClass}>
      <DatePickerInput
        value={selectedRange}
        onInputClick={openDatePicker}
        inputPlaceholder={inputPlaceholder}
        disabled={disabled}
      />
      {isOpen ? (
        <DatePickerComponent
          position={datePickerPosition}
          className={datepickerClass}
          startDateValue={startDateValue}
          endDateValue={endDateValue}
          startDateReset={startDateReset}
          endDateReset={endDateReset}
          onApplyDate={handleApplyDate}
          {...restProps}
        />
      ) : null}
      {isOpen ? <DatePickerBlanket onClick={() => setIsOpen(false)} /> : null}
    </DatePickerContainer>
  );
});

RangeDatePicker.defaultProps = defaultProps;
RangeDatePicker.propTypes = propTypes;

RangeDatePicker.Feedback = DatepickerFeedback;

export default RangeDatePicker;
