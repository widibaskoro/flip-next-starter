import styled from "styled-components";

import { colors, typography, zIndex } from "src/assets/styles/settings";

export const DatePickerContainer = styled.div`
  position: relative;
`;

export const DatePickerBlanket = styled.div`
  bottom: 0px;
  left: 0px;
  top: 0px;
  right: 0px;
  position: fixed;
  z-index: ${zIndex.dropdownOverlay};
`;

export const DatepickerFeedback = styled.div`
  color: ${colors.ketchup_tomato};
  display: none;
  font-size: ${typography.font_size.small};
  margin-top: 4px;
  order: 4;
  width: 100%;

  .c-range-datepicker.is-invalid ~ &,
  .c-range-datepicker__container.is-invalid ~ & {
    display: block;
  }
`;
