import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { ProgressWrapper, ProgressContainer, ProgressContent, ProgressLabel } from "./ProgressBar.styled";

const propsTypes = {
  /**
   * Set bar color
   *
   * @type {"orange" | "blue" | "green" | "grey" | "red" | "yellow" | "white" | "black"}
   */
  color: PropTypes.oneOf(["orange", "blue", "green", "grey", "red", "yellow", "white", "black"]),

  /**
   * Set current percentage of the bar
   */
  percent: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  /*
   * Show or hide percentage label
   */
  labeled: PropTypes.bool
};

const defaultProps = {
  color: "orange",
  percent: 0,
  labeled: false
};

const ProgressBar = React.forwardRef((props, ref) => {
  const { className, color, percent, labeled, ...restProps } = props;
  const progressBarClass = classNames("c-progress-bar", className);

  const cleanedPercent = percent < 0 ? 0 : percent;

  return (
    <ProgressWrapper data-testid="qa-progress-wrapper">
      <ProgressContainer data-testid="qa-progress-container" ref={ref} className={progressBarClass} {...restProps}>
        <ProgressContent data-testid="qa-progress-content" color={color} percent={cleanedPercent} />
      </ProgressContainer>
      {labeled && (
        <ProgressLabel
          data-testid="qa-progress-label"
          className={classNames([0, "0"].includes(cleanedPercent) && "is-zero")}
          color={color}
        >{`${cleanedPercent}%`}</ProgressLabel>
      )}
    </ProgressWrapper>
  );
});

ProgressBar.propTypes = propsTypes;
ProgressBar.defaultProps = defaultProps;

export default ProgressBar;
