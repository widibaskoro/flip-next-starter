import React from "react";
import { shallow, mount } from "enzyme";

import ProgressBar from "./index";
import { findByTestAttr, hexToRGBString } from "src/helpers/test";
import { colors } from "src/assets/styles/settings";

describe("ProgressBar Component", () => {
  describe("Components rendering", () => {
    it("Should render without error", () => {
      const wrapper = shallow(<ProgressBar />);
      expect(wrapper.length).toBe(1);
    });
  });

  describe("Bar width", () => {
    it("Should render bar with 0 widht", () => {
      const wrapper = mount(<ProgressBar />);
      const bar = findByTestAttr(wrapper, "qa-progress-content").first();
      const style = getComputedStyle(bar.getDOMNode());

      expect(style.width).toBe("0%");
    });

    it("Should render bar with correct width and default color (20%, orange)", () => {
      const wrapper = mount(<ProgressBar percent={20} />);
      const bar = findByTestAttr(wrapper, "qa-progress-content").first();
      const style = getComputedStyle(bar.getDOMNode());

      expect(style.background).toBe(hexToRGBString(colors.flip_orange));
      expect(style.width).toBe("20%");
    });

    it("Should keep max width to be 100% even the width is exceeded", () => {
      const wrapper = mount(<ProgressBar percent={400} />);
      const bar = findByTestAttr(wrapper, "qa-progress-content").first();
      const style = getComputedStyle(bar.getDOMNode());

      expect(style.background).toBe(hexToRGBString(colors.flip_orange));
      expect(style.width).toBe("400%");
      expect(style["max-width"]).toBe("100%");
    });

    it("Should set bar width to 0% if percent is negative (-1, orange)", () => {
      const wrapper = mount(<ProgressBar percent={-1} />);
      const bar = findByTestAttr(wrapper, "qa-progress-content").first();
      const style = getComputedStyle(bar.getDOMNode());

      expect(style.background).toBe(hexToRGBString(colors.flip_orange));
      expect(style.width).toBe("0%");
    });
  });

  describe("Bar color", () => {
    it("Should render bar with background jade", () => {
      const wrapper = mount(<ProgressBar percent={20} color="green" />);
      const bar = findByTestAttr(wrapper, "qa-progress-content").first();
      const style = getComputedStyle(bar.getDOMNode());

      expect(style.background).toBe(hexToRGBString(colors.jade));
    });

    it("Should render bar with background yellow", () => {
      const wrapper = mount(<ProgressBar percent={20} color="yellow" />);
      const bar = findByTestAttr(wrapper, "qa-progress-content").first();
      const style = getComputedStyle(bar.getDOMNode());

      expect(style.background).toBe(hexToRGBString(colors.mango_yellow));
    });

    it("Should render bar with background ketchup_tomato", () => {
      const wrapper = mount(<ProgressBar percent={20} color="red" />);
      const bar = findByTestAttr(wrapper, "qa-progress-content").first();
      const style = getComputedStyle(bar.getDOMNode());

      expect(style.background).toBe(hexToRGBString(colors.ketchup_tomato));
    });
  });

  describe("Label content and color", () => {
    it("Should render bar label with correct width and default color", () => {
      const wrapper = mount(<ProgressBar percent={20} labeled />);

      const bar = findByTestAttr(wrapper, "qa-progress-content").first();
      const barStyle = getComputedStyle(bar.getDOMNode());
      expect(barStyle.background).toBe(hexToRGBString(colors.flip_orange));
      expect(barStyle.width).toBe("20%");

      const label = findByTestAttr(wrapper, "qa-progress-label").first();
      const labelStyle = getComputedStyle(label.getDOMNode());
      expect(label.prop("children")).toBe("20%");
      expect(labelStyle.color).toBe(hexToRGBString(colors.flip_orange));
    });

    it("Should render label with dark grey if percent is 0", () => {
      const wrapper = mount(<ProgressBar percent={0} labeled />);
      const label = findByTestAttr(wrapper, "qa-progress-label").first();
      const labelStyle = getComputedStyle(label.getDOMNode());

      expect(label.prop("children")).toBe("0%");
      expect(labelStyle.color).toBe(hexToRGBString(colors.dark_grey));
    });

    it("Should render label with correct given color", () => {
      const wrapper = mount(<ProgressBar percent={50} color="green" labeled />);
      const label = findByTestAttr(wrapper, "qa-progress-label").first();
      const labelStyle = getComputedStyle(label.getDOMNode());

      expect(label.prop("children")).toBe("50%");
      expect(labelStyle.color).toBe(hexToRGBString(colors.jade));
    });

    it("Should set label to 0% if percent is negative (-1)", () => {
      const wrapper = mount(<ProgressBar percent={0} labeled />);
      const label = findByTestAttr(wrapper, "qa-progress-label").first();
      const labelStyle = getComputedStyle(label.getDOMNode());

      expect(label.prop("children")).toBe("0%");
      expect(labelStyle.color).toBe(hexToRGBString(colors.dark_grey));
    });
  });
});
