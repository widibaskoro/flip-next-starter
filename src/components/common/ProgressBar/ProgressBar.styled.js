import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

const backgrounds = {
  orange: colors.flip_orange,
  blue: colors.intel_blue,
  green: colors.jade,
  grey: colors.dark_grey,
  red: colors.ketchup_tomato,
  yellow: colors.mango_yellow,
  white: colors.white,
  black: colors.black_bekko
};

export const ProgressWrapper = styled.div`
  display: flex;
`;

export const ProgressContainer = styled.div`
  background: ${colors.main_background};
  border-radius: 8px;
  height: 6px;
  margin: 6px 0;
  width: 100%;
`;

export const ProgressContent = styled.div`
  background: ${({ color }) => backgrounds[color]};
  border-radius: 8px;
  height: 100%;
  max-width: 100%;
  -webkit-transition: width 0.3s ease;
  -o-transition: width 0.3 ease;
  transition: width 0.3 ease;
  width: ${({ percent }) => percent}%;
`;

export const ProgressLabel = styled.div`
  color: ${({ color }) => backgrounds[color]};
  font-size: ${typography.font_size.semi};
  margin-left: 8px;

  &.is-zero {
    color: ${colors.dark_grey} !important;
  }
`;
