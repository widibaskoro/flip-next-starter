import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

export const RadioButtonWrapper = styled.label`
  display: inline-flex;
  align-items: center;
  line-height: 1;
  margin-bottom: 12px;

  &:hover {
    cursor: pointer;
  }

  &.c-radio-button--tertiary {
    background: ${({ disabled }) => (disabled ? colors.disabled : colors.white)};
    border: 1px solid
      ${({ checked, disabled }) => (disabled ? colors.disabled : checked ? colors.flip_orange : colors.light_grey)};
    border-radius: 8px;
    cursor: ${({ disabled }) => (disabled ? "not-allowed" : "pointer")};
    display: inline-flex;
    align-items: center;
    max-height: 46px;
    padding: 11px 16px;
  }

  &.c-radio-button--tertiary:hover {
    border: 1px solid ${({ disabled }) => (disabled ? colors.disabled : colors.flip_orange)};

    .c-radio-button__radio {
      background: ${colors.light_anemia};
      border: 1px solid ${colors.light_anemia};
    }
  }
`;

export const RadioButtonLabel = styled.span`
  background: ${colors.white};
  border: 1px solid ${colors.dark_grey};
  border-radius: 50%;
  display: inline-block;
  min-height: 18px;
  min-width: 18px;
  height: 18px;
  width: 18px;
`;

export const CheckIcon = styled.svg`
  fill: ${colors.white};
  position: absolute;
  margin: 3px 2px;
  top: 0;
  visibility: hidden;
  width: 12px;
`;

export const BaseRadioButtonInput = styled.input.attrs({ type: "radio" })`
  border: 0;
  height: 1px;
  margin: -1px;
  opacity: 0;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`;

export const RadioButtonInputPrimary = styled(BaseRadioButtonInput)`
  .c-radio-button &:checked + .c-radio-button__radio {
    border: 1px solid ${colors.flip_orange};

    &::after {
      background: ${colors.flip_orange};
      border-radius: 50%;
      content: "";
      display: block;
      height: 12px;
      width: 12px;
      margin: 2px;
    }
  }

  .c-radio-button &:hover + .c-radio-button__radio {
    border: 1px solid ${colors.flip_orange};
  }

  .c-radio-button &:disabled + .c-radio-button__radio {
    background: ${colors.light_grey};
    border: 1px solid ${colors.dark_grey};

    &::after {
      background: ${colors.dark_grey};
    }
  }
`;

export const RadioButtonInputSecondary = styled(BaseRadioButtonInput)`
  .c-radio-button &:checked + .c-radio-button__radio {
    background: ${colors.flip_orange};
    border: 1px solid ${colors.flip_orange};
    position: relative;

    & > ${CheckIcon} {
      visibility: visible;
    }
  }

  .c-radio-button &:hover + .c-radio-button__radio {
    border: 1px solid ${colors.flip_orange};
  }

  .c-radio-button &:disabled + .c-radio-button__radio {
    background: ${colors.light_grey};
    border: 1px solid ${colors.dark_grey};

    & > ${CheckIcon} {
      fill: ${colors.dark_grey};
    }
  }
`;

export const RadioButtonInputTertiary = styled(BaseRadioButtonInput)`
  & + .c-radio-button__radio {
    background: ${colors.main_background};
    border: 1px solid ${colors.disabled};
  }

  .c-radio-button &:checked + .c-radio-button__radio {
    background: ${colors.flip_orange};
    border: 1px solid ${colors.flip_orange};
    position: relative;

    & > ${CheckIcon} {
      visibility: visible;
    }
  }

  .c-radio-button &:disabled + .c-radio-button__radio {
    background: ${colors.light_grey};
    border: 1px solid ${colors.light_grey};

    & > ${CheckIcon} {
      fill: ${colors.dark_grey};
    }
  }
`;

export const LabelText = styled.span`
  color: ${({ disabled }) => (disabled ? colors.dark_grey : colors.black_bekko)};
  display: inline-flex;
  font-weight: ${typography.font_weight.normal};
  font-size: ${typography.font_size.base};
  line-height: ${typography.line_height};
  margin-left: 9px;
  vertical-align: top;

  ${({ disabled }) =>
    disabled &&
    `
    filter: grayscale(1);

    .c-bank-logo {
      opacity: 0.4;
    }
  `};
`;
