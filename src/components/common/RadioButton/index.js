import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import {
  RadioButtonWrapper,
  RadioButtonLabel,
  RadioButtonInputPrimary,
  RadioButtonInputSecondary,
  RadioButtonInputTertiary,
  LabelText,
  CheckIcon
} from "./RadioButton.styled";
import { paths } from "src/assets/styles/settings";

const propTypes = {
  /**
   * Set radio button to be checked
   */
  checked: PropTypes.bool,

  /**
   * Set radio button to be disabled
   */
  disabled: PropTypes.bool,

  /**
   * Set radio button variant (primary or secondary)
   *
   * @type {"primary" | "secondary" | "tertiary"}
   */
  variant: PropTypes.oneOf(["primary", "secondary", "tertiary"])
};

const defaultProps = {
  checked: false,
  disabled: false,
  variant: "primary"
};

const RadioButton = React.forwardRef((props, ref) => {
  const { className, children, checked, disabled, variant, ...restProps } = props;
  const radioButtonClass = classNames("c-radio-button", `c-radio-button--${variant}`, className);

  if (variant === "primary") {
    return (
      <RadioButtonWrapper
        ref={ref}
        className={radioButtonClass}
        checked={checked}
        disabled={disabled}
        data-testid="qa-radio-button"
      >
        <RadioButtonInputPrimary
          className="c-radio-button__input"
          disabled={disabled}
          checked={checked}
          data-testid="qa-radio-button-primary"
          {...restProps}
        />
        <RadioButtonLabel className="c-radio-button__radio" data-testid="qa-radio-button-label" />
        <LabelText className="c-radio-button__text" disabled={disabled} data-testid="qa-radio-button-label-text">
          {children}
        </LabelText>
      </RadioButtonWrapper>
    );
  }

  if (variant === "secondary") {
    return (
      <RadioButtonWrapper
        ref={ref}
        className={radioButtonClass}
        checked={checked}
        disabled={disabled}
        data-testid="qa-radio-button"
      >
        <RadioButtonInputSecondary
          className="c-radio-button__input"
          disabled={disabled}
          checked={checked}
          data-testid="qa-radio-button-secondary"
          {...restProps}
        />
        <RadioButtonLabel className="c-radio-button__radio" data-testid="qa-radio-button-label">
          <CheckIcon className="c-radio-button__icon" viewBox="0 0 24 24" data-testid="qa-radio-button-check-icon">
            <path d={paths.check_icon} />
          </CheckIcon>
        </RadioButtonLabel>
        <LabelText className="c-radio-button__text" disabled={disabled} data-testid="qa-radio-button-label-text">
          {children}
        </LabelText>
      </RadioButtonWrapper>
    );
  }

  if (variant === "tertiary") {
    return (
      <RadioButtonWrapper
        ref={ref}
        className={radioButtonClass}
        checked={checked}
        disabled={disabled}
        data-testid="qa-radio-button"
      >
        <RadioButtonInputTertiary
          className="c-radio-button__input"
          disabled={disabled}
          checked={checked}
          data-testid="qa-radio-button-tertiary"
          {...restProps}
        />
        <RadioButtonLabel className="c-radio-button__radio" data-testid="qa-radio-button-label">
          <CheckIcon className="c-radio-button__icon" viewBox="0 0 24 24" data-testid="qa-radio-button-check-icon">
            <path d={paths.check_icon} />
          </CheckIcon>
        </RadioButtonLabel>
        <LabelText className="c-radio-button__text" disabled={disabled} data-testid="qa-radio-button-label-text">
          {children}
        </LabelText>
      </RadioButtonWrapper>
    );
  }

  return null;
});

RadioButton.propTypes = propTypes;
RadioButton.defaultProps = defaultProps;

export default RadioButton;
