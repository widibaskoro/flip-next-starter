import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import RadioButton from "./index";
import { colors } from "src/assets/styles/settings";
import { checkProps } from "src/helpers/test";

describe("RadioButton component", () => {
  describe("RadioButton rendering", () => {
    it("should render primary RadioButton without error", () => {
      render(<RadioButton onChange={() => {}} />);
      expect(screen.getByTestId("qa-radio-button")).toBeInTheDocument();
      expect(screen.getByTestId("qa-radio-button-primary")).toBeInTheDocument();
    });

    it("should render secondary RadioButton without error", () => {
      render(<RadioButton onChange={() => {}} variant="secondary" />);
      expect(screen.getByTestId("qa-radio-button")).toBeInTheDocument();
      expect(screen.getByTestId("qa-radio-button-secondary")).toBeInTheDocument();
    });

    it("should render tertiary RadioButton without error", () => {
      render(<RadioButton onChange={() => {}} variant="tertiary" />);
      expect(screen.getByTestId("qa-radio-button")).toBeInTheDocument();
      expect(screen.getByTestId("qa-radio-button-tertiary")).toBeInTheDocument();
    });
  });

  describe("RadioButton Functionality", () => {
    it("should response click on primary radio button when clicked", () => {
      const spy = jest.fn();
      render(<RadioButton onChange={spy} />);
      fireEvent.click(screen.getByTestId("qa-radio-button-primary"));
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it("should response click on secondary radio button when clicked", () => {
      const spy = jest.fn();
      render(<RadioButton onChange={spy} variant="secondary" />);
      fireEvent.click(screen.getByTestId("qa-radio-button-secondary"));
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it("should response click on tertiary radio button when clicked", () => {
      const spy = jest.fn();
      render(<RadioButton onChange={spy} variant="tertiary" />);
      fireEvent.click(screen.getByTestId("qa-radio-button-tertiary"));
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it("should not response click on primary radio button when clicked and is disabled", () => {
      const spy = jest.fn();
      render(<RadioButton onChange={spy} disabled />);
      userEvent.click(screen.getByTestId("qa-radio-button-primary"));
      expect(spy).toHaveBeenCalledTimes(0);
    });

    it("should not response click on secondary radio button when clicked and is disabled", () => {
      const spy = jest.fn();
      render(<RadioButton onChange={spy} disabled variant="secondary" />);
      userEvent.click(screen.getByTestId("qa-radio-button-secondary"));
      expect(spy).toHaveBeenCalledTimes(0);
    });

    it("should not response click on tertiary radio button when clicked and is disabled", () => {
      const spy = jest.fn();
      render(<RadioButton onChange={spy} disabled variant="tertiary" />);
      userEvent.click(screen.getByTestId("qa-radio-button-tertiary"));
      expect(spy).toHaveBeenCalledTimes(0);
    });
  });

  describe("RadioButton propTypes", () => {
    it("should not throw warning when expected checked props are provided", () => {
      checkProps(RadioButton, { checked: false }, "RadioButton");
    });

    it("should not throw warning when expected disabled props are provided", () => {
      checkProps(RadioButton, { disabled: false }, "RadioButton");
    });

    it("should not throw warning when expected variant props are provided", () => {
      checkProps(RadioButton, { variant: "primary" }, "RadioButton");
      checkProps(RadioButton, { variant: "secondary" }, "RadioButton");
      checkProps(RadioButton, { variant: "tertiary" }, "RadioButton");
    });
  });

  describe("RadioButton props", () => {
    it("should have custom className when provided", () => {
      render(<RadioButton onChange={() => {}} className="u-border-r-2" />);
      expect(screen.getByTestId("qa-radio-button").className).toContain("u-border-r-2");
    });

    it("should have child text when provided", () => {
      render(
        <RadioButton onChange={() => {}} className="u-border-r-2">
          Coba
        </RadioButton>
      );
      expect(screen.getByTestId("qa-radio-button-label-text")).toHaveTextContent("Coba");
    });

    it("should return null when unrecognized variant given", () => {
      jest.spyOn(console, "error").mockImplementation(() => {}); // disable warning from prop-types
      render(<RadioButton variant="nope" />);
      expect(screen.queryByTestId("qa-radio-button")).toBeNull();
      jest.restoreAllMocks();
    });
  });

  describe("RadioButton Style", () => {
    it("should have disabled label text style when disabled props is true", () => {
      render(<RadioButton disabled />);
      const expectedStyle = { color: colors.dark_grey, filter: "grayscale(1)" };
      expect(screen.getByTestId("qa-radio-button-label-text")).toHaveStyle(expectedStyle);
    });

    it("should have bang logo opacity decreased (0.4) when disabled props is true", () => {
      render(
        <RadioButton disabled>
          <span className="c-bank-logo c-bank-logo--mandiri" />
        </RadioButton>
      );
      const expectedStyle = { opacity: 0.4 };
      expect(screen.getByTestId("qa-radio-button-label-text").children[0]).toHaveStyle(expectedStyle);
    });

    it("should have disabled style when disabled props is true and variant is tertiary", () => {
      render(<RadioButton disabled variant="tertiary" />);
      const expectedStyle = {
        background: colors.disabled,
        cursor: "not-allowed",
        border: `1px solid ${colors.disabled}`
      };
      expect(screen.getByTestId("qa-radio-button")).toHaveStyle(expectedStyle);
    });

    it("should have checked style when checked props is true and variant is tertiary", () => {
      render(<RadioButton onChange={() => {}} checked variant="tertiary" />);
      const expectedStyle = {
        border: `1px solid ${colors.flip_orange}`
      };
      expect(screen.getByTestId("qa-radio-button")).toHaveStyle(expectedStyle);
    });

    it("should have checked style on label when checked props is true and variant is primary", () => {
      render(<RadioButton onChange={() => {}} checked />);
      const expectedStyle = {
        border: `1px solid ${colors.flip_orange}`
      };
      expect(screen.getByTestId("qa-radio-button-label")).toHaveStyle(expectedStyle);
    });

    it("should have checked style on label when checked props is true and variant is secondary", () => {
      render(<RadioButton onChange={() => {}} checked variant="secondary" />);
      const expectedStyle = {
        background: colors.flip_orange,
        border: `1px solid ${colors.flip_orange}`,
        position: "relative"
      };
      expect(screen.getByTestId("qa-radio-button-label")).toHaveStyle(expectedStyle);
    });

    it("should have checked style on label when checked props is true and variant is tertiary", () => {
      render(<RadioButton onChange={() => {}} checked variant="tertiary" />);
      const expectedStyle = {
        background: colors.flip_orange,
        border: `1px solid ${colors.flip_orange}`,
        position: "relative"
      };
      expect(screen.getByTestId("qa-radio-button-label")).toHaveStyle(expectedStyle);
    });

    it("should show check icon when checked props is true and variant is secondary", () => {
      render(<RadioButton onChange={() => {}} checked variant="secondary" />);
      expect(screen.getByTestId("qa-radio-button-check-icon")).toBeVisible();
    });

    it("should show check icon when checked props is true and variant is tertiary", () => {
      render(<RadioButton onChange={() => {}} checked variant="tertiary" />);
      expect(screen.getByTestId("qa-radio-button-check-icon")).toBeVisible();
    });

    it("should have disabled style on label when disabled props is true and variant is primary", () => {
      render(<RadioButton onChange={() => {}} disabled />);
      const expectedStyle = {
        background: colors.light_grey,
        border: `1px solid ${colors.dark_grey}`
      };
      expect(screen.getByTestId("qa-radio-button-label")).toHaveStyle(expectedStyle);
    });

    it("should have disabled style on label when disabled props is true and variant is secondary", () => {
      render(<RadioButton onChange={() => {}} disabled variant="secondary" />);
      const expectedStyle = {
        background: colors.light_grey,
        border: `1px solid ${colors.dark_grey}`
      };
      expect(screen.getByTestId("qa-radio-button-label")).toHaveStyle(expectedStyle);
    });

    it("should have disabled style on label when disabled props is true and variant is tertiary", () => {
      render(<RadioButton onChange={() => {}} disabled variant="tertiary" />);
      const expectedStyle = {
        background: colors.light_grey,
        border: `1px solid ${colors.light_grey}`
      };
      expect(screen.getByTestId("qa-radio-button-label")).toHaveStyle(expectedStyle);
    });

    it("should show grey check icon when disabled props is true and variant is secondary", () => {
      render(<RadioButton onChange={() => {}} disabled variant="secondary" />);
      const expectedStyle = {
        fill: colors.dark_grey
      };
      expect(screen.getByTestId("qa-radio-button-check-icon")).toHaveStyle(expectedStyle);
    });

    it("should show grey check icon when disabled props is true and variant is tertiary", () => {
      render(<RadioButton onChange={() => {}} disabled variant="tertiary" />);
      const expectedStyle = {
        fill: colors.dark_grey
      };
      expect(screen.getByTestId("qa-radio-button-check-icon")).toHaveStyle(expectedStyle);
    });
  });
});
