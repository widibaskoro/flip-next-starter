import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";

import Flash from "./index";
import { checkProps } from "src/helpers/test";
import { colors } from "src/assets/styles/settings";

describe("Flash component", () => {
  describe("Flash rendering", () => {
    it("should render without error", () => {
      render(<Flash />);
      expect(screen.getByTestId("qa-flash")).toBeInTheDocument();
    });
  });

  describe("Flash functionality", () => {
    afterEach(() => {
      jest.runOnlyPendingTimers();
      jest.useRealTimers();
    });

    it("should trigger onClose props when time is running out", async () => {
      jest.useFakeTimers();
      const spy = jest.fn();
      render(<Flash show onClose={spy} />);
      jest.advanceTimersByTime(3000);
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it("should not trigger onClose props when time isn't running out", async () => {
      jest.useFakeTimers();
      const spy = jest.fn();
      render(<Flash show onClose={spy} />);
      jest.advanceTimersByTime(2999);
      expect(spy).toHaveBeenCalledTimes(0);
    });

    it("should close when animation end and animationType is leave", async () => {
      jest.useFakeTimers();
      let show = true;
      const { rerender } = render(<Flash show={show} onClose={() => (show = false)} />);
      jest.advanceTimersByTime(3000);
      rerender(<Flash show={show} onClose={() => (show = false)} />);
      fireEvent.animationEnd(screen.getByTestId("qa-flash"));
      expect(screen.getByTestId("qa-flash")).not.toBeVisible();
    });
  });

  describe("Flash propTypes", () => {
    it("should not throw warning when expected color props (enum) provided", () => {
      const colorsEnum = ["blue", "green", "grey", "red", "yellow"];
      colorsEnum.forEach((color) => checkProps(Flash, { color }, "Flash"));
    });

    it("should not throw warning when all expected props types provided", () => {
      const defaultProps = {
        show: true,
        color: "red",
        icon: faExclamationTriangle,
        emoticon: "🎉",
        onClose: () => {}
      };

      checkProps(Flash, defaultProps, "Flash");
    });
  });

  describe("Flash color", () => {
    it("should have sakura and red_wine theme when given color: red", () => {
      render(<Flash show color="red" />);
      const expectedStyle = { "background-color": colors.sakura, border: "1px solid #e58297", color: colors.red_wine };
      expect(screen.getByTestId("qa-alert")).toHaveStyle(expectedStyle);
    });

    it("should have banana and coin theme when given color: yellow", () => {
      render(<Flash show color="yellow" />);
      const expectedStyle = { "background-color": colors.banana, border: "1px solid #ffda7a", color: colors.coin };
      expect(screen.getByTestId("qa-alert")).toHaveStyle(expectedStyle);
    });

    it("should have light_jade and spinach theme when given color: green", () => {
      render(<Flash show color="green" />);
      const expectedStyle = {
        "background-color": colors.light_jade,
        border: "1px solid #91dec6",
        color: colors.spinach
      };
      expect(screen.getByTestId("qa-alert")).toHaveStyle(expectedStyle);
    });

    it("should have bubble and space blue theme when given color: blue", () => {
      render(<Flash show color="blue" />);
      const expectedStyle = {
        "background-color": colors.bubble,
        border: "1px solid #6aafe8",
        color: colors.space_blue
      };
      expect(screen.getByTestId("qa-alert")).toHaveStyle(expectedStyle);
    });

    it("should have light_smoke and dark_smoke theme when given color: grey", () => {
      render(<Flash show color="grey" />);
      const expectedStyle = {
        "background-color": colors.light_smoke,
        border: "1px solid #dedede",
        color: colors.dark_smoke
      };
      expect(screen.getByTestId("qa-alert")).toHaveStyle(expectedStyle);
    });
  });
});
