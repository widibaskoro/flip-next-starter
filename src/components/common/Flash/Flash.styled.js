import styled, { keyframes } from "styled-components";

import { zIndex } from "src/assets/styles/settings";

const slideDownEnter = keyframes`
  from {
    transform: translate3d(-50%, -150px, 0);
  }
`;

const slideDownLeave = keyframes`
  to {
    transform: translate3d(-50%, -150px, 0);
  }
`;

export const FlashWrapper = styled.div`
  animation-duration: 300ms;
  top: 25%;
  left: 50%;
  position: fixed;
  transform: translate(-50%, -50%);
  z-index: ${zIndex.modalDialog};

  &.slide-enter {
    animation: ${slideDownEnter} both cubic-bezier(0.4, 0, 0, 1.5);
    animation-duration: 300ms;
  }

  &.slide-leave {
    animation: ${slideDownLeave} both;
    animation-duration: 300ms;
  }

  .c-alert {
    box-shadow: 0px 6px 20px rgba(222, 222, 222, 0.48);
  }
`;
