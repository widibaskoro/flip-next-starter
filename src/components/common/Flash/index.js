import React, { useState, useEffect } from "react";
import classNames from "classnames";

import { Alert } from "src/components/common";
import { FlashWrapper } from "./Flash.styled";

const IN_BROWSER = typeof window !== "undefined";
const UA = IN_BROWSER && window.navigator.userAgent.toLowerCase();
const IS_IE_9 = UA && UA.indexOf("msie 9.0") > 0;

function Flash(props) {
  const { children, show, color, icon, emoticon, onClose = () => {} } = props;

  const [isShow, setIsShow] = useState(false);
  const [animationType, setAnimationType] = useState("leave");

  useEffect(() => {
    if (show) enter();
    else leave();
  }, [show]);

  function enter() {
    setTimeout(() => {
      onClose();
    }, 3000);
    setIsShow(true);
    setAnimationType("enter");
  }

  function leave() {
    if (IS_IE_9) {
      setIsShow(false);
    } else {
      setAnimationType("leave");
    }
  }

  function animationEnd() {
    if (animationType === "leave") {
      setIsShow(false);
    }
  }

  const flashClass = classNames("c-flash", `slide-${animationType}`);
  const flashStyle = { display: isShow ? "" : "none" };

  return (
    <FlashWrapper data-testid="qa-flash" style={flashStyle} className={flashClass} onAnimationEnd={animationEnd}>
      <Alert color={color} icon={icon} emoticon={emoticon} bordered>
        {children}
      </Alert>
    </FlashWrapper>
  );
}

export default Flash;
