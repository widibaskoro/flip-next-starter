import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { LoaderWrapper, LoaderOne, LoaderTwo, LoaderThree } from "./Loader.styled";

const propsTypes = {
  /**
   * Set loader color
   *
   * @type {"orange" | "blue" | "green" | "grey" | "red" | "yellow" | "white", | "black"}
   */
  color: PropTypes.oneOf(["orange", "blue", "green", "grey", "red", "yellow", "white", "black"]),

  /**
   * Set loader size
   *
   * @type {"default" | "small"}
   */
  size: PropTypes.oneOf(["default", "small"])
};

const defaultProps = {
  color: "orange",
  size: "default"
};

const Loader = React.forwardRef((props, ref) => {
  const { className, color, size, ...restProps } = props;
  const loaderClass = classNames("c-loader", className);

  return (
    <LoaderWrapper data-testid="qa-loader" ref={ref} className={loaderClass} {...restProps}>
      <LoaderOne data-testid="qa-loader-dot-one" color={color} size={size} />
      <LoaderTwo data-testid="qa-loader-dot-two" color={color} size={size} />
      <LoaderThree data-testid="qa-loader-dot-three" color={color} size={size} />
    </LoaderWrapper>
  );
});

Loader.propTypes = propsTypes;
Loader.defaultProps = defaultProps;

export default Loader;
