import React from "react";
import { render, screen } from "@testing-library/react";

import Loader from "./index";
import { checkProps } from "src/helpers/test";
import { colors } from "../../../assets/styles/settings";

describe("Loader component", () => {
  describe("Loader rendering", () => {
    it("Should render without error", () => {
      render(<Loader />);
      expect(screen.getByTestId("qa-loader")).toBeInTheDocument();
    });
  });

  describe("Loader propTypes", () => {
    it("Should not throw warning when expected colors (enum) are provided", () => {
      checkProps(Loader, { color: "orange" }, "Loader");
      checkProps(Loader, { color: "blue" }, "Loader");
      checkProps(Loader, { color: "green" }, "Loader");
      checkProps(Loader, { color: "grey" }, "Loader");
      checkProps(Loader, { color: "red" }, "Loader");
      checkProps(Loader, { color: "yellow" }, "Loader");
      checkProps(Loader, { color: "white" }, "Loader");
      checkProps(Loader, { color: "black" }, "Loader");
    });

    it("Should not throw warning when expected size are provided", () => {
      checkProps(Loader, { size: "default" }, "Loader");
      checkProps(Loader, { size: "small" }, "Loader");
    });
  });

  describe("Loader props", () => {
    it("should have custom className when provided", () => {
      render(<Loader className="u-mr-2" />);
      expect(screen.getByTestId("qa-loader").className).toContain("u-mr-2");
    });
  });

  describe("Loader styles", () => {
    it("should have flip_orange color dot loader if no props is given", () => {
      render(<Loader />);
      expect(screen.getByTestId("qa-loader-dot-one")).toHaveStyle({ background: colors.flip_orange });
      expect(screen.getByTestId("qa-loader-dot-two")).toHaveStyle({ background: colors.flip_orange });
      expect(screen.getByTestId("qa-loader-dot-three")).toHaveStyle({ background: colors.flip_orange });
    });
  });

  describe("Loader color", () => {
    it("Should have background flip_orange if no color prop is given", () => {
      render(<Loader />);
      const expectedStyle = { background: colors.flip_orange };
      expect(screen.getByTestId("qa-loader-dot-one")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-two")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-three")).toHaveStyle(expectedStyle);
    });

    it("Should have background flip_orange when given color: orange", () => {
      render(<Loader color="orange" />);
      const expectedStyle = { background: colors.flip_orange };
      expect(screen.getByTestId("qa-loader-dot-one")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-two")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-three")).toHaveStyle(expectedStyle);
    });

    it("Should have background intel_blue when given color: blue", () => {
      render(<Loader color="blue" />);
      const expectedStyle = { background: colors.intel_blue };
      expect(screen.getByTestId("qa-loader-dot-one")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-two")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-three")).toHaveStyle(expectedStyle);
    });

    it("Should have background jade when given color: green", () => {
      render(<Loader color="green" />);
      const expectedStyle = { background: colors.jade };
      expect(screen.getByTestId("qa-loader-dot-one")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-two")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-three")).toHaveStyle(expectedStyle);
    });

    it("Should have background dark_grey when given color: grey", () => {
      render(<Loader color="grey" />);
      const expectedStyle = { background: colors.dark_grey };
      expect(screen.getByTestId("qa-loader-dot-one")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-two")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-three")).toHaveStyle(expectedStyle);
    });

    it("Should have background ketchup_tomato when given color: red", () => {
      render(<Loader color="red" />);
      const expectedStyle = { background: colors.ketchup_tomato };
      expect(screen.getByTestId("qa-loader-dot-one")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-two")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-three")).toHaveStyle(expectedStyle);
    });

    it("Should have background mango_yellow when given color: yellow", () => {
      render(<Loader color="yellow" />);
      const expectedStyle = { background: colors.mango_yellow };
      expect(screen.getByTestId("qa-loader-dot-one")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-two")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-three")).toHaveStyle(expectedStyle);
    });

    it("Should have background black_bekko when given color: black", () => {
      render(<Loader color="black" />);
      const expectedStyle = { background: colors.black_bekko };
      expect(screen.getByTestId("qa-loader-dot-one")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-two")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-three")).toHaveStyle(expectedStyle);
    });

    it("Should have background white when given color: white", () => {
      render(<Loader color="white" />);
      const expectedStyle = { background: colors.white };
      expect(screen.getByTestId("qa-loader-dot-one")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-two")).toHaveStyle(expectedStyle);
      expect(screen.getByTestId("qa-loader-dot-three")).toHaveStyle(expectedStyle);
    });
  });
});
