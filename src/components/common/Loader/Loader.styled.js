import styled, { keyframes } from "styled-components";

import { colors } from "src/assets/styles/settings";

const backgrounds = {
  orange: colors.flip_orange,
  blue: colors.intel_blue,
  green: colors.jade,
  grey: colors.dark_grey,
  red: colors.ketchup_tomato,
  yellow: colors.mango_yellow,
  white: colors.white,
  black: colors.black_bekko
};

const dimensions = {
  small: "0.4375rem",
  default: "0.75rem"
};

const bounce = keyframes`
  0%, 80%, 100% {
    transform: scale(0);
  }
  40% {
    transform: scale(1.0);
  }
`;

const bounceWebkit = keyframes`
  0%, 80%, 100% {
    -webkit-transform: scale(0);
  }
  40% {
    -webkit-transform: scale(1.0);
  }
`;

export const LoaderWrapper = styled.div`
  display: inline-block;
  text-align: center;
`;

export const LoaderOne = styled.div`
  background: ${({ color }) => backgrounds[color]};
  border-radius: 100%;
  display: inline-block;
  margin-left: 2px;
  margin-right: 2px;
  height: ${({ size }) => dimensions[size]};
  width: ${({ size }) => dimensions[size]};
  -webkit-animation: ${bounceWebkit} 1.4s infinite ease-in-out both;
  animation: ${bounce} 1.4s infinite ease-in-out both;
  -webkit-animation-delay: 0s;
  animation-delay: 0s;
`;

export const LoaderTwo = styled.div`
  background: ${({ color }) => backgrounds[color]};
  border-radius: 100%;
  display: inline-block;
  margin-left: 2px;
  margin-right: 2px;
  height: ${({ size }) => dimensions[size]};
  width: ${({ size }) => dimensions[size]};
  -webkit-animation: ${bounceWebkit} 1.4s infinite ease-in-out both;
  animation: ${bounce} 1.4s infinite ease-in-out both;
  -webkit-animation-delay: 0.16s;
  animation-delay: 0.16s;
`;

export const LoaderThree = styled.div`
  background: ${({ color }) => backgrounds[color]};
  border-radius: 100%;
  display: inline-block;
  margin-left: 2px;
  margin-right: 2px;
  height: ${({ size }) => dimensions[size]};
  width: ${({ size }) => dimensions[size]};
  -webkit-animation: ${bounceWebkit} 1.4s infinite ease-in-out both;
  animation: ${bounce} 1.4s infinite ease-in-out both;
  -webkit-animation-delay: 0.32s;
  animation-delay: 0.32s;
`;
