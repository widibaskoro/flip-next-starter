import React from "react";
import { render, screen } from "@testing-library/react";

import Panel from "./index";
import { checkProps } from "src/helpers/test";
import { colors } from "src/assets/styles/settings";

describe("Panel component", () => {
  describe("Panel rendering", () => {
    it("Should render without error", () => {
      render(<Panel />);
      expect(screen.getByTestId("qa-panel")).toBeInTheDocument();
    });

    it("Should render Panel with body without error", () => {
      render(
        <Panel>
          <Panel.Body></Panel.Body>
        </Panel>
      );
      expect(screen.getByTestId("qa-panel")).toBeInTheDocument();
      expect(screen.getByTestId("qa-panel-body")).toBeInTheDocument();
    });

    it("Should render Panel with body and footer without error", () => {
      render(
        <Panel>
          <Panel.Body></Panel.Body>
          <Panel.Footer></Panel.Footer>
        </Panel>
      );
      expect(screen.getByTestId("qa-panel")).toBeInTheDocument();
      expect(screen.getByTestId("qa-panel-body")).toBeInTheDocument();
      expect(screen.getByTestId("qa-panel")).toBeInTheDocument();
    });
  });

  describe("Panel propTypes", () => {
    it("Should not throw warning when expected bordered props are provided", () => {
      checkProps(Panel, { bordered: false }, "Panel");
    });

    it("Should not throw warning when expected shadowed props are provided", () => {
      checkProps(Panel, { shadowed: false }, "Panel");
    });
  });

  describe("Panel props", () => {
    it("should have custom className when provided", () => {
      render(<Panel className="u-mr-2" />);
      expect(screen.getByTestId("qa-panel").className).toContain("u-mr-2");
    });

    it("should have custom className when Panel body provided", () => {
      render(<Panel.Body className="u-mr-2" />);
      expect(screen.getByTestId("qa-panel-body").className).toContain("u-mr-2");
    });

    it("should have custom className when Panel footer provided", () => {
      render(<Panel.Footer className="u-mr-2" />);
      expect(screen.getByTestId("qa-panel-footer").className).toContain("u-mr-2");
    });
  });

  describe("Panel style", () => {
    it("Should have bordered style when bordered props is true", () => {
      render(<Panel bordered />);
      const expectedStyle = { border: `1px solid ${colors.light_grey}` };
      expect(screen.getByTestId("qa-panel")).toHaveStyle(expectedStyle);
    });

    it("Should have shadowed style when shadowed props is true", () => {
      render(<Panel shadowed />);
      const expectedStyle = { "box-shadow": "0px 6px 20px rgba(222,222,222,0.48)" };
      expect(screen.getByTestId("qa-panel")).toHaveStyle(expectedStyle);
    });
  });
});
