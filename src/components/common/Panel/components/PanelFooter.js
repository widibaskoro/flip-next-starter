import React from "react";
import classNames from "classnames";

import { StyledPanelFooter } from "../Panel.styled";

const PanelFooter = React.forwardRef((props, ref) => {
  const { children, className } = props;
  const cardClass = classNames("c-panel__footer", className);

  return (
    <StyledPanelFooter ref={ref} data-testid="qa-panel-footer" className={cardClass}>
      {children}
    </StyledPanelFooter>
  );
});

export default PanelFooter;
