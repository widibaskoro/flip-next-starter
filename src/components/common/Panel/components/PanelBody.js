import React, { useRef, useEffect } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { StyledPanelBody } from "../Panel.styled";

const propTypes = {
  /**
   * Set panel to be full height (to the bottom of page)
   */
  fullHeight: PropTypes.bool,

  /**
   * Set panel to be full height (to the bottom of page) (for panel with footer only)
   */
  fullHeightWithFooter: PropTypes.bool
};

const defaultProps = {
  fullHeight: false,
  fullHeightWithFooter: false
};

const PanelBody = React.forwardRef((props, ref) => {
  const { children, className, fullHeight, fullHeightWithFooter } = props;

  const defaultBodyPadding = 20;
  const defaultFooterHeight = 94;
  const panelBodyClass = classNames("c-panel__body", className);
  let panelBodyRef = useRef(null);
  if(ref){
    panelBodyRef = ref
  }
  function resizePanel() {
    const thisPanel = panelBodyRef.current;
    if (thisPanel && typeof thisPanel.getBoundingClientRect !== "undefined") {
      let minHeight = window.innerHeight - thisPanel.getBoundingClientRect().top - defaultBodyPadding;
      if (fullHeightWithFooter) minHeight -= defaultFooterHeight;
      thisPanel.style.minHeight = minHeight + "px";
    }
  }

  useEffect(() => {
    if (fullHeight || fullHeightWithFooter) resizePanel();
  }, []);

  return (
    <StyledPanelBody ref={panelBodyRef} data-testid="qa-panel-body" className={panelBodyClass}>
      {children}
    </StyledPanelBody>
  );
});

PanelBody.propTypes = propTypes;
PanelBody.defaultProps = defaultProps;

export default PanelBody;
