import styled from "styled-components";

import { colors } from "src/assets/styles/settings";

export const StyledPanel = styled.div`
  background: ${colors.white};
  border-radius: 8px;
  display: block;
  position: relative;
  word-wrap: break-word;

  &.c-panel--bordered {
    border: 1px solid ${colors.light_grey};
  }

  &.c-panel--shadowed {
    box-shadow: 0px 6px 20px rgba(222, 222, 222, 0.48);
  }
`;

export const StyledPanelBody = styled.div`
  display: block;
  padding: 20px;
`;

export const StyledPanelFooter = styled.div`
  border-top: 1px solid ${colors.disabled};
  display: block;
  padding: 20px;
`;
