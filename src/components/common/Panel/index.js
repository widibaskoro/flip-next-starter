import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { StyledPanel } from "./Panel.styled";
import PanelBody from "./components/PanelBody";
import PanelFooter from "./components/PanelFooter";

const propTypes = {
  /**
   * Set panel to be bordered
   */
  bordered: PropTypes.bool,

  /**
   * Set panel to be shadowed
   */
  shadowed: PropTypes.bool
};

const defaultProps = {
  bordered: false,
  shadowed: false
};

const Panel = React.forwardRef((props, ref) => {
  const { children, className, bordered, shadowed, ...restProps } = props;

  const panelClass = classNames("c-panel", bordered && "c-panel--bordered", shadowed && "c-panel--shadowed", className);

  return (
    <StyledPanel ref={ref} data-testid="qa-panel" className={panelClass} {...restProps}>
      {children}
    </StyledPanel>
  );
});

Panel.Body = PanelBody;
Panel.Footer = PanelFooter;

Panel.propTypes = propTypes;
Panel.defaultProps = defaultProps;

export default Panel;
