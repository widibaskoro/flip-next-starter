import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const propTypes = {
  /**
   * Set the icon
   * see these links for icon usage:
   *   - https://github.com/FortAwesome/react-fontawesome
   */
  icon: PropTypes.any.isRequired
};

function Icon(props) {
  const { icon, className, ...restProps } = props;
  const iconClass = classNames("c-icon", className);

  return <FontAwesomeIcon data-testid="qa-icon" icon={icon} className={iconClass} {...restProps} />;
}

Icon.propTypes = propTypes;

export default Icon;
