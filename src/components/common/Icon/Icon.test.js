import React from "react";
import { render, screen } from "@testing-library/react";

import Icon from "./index";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { checkProps } from "src/helpers/test";

describe("Icon component", () => {
  describe("Icon rendering", () => {
    it("Should render without error", () => {
      render(<Icon icon={faCheckCircle} />);
      expect(screen.getByTestId("qa-icon")).toBeInTheDocument();
    });
  });

  describe("Icon propTypes", () => {
    it("Should have fa-check-circle if icon prop is faCheckCircle", () => {
      checkProps(Icon, { icon: "fa-check-circle" }, "Icon");
    });
  });

  describe("Icon props", () => {
    it("should have custom className when provided", () => {
      render(<Icon icon={faCheckCircle} className="u-mr-2" />);
      expect(screen.getByTestId("qa-icon")).toHaveClass("svg-inline--fa fa-check-circle fa-w-16 c-icon u-mr-2");
    });
  });
});
