import React from "react";
import { render, screen } from "@testing-library/react";

import Container from "./Container";
import Row from "./Row";
import Col from "./Col";
import { checkProps } from "src/helpers/test";

describe("Grid component", () => {
  describe("Grid rendering", () => {
    it("should render Grid Container without error", () => {
      render(<Container />);
      expect(screen.getByTestId("qa-grid-container")).toBeInTheDocument();
    });

    it("should render Grid Row without error", () => {
      render(<Row />);
      expect(screen.getByTestId("qa-grid-row")).toBeInTheDocument();
    });

    it("should render Grid Col without error", () => {
      render(<Col />);
      expect(screen.getByTestId("qa-grid-col")).toBeInTheDocument();
    });
  });

  describe("Container functionality", () => {
    it("should have default className list when no fluid props provided", () => {
      render(<Container />);
      expect(screen.getByTestId("qa-grid-container").className).toBe(
        "u-container-gutters u-mx-auto u-w-12/12 sm:u-max-w-screen-sm md:u-max-w-screen-md lg:u-max-w-screen-lg xl:u-max-w-screen-xl"
      );
    });

    it("should have fluid className list when fluid props is true", () => {
      render(<Container fluid />);
      expect(screen.getByTestId("qa-grid-container").className).toBe("u-w-12/12 u-container-gutters");
    });

    it("should contain sm breakpoint adjuster className when fluid props is 'sm'", () => {
      render(<Container fluid="sm" />);
      expect(screen.getByTestId("qa-grid-container").className).toContain("sm-con:u-max-w-none sm-con:u-mx-0");
    });

    it("should contain md breakpoint adjuster className when fluid props is 'md'", () => {
      render(<Container fluid="md" />);
      expect(screen.getByTestId("qa-grid-container").className).toContain("md-con:u-max-w-none md-con:u-mx-0");
    });

    it("should contain lg breakpoint adjuster className when fluid props is 'lg'", () => {
      render(<Container fluid="lg" />);
      expect(screen.getByTestId("qa-grid-container").className).toContain("lg-con:u-max-w-none lg-con:u-mx-0");
    });

    it("should contain xl breakpoint adjuster className when fluid props is 'xl'", () => {
      render(<Container fluid="xl" />);
      expect(screen.getByTestId("qa-grid-container").className).toContain("xl-con:u-max-w-none xl-con:u-mx-0");
    });
  });

  describe("Row functionality", () => {
    it("should have child Col with default className when no props provided", () => {
      render(
        <Row>
          <Col>1</Col>
        </Row>
      );
      expect(screen.getByTestId("qa-grid-col").className).toBe("u-relative u-col-gutters u-flex-1");
    });

    it("should have child Col which contain specific breakpoint (xs) className when breakpoint props is provided as a number", () => {
      render(
        <Row xs={3}>
          <Col>1</Col>
        </Row>
      );
      expect(screen.getByTestId("qa-grid-col").className).toContain("xs:u-w-4/12 xs:u-flex-none");
    });

    it("should have child Col which contain multiple breakpoint className (sm, md, lg, xl) when multiple breakpoint props is provided as a number", () => {
      render(
        <Row sm={3} md={4} lg={5} xl={6}>
          <Col>1</Col>
        </Row>
      );
      expect(screen.getByTestId("qa-grid-col").className).toContain(
        "sm:u-w-4/12 md:u-w-3/12 lg:u-w-1/5 xl:u-w-2/12 sm:u-flex-none"
      );
    });

    it("should have child Col which contain specific breakpoint (xs) className when breakpoint props is provided as an object", () => {
      render(
        <Row xs={{ cols: 3 }}>
          <Col>1</Col>
        </Row>
      );
      expect(screen.getByTestId("qa-grid-col").className).toContain("xs:u-w-4/12 xs:u-flex-none");
    });

    it("should have child Col which contain multiple breakpoint className (sm, md, lg, xl) when multiple breakpoint props is provided as an object", () => {
      render(
        <Row sm={{ cols: 3 }} md={{ cols: 4 }} lg={{ cols: 5 }} xl={{ cols: 6 }}>
          <Col>1</Col>
        </Row>
      );
      expect(screen.getByTestId("qa-grid-col").className).toContain(
        "sm:u-w-4/12 md:u-w-3/12 lg:u-w-1/5 xl:u-w-2/12 sm:u-flex-none"
      );
    });

    it("should have child Col which contain className 'u-flex-none' on the lowest breakpoint provided (md) when multiple breakpoint props is provided", () => {
      render(
        <Row md={4} xl={6}>
          <Col>1</Col>
        </Row>
      );
      expect(screen.getByTestId("qa-grid-col").className).toContain("md:u-w-3/12 xl:u-w-2/12 md:u-flex-none");
    });

    it("should have child Col with correct width specified by row when row cols is 2", () => {
      render(
        <Row sm={{ cols: 2 }}>
          <Col>1</Col>
        </Row>
      );
      expect(screen.getByTestId("qa-grid-col").className).toContain("sm:u-w-6/12 sm:u-flex-none");
    });

    it("should have child Col with correct width specified by row when row cols is 3", () => {
      render(
        <Row sm={{ cols: 3 }}>
          <Col>1</Col>
        </Row>
      );
      expect(screen.getByTestId("qa-grid-col").className).toContain("sm:u-w-4/12 sm:u-flex-none");
    });

    it("should have child Col with correct width specified by row when row cols is 4", () => {
      render(
        <Row sm={{ cols: 4 }}>
          <Col>1</Col>
        </Row>
      );
      expect(screen.getByTestId("qa-grid-col").className).toContain("sm:u-w-3/12 sm:u-flex-none");
    });

    it("should have child Col with correct width specified by row when row cols is 5", () => {
      render(
        <Row sm={{ cols: 5 }}>
          <Col>1</Col>
        </Row>
      );
      expect(screen.getByTestId("qa-grid-col").className).toContain("sm:u-w-1/5 sm:u-flex-none");
    });

    it("should have child Col with correct width specified by row when row cols is 6", () => {
      render(
        <Row sm={{ cols: 6 }}>
          <Col>1</Col>
        </Row>
      );
      expect(screen.getByTestId("qa-grid-col").className).toContain("sm:u-w-2/12 sm:u-flex-none");
    });

    it("should have child Col with default className when row cols is out of range (1, less than 2)", () => {
      render(
        <Row sm={{ cols: 1 }}>
          <Col>1</Col>
        </Row>
      );
      expect(screen.getByTestId("qa-grid-col")).toHaveClass("u-relative u-col-gutters u-flex-1");
    });

    it("should have child Col with default className when row cols is out of range (10, more than 6)", () => {
      render(
        <Row sm={{ cols: 10 }}>
          <Col>1</Col>
        </Row>
      );
      expect(screen.getByTestId("qa-grid-col")).toHaveClass("u-relative u-col-gutters u-flex-1");
    });

    it("should override child Col width on specific breakpoint when row cols breakpoint is higher", () => {
      render(
        <Row lg={{ cols: 3 }}>
          <Col sm={6}>1</Col>
        </Row>
      );
      expect(screen.getByTestId("qa-grid-col").className).toContain(
        "lg:u-w-4/12 lg:u-flex-none u-w-12/12 sm:u-flex-none sm:u-w-6/12"
      );
    });

    it("should ignore row defined cols when child Col have same rank of breakpoint", () => {
      render(
        <Row sm={{ cols: 3 }}>
          <Col sm={6}>1</Col>
        </Row>
      );
      expect(screen.getByTestId("qa-grid-col").className).toContain("sm:u-flex-none sm:u-w-6/12");
    });
  });

  describe("Col functionality", () => {
    it("should have default className when no props provided", () => {
      render(<Col>1</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toBe("u-relative u-col-gutters u-flex-1");
    });

    it("should have default full base width when any breakpoint props provided", () => {
      render(<Col sm={2}>1</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toContain("u-w-12/12");
    });

    it("should have specific breakpoint (xs) className when breakpoint props is provided as a number", () => {
      render(<Col xs={3}>1</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toContain("xs:u-w-3/12");
    });

    it("should have multiple breakpoint className (sm, md, lg, xl) when multiple breakpoint props is provided as a number", () => {
      render(
        <Col sm={3} md={4} lg={5} xl={6}>
          1
        </Col>
      );
      expect(screen.getByTestId("qa-grid-col").className).toContain("sm:u-w-3/12 md:u-w-4/12 lg:u-w-5/12 xl:u-w-6/12");
    });

    it("should have 'u-flex-1' className when any breakpoint props is boolean true", () => {
      render(<Col lg>1</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toContain("lg:u-flex-1");
    });

    it("should have auto width when any breakpoint props is string 'auto'", () => {
      render(<Col lg="auto">1</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toContain("lg:u-w-auto");
    });

    it("should have specific breakpoint (xs) className when breakpoint props is an object with property span", () => {
      render(<Col xs={{ span: 3 }}>1</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toContain("xs:u-w-3/12");
    });

    it("should have multiple breakpoint className (sm, md, lg, xl) when multiple breakpoint props is an object with property span", () => {
      render(
        <Col sm={{ span: 3 }} md={{ span: 4 }} lg={{ span: 5 }} xl={{ span: 6 }}>
          1
        </Col>
      );
      expect(screen.getByTestId("qa-grid-col").className).toContain("sm:u-w-3/12 md:u-w-4/12 lg:u-w-5/12 xl:u-w-6/12");
    });

    it("should have 'u-flex-1' className when any breakpoint props is object with span property value true", () => {
      render(<Col sm={{ span: true }}>1</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toContain("sm:u-flex-1");
    });

    it("should have auto width when any breakpoint props is an object with span property value string 'auto'", () => {
      render(<Col lg={{ span: "auto" }}>1</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toContain("lg:u-w-auto");
    });

    it("should have Col order of 1 when any breakpoint props is an object with order property value 1", () => {
      render(<Col lg={{ order: 1 }}>1</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toContain("lg:u-order-1");
    });

    it("should have Col order of 2 when any breakpoint props is an object with order property value 2", () => {
      render(<Col lg={{ order: 2 }}>2</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toContain("lg:u-order-2");
    });

    it("should have Col order of 3 when any breakpoint props is an object with order property value 3", () => {
      render(<Col lg={{ order: 3 }}>3</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toContain("lg:u-order-3");
    });

    it("should have Col order of 'first' when any breakpoint props is an object with order property value 'first'", () => {
      render(<Col lg={{ order: "first" }}>1</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toContain("lg:u-order-first");
    });

    it("should have Col order of 'last' when any breakpoint props is an object with order property value 'last'", () => {
      render(<Col lg={{ order: "last" }}>1</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toContain("lg:u-order-last");
    });

    it("should have Col offset (margin-left) of 4 when any breakpoint props is an object with offset property value 4", () => {
      render(<Col lg={{ offset: 4 }}>4</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toContain("lg:u-ml-4/12");
    });

    it("should have Col offset (margin-left) of 5 when any breakpoint props is an object with offset property value 5", () => {
      render(<Col lg={{ offset: 5 }}>5</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toContain("lg:u-ml-5/12");
    });

    it("should have Col offset (margin-left) of 6 when any breakpoint props is an object with offset property value 6", () => {
      render(<Col lg={{ offset: 6 }}>6</Col>);
      expect(screen.getByTestId("qa-grid-col").className).toContain("lg:u-ml-6/12");
    });
  });

  describe("Container propTypes", () => {
    it("should not throw warning when expected props types provided", () => {
      const defaultProps = {
        className: "u-px-5",
        fluid: false
      };

      checkProps(Container, defaultProps, "Container");
    });
  });

  describe("Row propTypes", () => {
    it("should not throw warning when breakpoint props types is a number", () => {
      const breakpoint = {
        xs: 2,
        sm: 2,
        md: 3,
        lg: 4,
        xl: 5
      };

      checkProps(Row, breakpoint, "Row");
    });

    it("should not throw warning when breakpoint props types is an object with property 'cols' of number", () => {
      const breakpoint = {
        xs: { cols: 2 },
        sm: { cols: 2 },
        md: { cols: 3 },
        lg: { cols: 4 },
        xl: { cols: 5 }
      };

      checkProps(Row, breakpoint, "Row");
    });

    it("should not throw warning when expected props types provided", () => {
      const defaultProps = {
        className: "u-px-5",
        noGutters: false
      };

      checkProps(Row, defaultProps, "Row");
    });
  });

  describe("Col propTypes", () => {
    it("should not throw warning when breakpoint props types is a number", () => {
      const breakpoint = {
        xs: 2,
        sm: 2,
        md: 3,
        lg: 4,
        xl: 5
      };

      checkProps(Col, breakpoint, "Col");
    });

    it("should not throw warning when breakpoint props types is string 'auto'", () => {
      const breakpoint = {
        xs: "auto",
        sm: "auto",
        md: "auto",
        lg: "auto",
        xl: "auto"
      };

      checkProps(Col, breakpoint, "Col");
    });

    it("should not throw warning when breakpoint props types is boolean", () => {
      const breakpoint = {
        xs: true,
        sm: false,
        md: true,
        lg: false,
        xl: true
      };

      checkProps(Col, breakpoint, "Col");
    });

    it("should not throw warning when breakpoint props types is an object with expected properties", () => {
      const breakpoint = {
        xs: { offset: 2, span: "auto" },
        sm: { span: true },
        md: { span: 2, order: "last" },
        lg: { order: "first", offset: 3 },
        xl: { order: 5 }
      };

      checkProps(Col, breakpoint, "Col");
    });

    it("should not throw warning when expected props types provided", () => {
      const defaultProps = {
        className: "u-px-5",
        noGutters: false
      };

      checkProps(Col, defaultProps, "Col");
    });
  });

  describe("Container props", () => {
    it("should have custom className when provided", () => {
      render(<Container className="u-border-r-2" />);
      expect(screen.getByTestId("qa-grid-container").className).toContain("u-border-r-2");
    });
  });

  describe("Row props", () => {
    it("should have custom className when provided", () => {
      render(<Row className="u-border-r-2" />);
      expect(screen.getByTestId("qa-grid-row").className).toContain("u-border-r-2");
    });

    it("should pass only child when child isnt a valid react element (e.g text)", () => {
      render(<Row>Invalid Element</Row>);
      expect(screen.getByTestId("qa-grid-row")).toHaveTextContent("Invalid Element");
    });

    it("should have gutters when noGutters props is false", () => {
      render(<Row noGutters={false} />);
      expect(screen.getByTestId("qa-grid-row").className).toContain("u-row-gutters");
    });

    it("should not have gutters when noGutters props is true", () => {
      render(<Row noGutters />);
      expect(screen.getByTestId("qa-grid-row").className).not.toContain("u-row-gutters");
    });
  });

  describe("Col props", () => {
    it("should have custom className when provided", () => {
      render(<Col className="u-border-r-2" />);
      expect(screen.getByTestId("qa-grid-col").className).toContain("u-border-r-2");
    });
  });
});
