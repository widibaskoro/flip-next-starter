import React from "react";
import PropTypes from "prop-types";
import clsx from "classnames";

const defaultProps = {
  className: "",
  fluid: false
};

const containerPropTypes = {
  className: PropTypes.string,

  /**
   * Allow the Container to fill all of its available horizontal space.
   * type -> true | "sm" | "md" | "lg" | "xl"
   */
  fluid: PropTypes.oneOfType([PropTypes.oneOf(["sm", "md", "lg", "xl"]), PropTypes.bool])
};

function Container(props) {
  const { children, fluid, className, ...resProps } = props;

  let defaultFluid =
    "u-container-gutters u-mx-auto u-w-12/12 sm:u-max-w-screen-sm md:u-max-w-screen-md lg:u-max-w-screen-lg xl:u-max-w-screen-xl";

  if (["sm", "md", "lg", "xl"].includes(fluid)) {
    // notice the space at first. its required
    defaultFluid += ` ${fluid}-con:u-max-w-none ${fluid}-con:u-mx-0`;
  } else if (fluid === true) {
    defaultFluid = "u-w-12/12 u-container-gutters";
  }

  return (
    <div data-testid="qa-grid-container" className={clsx(defaultFluid, className)} {...resProps}>
      {children}
    </div>
  );
}

Container.defaultProps = defaultProps;
Container.propTypes = containerPropTypes;

export default Container;
