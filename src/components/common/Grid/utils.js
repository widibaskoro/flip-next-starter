import clsxd from "classnames/dedupe";
import clsx from "classnames";

// rank breakpoints
const rank = {
  xs: 1,
  sm: 2,
  md: 3,
  lg: 4,
  xl: 5
};

/**
 * Remove falsy value from objects
 * @param {Object} obj
 * @return {Object}
 */
function objectCleaner(obj) {
  const copy = { ...obj };
  Object.keys(copy).forEach((key) => !copy[key] && delete copy[key]);
  return copy;
}

/**
 * check if span is a number or string of number
 * then check if it is inside valid span range (1-12);
 * --------------------------------------------------------------
 * @param {Number|String} span - the span range of column
 * @return {Boolean}
 */
function checkSpan(span) {
  return typeof parseInt(span) === "number" && !isNaN(parseInt(span)) && span > 0 && span < 13;
}

/**
 * check if cols provided is in valid range (2-6).
 * 1 cols is considered false, because its same with full cols span
 * out of range cols will return undefined colsCount
 * @param {Number|String} cols - number of cols a Row could have
 * @return {Array[Boolean, Number]} - [cols is in range, adjusted cols based on rules]
 */
function checkCols(cols) {
  const normalizedCols = typeof cols === "object" ? +cols.cols : +cols;
  const isInRange = normalizedCols > 1 && normalizedCols < 7;
  let colsCount;

  if (normalizedCols === 5) {
    colsCount = "u-w-1/5";
  } else if (isInRange) {
    colsCount = `u-w-${12 / normalizedCols}/12`;
  }

  return [isInRange, colsCount];
}

/**
 * helpers to handle folowing config from user and add the breakpoint
 * --------------------------------------------------------------
 * SPAN: generate width of <Col>
 * - md={{ span: 5 }} -> "md:u-w-5/12"
 * - md={{ span: auto }} -> "md:u-w-auto"
 * --------------------------------------------------------------
 * OFFSET: generate margin left of <Col>
 * - md={{ offset: 2 }} -> "md:u-ml-2/12"
 * --------------------------------------------------------------
 * ORDER: place <Col> on specified order
 * - md={{ order: (1-12) }} -> "md:u-order-(1-2)"
 * - md={{ order: (first|last) }} -> "md:u-order-(first|last)"
 * --------------------------------------------------------------
 * @param {String} breakPoint - breakpoints type: xs, sm, md, lg, xl
 * @param {Object} config - object of <Col> component configuration
 * @return {Array} - Result is array contains class string
 */
function colsSpanObjectParser(breakPoint, config) {
  const { span, offset, order } = config;

  return clsx(
    checkSpan(span) && `${breakPoint}:u-w-${span}/12`,
    span === "auto" && `${breakPoint}:u-w-auto`,
    span === true && `${breakPoint}:u-flex-1`,
    offset && `${breakPoint}:u-ml-${offset}/12`,
    order && `${breakPoint}:u-order-${order}`
  );
}

/**
 * generate class string for <Col> component
 * @param {Object} colsConfig - contains breakpoints and config of <Col> component
 * @param {Object} rowConfig - contains breakpoints and config of <Row> component
 */
export function colsSpanGenerator(colsConfig, rowConfig) {
  const span = [];
  const cleanRowConfig = objectCleaner(rowConfig);
  const cleanColsConfig = objectCleaner(colsConfig);
  const rowEntries = Object.entries(cleanRowConfig);
  const colsEntries = Object.entries(cleanColsConfig);

  // to track the lowest breakpoints defined
  // It will be used to add flex-none to cancel default flex-1
  let lowest = "xl";

  // map cols defined in <Row> component
  if (rowEntries.length > 0) {
    // give default flex-1 to persist <Col> growing behavior
    // when no specific breakpoint is defined
    span.push("u-flex-1");

    rowEntries.forEach(([breakPoint, cols]) => {
      // to check if <Col> span have same breakPoint
      // with <Row> cols. If it have similar breakPoint, then we shall not
      // give <Col> span based on <Row> cols
      const haveSimilarbreakPoint = cleanColsConfig[breakPoint];

      const [isInRange, colsCount] = checkCols(cols);

      if (isInRange && !haveSimilarbreakPoint) {
        span.push(`${breakPoint}:${colsCount}`);
      }

      // check which breakpoint is the lowest defined
      if (rank[lowest] > rank[breakPoint]) {
        lowest = breakPoint;
      }
    });

    // stop the growing behavior at the lowest breakpoint defined and up if valid cols specified
    if (span.length > 1) span.push(`${lowest}:u-flex-none`);
  }

  // map span in <Col> component
  if (colsEntries.length > 0) {
    colsEntries.forEach(([breakPoint, config]) => {
      // if <Row> defines a cols, add stopper to prevent flex-1 took effect
      const stopper = clsx(rowEntries.length > 0 && `${breakPoint}:u-flex-none`);
      const colsSpan = clsx(
        "u-w-12/12",
        stopper,
        checkSpan(config) && `${breakPoint}:u-w-${config}/12`,
        config === "auto" && `${breakPoint}:u-w-auto`,
        config === true && `${breakPoint}:u-flex-1`,
        typeof config === "object" && colsSpanObjectParser(breakPoint, config)
      );

      span.push(colsSpan);
    });
  }

  // fallback if no specific breakpoints and span provided
  // give flex-1 to let component span along available space
  if (span.length === 0) {
    span.push("u-flex-1");
  }

  return clsxd(span);
}
