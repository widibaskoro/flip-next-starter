import React from "react";
import PropTypes from "prop-types";
import clsx from "classnames";

const rowDefaultProps = {
  className: "",
  noGutters: false
};

const rowPropTypes = {
  className: PropTypes.string,

  /**
   * Removes the gutter spacing between Cols as well as any added negative margins.
   */
  noGutters: PropTypes.bool,

  /**
   * The number of columns that will fit next to each other on extra small devices (<576px)
   * type -> number | { cols: number }
   */
  xs: PropTypes.oneOfType([
    PropTypes.shape({
      cols: PropTypes.number
    }),
    PropTypes.number
  ]),

  /**
   * The number of columns that will fit next to each other on small devices (≥576px)
   * type -> number | { cols: number }
   */
  sm: PropTypes.oneOfType([
    PropTypes.shape({
      cols: PropTypes.number
    }),
    PropTypes.number
  ]),

  /**
   * The number of columns that will fit next to each other on medium devices (≥768px)
   * type -> number | { cols: number }
   */
  md: PropTypes.oneOfType([
    PropTypes.shape({
      cols: PropTypes.number
    }),
    PropTypes.number
  ]),

  /**
   * The number of columns that will fit next to each other on large devices (≥992px)
   * type -> number | { cols: number }
   */
  lg: PropTypes.oneOfType([
    PropTypes.shape({
      cols: PropTypes.number
    }),
    PropTypes.number
  ]),

  /**
   * The number of columns that will fit next to each other on extra large devices (≥1200px)
   * type -> number | { cols: number }
   */
  xl: PropTypes.oneOfType([
    PropTypes.shape({
      cols: PropTypes.number
    }),
    PropTypes.number
  ])
};

function Row(props) {
  const { children, className, noGutters, xs, sm, md, lg, xl, ...resProps } = props;
  const RowClass = clsx(!noGutters && "u-row-gutters", "u-flex u-flex-wrap", className);

  return (
    <div data-testid="qa-grid-row" className={RowClass} {...resProps}>
      {React.Children.map(children, (child) =>
        React.isValidElement(child) ? React.cloneElement(child, { noGutters, rowCols: { xs, sm, md, lg, xl } }) : child
      )}
    </div>
  );
}

Row.propTypes = rowPropTypes;
Row.defaultProps = rowDefaultProps;

export default Row;
