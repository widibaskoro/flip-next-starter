import React from "react";
import PropTypes from "prop-types";
import clsx from "classnames";

import { colsSpanGenerator } from "./utils";

const colDefaultProps = {
  className: "",
  noGutters: false
};

const breakpointPropTypes = PropTypes.oneOfType([
  PropTypes.oneOf(["auto"]),
  PropTypes.shape({
    span: PropTypes.oneOfType([PropTypes.number, PropTypes.bool, PropTypes.oneOf(["auto"])]),
    offset: PropTypes.number,
    order: PropTypes.oneOfType([PropTypes.oneOf(["last", "first"]), PropTypes.number])
  }),
  PropTypes.number,
  PropTypes.bool
]);

const colPropTypes = {
  className: PropTypes.string,

  /**
   * Removes the gutter spacing between Cols as well as any added negative margins.
   */
  noGutters: PropTypes.bool,

  /**
   * The number of columns to span on extra small devices (<576px)
   * type -> (true|false) | "auto" | number | { span: (true|false) | "auto" | number, offset: number, order: "first" | "last" | number }
   */
  xs: breakpointPropTypes,

  /**
   * The number of columns to span on small devices (≥576px)
   * type -> (true|false) | "auto" | number | { span: (true|false) | "auto" | number, offset: number, order: "first" | "last" | number }
   */
  sm: breakpointPropTypes,

  /**
   * The number of columns to span on medium devices (≥768px)
   * type -> (true|false) | "auto" | number | { span: (true|false) | "auto" | number, offset: number, order: "first" | "last" | number }
   */
  md: breakpointPropTypes,

  /**
   * The number of columns to span on large devices (≥992px)
   * type -> (true|false) | "auto" | number | { span: (true|false) | "auto" | number, offset: number, order: "first" | "last" | number }
   */
  lg: breakpointPropTypes,

  /**
   * The number of columns to span on extra large devices (≥1200px)
   * type -> (true|false) | "auto" | number | { span: (true|false) | "auto" | number, offset: number, order: "first" | "last" | number }
   */
  xl: breakpointPropTypes
};

function Col(props) {
  const { children, className, noGutters, rowCols, xs, sm, md, lg, xl, ...resProps } = props;

  const width = colsSpanGenerator({ xs, sm, md, lg, xl }, rowCols);
  const ColClass = clsx("u-relative", !noGutters && "u-col-gutters", width, className);

  return (
    <div data-testid="qa-grid-col" className={ColClass} {...resProps}>
      {children}
    </div>
  );
}

Col.propTypes = colPropTypes;
Col.defaultProps = colDefaultProps;

export default Col;
