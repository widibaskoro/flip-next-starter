import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { Button } from "src/components/common";
import {
  ModalWrapper,
  ModalOverlay,
  ModalDialog,
  ModalHeader,
  ModalTitle,
  ModalBody,
  ModalBodyContainer
} from "./Modal.styled";

import { getTranslationManually } from "src/helpers/locale";

const propTypes = {
  /**
   * Set modal title
   */
  title: PropTypes.string,

  /**
   * Set modal showed or hidden
   */
  show: PropTypes.bool,

  /**
   * Set modal size
   *
   * @type {"default" | "small" | "large" | "huge"}
   */
  size: PropTypes.oneOf(["default", "small", "large", "huge"]),

  /**
   * Set modal to be hidden when overlay clicked
   */
  closeOnOverlay: PropTypes.bool,

  /**
   * Set modal has no header
   */
  withoutHeader: PropTypes.bool,

  /**
   * Set modal has no close button
   */
  withoutCloseButton: PropTypes.bool,

  /**
   * Function for hiding modal
   */
  onClose: PropTypes.func
};

const defaultProps = {
  title: "",
  show: false,
  size: "default",
  closeOnOverlay: false,
  withoutHeader: false,
  withoutCloseButton: false,
  onClose: () => {}
};

const Modal = React.forwardRef((props, ref) => {
  const {
    children,
    className,
    title,
    show,
    size,
    closeOnOverlay,
    withoutHeader,
    withoutCloseButton,
    onClose,
    ...restProps
  } = props;

  function handleOverlayClick() {
    if (closeOnOverlay) onClose();
  }

  const modalClass = classNames("c-modal", show ? "c-modal-on" : "c-modal-off", className);
  const modalDialogClass = classNames("c-modal__dialog", className);

  return (
    <ModalWrapper ref={ref} className={modalClass} {...restProps} data-testid="qa-modal">
      <ModalOverlay className="c-modal__overlay" onClick={handleOverlayClick} data-testid="qa-modal-overlay" />
      <ModalDialog className={modalDialogClass} size={size} data-testid="qa-modal-dialog">
        {!withoutHeader && (
          <ModalHeader className="c-modal__header" data-testid="qa-modal-header">
            <ModalTitle data-testid="qa-modal-title">{title}</ModalTitle>
            {!withoutCloseButton && (
              <Button variant="tertiary" onClick={onClose} data-testid="qa-modal-close-button">
                {getTranslationManually("Tutup", "Close")}
              </Button>
            )}
          </ModalHeader>
        )}
        <ModalBody className="c-modal__body" data-testid="qa-modal-body">
          <ModalBodyContainer data-testid="qa-modal-body-container">{children}</ModalBodyContainer>
        </ModalBody>
      </ModalDialog>
    </ModalWrapper>
  );
});

Modal.defaultProps = defaultProps;
Modal.propTypes = propTypes;

export default Modal;
