import styled from "styled-components";

import { colors, typography, zIndex } from "src/assets/styles/settings";

const maxWidth = {
  small: "380px",
  default: "540px",
  large: "696px",
  huge: "900px"
};

const defaultYPadding = "32px";
const defaultHeaderHeight = "80px";

export const ModalWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: ${zIndex.modal};

  &.c-modal-on {
    opacity: 1;
    pointer-events: auto;
    transition: opacity 0.2s ease-in;
  }

  &.c-modal-off {
    opacity: 0;
    pointer-events: none;
    transition: opacity 0.3s ease-out;
  }
`;

export const ModalOverlay = styled.div`
  background: rgba(0, 0, 0, 0.5);
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
`;

export const ModalDialog = styled.div`
  background: ${colors.white};
  border-radius: 8px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
  height: max-content;
  margin: auto;
  max-width: ${({ size }) => maxWidth[size]};
  width: 100%;
  opacity: 0;
  z-index: ${zIndex.modal};

  .c-modal-on & {
    opacity: 1;
    transform: scale(1);
    transition: all 0.2s ease-in;
  }

  .c-modal-off & {
    opacity: 0;
    transform: scale(0.9);
    transition: all 0.3s ease-out;
  }
`;

export const ModalHeader = styled.div`
  border-bottom: 1px solid ${colors.light_grey};
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 20px 32px;
`;

export const ModalTitle = styled.p`
  color: ${colors.black_bekko};
  display: inline-block;
  font-size: ${typography.font_size.large};
  font-style: normal;
  font-weight: ${typography.font_weight.bold};
  line-height: ${typography.line_height};
  margin: 0;
`;

export const ModalBody = styled.div`
  max-height: calc(100vh - ${defaultHeaderHeight} - ${defaultYPadding});
  overflow-y: auto;
  padding: 0;
  position: relative;
`;

export const ModalBodyContainer = styled.div`
  padding: ${defaultYPadding};
`;
