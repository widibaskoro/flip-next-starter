import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import Modal from "./index";
import { checkProps } from "src/helpers/test";

describe("Modal component", () => {
  describe("Modal rendering", () => {
    it("should render without error", () => {
      render(<Modal />);
      expect(screen.getByTestId("qa-modal")).toBeInTheDocument();
    });
  });

  describe("Modal Functionality", () => {
    it("should close when close button is clicked", () => {
      const spy = jest.fn();
      render(<Modal onClose={spy} />);
      fireEvent.click(screen.getByTestId("qa-modal-close-button"));
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it("should close when overlay is clicked and closeOnOverlay is true", () => {
      const spy = jest.fn();
      render(<Modal onClose={spy} closeOnOverlay></Modal>);
      fireEvent.click(screen.getByTestId("qa-modal-overlay"));
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it("should NOT close when overlay is clicked and closeOnOverlay is false", () => {
      const spy = jest.fn();
      render(<Modal onClose={spy} closeOnOverlay={false} />);
      fireEvent.click(screen.getByTestId("qa-modal-overlay"));
      expect(spy).toHaveBeenCalledTimes(0);
    });

    /**
     * unfortunately this test won't pass. RTL won't consider pointer-events: none
     * if we fire a click to a button, it will fired.
     * https://github.com/testing-library/react-testing-library/issues/356
     *
     * it("should not receive any pointer events when hidden", () => {
     *   const spy = jest.fn();
     *   render(
     *     <Modal>
     *       <Button onClick={spy}>Check</Button>
     *     </Modal>
     *   );
     *   screen.debug();
     *   fireEvent.click(screen.getByTestId("qa-button"));
     *   expect(spy).toHaveBeenCalledTimes(0);
     * });
     */
  });

  describe("Modal propTypes", () => {
    it("should not throw warning when expected title are provided", () => {
      checkProps(Modal, { title: "Judul" }, "Modal");
    });

    it("should not throw warning when expected show props are provided", () => {
      checkProps(Modal, { show: true }, "Modal");
      checkProps(Modal, { show: false }, "Modal");
    });

    it("should not throw warning when expected size are provided", () => {
      checkProps(Modal, { size: "default" }, "Modal");
      checkProps(Modal, { size: "small" }, "Modal");
      checkProps(Modal, { size: "large" }, "Modal");
      checkProps(Modal, { size: "huge" }, "Modal");
    });

    it("should not throw warning when expected closeOnOverlay props are provided", () => {
      checkProps(Modal, { closeOnOverlay: true }, "Modal");
      checkProps(Modal, { closeOnOverlay: false }, "Modal");
    });

    it("should not throw warning when expected withoutHeader props are provided", () => {
      checkProps(Modal, { withoutHeader: true }, "Modal");
      checkProps(Modal, { withoutHeader: false }, "Modal");
    });

    it("should not throw warning when expected withoutCloseButton props are provided", () => {
      checkProps(Modal, { withoutCloseButton: true }, "Modal");
      checkProps(Modal, { withoutCloseButton: false }, "Modal");
    });

    it("should not throw warning when expected onClose props are provided", () => {
      checkProps(Modal, { onClose: () => {} }, "Modal");
    });
  });

  describe("Modal props", () => {
    it("should have custom className when provided", () => {
      render(<Modal className="u-border-r-2" />);
      expect(screen.getByTestId("qa-modal").className).toContain("u-border-r-2");
    });

    it("should have children when provided", () => {
      render(<Modal>Halo</Modal>);
      expect(screen.getByTestId("qa-modal-body-container")).toHaveTextContent("Halo");
    });

    it("should hidden (opacity 0) when show props false", () => {
      render(<Modal />);
      const expectedStyle = { opacity: 0 };
      expect(screen.getByTestId("qa-modal")).toHaveStyleRule(expectedStyle);
    });

    it("should be shown (opacity 1) when props show true", () => {
      render(<Modal show={true} />);
      const expectedStyle = { opacity: 1 };
      expect(screen.getByTestId("qa-modal")).toHaveStyleRule(expectedStyle);
    });

    it("should have header when withoutHeader props is false", () => {
      render(<Modal withoutHeader={false} />);
      expect(screen.getByTestId("qa-modal-header")).toBeInTheDocument();
    });

    it("should NOT have header when withoutHeader props is true", () => {
      render(<Modal withoutHeader={true} />);
      expect(screen.queryByTestId("qa-modal-header")).toBeNull();
    });

    it("should have title when title props is provided", () => {
      render(<Modal title="Halo" />);
      expect(screen.getByTestId("qa-modal-title")).toHaveTextContent("Halo");
    });

    it("should have close button when withoutCloseButton props is false", () => {
      render(<Modal withoutCloseButton={false} />);
      expect(screen.getByTestId("qa-modal-close-button")).toBeInTheDocument();
    });

    it("should NOT have close button when withoutCloseButton props is true", () => {
      render(<Modal withoutCloseButton={true} />);
      expect(screen.queryByTestId("qa-modal-close-button")).toBeNull();
    });
  });

  describe("Modal Style", () => {
    it("should have opacity 0 when show props false", () => {
      render(<Modal show={false} />);
      const expectedStyle = { opacity: 0 };
      expect(screen.getByTestId("qa-modal")).toHaveStyleRule(expectedStyle);
    });

    it("should have opacity 1 when show props true", () => {
      render(<Modal show={true} />);
      const expectedStyle = { opacity: 0 };
      expect(screen.getByTestId("qa-modal")).toHaveStyleRule(expectedStyle);
    });

    it("should have maxWidth of 380px when size props is 'small'", () => {
      render(<Modal size="small" />);
      const expectedStyle = { "max-width": "380px" };
      expect(screen.getByTestId("qa-modal-dialog")).toHaveStyleRule(expectedStyle);
    });

    it("should have maxWidth of 540px when size props is 'default'", () => {
      render(<Modal size="default" />);
      const expectedStyle = { "max-width": "540px" };
      expect(screen.getByTestId("qa-modal-dialog")).toHaveStyleRule(expectedStyle);
    });

    it("should have maxWidth of 696px when size props is 'large'", () => {
      render(<Modal size="large" />);
      const expectedStyle = { "max-width": "696px" };
      expect(screen.getByTestId("qa-modal-dialog")).toHaveStyleRule(expectedStyle);
    });

    it("should have maxWidth of 900px when size props is 'huge'", () => {
      render(<Modal size="huge" />);
      const expectedStyle = { "max-width": "900px" };
      expect(screen.getByTestId("qa-modal-dialog")).toHaveStyleRule(expectedStyle);
    });
  });
});
