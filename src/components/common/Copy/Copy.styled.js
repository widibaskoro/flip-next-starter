import styled from "styled-components";

import Button from "../Button";

import { colors } from "src/assets/styles/settings";

export const CopyContainer = styled.div`
  display: inline-block;
  position: relative;

  &:not(.c-copy--icon-only) .c-tooltip {
    top: 0;
  }

  .c-copy__tooltip-info {
    display: none;
  }
`;

export const CopyButton = styled(Button)`
  .c-copy--icon-only & {
    border: none;
    height: auto;
    line-height: 1;
    padding: 0;
    text-align: center;
    width: auto;

    > span:first-child {
      margin: 0;
    }
  }

  &:hover {
    color: ${colors.brick};
  }

  &:hover + .c-copy__tooltip-info {
    display: block;
  }
`;
