import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import Copy from "./index";
import { checkProps } from "src/helpers/test";

describe("Copy component", () => {
  describe("Copy rendering", () => {
    it("Should render without error", () => {
      render(<Copy />);
      expect(screen.getByTestId("qa-copy")).toBeInTheDocument();
    });
  });

  describe("Copy functionality", () => {
    it("Should call execCommand with copy param", () => {
      render(<Copy />);
      document.execCommand = jest.fn();
      fireEvent.click(screen.getByTestId("qa-copy-button"));
      expect(document.execCommand).toHaveBeenCalledWith("copy");
    });
  });

  describe("Copy propTypes", () => {
    it("Should not throw warning when expected textCopy prop types are provided", () => {
      checkProps(Copy, { textCopy: "abc" }, "Copy");
      checkProps(Copy, { textCopy: 123 }, "Copy");
    });
  });

  describe("Copy button type", () => {
    it("Should show the 'SALIN' text", () => {
      render(<Copy />);
      expect(screen.getByTestId("qa-copy-button")).toHaveTextContent("SALIN");
    });

    it("Should hide the 'SALIN' text if prop iconOnly is true", () => {
      render(<Copy iconOnly />);
      expect(screen.getByTestId("qa-copy-button")).not.toHaveTextContent("SALIN");
    });
  });

  describe("Copy tooltip", () => {
    it("Should have info tooltip with correct text", () => {
      render(<Copy textCopy="abc" />);
      expect(screen.getByTestId("qa-copy-tooltip")).toBeInTheDocument();
      expect(screen.getByTestId("qa-copy-tooltip")).toHaveTextContent("Salin abc");
    });
  });

  describe("Copy tooltip after clicked", () => {
    it("Should hide copied tooltip before clicked", () => {
      render(<Copy textCopy="abc" />);
      expect(screen.queryByTestId("qa-copy-tooltip-copied")).toBeNull();
    });

    it("Should show copied tooltip after clicked", () => {
      render(<Copy textCopy="abc" />);
      fireEvent.click(screen.getByTestId("qa-copy-button"));
      expect(screen.getByTestId("qa-copy-tooltip-copied")).toBeInTheDocument();
      expect(screen.getByTestId("qa-copy-tooltip-copied")).toHaveTextContent("abc disalin");
    });
  });
});
