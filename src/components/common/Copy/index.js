import React, { useState } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import Tooltip from "../Tooltip";
import { CopyContainer, CopyButton } from "./Copy.styled";

import { copyText } from "src/helpers/string";
import { getTranslationManually } from "src/helpers/locale";

const propTypes = {
  /**
   * Set text to be copied
   */
  textCopy: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  /**
   * Set button to show only the icon
   */
  iconOnly: PropTypes.bool,

  /**
   * Override default icon type
   *
   * * @type {"outline" | "fill" }
   */
  iconType: PropTypes.oneOf(["outline", "fill"]),

  /**
   * Override default tooltip position
   */
  tooltipPosition: PropTypes.string
};

const defaultProps = {
  textCopy: "",
  iconOnly: false,
  iconType: "outline",
  tooltipPosition: "top"
};

const Copy = React.forwardRef((props, ref) => {
  const { children, className, textCopy, iconOnly, tooltipPosition, iconType, ...restProps } = props;
  const copyClass = classNames("c-copy", iconOnly && "c-copy--icon-only", className);
  const [showTooltip, setShowTooltip] = useState(false);

  function handleCopyText() {
    copyText(textCopy);
    setShowTooltip(true);
    setTimeout(() => setShowTooltip(false), 1000);
  }

  return (
    <CopyContainer data-testid="qa-copy" ref={ref} className={copyClass}>
      <Tooltip.Wrapper>
        <CopyButton data-testid="qa-copy-button" onClick={() => handleCopyText()} variant="tertiary" {...restProps}>
          <div className="u-inline-flex u-items-center">
            <img
              src={iconType === "outline" ? './icon-outline.png' : './icon-fill.png'}
              alt="copy"
              className={classNames(!iconOnly && "u-mr-2")}
              height="15"
            />
            {!iconOnly && getTranslationManually("SALIN", "COPY")}
          </div>
        </CopyButton>
        {showTooltip && (
          <Tooltip data-testid="qa-copy-tooltip-copied" position={tooltipPosition}>
            {textCopy} {getTranslationManually("disalin", "copied")}!
          </Tooltip>
        )}
        <Tooltip data-testid="qa-copy-tooltip" className="c-copy__tooltip-info" position={tooltipPosition}>
          {getTranslationManually("Salin", "Copy")} {textCopy}
        </Tooltip>
      </Tooltip.Wrapper>
    </CopyContainer>
  );
});

Copy.propTypes = propTypes;
Copy.defaultProps = defaultProps;

export default Copy;
