import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { StyledField, FieldFeedback } from "./Field.styled";

const propTypes = {
  /**
   * Set input is invalid
   */
  isInvalid: PropTypes.bool
};

const defaultProps = {
  isInvalid: false
};

const Field = React.forwardRef((props, ref) => {
  const { className, isInvalid, ...restProps } = props;
  const fieldClass = classNames("c-field", { "is-invalid": isInvalid }, className);

  return <StyledField data-testid="qa-field" ref={ref} className={fieldClass} {...restProps} />;
});

Field.Feedback = FieldFeedback;

Field.defaultProps = defaultProps;
Field.propTypes = propTypes;

export default Field;
