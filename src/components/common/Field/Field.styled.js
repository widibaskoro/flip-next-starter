import styled from "styled-components";

import { colors, typography, zIndex } from "src/assets/styles/settings";

export const StyledField = styled.input`
  background: ${colors.white};
  background-clip: padding-box;
  border: 1px solid ${colors.light_grey};
  border-radius: 4px;
  box-sizing: border-box;
  color: ${colors.black_bekko};
  display: block;
  font-size: ${typography.font_size.base};
  font-weight: ${typography.font_weight.normal};
  height: ${({ as }) => (as && as === "textarea" ? "auto" : "40px")};
  line-height: ${typography.line_height};
  order: 1;
  padding: 6px 12px;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  width: 100%;

  &:focus {
    background: ${colors.white};
    border-color: ${colors.tie_dye};
    box-shadow: 0 0 0 0.2rem ${colors.bubble};
    outline: 0;
    z-index: ${zIndex.fieldActive};
  }

  &:disabled,
  &[readonly] {
    background-color: ${colors.disabled};
    color: ${colors.dark_grey};
    cursor: not-allowed;
    opacity: 1;
  }

  &.is-invalid {
    border-color: ${colors.ketchup_tomato};
  }

  &.is-invalid:focus {
    box-shadow: 0 0 0 0.2rem ${colors.sakura};
  }

  &::placeholder {
    color: ${colors.light_grey};
  }
`;

export const FieldFeedback = styled.div`
  color: ${colors.ketchup_tomato};
  display: none;
  font-size: ${typography.font_size.small};
  margin-top: 4px;
  order: 4;
  width: 100%;

  .c-field.is-invalid ~ & {
    display: block;
  }
`;
