import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import Field from "./index";
import { colors } from "src/assets/styles/settings";

describe("Field component", () => {
  describe("Field rendering", () => {
    it("should render without error", () => {
      render(<Field />);
      expect(screen.getByTestId("qa-field")).toBeInTheDocument();
    });
  });

  describe("Field input value", () => {
    it("should have default value if default value is given", () => {
      render(<Field defaultValue="input value" />);
      expect(screen.getByTestId("qa-field").value).toBe("input value");
    });
  });

  describe("Field props", () => {
    it("should have custom className when provided", () => {
      render(<Field className="u-mr-2" />);
      expect(screen.getByTestId("qa-field").className).toContain("u-mr-2");
    });

    it("should be disabled when disabled props is true", () => {
      render(<Field disabled />);
      expect(screen.getByTestId("qa-field")).toBeDisabled();
    });
  });

  describe("Field style", () => {
    it("should have styles when provided", () => {
      render(<Field as="textarea" />);
      expect(screen.getByTestId("qa-field")).toHaveStyle({ height: "auto" });
    });

    it("should have styles border red when invalid", () => {
      render(<Field isInvalid />);
      expect(screen.getByTestId("qa-field")).toHaveStyle({ "border-color": colors.ketchup_tomato });
    });
  });

  describe("Field functionality", () => {
    it("should update on change", () => {
      render(<Field />);
      const field = screen.getByTestId("qa-field");
      fireEvent.change(field, { target: { value: "test" } });
      expect(field.value).toBe("test");
    });
  });
});
