import React from "react";
import { render, screen } from "@testing-library/react";

import FieldGroup from "./index";

describe("FieldGroup component", () => {
  describe("FieldGroup rendering", () => {
    it("should render without error", () => {
      render(<FieldGroup />);
      expect(screen.getByTestId("qa-field-group")).toBeInTheDocument();
    });

    it("should render without error if input is given", () => {
      render(
        <FieldGroup>
          <input type="text" />
        </FieldGroup>
      );
      expect(screen.getByTestId("qa-field-group")).toBeInTheDocument();
    });

    it("should render without error if prepend is given", () => {
      render(
        <FieldGroup>
          <FieldGroup.Prepend>Rp</FieldGroup.Prepend>
          <input type="text" />
        </FieldGroup>
      );
      expect(screen.getByTestId("qa-field-group")).toBeInTheDocument();
      expect(screen.getByTestId("qa-field-group-prepend")).toBeInTheDocument();
    });

    it("should render without error if append is given", () => {
      render(
        <FieldGroup>
          <FieldGroup.Append>Rp</FieldGroup.Append>
          <input type="text" />
        </FieldGroup>
      );
      expect(screen.getByTestId("qa-field-group")).toBeInTheDocument();
      expect(screen.getByTestId("qa-field-group-append")).toBeInTheDocument();
    });
  });

  describe("FieldGroup props", () => {
    it("should have custom className when provided", () => {
      render(<FieldGroup className="u-mr-2" />);
      expect(screen.getByTestId("qa-field-group").className).toContain("u-mr-2");
    });

    it("should show children text if prepend is given", () => {
      render(
        <FieldGroup>
          <FieldGroup.Prepend>Text</FieldGroup.Prepend>
          <input type="text" />
        </FieldGroup>
      );
      expect(screen.getByTestId("qa-field-group-prepend")).toHaveTextContent("Text");
    });

    it("should show children text if append is given", () => {
      render(
        <FieldGroup>
          <FieldGroup.Append>Text</FieldGroup.Append>
          <input type="text" />
        </FieldGroup>
      );
      expect(screen.getByTestId("qa-field-group-append")).toHaveTextContent("Text");
    });
  });
});
