import React from "react";
import classNames from "classnames";

import { FieldGroupPrepend as StyledPrepend } from "../FieldGroup.styled";

const FieldGroupPrepend = React.forwardRef((props, ref) => {
  const { className, ...restProps } = props;
  const fieldGroupPrependClass = classNames("c-field-group__prepend", className);

  return (
    <StyledPrepend data-testid="qa-field-group-prepend" ref={ref} className={fieldGroupPrependClass} {...restProps} />
  );
});

export default FieldGroupPrepend;
