import React from "react";
import classNames from "classnames";

import { FieldGroupAppend as StyledAppend } from "../FieldGroup.styled";

const FieldGroupAppend = React.forwardRef((props, ref) => {
  const { className, ...restProps } = props;
  const fieldGroupAppendClass = classNames("c-field-group__append", className);

  return (
    <StyledAppend data-testid="qa-field-group-append" ref={ref} className={fieldGroupAppendClass} {...restProps} />
  );
});

export default FieldGroupAppend;
