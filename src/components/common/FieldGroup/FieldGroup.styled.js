import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

const dimensions = {
  base: "40px"
};

export const FieldGroupWrapper = styled.div`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  align-items: stretch;
  width: 100%;

  & .c-field:not(.c-dropdown__search-input),
  & .c-dropdown:not(.c-field-group__prepend):not(.c-field-group__append) {
    position: relative;
    flex: 1 1 auto;
    width: 1%;
    order: 2;
  }
`;

const FieldGroupPrependAppend = styled.div`
  border-radius: 4px;
  align-items: center;
  display: inline-block;
  font-size: ${typography.font_size.base};
  font-weight: ${typography.font_weight.normal};
  height: ${dimensions.base};
  line-height: ${typography.line_height};
  margin-bottom: 0;
  min-width: ${dimensions.base};
  padding: 8px 12px;
  text-align: center;
  white-space: nowrap;

  &:not(.c-button) {
    color: ${colors.dark_grey};
    background-color: ${colors.white};
    border: 1px solid ${colors.light_grey};
  }

  &.c-button {
    font-size: ${typography.font_size.semi};
    font-weight: ${typography.font_weight.bold};
    padding: 8px 20px;
  }

  &.c-dropdown .c-dropdown__input {
    border: none;
    color: ${colors.dark_grey};
    height: 38px;
    margin: -8px 0 0 -12px;
    text-align: left;
    width: calc(100% + 24px);

    .c-dropdown__input-value {
      max-width: unset;
    }

    .c-dropdown__triangle {
      fill: ${colors.dark_grey};
    }
  }
`;

export const FieldGroupPrepend = styled(FieldGroupPrependAppend)`
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
  margin-right: -1px;
  order: 1;

  & ~ .c-field,
  & ~ .c-dropdown .c-dropdown__input {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
  }

  &.c-button ~ .c-field,
  &.c-button ~ .c-dropdown .c-dropdown__input {
    border-left: none;
  }
`;

export const FieldGroupAppend = styled(FieldGroupPrependAppend)`
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
  margin-left: -1px;
  order: 3;

  & ~ .c-field,
  & ~ .c-dropdown .c-dropdown__input {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }

  &.c-button ~ .c-field,
  &.c-button ~ .c-dropdown .c-dropdown__input {
    border-right: none;
  }
`;
