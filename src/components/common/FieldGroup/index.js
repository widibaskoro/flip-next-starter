import React from "react";
import classNames from "classnames";

import { FieldGroupWrapper } from "./FieldGroup.styled";
import FieldGroupPrepend from "./components/FieldGroupPrepend";
import FieldGroupAppend from "./components/FieldGroupAppend";

const FieldGroup = React.forwardRef((props, ref) => {
  const { className, ...restProps } = props;
  const fieldGroupClass = classNames("c-field-group", className);

  return <FieldGroupWrapper data-testid="qa-field-group" ref={ref} className={fieldGroupClass} {...restProps} />;
});

FieldGroup.Prepend = FieldGroupPrepend;
FieldGroup.Append = FieldGroupAppend;

export default FieldGroup;
