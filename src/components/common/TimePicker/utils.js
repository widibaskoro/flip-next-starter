export const CLOCK_RADIUS = 104;
export const HALF_CIRCUMFERENCE = 180;
export const FULL_CIRCUMFERENCE = 360;
export const NUMBER_DEGREE = 30;
export const NUMBER_DEGREE_THRESHOLD = 15;
export const minutes = ["05", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "00"];
export const hours = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];

/**
 * helpers to get angle of coordinate
 * @param {Object} param0 - x and y coordinate of clicl
 */
export function calcAngleDegrees({ x, y }) {
  const calc = (Math.atan2(x, y) * HALF_CIRCUMFERENCE) / Math.PI;
  if (calc < 0) {
    return FULL_CIRCUMFERENCE + calc;
  }

  return calc;
}

/**
 * helper to calculate the closest number (hour and minutes) based on current degree
 * @param {Number} degree - degree of clicked coordinate
 */
export function getClosestDegree(degree) {
  const modulus = degree % NUMBER_DEGREE;
  const closestDown = degree - modulus;

  if (modulus > NUMBER_DEGREE_THRESHOLD) {
    return closestDown + NUMBER_DEGREE;
  }

  return closestDown;
}

/**
 * helper to adjust hour due to display is 12h format and value is 24h format
 * @param {String} time - time string in 24h format
 */
function convert24hTo12hConfig(time) {
  const [hour, minute] = time.split(":");
  const period = +hour >= 12 ? "PM" : "AM";
  let adjustedHour = hour;

  if ((period === "AM" && hour === "00") || (period === "PM" && hour === "12")) {
    adjustedHour = "12";
  } else if (period === "PM") {
    adjustedHour = `${+hour - 12}`.padStart(2, "0");
  }

  return [adjustedHour, minute, period];
}

/**
 * convert 24 hour period to 12
 * @param {String} time - time string on 24 hour period
 */
export function displayTimeFormatter(time, format) {
  if (!time) return "";
  if (format === "24h") {
    return time;
  }

  const [hour, minute, period] = convert24hTo12hConfig(time);
  return `${hour}:${minute} ${period}`;
}

/**
 * convert selected time, which is 24 hour format, to 12 hour and calculate the dehree
 * @param {String} time - time string on 12 hour period
 */
export function toPickerData(time) {
  const data = {};
  if (!time) return data;

  const [hour, minute, period] = convert24hTo12hConfig(time);

  data.selectedHour = hour;
  data.selectedMinute = minute;
  data.hourDegree = (hours.indexOf(hour) + 1) * NUMBER_DEGREE;
  data.minuteDegree = (minutes.indexOf(minute) + 1) * NUMBER_DEGREE;
  data.selectedPeriod = period;
  return data;
}
