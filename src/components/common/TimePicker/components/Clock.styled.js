import styled from "styled-components";

import { colors } from "src/assets/styles/settings";

export const ClockContainer = styled.div`
  height: 204px;
  width: 204px;
  background: ${colors.light_smoke};
  border-radius: 50%;
  position: relative;
`;

export const ClockCircleMinutes = styled.div`
  height: 204px;
  width: 204px;
  border-radius: 50%;
  position: absolute;
  z-index: 3;
  transform: scale(1.3);
  opacity: 0;
  transition: transform 0.3s ease-in, opacity 0.3s ease-in;
  pointer-events: none;

  &.pick-minute {
    transform: scale(1);
    opacity: 1;
    pointer-events: auto;
  }
`;

export const ClockCircleHour = styled.div`
  height: 204px;
  width: 204px;
  border-radius: 102px;
  position: absolute;
  z-index: 4;
  transform: scale(0.7);
  opacity: 0;
  transition: transform 0.3s ease-in, opacity 0.3s ease-in;
  pointer-events: none;

  &.pick-hour {
    transform: scale(1);
    opacity: 1;
    pointer-events: auto;
  }
`;

export const ClockNumberContainer = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  pointer-events: none;
  transform: rotate(${({ position }) => (position + 1) * 30}deg);
`;

export const ClockNumber = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 32px;
  height: 32px;
  border-radius: 16px;
  font-size: 14px;
  user-select: none;
  transform: rotate(-${({ position }) => (position + 1) * 30}deg);
  cursor: default;

  &:not(.is-active):hover {
    background: ${colors.anemia};
  }

  &.is-active {
    color: ${colors.white};
  }

  .pick-minute & {
    pointer-events: auto;
  }

  .pick-hour & {
    pointer-events: auto;
  }
`;

export const ClockHandContainer = styled.div`
  width: 32px;
  height: 50%;
  position: absolute;
  transform: translateX(-50%) rotate(${({ handsOn }) => handsOn}deg);
  top: 0;
  left: 50%;
  display: flex;
  flex-direction: column;
  align-items: center;
  z-index: 2;
  transform-origin: bottom center;

  &.transition {
    transition: transform 0.3s linear;
  }
`;

export const ClockHandHead = styled.div`
  width: 32px;
  height: 32px;
  border-radius: 16px;
  background: ${colors.flip_orange};
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ClockHandBody = styled.div`
  width: 1px;
  height: calc(100% - 32px);
  background: ${colors.flip_orange};
`;

export const ClockHandRoot = styled.div`
  width: 8px;
  height: 8px;
  transform: translateY(4px);
  border-radius: 4px;
  background: ${colors.flip_orange};
  position: absolute;
  bottom: 0;
`;
