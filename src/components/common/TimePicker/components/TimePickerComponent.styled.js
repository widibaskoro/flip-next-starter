import styled from "styled-components";

import { colors, typography, zIndex } from "src/assets/styles/settings";

function getPosition(pos) {
  if (pos === "right bottom") {
    return "right: 0; bottom: 40px";
  }

  return `${pos}: 0`;
}

export const TimePickerWrapper = styled.div`
  position: absolute;
  z-index: ${zIndex.dropdown};
  background: ${colors.white};
  height: 462px;
  width: 276px;
  border-radius: 4px;
  overflow: hidden;
  box-shadow: 0px 2px 4px rgba(222, 222, 222, 0.48), 0px 6px 20px rgba(222, 222, 222, 0.48);
  ${({ position }) => getPosition(position)};
`;

export const TimePickerContent = styled.div`
  height: 372px;
  width: 100%;
  padding: 24px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

export const TimePickedContainer = styled.div`
  height: 90px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${colors.flip_gradient};
  color: ${colors.anemia};
  font-size: ${typography.font_size.huge};
  font-weight: ${typography.font_weight.bold};
`;
