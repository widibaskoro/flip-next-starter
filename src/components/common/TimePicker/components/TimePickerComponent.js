import React, { useState } from "react";
import clsx from "classnames";

import { getTranslationManually } from "src/helpers/locale";

import { ButtonGroup, Button } from "src/components/common";
import { TimePickerWrapper, TimePickedContainer, TimePickerContent } from "./TimePickerComponent.styled";
import Clock from "./Clock";

const DEFAULT_HOUR = "12";
const DEFAULT_MINUTE = "00";
const DEFAULT_HOUR_DEGREE = 0;
const DEFAULT_MINUTE_DEGREE = 0;

function TimePickerComponent(props) {
  const { toggle, selectedTime, onChangeTime, position, refs } = props;
  const { selectedHour, selectedMinute, hourDegree, minuteDegree, selectedPeriod } = selectedTime;
  const [periods, setPeriods] = useState(selectedPeriod || "AM");
  const [phase, setPhase] = useState("H");
  const [hour, setHour] = useState(selectedHour || DEFAULT_HOUR);
  const [minute, setMinute] = useState(selectedMinute || DEFAULT_MINUTE);
  const [handsHour, setHandsHour] = useState(hourDegree || DEFAULT_HOUR_DEGREE);
  const [handsMinute, setHandsMinute] = useState(minuteDegree || DEFAULT_MINUTE_DEGREE);
  const [transition, setTransition] = useState(false);

  function toHour() {
    setPhase("H");
    setTransition(true);
  }

  function toMinute() {
    setPhase("M");
    setTransition(true);
  }

  function changePeriods() {
    setPeriods((current) => (current === "AM" ? "PM" : "AM"));
  }

  function resetClock() {
    setTransition(true);
    setHour(DEFAULT_HOUR);
    setMinute(DEFAULT_MINUTE);
    setHandsHour(DEFAULT_HOUR_DEGREE);
    setHandsMinute(DEFAULT_MINUTE_DEGREE);
    setPhase("H");
    setPeriods("AM");

    onChangeTime("");
    toggle(false);
  }

  function submitTime() {
    // convert to 24h format
    let adjustedHour = hour;
    if (periods === "AM" && hour === "12") {
      adjustedHour = "00";
    } else if (periods === "PM") {
      adjustedHour = +hour + 12 === 24 ? "12" : +hour + 12;
    }

    onChangeTime(`${adjustedHour}:${minute}`);
    toggle(false);
  }

  return (
    <TimePickerWrapper position={position} ref={refs}>
      <TimePickedContainer>
        <span onClick={toHour} className={clsx(phase === "H" && "u-text-white u-underline", "u-cursor-pointer")}>
          {hour}
        </span>
        .
        <span
          onClick={toMinute}
          className={clsx(phase === "M" && "u-text-white u-underline", "u-cursor-pointer u-mr-2")}
        >
          {minute}
        </span>
        <span className="u-cursor-pointer" onClick={changePeriods}>
          {periods}
        </span>
      </TimePickedContainer>
      <TimePickerContent>
        <ButtonGroup variant="mini">
          <ButtonGroup.Button type="button" active={periods === "AM"} onClick={() => setPeriods("AM")}>
            AM
          </ButtonGroup.Button>
          <ButtonGroup.Button type="button" active={periods === "PM"} onClick={() => setPeriods("PM")}>
            PM
          </ButtonGroup.Button>
        </ButtonGroup>
        <Clock
          handsHour={handsHour}
          setHandsHour={setHandsHour}
          handsMinute={handsMinute}
          setHandsMinute={setHandsMinute}
          setPhase={setPhase}
          setHour={setHour}
          setMinute={setMinute}
          phase={phase}
          transition={transition}
          setTransition={setTransition}
        />
        <div className="u-flex">
          <Button variant="tertiary" color="grey" onClick={resetClock}>
            Reset
          </Button>
          <Button onClick={submitTime} variant="secondary">
            {getTranslationManually("Simpan", "Apply")}
          </Button>
        </div>
      </TimePickerContent>
    </TimePickerWrapper>
  );
}

export default TimePickerComponent;
