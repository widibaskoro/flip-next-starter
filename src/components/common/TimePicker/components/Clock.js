import React, { useRef, useState } from "react";
import clsx from "classnames";

import {
  ClockCircleMinutes,
  ClockCircleHour,
  ClockContainer,
  ClockNumber,
  ClockNumberContainer,
  ClockHandContainer,
  ClockHandHead,
  ClockHandBody,
  ClockHandRoot
} from "./Clock.styled";

import { minutes, hours } from "../utils";
import { CLOCK_RADIUS, calcAngleDegrees, getClosestDegree, FULL_CIRCUMFERENCE, NUMBER_DEGREE } from "../utils";

function Clock(props) {
  const {
    setHour,
    setMinute,
    setPhase,
    phase,
    handsMinute,
    setHandsMinute,
    handsHour,
    setHandsHour,
    transition,
    setTransition
  } = props;
  const containerRef = useRef(null);
  const [dragging, setDragging] = useState(false);

  function processCoordinate(e) {
    // get position of clock circle in screen
    const rect = containerRef.current.getBoundingClientRect();
    // calculate x and y position where clock in screen
    const relativeX = e.clientX - rect.left;
    const relativeY = e.clientY - rect.top;
    // get quadrant of x and y
    const quadrant = { x: relativeX - CLOCK_RADIUS, y: CLOCK_RADIUS - relativeY };
    // calculate angle of coordinate clicked
    const calculatedDegrees = calcAngleDegrees(quadrant);
    // return the closest number
    return getClosestDegree(calculatedDegrees);
  }

  function positionSetter(e) {
    const closestDegree = processCoordinate(e);
    const target = phase === "H" ? hours : minutes;
    const position = target[(closestDegree || FULL_CIRCUMFERENCE) / NUMBER_DEGREE - 1];

    if (phase === "H") {
      setHandsHour(closestDegree);
      setHour(position);
    } else {
      setHandsMinute(closestDegree);
      setMinute(position);
    }
  }

  function isActive(index, handsDegree) {
    const currentDegree = (index + 1) * NUMBER_DEGREE;
    const currentDegreeOn360or0 = currentDegree === FULL_CIRCUMFERENCE || currentDegree === 0;
    const handsDegreeOn360or0 = handsDegree === FULL_CIRCUMFERENCE || handsDegree === 0;

    if (currentDegreeOn360or0 && handsDegreeOn360or0) {
      return true;
    }

    return currentDegree === handsDegree;
  }

  function handleMove(e) {
    if (dragging) {
      positionSetter(e);
    }
  }

  function startDragging(e) {
    setDragging(true);
    positionSetter(e);
  }

  function stopDragging(e) {
    setDragging(false);
    if (phase === "H") {
      setPhase("M");
      setTransition(true);
    }
  }

  return (
    <ClockContainer onMouseDown={startDragging} onMouseUp={stopDragging} onMouseMove={handleMove} ref={containerRef}>
      <ClockCircleHour className={clsx(phase === "H" && "pick-hour")}>
        {hours.map((hour, index) => (
          <ClockNumberContainer key={hour} position={index}>
            <ClockNumber position={index} className={clsx(isActive(index, handsHour) && "is-active")}>
              {hour}
            </ClockNumber>
          </ClockNumberContainer>
        ))}
      </ClockCircleHour>
      <ClockCircleMinutes className={clsx(phase === "M" && "pick-minute")}>
        {minutes.map((hour, index) => (
          <ClockNumberContainer key={hour} position={index}>
            <ClockNumber position={index} className={clsx(isActive(index, handsMinute) && "is-active")}>
              {hour}
            </ClockNumber>
          </ClockNumberContainer>
        ))}
      </ClockCircleMinutes>
      <ClockHandContainer
        onTransitionEnd={() => setTransition(false)}
        className={clsx(transition && "transition")}
        handsOn={phase === "H" ? handsHour : handsMinute}
      >
        <ClockHandHead />
        <ClockHandBody />
        <ClockHandRoot />
      </ClockHandContainer>
    </ClockContainer>
  );
}

export default Clock;
