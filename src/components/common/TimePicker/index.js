import React, { useState, useRef } from "react";
import clsx from "classnames";
import PropTypes from "prop-types";

import TimePickercomponent from "./components/TimePickerComponent";
import { TimePickerContainer, TimePickerInput, TimePickerFeedback } from "./TimePicker.styled";

import { displayTimeFormatter, toPickerData } from "./utils";
import { useOutsideClick } from "src/hooks/outside-click";

const propTypes = {
  /**
   * Set time value
   */
  selectedTime: PropTypes.string,

  /**
   * Callback after time has been selected
   */
  onChangeTime: PropTypes.func,

  /**
   * Set timepicker input placeholder
   */
  placeholder: PropTypes.string,

  /*
   * Set timepicker disabled
   */
  disabled: PropTypes.bool,

  /**
   * set timepicker is invalid
   */
  isInvalid: PropTypes.bool,

  /**
   * position of timepicker modal
   *
   * @type { "right" | "left" | "right bottom" }
   */
  position: PropTypes.oneOf(["right", "left", "right bottom"]),

  /**
   * display time format
   *
   * @type { "24h" | "12h" }
   */
  timeFormat: PropTypes.oneOf(["24h", "12h"])
};

const defaultProps = {
  selectedTime: "",
  onChangeTime: () => {},
  placeholder: "12:00 AM",
  disabled: false,
  isInvalid: false,
  position: "left",
  timeFormat: "12h"
};

const TimePicker = React.forwardRef((props, ref) => {
  const { selectedTime, onChangeTime, placeholder, disabled, isInvalid, position, timeFormat, className } = props;
  const [isOpen, setIsOpen] = useState(false);

  const containerClass = clsx("c-timepicker__container", { "is-invalid": isInvalid }, className);

  function openTimePicker() {
    setIsOpen(true);
  }

  const timePickerCompRef = useRef(null);
  useOutsideClick(timePickerCompRef, () => {
    setIsOpen(false);
  });

  return (
    <TimePickerContainer ref={ref} className={containerClass}>
      <TimePickerInput
        readOnly
        placeholder={placeholder}
        disabled={disabled}
        onClick={openTimePicker}
        value={displayTimeFormatter(selectedTime, timeFormat)}
      />
      {isOpen && (
        <TimePickercomponent
          refs={timePickerCompRef}
          toggle={setIsOpen}
          selectedTime={toPickerData(selectedTime)}
          onChangeTime={onChangeTime}
          position={position}
        />
      )}
    </TimePickerContainer>
  );
});

TimePicker.defaultProps = defaultProps;
TimePicker.propTypes = propTypes;

TimePicker.Feedback = TimePickerFeedback;

export default TimePicker;
