import styled from "styled-components";

import { colors, typography } from "src/assets/styles/settings";

export const StyledLabel = styled.label`
  color: ${colors.black_bekko};
  display: inline-block;
  font-size: ${typography.font_size.semi};
  font-weight: ${typography.font_weight.bold};
  line-height: ${typography.line_height};
  margin-bottom: 4px;
  text-align: left;
  text-transform: none;
  width: 100%;
`;

export const Optional = styled.span`
  color: ${colors.dark_grey};
  font-size: ${typography.font_size.small};
  font-style: italic;
  font-weight: ${typography.font_weight.normal};
  margin-left: 4px;
`;
