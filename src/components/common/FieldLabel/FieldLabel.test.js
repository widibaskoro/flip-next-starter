import React from "react";
import { render, screen } from "@testing-library/react";

import FieldLabel from "./index";

describe("FieldLabel component", () => {
  describe("FieldLabel rendering", () => {
    it("Should render without error", () => {
      render(<FieldLabel />);
      expect(screen.getByTestId("qa-field-label")).toBeInTheDocument();
    });
  });

  describe("FieldLabel children", () => {
    it("Should render without error if given text", () => {
      render(<FieldLabel>Content</FieldLabel>);
      expect(screen.getByTestId("qa-field-label")).toHaveTextContent("Content");
    });
  });

  describe("FieldLabel variant", () => {
    it("Should show the asterisk symbol when given prop: required = true", () => {
      render(<FieldLabel required>Content</FieldLabel>);
      expect(screen.getByTestId("qa-field-label")).toHaveTextContent("Content");
      expect(screen.getByTestId("qa-field-label-required")).toBeInTheDocument();
      expect(screen.getByTestId("qa-field-label-required")).toHaveTextContent("*");
    });

    it("Should show the optinal text when given prop: optional = true", () => {
      render(<FieldLabel optional>Content</FieldLabel>);
      expect(screen.getByTestId("qa-field-label")).toHaveTextContent("Content");
      expect(screen.getByTestId("qa-field-label-optional")).toBeInTheDocument();
      expect(screen.getByTestId("qa-field-label-optional")).toHaveTextContent("Opsional");
    });
  });
});
