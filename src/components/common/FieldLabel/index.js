import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { StyledLabel, Optional } from "./FieldLabel.styled";

import { getTranslationManually } from "src/helpers/locale";

const propTypes = {
  /**
   * Set label with required mark
   */
  required: PropTypes.bool,

  /**
   * Set label with optional mark
   */
  optional: PropTypes.bool
};

const defaultProps = {
  required: false,
  optional: false
};

const FieldLabel = React.forwardRef((props, ref) => {
  const { children, className, required, optional, ...restProps } = props;
  const labelClass = classNames("c-field-label", className);

  return (
    <StyledLabel ref={ref} className={labelClass} data-testid="qa-field-label" {...restProps}>
      {children}
      {required && (
        <span className="u-text-ketchup-tomato u-ml-1" data-testid="qa-field-label-required">
          *
        </span>
      )}
      {optional && (
        <Optional data-testid="qa-field-label-optional">({getTranslationManually("Opsional", "Optional")})</Optional>
      )}
    </StyledLabel>
  );
});

FieldLabel.propTypes = propTypes;
FieldLabel.defaultProps = defaultProps;

export default FieldLabel;
