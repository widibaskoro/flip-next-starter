export const HIDDEN_BANKS = ["e2pay"];

/*
 * Some banks that are no longer supported from BE
 */
export const REMOVED_TRANSACTION_BANKS = ["danamon_syr"];

export const BANKS = {
  bca: {
    name: "BCA",
    key: "bca"
  },
  bni: {
    name: "BNI / BNI Syariah",
    key: "bni"
  },
  mandiri: {
    name: "Mandiri",
    key: "mandiri"
  },
  bsm: {
    name: "Bank Syariah Mandiri",
    key: "bsm"
  },
  bri: {
    name: "BRI",
    key: "bri"
  },
  bri_syr: {
    name: "BRI (Bank Rakyat Indonesia) Syariah",
    key: "bri_syr"
  },
  permata: {
    name: "Permata / Permata Syariah",
    key: "permata"
  },
  cimb: {
    name: "CIMB Niaga",
    key: "cimb"
  },
  tabungan_pensiunan_nasional: {
    name: "BTPN / Jenius / BTPN Wow!",
    key: "tabungan_pensiunan_nasional"
  },
  dbs: {
    name: "DBS Indonesia",
    key: "dbs"
  },
  muamalat: {
    name: "Muamalat",
    key: "muamalat"
  }
};

export const SUPPORTED_BANKS = {
  mandiri: {
    name: "Mandiri",
    key: "mandiri"
  },
  bri: {
    name: "BRI",
    key: "bri"
  },
  bri_syr: {
    name: "BRI (Bank Rakyat Indonesia) Syariah",
    key: "bri_syr"
  },
  bni: {
    name: "BNI / BNI Syariah",
    key: "bni"
  },
  bca: {
    name: "BCA",
    key: "bca"
  },
  bca_syr: {
    name: "BCA (Bank Central Asia) Syariah",
    key: "bca_syr"
  },
  bsm: {
    name: "Bank Syariah Mandiri",
    key: "bsm"
  },
  cimb: {
    name: "CIMB Niaga",
    key: "cimb"
  },
  muamalat: {
    name: "Muamalat",
    key: "muamalat"
  },
  tabungan_pensiunan_nasional: {
    name: "BTPN / Jenius / BTPN Wow!",
    key: "tabungan_pensiunan_nasional"
  },
  permata: {
    name: "Permata / Permata Syariah",
    key: "permata"
  },
  danamon: {
    name: "Danamon / Danamon Syariah",
    key: "danamon"
  },
  bii: {
    name: "Maybank Indonesia",
    key: "bii"
  },
  panin: {
    name: "Panin Bank",
    key: "panin"
  },
  uob: {
    name: "UOB Indonesia",
    key: "uob"
  },
  ocbc: {
    name: "OCBC NISP",
    key: "ocbc"
  },
  citibank: {
    name: "Citibank",
    key: "citibank"
  },
  artha: {
    name: "Bank Artha Graha Internasional",
    key: "artha"
  },
  tokyo: {
    name: "Bank of Tokyo Mitsubishi UFJ",
    key: "tokyo"
  },
  dbs: {
    name: "DBS Indonesia",
    key: "dbs"
  },
  standard_chartered: {
    name: "Standard Chartered Bank",
    key: "standard_chartered"
  },
  capital: {
    name: "Bank Capital Indonesia",
    key: "capital"
  },
  anz: {
    name: "ANZ Indonesia",
    key: "anz"
  },
  boc: {
    name: "Bank of China (Hong Kong) Limited",
    key: "boc"
  },
  bumi_arta: {
    name: "Bank Bumi Arta",
    key: "bumi_arta"
  },
  hsbc: {
    name: "HSBC Indonesia",
    key: "hsbc"
  },
  rabobank: {
    name: "Rabobank International Indonesia",
    key: "rabobank"
  },
  mayapada: {
    name: "Bank Mayapada",
    key: "mayapada"
  },
  bjb: {
    name: "BJB",
    key: "bjb"
  },
  bjb_syr: {
    name: "BJB Syariah",
    key: "bjb_syr"
  },
  dki: {
    name: "Bank DKI Jakarta",
    key: "dki"
  },
  daerah_istimewa: {
    name: "BPD DIY",
    key: "daerah_istimewa"
  },
  jawa_tengah: {
    name: "Bank Jateng",
    key: "jawa_tengah"
  },
  jawa_timur: {
    name: "Bank Jatim",
    key: "jawa_timur"
  },
  jambi: {
    name: "Bank Jambi",
    key: "jambi"
  },
  sumut: {
    name: "Bank Sumut",
    key: "sumut"
  },
  sumatera_barat: {
    name: "Bank Sumbar (Bank Nagari)",
    key: "sumatera_barat"
  },
  riau_dan_kepri: {
    name: "Bank Riau Kepri",
    key: "riau_dan_kepri"
  },
  sumsel_dan_babel: {
    name: "Bank Sumsel Babel",
    key: "sumsel_dan_babel"
  },
  lampung: {
    name: "Bank Lampung",
    key: "lampung"
  },
  kalimantan_selatan: {
    name: "Bank Kalsel",
    key: "kalimantan_selatan"
  },
  kalimantan_barat: {
    name: "Bank Kalbar",
    key: "kalimantan_barat"
  },
  kalimantan_timur: {
    name: "Bank Kaltim",
    key: "kalimantan_timur"
  },
  kalimantan_tengah: {
    name: "Bank Kalteng",
    key: "kalimantan_tengah"
  },
  sulselbar: {
    name: "Bank Sulselbar",
    key: "sulselbar"
  },
  sulut: {
    name: "Bank SulutGo",
    key: "sulut"
  },
  nusa_tenggara_barat: {
    name: "Bank NTB",
    key: "nusa_tenggara_barat"
  },
  bali: {
    name: "BPD Bali",
    key: "bali"
  },
  nusa_tenggara_timur: {
    name: "Bank NTT",
    key: "nusa_tenggara_timur"
  },
  maluku: {
    name: "Bank Maluku",
    key: "maluku"
  },
  papua: {
    name: "Bank Papua",
    key: "papua"
  },
  bengkulu: {
    name: "Bank Bengkulu",
    key: "bengkulu"
  },
  sulawesi: {
    name: "Bank Sulteng",
    key: "sulawesi"
  },
  sulawesi_tenggara: {
    name: "Bank Sultra",
    key: "sulawesi_tenggara"
  },
  nusantara_parahyangan: {
    name: "Bank Nusantara Parahyangan",
    key: "nusantara_parahyangan"
  },
  india: {
    name: "Bank of India Indonesia",
    key: "india"
  },
  mestika_dharma: {
    name: "Bank Mestika Dharma",
    key: "mestika_dharma"
  },
  sinarmas: {
    name: "Bank Sinarmas",
    key: "sinarmas"
  },
  maspion: {
    name: "Bank Maspion Indonesia",
    key: "maspion"
  },
  ganesha: {
    name: "Bank Ganesha",
    key: "ganesha"
  },
  icbc: {
    name: "ICBC Indonesia",
    key: "icbc"
  },
  qnb_kesawan: {
    name: "QNB Indonesia",
    key: "qnb_kesawan"
  },
  btn: {
    name: "BTN (Bank Tabungan Negara)",
    key: "btn"
  },
  woori: {
    name: "Bank Woori Saudara",
    key: "woori"
  },
  mega: {
    name: "Bank Mega",
    key: "mega"
  },
  bukopin: {
    name: "Bukopin",
    key: "bukopin"
  },
  jasa_jakarta: {
    name: "Bank Jasa Jakarta",
    key: "jasa_jakarta"
  },
  hana: {
    name: "KEB Hana Bank Indonesia",
    key: "hana"
  },
  mnc_internasional: {
    name: "Bank MNC Internasional",
    key: "mnc_internasional"
  },
  agroniaga: {
    name: "BRI Agroniaga",
    key: "agroniaga"
  },
  sbi_indonesia: {
    name: "SBI Indonesia",
    key: "sbi_indonesia"
  },
  royal: {
    name: "Bank Royal Indonesia",
    key: "royal"
  },
  nationalnobu: {
    name: "Nobu (Nationalnobu) Bank",
    key: "nationalnobu"
  },
  mega_syr: {
    name: "Bank Mega Syariah",
    key: "mega_syr"
  },
  ina_perdana: {
    name: "Bank Ina Perdana",
    key: "ina_perdana"
  },
  sahabat_sampoerna: {
    name: "Bank Sahabat Sampoerna",
    key: "sahabat_sampoerna"
  },
  kesejahteraan_ekonomi: {
    name: "Bank Kesejahteraan Ekonomi",
    key: "kesejahteraan_ekonomi"
  },
  artos: {
    name: "Bank Artos Indonesia",
    key: "artos"
  },
  mayora: {
    name: "Bank Mayora Indonesia",
    key: "mayora"
  },
  index_selindo: {
    name: "Bank Index Selindo",
    key: "index_selindo"
  },
  victoria_internasional: {
    name: "Bank Victoria International",
    key: "victoria_internasional"
  },
  agris: {
    name: "Bank Agris",
    key: "agris"
  },
  chinatrust: {
    name: "CTBC (Chinatrust) Indonesia",
    key: "chinatrust"
  },
  commonwealth: {
    name: "Commonwealth Bank",
    key: "commonwealth"
  },
  ccb: {
    name: "Bank China Construction Bank Indonesia",
    key: "ccb"
  },
  victoria_syr: {
    name: "Bank Victoria Syariah",
    key: "victoria_syr"
  },
  banten: {
    name: "BPD Banten",
    key: "banten"
  },
  mutiara: {
    name: "Bank Mutiara",
    key: "mutiara"
  },
  panin_syr: {
    name: "Bank Panin Dubai Syariah",
    key: "panin_syr"
  },
  // Danamon syariah just used for any other than Multi Input and filters
  danamon_syr: {
    name: "Bank Danamon Syariah",
    key: "danamon_syr"
  }
};

export const DIGITAL_WALLET = {
  ovo: {
    name: "OVO",
    key: "ovo"
  },
  dana: {
    name: "Dana",
    key: "dana"
  },
  doku: {
    name: "Doku",
    key: "doku"
  },
  linkaja: {
    name: "LinkAja",
    key: "linkaja"
  }
};

/*
 * Used for showing banks in history or etc.
 */
export const ALL_SHOWN_BANKS = {
  ...SUPPORTED_BANKS,
  ...DIGITAL_WALLET
};
