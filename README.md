# flip-next-starter

Basic NextJS starter kit for Flip frontend project. 

## Documentations

Please refer to these guides to start sailing.

1. [Getting Started!](docs/getting-started.md)
2. [Some JavaScript libraries used](docs/libraries-used.md)
3. [General project (folder) structuring](docs/project-structure.md)
4. [Coding convention guide](docs/coding-convention.md)
5. [Contributing guide](docs/contributing-guide.md)

Some technical documentations:

1. [Common components](docs/technicals/common-components.md)
2. [Locales](docs/technicals/locales.md)
3. [Unit Test](docs/technicals/unit-test.md)
